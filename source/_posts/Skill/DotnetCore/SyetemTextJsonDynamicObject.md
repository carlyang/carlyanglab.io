---
title: .NET Core - Serializing Dynamic Object which is from Newtonsoft.Json in System.Text.Json
categories:
 - 技術文件
 - .NET Core
date: 2022/1/29
updated: 2022/1/29
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [.NET Core, System.Text.Json, Newtonsoft.Json]
---
作者: Carl Yang

![](/Teamdoc/image/DotnetCore/Logo.png)

## Introduction
最近在使用`System.Text.Json`發現了一個不算大、但也容易不小心踩到的雷，就是在專案`System.Text.Json`與`Newtonsoft.Json`搭配混用時會發生序列化結果異常的狀況。  
尤其是在撰寫`ASP.NET Core Web API`時，基本上不另外指定的話預設都是用`System.Text.Json`處理API Response，但我們的Code可能還是使用`Newtonsoft.Json`處理`dynamic`物件，這時候API Response用`System.Text.Json`序列化的結果就會造成空的JSON物件。
<!-- more -->

## Prerequisites
本文是使用.NET Core 3.1作為開發環境進行練習，請先安裝下列項目。
- 請先安裝.NET Core 3.1 SDK([Download](https://dotnet.microsoft.com/download/dotnet/3.1))

## Experimental
我們先用`Newtonsoft.Json`反序列化一個`dynamic`物件出來，再用`System.Text.Json`將它序列化為JSON字串。

```csharp
public class TestModel
{
    public string Name { get; set; }
    public dynamic Data { get; set; }
}

public static void Main(string[] args)
{
    var model = new TestModel()
    {
        Name = "Test1",
        Data = JsonConvert.DeserializeObject<dynamic>("{\"Prop1\":\"A\",\"Prop2\":\"B\"}")
    };

    var result = System.Text.Json.JsonSerializer.Serialize(model);
}
```

接著我們來看看序列化結果如下，Property Value竟然變成陣列了...

```JSON
{"Name":"Test1","Data":{"Prop1":[],"Prop2":[]}}
```

再來我們把`Newtonsoft.Json`反序列化那行改成用`System.Text.Json`處理。

```csharp
System.Text.Json.JsonSerializer.Deserialize<dynamic>("{\"Prop1\":\"A\",\"Prop2\":\"B\"}")
```

序列化的結果如下，Property Value就正常了。

```json
{"Name":"Test1","Data":{"Prop1":"A","Prop2":"B"}}
```

## 結論
目前來說，`Newtonsoft.Json`與`System.Text.Json`混用的案例應該還是很多，畢竟`Newtonsoft.Json`穩定、支援度完整、又是長久以來大家使用的工具，一時間要全部轉換過去`System.Text.Json`也是有些歷史包袱，所以就必須要多注意混用時會不會有什麼異常問題發生。  
基本上只要多注意`序列化/反序列化前後都使用相同的套件，應該就不至於出現太嚴重的問題`，像這次的案例就是因為混用不同套件所造成的，本文也是記錄一下免得自己以後又不小心掉坑了XD。

## 參考
1. N/A