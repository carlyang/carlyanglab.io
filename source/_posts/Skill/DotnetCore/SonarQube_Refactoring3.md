---
title: .NET Core - SonarQube Refactoring in C#(3)
categories:
 - 技術文件
 - .NET Core
 - SonarQube
date: 2021/8/9
updated: 2021/8/9
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [ASP.NET Core, SonarQube, Asyncronous Funciton]
---
作者: Carl Yang

![](/Teamdoc/image/DotnetCore/Logo.png)

## Purpose
我們常會在特定的使用情境下，使用非同步的方式來提升程式效能，但如果我們沒有用await等待結果會先得到`CS4014 Warning`，`SonarQube`也會藉由.Net Build的結果回報`Major Error`，本文就來介紹這種情境的處理方式。
<!-- more -->

## Prerequisites
本文是使用ASP.NET Core 3.1作為開發環境進行練習，請先安裝下列項目。
- 請先安裝ASP.NET Core 3.1 SDK([Download](https://dotnet.microsoft.com/download/dotnet/3.1))

## Introduction
當我們在特定情境下使用`非同步Function`來處理時，為了避免無謂的等待回傳結果(甚至根本不需要處理回傳結果)，通常會拿掉`await`關鍵字、直接往程式下一行跑。  
但是一個`async Function`中可能其他行是需要`await`結果的，這時候就會有部分要等待、部分不用等待，沒有`await`的部分就會有`CS4014 Warning`訊息警告說這個call應該要用`await`，詳細訊息如下。

>Because this call is not awaited, execution of the current method continues before the call is completed. Consider applying the await operator to the result of the call.
[[MS Doc]](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/compiler-messages/cs4014)

Note:  
`請注意，在一般的同步Function中、進行非同步呼叫(沒有await)是不會有CS4014 Warning的`，以下範例會加以說明。

## Sample - Call in Sync. Function
首先我們來看如下在同步Function中、使用非同步呼叫的範例，`TestAsync()`是非同步Function在同步`Main()`中被呼叫、且不`await`等待結果，`這時候不會有CS4014 Warning`。

```csharp
void Main()
{
	TestAsync("1").ConfigureAwait(false);

	Console.WriteLine("Finished");
}

async Task TestAsync(string number)
{
	await new HttpClient().GetAsync("http://www.google.com");

	Console.WriteLine($"HttpClient {number}");
}
```

執行結果如下:
```test
Finished
HttpClient 1
```

從結果我們可以看到，是先`Finished`、爾後才印出`HttpClient 1`，表示`TestAsync()`是真的用非同步的方式執行，也不會有`CS4014 Warning`。

## Sample - Call in Async. Function
接下來，我們再來看在非同步Function中、呼叫非同步Function的情況，

```csharp
async Task Main()
{
	await TestAsync("1").ConfigureAwait(false);
	
	TestAsync("2").ConfigureAwait(false);

	Console.WriteLine("Finished");
}

async Task TestAsync(string number)
{
	await new HttpClient().GetAsync("http://www.google.com");

	Console.WriteLine($"HttpClient {number}");
}
```

![](/Teamdoc/image/DotnetCore/SonarQube/SonarQube_Refactoring3/img1.png)

執行結果如下:
因為Row 3是有`await`的、所以會先印出`HttpClient 1`，Row 5是沒有`await`的，所以先印出`Finished`、再印出`HttpClient 2`，關鍵在於`有沒有使用await等待結果，如果有await他就會等待結果後、才繼續下一行`。

```text
HttpClient 1
Finished
HttpClient 2
```

可能有人會認為跟有沒有拿`Response`有關，單純只有`await`不影響非同步機制，我們來調整Code實際做個實驗，改成如下。

```csharp
async Task Main()
{
	await TestAsync("1").ConfigureAwait(false);
	
	var response = await TestAsync("2").ConfigureAwait(false);

	Console.WriteLine("Finished");
}

async Task<HttpResponseMessage> TestAsync(string number)
{
	var response = await new HttpClient().GetAsync("http://www.google.com");

	Console.WriteLine($"HttpClient {number}");
	
	return response;
}
```

執行結果如下:

```text
HttpClient 1
HttpClient 2
Finished
```

從執行結果來看，就會等待`HttpClient 2`後、才印出`Finished`，沒有跑非同步的方式。

接著我們再改成單純只有`await`、沒有用`Response`接。

```csharp
async Task Main()
{
	await TestAsync("1").ConfigureAwait(false);
	
	await TestAsync("2").ConfigureAwait(false);

	Console.WriteLine("Finished");
}

async Task<HttpResponseMessage> TestAsync(string number)
{
	var response = await new HttpClient().GetAsync("http://www.google.com");

	Console.WriteLine($"HttpClient {number}");
	
	return response;
}
```

執行結果如下:

```text
HttpClient 1
HttpClient 2
Finished
```

Hint:  
`還是跟前一個範例結果一樣，並不因為我們有沒有接Response而有差別。`

## Prevent SonarQube Error
以上幾個實驗可以知道，有沒有`await`才是影響是否跑非同步的關鍵，`也就是說，就算建了一大堆的非同步Function、但使用時都加上await就跟同步是一樣的意思`，所以可以衡量一下是否真的要用非同步。  

再回到本文一開始提到的`SonarQube`錯誤，在我當下的使用情境下、確實是真的要跑非同步的方式，也就是丟出去後不需要管結果、直接往下一行跑就好，但是`呼叫Function還有其他行Code還是需要await結果才能繼續的，如果非同步那行又await它、將會造成後續動作的延遲`。  

實際用`Postman`打下來，有無使用非同步花費的時間差了2倍以上(約3~4秒與8~10秒的差距)，一個HTTP Request花費時間就差距這麼大，擴大到一個系統來看、這可是不能被接受的，但`SonarQube`又會報錯誤，這可怎麼辦呢?`其實有個方法，那就是前置處理器指示詞(Preprocessor directives)`，我們只要在造成`CS4014 Warning`的地方加上前置詞即可，如下範例。

```csharp
#pragma warning disable 4014
	TestAsync("2").ConfigureAwait(false);
#pragma warning restore 4014
```

`disable`就是告訴Compiler編譯到這一行時關閉4014這個警告、`restore`就是告訴Compiler編譯到這一行時開啟4014這個警告。
詳細可參考[[MS Doc]](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/preprocessor-directives#pragmas)

另外，如果有用到`C# 7.0`以上版本，可以直接使用`discards`的方式(直接用"`_`"asign非同步結果)，避免發生這個錯誤喔!個人是比較推薦這個方式，簡潔又乾淨、也是`native code`的方式。[[MS Doc]](https://docs.microsoft.com/en-us/dotnet/csharp/fundamentals/functional/discards)

```csharp
_ = TestAsync("2").ConfigureAwait(false);
```

## 結論
本文說明在非同步Function中、使用`await`等待以及不使用等待時跑非同步的方式，透過相互的實驗比較、指出非同步與同步的差異，並針對要使用非同步的情境下、不`await`的兩種解法。  
但是，還是強烈建議使用第二種`discards`的方式，解決非`await`時的Warning錯誤，個人覺得針對這種特定情境，事實上不使用`await`等待、直接往下跑，才是真正有用到非同步機制。  
不過，本文雖然以`SonarQube`起頭，但最主要想說明的還是非同步機制`await`部分的重要觀念，希望能幫助讀者一點小忙。

## 參考
1. [[Compiler Warning (level 1) CS4014]](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/compiler-messages/cs4014)
2. [[Pragmas]](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/preprocessor-directives#pragmas)
3. [[Discards - C# Fundamentals]](https://docs.microsoft.com/en-us/dotnet/csharp/fundamentals/functional/discards)