---
title: Azure - AzCopy Throughput Rate Limit
categories:
 - 技術文件
 - Azure
date: 2024/5/24
updated: 2024/5/24
thumbnail: https://i.imgur.com/8uMcIBL.png
layout: post
tags: [Azure, AzCopy]
---
作者: Carl Yang

![](/Teamdoc/image/azure/logos/storage_account.png)

## Purpose
由於公司是使用專線的方式連線`Azure`服務，而頻寬也僅有50 mbps，所以在使用`AzCopy`同步`Blobs`檔案時，我們必須要使用`--cap-mbps`來限制傳輸速率，否則很容易在傳輸大量檔案時造成頻寬被塞滿、導致其他服務無法正常連線。  
但在使用的過程中發現，當我們下載`Blobs`到地端的時候會發生有些大檔案下載失敗，造成這個失敗的原因就是因為使用了`--cap-mbps`來限制傳輸速率，如果沒有使用就可以順利下載，故本文的目的即在說明這個問題的目前狀況，`但因為目前這個Bug在Github上仍為Open狀態，尚未獲得解決，故本文僅能先做說明、提醒大家要注意，避免各位實際跑AzCopy後才發現這個問題`。
<!-- more -->

## --cap-mbps Option
`--cap-mbps`就是用來限制每一次執行`AzCopy copy/sync`指令時的傳輸速率，根據[[微軟文件]](https://learn.microsoft.com/en-us/azure/storage/common/storage-ref-azcopy-sync#options-inherited-from-parent-commands)說明，以`AzCopy sync`為範例，指令如下。

```cmd
azcopy sync [Your Blob Container URL] [Your local Path] --recursive --cap-mbps=10
```

以上的範例就是限制該次傳輸速率的指令，我們可以了解這次限制為每秒10 mbps，`請注意，這個限制與限制AzCopy的Jobs數不同，也就是說，假設現在限制只能並行用兩個Jobs來同步Blob，但如果沒有限制傳輸速率，還是有可能會將頻寬塞滿的!`

## AzCopy Error Log
當我們在下載比較大的檔案時，我們觀察到該檔案僅約3.95 MB、而我們同時限制`--cap-mbps=10`，這時候該檔就會發生下載失敗，我們可以在`AzCopy`的Error Logs中發現錯誤訊息如下。

```
request size greater than pacer target. When Enqueuing chunk.
```

但是，`如果將Option調整為--cap-mbps=34，該檔案就可以順利下載`，觀察下來似乎這個傳輸速率限制跟檔案大小有關，`同時我們也改用`azcopy copy`指令加上--cap-mbps來測試該檔案，依然後是下載失敗且相同錯誤`。  
但回過頭來思考，一個約3.95 MB的檔案就必須要限制傳輸速率到34 mbps，而我們專線的頻寬也不過就50 mbps、Blobs檔案大於3.95 MB者更是多到數不清，所以目前這個錯誤讓我們無法使用`--cap-mbps`來限制傳輸速率。

## Github Bug
經調查發現`Github`上已經有人反應此[[Bug #2380]](https://github.com/Azure/azure-storage-azcopy/issues/2380)、時間大約是2023年9月中、距今也超過8個月了，看來似乎是個棘手的問題不容易解決，所以可能短期內仍然是無解的狀態。

![](/Teamdoc/image/azure/AzCopyThroughputRateLimit/bug1.png)

## Conclusion
本文主要是說明目前`AzCopy`下載大檔案時使用`--cap-mbps`來限制傳輸速率、會發生下載失敗的問題，`我們另外也有使用--cap-mbps上傳大檔案到Azure Blobs的情境，但是並不會有這個錯誤!`所以問題還是在下載的時候。由於專線頻寬有限、我們也不得不限制傳輸速率，所以目前下載的部分是暫時停止的狀態，但其它95%的功能都還是正常使用，所以各位如果沒有`需要下載又要限制傳輸速率`的需求，不用太過擔心這個問題，本文只是就目前的狀況為各位記錄並說明一下，`AzCopy`的開發者目前也在積極地解決這個問題，雖然已經過了8個月但最後應該還是會解決的，否則這個無法下載的狀況也不能長久這樣下去。

## References
1. [Limit the throughput data rate](https://learn.microsoft.com/en-us/azure/storage/common/storage-use-azcopy-optimize#limit-the-throughput-data-rate)
2. [File cannot be downloaded when throughput throttled](https://github.com/Azure/azure-storage-azcopy/issues/2380)
3. [Options inherited from parent commands](https://learn.microsoft.com/en-us/azure/storage/common/storage-ref-azcopy-sync#options-inherited-from-parent-commands)