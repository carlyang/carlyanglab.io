---
title: .NET Core - Middleware
categories:
 - 技術文件
 - .NET Core
date: 2019/01/06
updated: 2019/01/06
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [.NET Core, Middleware]
---
作者: Carl Yang

![](/Teamdoc/image/DotnetCore/Logo.png)

## Middleware
以前MVC時代，如果想在Controller之前做些事情、要使用各種Filters、Handlers，例如: AuthorizationFilter驗證要求、Message Handler處理訊息等等，現在進入.NET Core後可以統一在Middleware內自行實作邏輯，並決定在程式哪一行進入下個Step(也稱為Pipeline管線，意指可以一個個串接起來執行之意)、等待回傳後再繼續後面的處理，使用上簡單又方便，非常吸引人、用過都說讚!!
<!-- more -->

## Prerequisites
本文是使用.NET Core 2.2作為開發環境進行練習，請先安裝下列項目。
- 請先安裝.NET Core 2.2 SDK([Download](https://dotnet.microsoft.com/download))

## Work Flow
Middleware是採用先進後出的方式進行，類似堆疊的概念、以巢狀的呼叫方式一層層進入、再一層層出來。讓我們來實際看看下圖，由右上角的Request開始一層層呼叫、假設Middleware邏輯都通過、就進入Action處理、最後再一層層回傳、直到左上角Response給Client端，其中的next()表示依照Middleware的註冊順序、依序呼叫下一個Middleware執行，如果沒有Middleware了、就會進入API的Action中。

![](/Teamdoc/image/DotnetCore/middleware1.png)

## 註冊Middleware
請先新建一個空白Web API專案，接著在Startup.cs的Configure()中註冊Middleware，請加入下列程式碼。

Note:
由於註冊有其先後順序，為了能夠先執行Middleware、才進入Action，請務必加在app.UseMvc()之前。
```csharp
app.Use(async (context, next) =>
{
	await context.Response.WriteAsync($"This is test1 start<br/>");
	await next.Invoke();
	await context.Response.WriteAsync($"This is test1 end<br/>");
});

app.Use(async (context, next) =>
{
	await context.Response.WriteAsync($"This is test2 start<br/>");
	await next.Invoke();
	await context.Response.WriteAsync($"This is test2 end<br/>");
});

app.Use(async (context, next) =>
{
	await context.Response.WriteAsync($"This is test3 start<br/>");
	await next.Invoke();
	await context.Response.WriteAsync($"This is test3 end<br/>");
});

app.Run(async (context) =>
{
	await context.Response.WriteAsync("Hello World! \r\n");
});
```

執行後可見到如下結果，如圖中先123、再321地順序回來。
![](/Teamdoc/image/DotnetCore/middleware2.png)

- app.Use:
Middleware的註冊都是透過app.Use開頭的Function進行，如上的Sample Code、都是一個個呼叫app.Use(Action)來委派處理。

- app.Run
app.Run則是註冊終端Middleware、亦即最後一個直接執行、不調用下一個Middleware的Middleware。

- app.Map
接著我們來看看另一種具有Path Mapping條件的Middleware，他能夠簡單地根據不同路由決定要調用哪個Middleware，我們將test2註冊的Middleware改成以下Sample。

1. 第一個參數傳入要Map的路徑，<font style="color:red;">請注意這個Path是以Host為基準的相對路徑表示，例如現在Host是http://localhost:4000、要Map的路徑是/api/values，就要傳入後面的整段路徑字串。</font>
2. 第二個參數就是委派處理Action，而這個Action則是註冊一個Middleware、一個終端Middleware，並告訴它符合Path的要執行哪個委派的Action、及其終端的app.Run。
```csharp
app.Map("/api/values", p =>
{
	p.Use(async (context, next) =>
	{
		await context.Response.WriteAsync($"This is test2 start<br/>");
		await next.Invoke();
		await context.Response.WriteAsync($"This is test2 end<br/>");
	});
	p.Run(async context =>
	{
		await context.Response.WriteAsync($"Map Middleware<br/>");
	});
});
```
3. 執行結果如下圖，如果目前進來的Request符合Path就會跑test2的Middleware，<font style="color:red;">但不會接著跑test3</font>。
![](/Teamdoc/image/DotnetCore/middleware3.png)

如果沒有符合條件就不會跑test2的Middleware、直接跑test3的Middleware。
![](/Teamdoc/image/DotnetCore/middleware4.png)

Note:
1. <font style="color:red;">請注意，當它被Map進去test2的Middleware後，執行完就不會往下跑其它的Middleware。</font>
2. 其實它的概念就是Pipeline(管道)，當我們在一條條的Pipeline上行走、如果轉進其他分支，就不會再回到原本的分支路線了。

- app.MapWhen
1. app.MapWhen跟app.Map其實是一樣的，兩者都是進入另一個分支後，就不會再回頭，差異在於app.Map具有PathMatch的功能、app.MapWhen則是直接處理傳進來的HttpContext。
```csharp
app.MapWhen(context => context.Request.Path.Value.StartsWith("/api/values"),
	p =>
	{
		p.Use(async (context, next) =>
		{
			await context.Response.WriteAsync($"This is test2 start<br/>");
			await next.Invoke();
			await context.Response.WriteAsync($"This is test2 end<br/>");
		});
		p.Run(async context =>
		{
			await context.Response.WriteAsync($"Map Middleware<br/>");
		});
	});
```

2.執行結果如下圖，與app.Map是一樣的結果。
![](/Teamdoc/image/DotnetCore/middleware4.png)

- app.UseWhen
1. 由app.Map及app.MapWhen的實驗得知，你轉進巷子後就不回頭了、它並不會接著跑下一個test3的Middleware，但這時候就有人可能會說，那走進巷子後被狗追必須再繞回原路繼續走，難道不可以嗎?!答案是 <font style="color:red;">當然可以，就是改用app.UseWhen!!</font>讓我們來接著看下去。
app.UseWhen會判斷條件是否符合後、決定是否執行其內註冊的Middleware，它跟app.Map很類似、但如不符合條件僅會略過其內註冊的Middleware，app.UseWhen跟app.Map的差別在於如果發現有符合條件並執行完其內的Middleware後、app.UseWhen會接著往後跑、app.Map則不然，這對於一些不同情況的判斷是很有用的。

以下範例是當Path符合"/api/values"結尾時，就跑test2的Middleware、再接著跑test3的Middleware，如果不符合就會略過test2、直接跑test3的Middleware。
```csharp
app.UseWhen(context => context.Request.Path.ToString().EndsWith(@"/api/values"), p =>
{
	p.Use(async (context, next) =>
	{
		await context.Response.WriteAsync($"This is test2 start<br/>");
		await next.Invoke();
		await context.Response.WriteAsync($"This is test2 end<br/>");
	});
});
```

2. 執行結果如下圖，不符合條件就略過test2繼續執行test3。
![](/Teamdoc/image/DotnetCore/middleware5.png)

- app.UsePathBase
app.UsePathBase是用來拆解請求路徑的，也就是說當Request的路徑含有我們帶入的PathString時，它會自動將Request的PathBase設為我們指定的base path、Request的Path則是剩下的路徑。
而app.UsePathBase也不會新建Middleware或是分支、也不影響原有的流程。
>app.UsePathBase傳入的PathString必須以'/'開頭、不以'/'結尾，如果以'/'結尾則會自動去除。

```csharp
app.UsePathBase(new PathString(@"/MyTestMiddleware"));
```

## 自訂Middleware
- app.UseMiddleware
當我們想要有自己的Middleware處理邏輯時，就可以使用自訂的Class撰寫，並在其中實作Invoke或InvokeAsync，最後使用UseMiddleware註冊。
以下範例使用兩個假的Middleware，HttpCallLogMiddleware處理每個Request及Response的Log、AuthenticationMiddleware則是用來檢查Header中Authorization帶入的Token是否合法。

1. HttpCallLogMiddleware.cs
在這個範例中僅是將每次的Request及Response用Debug.WriteLine寫進Output，以用來驗證執行結果，各位可自行修改該兩行寫Log的方式。
```csharp
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace NetCoreMiddlewareSample.Middlewares
{
    public class HttpCallLogMiddleware
    {
        private readonly RequestDelegate _next;

        public HttpCallLogMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var request = await GetRequestLog(context.Request);

            Debug.WriteLine($"Request: {DateTime.Now: yyyy-MM-dd HH:mm:ss.fff}{Environment.NewLine}{request}");

            var storeBackStream = context.Response.Body;
            using (var responseBody = new MemoryStream())
            {
                context.Response.Body = responseBody;

                await _next.Invoke(context);

                var response = await GetResponseLog(context.Response);

                Debug.WriteLine($"Response: {DateTime.Now: yyyy-MM-dd HH:mm:ss.fff}{Environment.NewLine}{response}");

                await responseBody.CopyToAsync(storeBackStream);
            }
        }

        private async Task<string> GetRequestLog(HttpRequest request)
        {
            var builder = new StringBuilder();

            builder.AppendLine($"{request.Method} {request.Scheme}://{request.Host}{request.Path} {request.Protocol}");

            request.Headers.ToList().ForEach(p =>
            {
                builder.AppendLine($"{p.Key}: {p.Value}");
            });

            var buffer = new byte[Convert.ToInt32(request.ContentLength)];
            await request.Body.ReadAsync(buffer, 0, buffer.Length);
            var bodyAsText = Encoding.UTF8.GetString(buffer);

            builder.AppendLine(string.Empty);
            builder.AppendLine($"{bodyAsText}");

            var storeBackStream = new MemoryStream();
            var bytesStoreBack = Encoding.UTF8.GetBytes(bodyAsText);
            storeBackStream.Write(bytesStoreBack, 0, bytesStoreBack.Length);
            storeBackStream.Seek(0, SeekOrigin.Begin);

            //To store the same value back for next step using
            request.Body = storeBackStream;

            return builder.ToString();
        }

        private async Task<string> GetResponseLog(HttpResponse response)
        {
            var builder = new StringBuilder();

            builder.AppendLine($"{response.HttpContext.Request.Protocol} {response.StatusCode} {((HttpStatusCode)response.StatusCode)}");

            response.Headers.ToList().ForEach(p =>
            {
                builder.AppendLine($"{p.Key}: {p.Value}");
            });

            response.Body.Seek(0, SeekOrigin.Begin);
            var bodyAsText = await new StreamReader(response.Body).ReadToEndAsync();
            response.Body.Seek(0, SeekOrigin.Begin);

            builder.AppendLine(string.Empty);
            builder.AppendLine($"{bodyAsText}");

            return builder.ToString();
        }
    }
}
```

2. AuthenticationMiddleware.cs
這邊僅簡單將Request的Headers中Authorization帶入的Token跟"123456"比較，如果相同則回傳驗證成功、反之則驗證失敗，各位可以自行修改使用如JWT、Bearer等Token機制實作。
```csharp
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace NetCoreMiddlewareSample.Middlewares
{
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;

        public AuthenticationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            if (!CheckToken(context.Request))
            {
                context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                await context.Response.WriteAsync(@"Token is invalid");

                return;
            }
            
            await _next.Invoke(context);
        }

        private bool CheckToken(HttpRequest request)
        {
            bool result = false;

            if (request.Headers.Keys.Contains(@"Authorization"))
            {
                result = request.Headers[@"Authorization"].ToString().Equals(@"123456", StringComparison.OrdinalIgnoreCase);
            }

            return result;
        }
    }
}
```

3. app.UseMiddleware
自訂的Class完成後，只要使用app.UseMiddleware<[Your middleware name]>()註冊後即可使用。
```csharp
app.UseMiddleware<HttpCallLogMiddleware>();
app.UseMiddleware<AuthenticationMiddleware>();
```

4. 執行結果如下。

Output已有Request及Response的Log。
![](/Teamdoc/image/DotnetCore/middleware6.png)

Authentication也成功驗證完成，並將Request往後送至API處理後、收到的Response Log。
![](/Teamdoc/image/DotnetCore/middleware7.png)

最後Client端收到的Http Response。
```
HTTP/1.1 200 OK
Transfer-Encoding: chunked
Content-Type: application/json; charset=utf-8
Server: Microsoft-IIS/10.0
X-SourceFiles: =?UTF-8?B?QzpcVXNlcnNcY2FybHlhbmdcc291cmNlXHJlcG9zXE5ldENvcmVNaWRkbGV3YXJlU2FtcGxlXE5ldENvcmVNaWRkbGV3YXJlU2FtcGxlXGFwaVx2YWx1ZXM=?=
X-Powered-By: ASP.NET
Date: Sun, 06 Jan 2019 10:02:45 GMT

{"result":"Request body is test custome middleware"}
```

## 全域註冊
如下的自訂Middleware用的app.UseMiddleware<[Your middleware name]>()的方式、在Configure()中註冊影響範圍是全域的，也就是說只要在這個API站台內的所有Controller或Action都會套用該Middleware。
```csharp
app.UseMiddleware<HttpCallLogMiddleware>();
app.UseMiddleware<AuthenticationMiddleware>();
```

## 區域註冊
那我們可能就會有疑問，如果有些自訂的Middleware只想要套用在特定的Controller或Action上，那要怎麼用呢?
答案就是<font style="color:red;">用Attribute的方式、將MiddlewareFilter套用在個別的Controller或Action上，同時指定其傳入的Type。<font>
```csharp
//...
[MiddlewareFilter(typeof(HttpCallLogMiddleware))]
public class ValuesController : ControllerBase
{
	//...
	[MiddlewareFilter(typeof(AuthenticationMiddleware))]
	public ActionResult<string> Post([FromBody] string value)
	{
		//...
	}
	
	//...
```

## 擴充方法(Extensions)
在C#中我們可以用static撰寫擴充方法，當然Middleware也不例外，如UseMvc()、UseRewriter()等等的使用方式，讓我們來為HttpCallLogMiddleware及AuthenticationMiddleware寫個Extensions。
```csharp
public static class HttpCallLogMiddlewareExtensions
{
	public static IApplicationBuilder UseHttpCallLogMiddleware(this IApplicationBuilder builder)
	{
		return builder.UseMiddleware<HttpCallLogMiddleware>();
	} 
		
}

public static class AuthenticationMiddlewareExtesions
{
	public static IApplicationBuilder UseAuthenticationMiddleware(this IApplicationBuilder builder)
	{
		return builder.UseMiddleware<AuthenticationMiddleware>();
	}
}
```

最後回到Configure()用新的擴充方法註冊即可。
```csharp
app.UseHttpCallLogMiddleware();
app.UseAuthenticationMiddleware();
```

## 結論
Middleware提供了很好的中介層，讓我們可以在Client與Server間、針對每個請求做不同的邏輯處理，透過Pipeline(管道)及分支的流程、串接多個不同的Middlewares，故可藉以組成大部分需要的商業邏輯，而且使用上簡單易懂、簡潔又方便，真的是很好用功能。

## 參考
1. [ASP.NET Core Middleware](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/middleware/?view=aspnetcore-2.2)
2. [ASP.NET Core 运行原理解剖[3]:Middleware-请求管道的构成](https://www.cnblogs.com/RainingNight/p/middleware-in-asp-net-core.html)