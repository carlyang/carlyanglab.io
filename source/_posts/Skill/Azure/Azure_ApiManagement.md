---
title: Azure - Policies of API Management
categories:
 - 技術文件
 - Azure
date: 2020/1/4
updated: 2020/1/4
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management, API Getway, API Proxy, Policies]
---
Author: Carl Yang

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
API Management(以下簡稱APIM)是Azure上作為雲端微服務架構上的Getway(匣道器)，可以進行API版本控管、流量監控、權限管控等功能，`本文主要在說明APIM的Policy類型`。
<!-- more -->

## Introduction
現今微服務(Micro-Service)當道，很多人都在致力於拆解自家的微服務，盡量讓每一個服務都具有最小、不可拆分的ATOM單位，然後每個微服務就可以互相調用、達成再利用、邏輯一致性等目的，`但拆分成微服務意味著每一隻服務的Size變小、但服務數量變大，前端應用要調用複雜度增加、後端監控及權限控管變得更加不易`，所以可以透過APIM規劃一致性入口進行這些更複雜作業。

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Create API Management Service

## Create APIM Service
首先請建立APIM服務，如下圖我們先選擇Developer層級。

Note:  
`由於我們只是測試服務，所以開啟Developer層級、也意味著它沒有保證SLA(99.9%)`。
![](/Teamdoc/image/azure/ApiManagement/create1.png)

下圖可以同時設定是否使用Application Insights功能，但由於我們只是測試就先不使用，但`如果是正式環境舊建議要開啟，因為APIM的其中一項重要工作就是監控各個後端API的流量`。
![](/Teamdoc/image/azure/ApiManagement/create2.png)

## FrontEnd & Backend
APIM中主要概念分為前、後端服務。
- FrontEnd:  
`前端服務指的是調用端、也就是對APIM發出Request的應用程式，它可能是另一個Website、API、或是其它任何有HTTP連線能的的系統`，也可視為API Consumer(API消費者)。
- Backend:  
`後端服務就是指APIM負責Relay的一個個API服務，也就是一個個的微服務`，也可視為Managed API(受控API)。

## Policies
APIM是透過定義出一個個不同的Policy進行前後端的對應及處理，引述官方文件如下:
>In Azure API Management (APIM), policies are a powerful capability of the system that allow the publisher to change the behavior of the API through configuration. Policies are a collection of Statements that are executed sequentially on the request or response of an API. Popular Statements include format conversion from XML to JSON and call rate limiting to restrict the amount of incoming calls from a developer.

[[MS Doc]](https://docs.microsoft.com/en-us/azure/api-management/api-management-howto-policies)

由以上引述不難看出，`Policies的作用，就是負責轉換前端發送過來的要求、並轉換成對應後端API的要求、再發送出去，而這個過程就可以進行身分驗證、流量管控等處理`，如下圖所示。
![](https://docs.microsoft.com/en-us/azure/api-management/media/set-edit-policies/code-editor.png)

Note:  
`Policies有作用範圍的概念，我們可以設定將某個Policy指作用於特定API的呼叫上，而不影響其他的調用`，詳細可參考[[MS Doc]](https://docs.microsoft.com/en-us/azure/api-management/set-edit-policies#configure-scope)

Policies的設定，可透過Azure Portal直接編輯XML組態，基本結構如下。
```xml
<policies>
  <inbound>
    <!-- statements to be applied to the request go here -->
  </inbound>
  <backend>
    <!-- statements to be applied before the request is forwarded to 
         the backend service go here -->
  </backend>
  <outbound>
    <!-- statements to be applied to the response go here -->
  </outbound>
  <on-error>
    <!-- statements to be applied if there is an error condition go here -->
  </on-error>
</policies>
```

## Indound
就是指每個進來的Request，在這個區塊中我們可以加入Head、驗證Access Token、組織Body等，並且指定對應到的Backend API URL。

Note:  
1. Set-variable:  
用來設定context.Request.Head如UserId等資訊，並pass給後端Service。
2. cache-lookup-value:  
設定cache(Context)中某key-value值，此屬性必須跟cache-store-value對應使用。

## Backend
就是指後端的API服務，我們必須事先定義好其格式，例如:URL。

## Outbound
當我們收到Backend送來的Response後，我們要進行那些處理、例如:加Head、Body重組、自訂回傳訊息等，再把Response傳回給Frontend應用系統。

Note:  
1. cache-store-value:  
取得cache(Context)中某key-value值，此屬性必須跟cache-lookup-value對應使用。
2. Find-and-replace:  
取得key-value並設定Response中回傳給前端。

## on-error
這部分是設定當Policy處理發生錯誤時，我們要另外進行的處理。

## Client Security
AIPM可以`允許前端使用者使用憑證登入使用後端的API服務`，詳細的官方文件說明如下。
[[MS Doc]](https://docs.microsoft.com/en-us/azure/api-management/api-management-howto-mutual-certificates)

## 結論:
透過APIM的處理，我們可以統一API微服務的入口、HTTP Request/Response的一致性、流量監控、權限管控等，`也可以與Application Insights搭配進行進一步的分析，而後端API只要賦予APIM的存取權限即可`，如此能夠簡化整個雲端微服務權限管控的複雜度，達成更高的按全性。

## 參考
1. [Policies in Azure API Management](https://docs.microsoft.com/en-us/azure/api-management/api-management-howto-policies)
2. [How to set or edit Azure API Management policies](https://docs.microsoft.com/en-us/azure/api-management/set-edit-policies)