---
title: Azure - 如何在API Management中的快取使用Azure Cache for Redis
categories:
 - 技術文件
 - Azure
date: 2022/5/21
updated: 2022/5/21
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management]
---

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
[[上一篇]](/2022/05/19/Skill/Azure/APIMCache/)介紹了如何在`APIM`中設定`cache-lookup`及`cache-store`，不過因為`APIM`內建的快取有些缺點、有可能造成快取資料不可預期的遺失，雖然說快取資料遺失可能還好(前提是原始資料還在)，但如果今天快取的目的是暫存來源資料、再一筆筆慢慢塞進資料庫，那這時候如果遺失快取資料就不是鬧著玩的了!故本文目的在說明如何設定`APIM`使其可以用外部的`Azure Cache for Redis`服務作為快取資料站。
<!-- more -->

## Redis-compatible cache
首先引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/api-management/api-management-howto-cache-external)的說明如下。

>In addition to utilizing the built-in cache, Azure API Management allows for caching responses in an external Redis-compatible cache, such as Azure Cache for Redis.  
>Using an external cache allows you to overcome a few limitations of the built-in cache:
>- Avoid having your cache periodically cleared during API Management updates
>- Have more control over your cache configuration
>- Cache more data than your API Management tier allows
>- Use caching with the Consumption tier of API Management
>- Enable caching in the API Management self-hosted gateway

上述說明了使用外部快取服務可以幫助我們克服一些內建快取的限制。
- 當`APIM`更新時造成的快取資料遺失。
- 我們可以有更多的快取設定控制權。
- 能夠用來快取的容量更大。
- `On-demand`的快取使用量計價。
- 可以在`APIM`的`Self-hosted gateway`使用(`Linux container`)外部快取。

## Configuration
接下來讓我們著手設定`APIM`的外部儲存快取為`Azure Cache for Redis`服務。

1. 建立`Azure Cache for Redis`服務
- 新建`Azure Cache for Redis`
![](/Teamdoc/image/azure/APIMCacheWithRedis/redis2.png)

- 等待Deploy
![](/Teamdoc/image/azure/APIMCacheWithRedis/redis1.png)

- Deploy完成後、記下`Azure Cache for Redis`服務的連線字串
![](/Teamdoc/image/azure/APIMCacheWithRedis/redis3.png)

2. 設定`APIM`的外部快取服務
- 新增快取服務
![](/Teamdoc/image/azure/APIMCacheWithRedis/apim_cache1.png)

- 選擇剛建好的`Azure Cache for Redis`服務，接著下方就會自動帶出相關的資訊及連線字串。
![](/Teamdoc/image/azure/APIMCacheWithRedis/apim_cache2.png)

如果是手動輸入步驟如下:
- `Used from`是指建立`Azure Cache for Redis`服務所在的`Region`名稱，必填欄位。
- `將先前記下來的連線字串貼到這邊的Connection string欄位`。
- 最後按下Save儲存。
![](/Teamdoc/image/azure/APIMCacheWithRedis/apim_cache3.png)

- 最後回到`External cache`就可以看到剛才設定好的`Azure Cache for Redis`服務。
![](/Teamdoc/image/azure/APIMCacheWithRedis/apim_cache4.png)

## Conclusion
在`APIM`中使用外部的快取服務真是有不少的好處，連`On-premise`端的`Self-hosted gateway`也都可以使用，不過可能要注意一下網路對外通訊，像我公司的安全政策可是非常嚴格的，如果被抓到將資料快取到外部雲端上可是很麻煩的，就看各位朋友的考量了。基本上，使用外部快取優點還是比缺點多的，例如我們可以隨著使用量調高調低`Redis`的`Scale`，對於維運來說也是非常方便的，而且`Azure Cache for Redis`服務也能給其他服務用、不是只有給`APIM`使用而已。

## References
1. [Use an external Redis-compatible cache in Azure API Management](https://learn.microsoft.com/en-us/azure/api-management/api-management-howto-cache-external)