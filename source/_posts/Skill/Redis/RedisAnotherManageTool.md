---
title: Redis - Another Redis Desktop Manager
categories:
 - 技術文件
 - Redis
date: 2021/3/21
updated: 2021/3/21
thumbnail: /Teamdoc/image/Redis/RedisTitle1.png
layout: post
tags: [REDIS Management Tool]
---
作者: Carl Yang

## Redis
![](/Teamdoc/image/Redis/RedisTitle1.png)

原本都是使用Redis Desktop Manager在連線REDIS Server進行一般操作，無意中發現另一個Github上開放的工具、叫做Another Redis Desktop Manager，基本上可使用的功能與原本的RDM沒什麼差異，但好處是它不收費、顯示資訊也很直覺，也是很不錯的工具。
<!--more-->

## Introduction
詳細的資訊可以直接參考Github上的說明，但對我來說，原本的RDM雖然也可以自行Build安裝檔，但也是挺麻煩的，如果不想自己Build、又必須要付費才能下載安裝使用，這個工具則是完全幫我們準備好，我們只要下載安裝即可。  
另一方面，它每台Redis Server的首頁就會顯示詳細的資訊，不像RDM還必須進入Server Info才能查看，也算是有點好處(看個人喜好囉，有些人可能會認為不應該一開始就抓Server Info)。
而Another RDM也有Dark Mode、眼睛舒服也開心，個人還滿喜歡這點的。不過，它的圖形化管理倒是不及RDM，要看後續作者是不是會再加上去囉。  
![](/Teamdoc/image/Redis/AnotherRDM2.png)

## Downloads
大家可以在以下的Github連結下載適合自己系統的安裝檔。
>[Another Redis Desktop Manager](https://github.com/qishibo/AnotherRedisDesktopManager)

## 新增連線
基本上操作都跟原本的RDM大同小異，如下圖，填入各欄位的資訊後就可以連線了。
![](/Teamdoc/image/Redis/AnotherRDM1.png)

### SSL連線
我使用這個工具是用來連線Azure Cache(Redis)的，但預設是不允許Non-SSL的PORT: 6379連線，所以要記得勾選SSL選項才可成功連線。不過，如果沒有要另外使用SSL Certificates，下方就不需要指定憑證就可直接連線(不勾選SSL仍然無法連線)。

## Dark Mode
要開啟Dark Mode如下圖。
![](/Teamdoc/image/Redis/AnotherRDM3.png)

## 多語系
它也支援多語系喔!
![](/Teamdoc/image/Redis/AnotherRDM4.png)

## Console
他也同樣支援redis cli、讓我們可以直接下CMD操作。
![](/Teamdoc/image/Redis/AnotherRDM5.png)

## Insert
手動新增的各種類型也都有支援。
![](/Teamdoc/image/Redis/AnotherRDM6.png)

## Update
跟RDM一樣、我們也可以手動直接更改Key值。
![](/Teamdoc/image/Redis/AnotherRDM7.png)

## 結論
目前使用上功能跟RDM其實大同小異、有用到的功能也都具備，而且它不需要每次自己Build程式，感覺是蠻方便的工具，推薦大家使用看看囉。