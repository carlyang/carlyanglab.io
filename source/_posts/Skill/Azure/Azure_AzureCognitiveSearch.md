---
title: Azure - Azure Cognitive Search
categories:
 - 技術文件
 - Azure
date: 2019/12/29
updated: 2019/12/29
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, Azure Cognitive Search, Azure Search]
---
作者: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Purpose
 `Azure Cognitive Search`為`Microsoft Azure`平台上的全文搜尋服務，透過 [匯入資料&rarr;分析&rarr;建立索引] 等步驟，最後提供語意式的RESTful API供外部查詢，本文主要在示範如何透過`Microsoft.Azure.Search SDK`匯入文件並分析及查詢等動作。
<!-- more -->

## Introduction
Azure Cognitive Search(原Azure Search)為Azure上的認知搜尋服務，透過Cognitive Service的語意分析，將特定欄位(自行指定)的文字內容進行分類、拆解、建立關鍵字索引，最後提供豐富的語意查詢RESTful API，可以提供快速的文件搜尋。  
經過分析後的索引資料，都會賦予一個分數，而這個分數越高、表示越接近搜尋條件，也更能快速獲得想要的資料。  
而Azure Cognitive Search更可以透過`Scale`功能，提供`Replicas(Basic Tier)`、`Partition(Standard Tier)`功能，也就是複本及資料分割等功能，提供高達99.99%的SLA(Microsoft宣稱的阿，有問題別找我XD)

詳細說明可參考:
[MS Docs](https://docs.microsoft.com/en-us/azure/search/search-what-is-azure-search
)

## Prerequisites
本文範例皆以下列項目為主要開發環境，請先確認安裝以下項目。
- .NET Core 3.1 SDK
- Visual Studio 2019 Version 16.4.2
- Microsoft.Azure.Search 10.1.0

## Analysis
如下圖所示，左方將我們的資料匯入`Azure Cognitive Search`後、它會將資料進行ML演算分析(`Indexing`)、建立索引(`Indexes`)，最後提供RESTful API給外部查詢、且此API具有語意式查詢的功能，讓撰寫查詢的開發者更容易使用。
![](https://docs.microsoft.com/en-us/azure/search/media/search-what-is-azure-search/azure-search-diagram.svg)

## Index & Indexer
- Index: `Index是指相同類型的資料`，例如產品資料、庫存資料...等，這些資料具有相同的資料欄位(Filelds)、資料類別(Type)、欄位屬性，也可以視為一個群組。
- Indexer: 用來匯入Index資料之用，具有排程的功能。例如，資料來源是Table Storage、且會持續增加時，可以用Indexer固定時間從資料來源撈取資料，`而且它是針對Azure Cognitive Search進行差異化更新`，不會每次都要更新整個服務內的資料，`而要在服務內搜尋到最新的文件，也必須仰賴Indexer持續更新才行`。

Note:  
`如果是在地端持續上傳資料的話，Indexer其實可以不需要用到、Index也可以動態增加，不用事先在Portal內定義好格式，SDK都可以完成這些工作`，在後面的程式範例就是使用這種方式。
![](/Teamdoc/image/azure/AzureSearch/index3.png)

## Process Flow
基本的Azure Cognitive Search就是[`建立服務` &rarr; `建立Index` &rarr; `建立Indexer` &rarr; `匯入資料` &rarr; `分析` &rarr; `語意查詢(RESTful API)`]。

Note:
1. 也可以使用Azure Portal上的[`Import Data`]匯入資料。
![](/Teamdoc/image/azure/AzureSearch/importData1.png)
2. 查詢的部分也可以用Azure Portal上的[`Search Explorer`]查詢。
![](/Teamdoc/image/azure/AzureSearch/searchExplorer1.png)

## Data Structure
要使用Azure Cognitive Service就要先到Azure上建立此服務(我承認這句是廢話XD)，建立完後我們再加入Index並設計其中個欄位的屬性，如下圖。
![](/Teamdoc/image/azure/AzureSearch/index1.png)

使用左上角[`Add Field`]按鈕加入新欄位，並指定各欄位的屬性。
![](/Teamdoc/image/azure/AzureSearch/index2.png)

屬性說明:  
1. Key: 此份資料的唯一識別欄位。(`必填欄位，但與以下五個搜尋屬性較無關係`)
2. Retrivable: 設定此欄位是否可被擷取，也就是透過API查詢回來的資料是否包含此欄位。
3. Filterable: 設定此欄位是否可作為過濾條件，也就是透過API查詢時，可套用過濾條件的欄位。
4. Sortable: 設定此欄位是否可被排序，也就是透過API查詢回來的該欄位能否排序。
5. Facetable: 設定此欄位是否可被切割，也就是透過Facet的功能，針對此欄位的值進行切割，例如: 庫存量，我們可以將其切割為100|1000|10000三個區塊、個別統計。(`通常這個欄位都是數值，不會是字串`)
6. Searchable: 設定此欄位是否可被搜尋，也就是透過輸入的關鍵字，Azure Cognitive Search是否會用來搜尋的欄位。(`換句話說，如果欄位沒有設定Searchable，就算其內容含有搜尋關鍵字，也不會因為這欄位而被搜尋到`)
7. Analyer: 分析器，`指定要用來處理此欄位資料的處理器，由於文字(或稱語言)有很多種，我們可以指定特定的分析器以精準分析文字`。

引用文件如下:
>- Retrievable means that it shows up in search results list. You can mark individual fields as off limits for search results by clearing this checkbox, for example for fields used only in filter expressions.
>- Key is the unique document identifier. It's always a string, and it is required.
>- Filterable, Sortable, and Facetable determine whether fields are used in a filter, sort, or faceted navigation structure.
>- Searchable means that a field is included in full text search. Strings are searchable. Numeric fields and Boolean fields are often marked as not searchable.

詳細說明可參考:
[MS Docs](https://docs.microsoft.com/en-us/azure/search/search-get-started-portal)

## Sample
程式範例如下:
```csharp
public class AzureSearchService : IAzureSearchService
{
    private readonly ConfigModel _config;

    private readonly ISearchServiceClient _searchServiceClient;

    public AzureSearchService(IOptions<ConfigModel> config)
    {
        _config = config.Value;
        _searchServiceClient = CreateSearchServiceClient();
    }

    private ISearchServiceClient CreateSearchServiceClient()
    {
        var searchServiceName = _config.AzureSearchInfo.SearchServiceName;
        var adminApiKey = _config.AzureSearchInfo.AdminKey;

        var serviceClient =
            new SearchServiceClient(
                searchServiceName,
                new SearchCredentials(adminApiKey)
                );

        return serviceClient;
    }
    private ISearchIndexClient CreateSearchIndexClient(string indexName)
    {
        var searchServiceName = _config.AzureSearchInfo.SearchServiceName;
        var queryKey = _config.AzureSearchInfo.QueryKey;

        var searchIndexClient =
            new SearchIndexClient(
                searchServiceName,
                indexName,
                new SearchCredentials(queryKey)
                );

        return searchIndexClient;
    }
    private void DeleteIndexIfExists(string indexName)
    {
        if (_searchServiceClient.Indexes.Exists(indexName))
        {
            _searchServiceClient.Indexes.Delete(indexName);
        }
    }
    private void CreateIndex(string indexName)
    {
        _searchServiceClient.Indexes.CreateAsync(
            new Index()
            {
                Name = indexName,
                Fields = FieldBuilder.BuildForType<ProductModel>()
            }
            ).Wait();
    }
    private void UploadDocuments(ISearchIndexClient indexClient)
    {
        try
        {
            var products = new List<ProductModel>();

            for (var i = 0; i < 10; i++)
            {
                products.Add(
                    new ProductModel
                    {
                        PartitionKey = $"Test{i}",
                        RowKey = Guid.NewGuid().ToString(),
                        ETag = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff}",
                        Timestamp = DateTime.Now,
                        name = $"TestName{i}",
                        price = new Random().NextDouble() * 100,
                        quantity = new Random().Next(1, 9999)
                    }
                );
            }

            var batch = IndexBatch.Upload(products);

            indexClient.Documents.Index(batch);
        }
        catch (IndexBatchException e)
        {
            // Sometimes when your Search service is under load, indexing will fail for some of the documents in
            // the batch. Depending on your application, you can take compensating actions like delaying and
            // retrying. For this simple demo, we just log the failed document keys and continue.
            Console.WriteLine(
                "Failed to index some of the documents: {0}",
                string.Join(", ", e.IndexingResults.Where(r => !r.Succeeded).Select(r => r.Key)));
        }
    }
    private void RunQueries(ISearchIndexClient searchIndexClient)
    {
        var selectColumns =
            new[]
            {
                "PartitionKey",
                "RowKey",
                "ETag",
                "Timestamp",
                "name",
                "price",
                "quantity"
            };

        var parameters = new SearchParameters()
        {
            Select = selectColumns
        };

        var results = searchIndexClient.Documents
            .Search<ProductSearchModel>("*", parameters);

        WriteDocuments(results);

        parameters =
            new SearchParameters()
            {
                Filter = "quantity lt 100",
                Select = selectColumns
            };

        results = searchIndexClient.Documents
            .Search<ProductSearchModel>("*", parameters);

        WriteDocuments(results);

        parameters =
            new SearchParameters()
            {
                OrderBy = new[] { "price desc" },
                Select = selectColumns,
                Top = 3
            };

        results = searchIndexClient.Documents.Search<ProductSearchModel>("*", parameters);

        WriteDocuments(results);

        parameters = new SearchParameters();
        results = searchIndexClient.Documents.Search<ProductSearchModel>("test1", parameters);

        WriteDocuments(results);
    }
    private void WriteDocuments<T>(DocumentSearchResult<T> searchResults) where T : class
    {
        foreach (var result in searchResults.Results)
        {
            Console.WriteLine($"{JsonSerializer.Serialize(result.Document)}");
        }

        Console.WriteLine();
    }

    public void Process()
    {
        var indexName = _config.AzureSearchInfo.IndexNameProducts;

        Console.WriteLine("{0}", "Deleting index...\n");
        DeleteIndexIfExists(indexName);

        Console.WriteLine("{0}", "Creating index...\n");
        CreateIndex(indexName);

        //For Administrator to upload documents
        var searchIndexClient = 
            _searchServiceClient.Indexes.GetClient(indexName);

        Console.WriteLine("{0}", "Uploading documents...\n");
        UploadDocuments(searchIndexClient);

        //For client to query documents
        var searchIndexClientForQueries = CreateSearchIndexClient(indexName);

        RunQueries(searchIndexClientForQueries);

        Console.WriteLine("{0}", "Complete.  Press any key to end application...\n");
        Console.ReadKey();
    }
}
```

上傳資料Model:
```csharp
public class ProductModel
{
    [Analyzer(AnalyzerName.AsString.EnLucene)]
    [IsSearchable, IsFilterable, IsSortable]
    public string PartitionKey { get; set; }
    [Analyzer(AnalyzerName.AsString.EnLucene)]
    [System.ComponentModel.DataAnnotations.Key]
    [IsSearchable, IsFilterable, IsSortable]
    public string RowKey { get; set; }
    public string ETag { get; set; }
    [IsSortable]
    public DateTime Timestamp { get; set; }
    [Analyzer(AnalyzerName.AsString.EnLucene)]
    [IsSearchable, IsFilterable, IsSortable]
    public string name { get; set; }
    [IsFilterable, IsSortable, IsFacetable]
    public double price { get; set; }
    [IsFilterable, IsSortable, IsFacetable]
    public int quantity { get; set; }
}
```
上傳範例的資料Model都採用Annotation的方式進行宣告，可針對每個Property進行設定，各Annotation屬性可參考上一小節說明。

`Microsoft.Azure.Search SDK`主要有下列兩個元件。
- ISearchServiceClient: 存取Azure Cognitive Search服務的Client端，`主要用於具有管理權限、可以修改服務相關功能設定的部分，執行時需要帶入Admin Key`，此Key可於下圖左上方紅框取得。
- ISearchIndexClient: 查詢文件所需要的元件，`使用時必須帶入Query Key、與ISearchServiceClient不同的地方在於它只能提供查詢功能，其他操作是不允許的`，Query Key可於下圖右下方的紅框取得。
![](/Teamdoc/image/azure/AzureSearch/Key1.png)


### 範例說明:
- 提供外部呼叫的主要Function。
```csharp
public void Process()
```

- 取得Service Client物件，帶入Admin Key用以操作Azure Cognitive Search服務。
```csharp
private ISearchServiceClient CreateSearchServiceClient()
```

- 取得Index Client物件，帶入Query Key用以查詢文件之用。
```csharp
private ISearchIndexClient CreateSearchIndexClient(string indexName)
```

- 使用Service Client物件刪除已存在的Index。
```csharp
private void DeleteIndexIfExists(string indexName)
```

- 使用Service Client物件建立新的Index。
```csharp
private void CreateIndex(string indexName)
```

- 使用Service Client物件上傳文件資料進指定的Index。
```csharp
private void UploadDocuments(ISearchIndexClient indexClient)
```

- 使用Index Client物件搜尋Index內的文件，其中包含幾項基本的語意式查詢，其他進階查詢可參考[MS Doc](https://docs.microsoft.com/en-us/azure/search/query-odata-filter-orderby-syntax)。
```csharp
private void RunQueries(ISearchIndexClient searchIndexClient)
```

- 將查詢結果印到畫面上，以檢視查詢結果。
```csharp
private void WriteDocuments<T>(DocumentSearchResult<T> searchResults)
```

## Others
Azure Search SDK也`提供Regular Expression的方式進行查詢，主要位於SearchParameters.QuertType()中`，詳細可參考[[MS Doc]](https://docs.microsoft.com/en-us/dotnet/api/microsoft.azure.search.models.searchparameters?view=azure-dotnet#properties)。

## 結論:
Azure Cognitive Search提供豐富、快速的文件搜尋，而[文件]一詞其實只是一種不精準的說法，事實上只要是資料可在語意上被分析、拆解的都可以使用此服務(`通常是文字資料`)，資料量越多就越能夠增加其準確度，是非常好用的服務。

P.S.不過，`成本還是要考慮進來，畢竟雲端服務要好用、高HA、高SLA，花的費用可不少，使用前請謹慎評估`。

## 參考
1. [What is Azure Cognitive Search?](https://docs.microsoft.com/en-us/azure/search/search-what-is-azure-search)
2. [How to use Azure Cognitive Search from a .NET Application](https://docs.microsoft.com/en-us/azure/search/search-howto-dotnet-sdk)
3. [OData language overview for $filter, $orderby, and $select in Azure Cognitive Search](https://docs.microsoft.com/en-us/azure/search/query-odata-filter-orderby-syntax)