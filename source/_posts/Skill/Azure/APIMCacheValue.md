---
title: Azure - 如何在API Management中快取具特定Key的資料
categories:
 - 技術文件
 - Azure
date: 2022/5/20
updated: 2022/5/20
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management]
---

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
[[上一篇]](/2022/05/19/Skill/Azure/APIMCache/)介紹了`如何儲存及取得相同條件下的回應快取`，但有些情境下我們可能很明確地知道要快取些什麼值，例如:當下的User描述資訊，本文目的就在介紹如何快取特定的Key值。
<!-- more -->

## Cache Value
要快取特定的Key必須要使用`cache-lookup-value`這個Policy，引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/api-management/cache-lookup-value-policy)說明如下。

>Use the cache-lookup-value policy to perform cache lookup by key and return a cached value. The key can have an arbitrary string value and is typically provided using a policy expression.

也就是說，我們可以用`cache-lookup-value`取得之前快取下來具有特定Key的值，而這個值可以是任何的字串，範例如下。

```xml
<cache-lookup-value key="cache key value"
    default-value="value to use if cache lookup resulted in a miss"
    variable-name="name of a variable looked up value is assigned to"
    caching-type="prefer-external | external | internal" />
```

說明:
- `key`: 取得快取的特定Key值。
- `default-value`: 當找不到該快取時要回應的預設值。
- `variable-name`: 當我們取得快取值時要使用的變數名稱。
- `caching-type`: 快取類型，分外部快取及內部快取，要看有沒有另外設定。

舉例來說，我們可能會想要在部分回應的資訊加上之前快取過的特定Key資料如下的`userprofile`。

```json
{
  "Test1" : "TestValue1",
  "Test2" : "TestValue2",
  "userprofile" : "$userprofile$"
}
```

我們可以先自訂變數存入`enduserid`資訊(資訊來自驗證的`JWT`權杖)，如下範例。

```xml
<set-variable
  name="enduserid"
  value="@(context.Request.Headers.GetValueOrDefault("Authorization","").Split(' ')[1].AsJwt()?.Subject)" />
```

接著我們可以用`cache-lookup-value`取得快取值、並存入變數`userprofile`中，這樣在回應時就會透過`$userprofile$`標記法取得變數內容、並且帶入回應中，範例如下。

```xml
<cache-lookup-value
key="@("userprofile-" + context.Variables["enduserid"])"
variable-name="userprofile" />
```

限制:
1. 每個`cache-lookup-value`都要有搭配的`cache-store-value`來指定快取相同的Key值。
2. 如果是使用內建的`APIM`快取，那同一個`Region`內的`APIM`都會共用相同的快取分享，所以為了安全性建議還是使用外部快取比較安全。

## Cache Store
上一小節是說明`cache-lookup-value`的使用方法，但是千萬別忘了`cache-lookup-value`必須搭配`cache-store-value`使用，才能夠快取特定的值後、`cache-lookup-value`抓取快取值出來使用，引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/api-management/cache-store-value-policy)說明如下。

>The cache-store-value performs cache storage by key. The key can have an arbitrary string value and is typically provided using a policy expression.

也就是說`cache-store-value`可以幫助我們快去特定Key的任意字串值，範例如下。

```xml
<cache-store-value key="cache key value" value="value to cache" duration="seconds" caching-type="prefer-external | external | internal" />
```

說明:
- `key`: 要快取的特定Key。
- `value`: 要快取的特定Key值。
- `duration`: 存活秒數。
- `caching-type`: 快取類型，分外部快取及內部快取，要看有沒有另外設定。

限制:
1. 因為`快取動作是採用非同步的方式`，所以有可能還沒快取進去、另一個操作就先讀取快取，這時候有可能還拿不到快取內容。
2. 如果是使用內建的`APIM`快取，那同一個`Region`內的`APIM`都會共用相同的快取分享，所以為了安全性建議還是使用外部快取比較安全。

## Conclusion
本文說明`cache-lookup-value`與`cache-store-value`的快取搭配，能夠讓我們快取自訂的特定Key值，並在合適的情境下運用。

## References
1. [Get value from cache](https://learn.microsoft.com/en-us/azure/api-management/cache-lookup-value-policy)
2. [Store value in cache](https://learn.microsoft.com/en-us/azure/api-management/cache-store-value-policy)