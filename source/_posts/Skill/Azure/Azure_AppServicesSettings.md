---
title: Azure - The Application Settings of App Services
categories:
 - 技術文件
 - Azure
date: 2020/4/13
updated: 2020/4/13
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, App Services, Application Settings]
---
Author: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Purpose
傳統於IIS上架設Web Site時，通常都會將一些組態設定放置於Web.config檔內，爾後Web site啟動時會將這些組態設定載入使用。這種方式在舊有的.NET Framework所Publish的Package沒有什麼問題，但如果是Publish到Azure App Services上的話，就會有問題了。`本文主要在說明發生的問題，以及解決方法`。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- App Services

## Introduction
簡單來說，本文遇到的問題就是`當我們在Web.config檔具有同名的參數時，例如:ASPNETCORE_ENVIRONMENT，它的值會蓋掉我們在App Services的Application Settings所新增的同名參數，造成不論我們怎麼改Application設定都沒有作用`。

## Problem Details
當我們想要將寫好的網站Publish上去Azure App Services時，不管是舊的ASP.NET Framework或是新的ASP.NET Core，最終放上IIS都會含有一個Web.config檔，這是為了遵循IIS的架構，有一些基礎組態或自訂參數都是從這裡而來。  

就算是ASP.NET Core已經重構組態設定，可將所有的靜態參數自訂在appsettings.json檔中，但最後仍有少數幾個基本的設定，需要由Web.config檔提供、這樣IIS才能夠認得，如下圖所架設的App Services。  
![](/Teamdoc/image/azure/AppServices/web_config_1.png)

由上圖可以看到，即便是ASP.NET Core也是需要利用Web.config檔內的`<aspNetCore />`標記、告訴IIS現在是要用dotnet命令執行XXX.dll。

這時候問題來了，`如果我們依照傳統的方式在Web.config內加上下面這個設定，而這個環境變數是用來在Runtime時告訴.NET Core要載入哪個環境的appsettings.{env}.json檔`。  
```xml
<aspNetCore processPath="dotnet" arguments=".\XXX.dll" stdoutLogEnabled="false" stdoutLogFile="\\?\%home%\LogFiles\stdout">
  <environmentVariables>
      <environmentVariable name="ASPNETCORE_ENVIRONMENT" value="Debug" />
  </environmentVariables>
</aspNetCore>
```

接著，我們再去App Services新增同名參數，如下圖。  
![](/Teamdoc/image/azure/AppServices/application_settings_1.png)

`最後，不論我們把Application Settings中的參數改成何值，IIS內的ASPNETCORE_ENVIRONMENT參數永遠是Debug，由於這個Case是要依靠這個參數的不同值、切換不同環境組態設定，如果永遠都是Debug環境的話就會有問題了。`

## Solution
根據以上調查，很直覺地我們就知道要去把Web.config內的同名參數刪除，其實就可以解決這個問題。

## Others
其實發現這個問題的時候，專案的Web.config有使用`http://schemas.microsoft.com/XML-Document-Transform`來進行環境組態的切換，也就是說其實參數ASPNETCORE_ENVIRONMENT並不直接置於Web.config檔中、而是放在其他的XML檔案內，最後Publish時才被轉換、帶入Web.config，這部分就要請讀者自行尋找了。

## 結論:
這篇主要是說明、也記錄一下這個狀況，雖然很簡單但有時候就是不小心會忘記，以防未來再遇到時可以回來查一下。  
`雖然目前來說，ASP.NET Core專案並不會阻擋我們同時使用appsettings.json及web.config，但基本上使用.NET Core架構，還是使用appsettings.json來得更單純簡潔，而且也更容易維護(基本上json內容你設定如何、上正式環境內容一樣不會被另外處理過，不像web.config會轉換來轉換去、不一定跟版控一致)，所以強烈建議自訂的組態就使用appsettings.json，環境的切換就使用application settings來設定即可`。

## 參考
1. N/A