---
title: Redis - Hash Set & Get
categories:
 - 技術文件
 - Redis
date: 2021/4/27
updated: 2021/4/27
thumbnail: /Teamdoc/image/Redis/RedisTitle1.png
layout: post
tags: [REDIS, HASH, HMSET, HMGET]
---
作者: Carl Yang

## Redis
![](/Teamdoc/image/Redis/RedisTitle1.png)

## Purpose
利用REDIS進行快取已經是很普及的方式了，但因為有些程式上的REDIS使用技巧，可以有較好的效能及避免未來可能發生的問題，故寫這篇文章記錄一下、也加深記憶並分享給大家參考。
<!--more-->

## Introduction
本篇文章主要用StackExchange.Redis展示三個開發REDIS時的小技巧，如下。
1. `利用SHA256設定Hash Key，以獲得固定字串長度的Key值，且可以透過相同的Input、取得相同的Output Key`，可以直接透過Key Exists判斷是要從REDIS抓取已Cache的值、或是要從資料來源重新抓取。
2. 利用效能較好的REDIS Hash、進行`HMSET/HMGET資料存取`，並可進行批次操作。
3. 針對資料進行`壓縮、資料分割`存取，避免因REDIS Server的Single Process、造成可能在存取單一大量資料時的延遲。

## SHA256 Key
- `REDIS的Key最大可以到512 MB`，但實務上應該不會有人真的去存這麼大的Key，不僅Key值過大、也會在搜尋Key時造成`長時間搜尋的效能問題`，而使用SHA256所產出的字串是`固定長度`的，不會有過長的問題。 
- 另一個使用SHA256的原因，是因為它`相同的Input、可以得到相同的Output`，這在判斷是否使用Cache時是很有幫助的，例如:條件A = 1 & B = 2 & C = 3、我們可以Input為A_1_B_2_C_3，就可以得到相同的Output字串、再判斷REDIS是否有相同Key存在、有則使用REDIS、沒有則重新從資料來源撈、再Cache進REDIS，以下為範例。  
```csharp
public static string Sha256Encrypt(string plainTxt)
{
    var bytes = System.Text.Encoding.Default.GetBytes(plainTxt);
    var sha256 = new System.Security.Cryptography.SHA256CryptoServiceProvider();
    var encryptBytes = sha256.ComputeHash(bytes);

    return Convert.ToBase64String(encryptBytes);
}
```

## HMSET/HMGET
在REDIS的命令中，`HMSET/HMGET是可以用來進行批次操作的`，這樣可以`節省重複建立連線的時間`、增加REDIS存取效率，而Hash本身的效能相較於單純的String Set/Get也快上許多，以下為使用StackExchange.Redis進行Hash批次處理的範例。  
```csharp
public bool HashSet(
    string key,
    List<KeyValuePair<string, RedisValue>> values,
    out string errorMsg,
    int expireSeconds = -1
    )
{
    errorMsg = string.Empty;

    if (null != values && 0 < values.Count)
    {
        var batch = _cacheDb.CreateBatch();
        var tasks = new List<Task>();

        values.ForEach(x =>
        {
            tasks.Add(
                batch.HashSetAsync(
                    key,
                    new[]
                    {
                        new HashEntry(x.Key, x.Value)
                    }
                    )
                );
        });

        batch.Execute();

        Task.WaitAll(tasks.ToArray());

        if (KeyExists(key))
            _cacheDb.KeyExpire(key, new TimeSpan(0, 0, expireSeconds));

        return true;
    }

    errorMsg = "Key or Value can't be null or empty";

    return false;
}
```
Note:  
上方的Hash Set function重點在於使用`_cacheDb.CreateBatch()`及`batch.Execute()`建立批次寫入。  

```csharp
public bool HashGet(
    string key,
    out List<KeyValuePair<string, RedisValue>> values, 
    out string errorMsg
    )
{
    values = null;
    errorMsg = string.Empty;

    if (string.IsNullOrEmpty(key))
    {
        errorMsg = "Key can't be null or empty";

        return false;
    }

    if (!_cacheDb.KeyExists(key))
    {
        errorMsg = "Key doesn't exist";

        return false;
    }

    values = _cacheDb
        .HashGetAll(key)
        .Select(x => 
            new KeyValuePair<string, RedisValue>(x.Name, x.Value)
        )
        .ToList();

    return true;
}
```
Note:  
上方的Hash Get function重點在於使用`HashGetAll(key)`建立批次讀取。  

## 資料壓縮
進行壓縮的目的，是為了`降低REDIS的Memory使用量、減少資料傳輸時間`，雖然在AP端要額外多出壓縮/解壓縮的成本，但是相比資料縮減後減少的傳輸花費時間、顯得經濟許多，以下為使用.NET Core內建的GZIP進行壓縮/解壓縮的範例。  
```csharp
private static void CopyTo(
    Stream source, 
    Stream target
    )
{
    var buffer = new byte[4096];
    int count;
    while (0 != (count = source.Read(buffer, 0, buffer.Length)))
    {
        target.Write(buffer, 0, count);
    }
}

public static byte[] Compress(string value)
{
    var bytes = Encoding.UTF8.GetBytes(value);

    using (var input = new MemoryStream(bytes))
    {
        using (var output = new MemoryStream())
        {
            using (var zipped = new GZipStream(output, CompressionMode.Compress))
            {
                CopyTo(input, zipped);
            }

            return output.ToArray();
        }
    }
}

public static string Decompress(byte[] bytes)
{
    using (var input = new MemoryStream(bytes))
    {
        using (var output = new MemoryStream())
        {
            using (var unzipped = new GZipStream(input, CompressionMode.Decompress))
            {
                CopyTo(unzipped, output);
            }

            return Encoding.UTF8.GetString(output.ToArray());
        }
    }
}
```

## 資料分割
由於REDIS Server是Single Process的機制，所以如果有某個資料值過大(例如:500 MB)時，其它Request就會需要等待它存取完成時才會輪到，但通常會做Cache應該單一資料大小都不會太大，`所以資料分割基本上只是預防性質，正常狀況應該是不太會需要用到`，加上REDIS本身就是Memory存取、效能已經非常好，所以這個機制`一般來說可做可不做，就看大家自行衡量看看是否需要`，以下為範例。
```csharp
public void SetValue(string key, string value, int? expirySeconds = null)
{
    var count = 0;
    var offset = 1048576;//1 mb
    var zipped = GZipHelper.Compress(value);

    var batchSet = new List<KeyValuePair<string, RedisValue>>();

    //Prepare batch list to insert
    do
    {
        var data = zipped.Skip(count * offset).Take(offset).ToArray();

        if (0 >= data.Length)
            break;

        batchSet.Add(new KeyValuePair<string, RedisValue>($"{count + 1}", data));

        count += 1;
    } while (true);

    if (!_redisService.HashSet(
        key,
        batchSet,
        out var errorMsg,
        expirySeconds ?? 259200//3 days
    ))
    {
        Console.WriteLine($"[{nameof(CacheService)}] {nameof(SetValue)}()");
        Console.WriteLine($"CurrentTime: {DateTime.Now:yyyy-MM-dd HH:mm:ss.fff}");
        Console.WriteLine($"Error Msg: {errorMsg}");
    }
}
```
Note:  
- 上方的SetValue function重點在於使用`zipped.Skip(count * offset).Take(offset).ToArray()`將壓縮後的bytes分批存入`List<KeyValuePair<string, RedisValue>>()`，最後再存入REDIS Hash。 
- 而REDIS的Hash跟String不同，它在Hash內還有區分`Field/Value`，正好可以用來排序分割後的bytes，爾後`取回時再利用Field排序、將bytes排成原來的順序`。  

```csharp
public string GetValue(string key)
{
    if (!_redisService.KeyExists(key)) return null;

    var data = new StringBuilder();

    if (_redisService.HashGet(
        key,
        out var values,
        out var errorMsg
    ))
    {
        var temp = values
            .Select(x => new
            {
                x.Key,
                x.Value
            })
            .OrderBy(x => int.Parse(x.Key))
            .ToList()
            .Select(x => (byte[]) x.Value)
            .ToList();

        var offset = 0;
        var bytes = new byte[temp.Sum(x => x.Length)];
        temp.ForEach(x =>
        {
            Buffer.BlockCopy(
                x, 
                0, 
                bytes,
                offset, 
                x.Length
                );

            offset += x.Length;
        });

        data.Append(GZipHelper.Decompress(bytes));
    }
    else
    {
        Console.WriteLine($"[{nameof(CacheService)}] {nameof(GetValue)}()");
        Console.WriteLine($"CurrentTime: {DateTime.Now:yyyy-MM-dd HH:mm:ss.fff}");
        Console.WriteLine($"Error Msg: {errorMsg}");
    }

    return data.ToString();
}
```
Note:  
- 上方的GetValue function重點在於使用`Buffer.BlockCopy()`將取回的bytes一批批按照排序存入bytes array，最後再進行解壓縮即可獲得原本的字串。  

## 結論
透過以上的說明，就可以順利進行REDIS的Hash操作，不僅有較快的效能、也能夠處理大部分的Cache情境，完整的程式範例，可參考以下Github位置。

## 參考
1. [AzureCache.Redis](https://github.com/carlyang920/AzureCache.Redis)