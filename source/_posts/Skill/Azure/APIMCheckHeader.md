---
title: Azure - 在API Management中針對每個要求/回應檢查特定Header
categories:
 - 技術文件
 - Azure
date: 2022/4/21
updated: 2022/4/21
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management]
---

![](/Teamdoc/image/azure/logos/apim.png)s

## Purpose
許多情境下我們可能會希望自訂一些驗證資訊，以確保每個來源要求都是合法的，所以我們可能必須在`HTTP Header`中帶入一些自訂的資訊，然後在`APIM`中進行處理，本文的目的在於說明如何檢查自訂`HTTP Header`及自訂回傳狀態及訊息。
<!-- more -->

## check-header
引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/api-management/check-header-policy)如下。
>Use the check-header policy to enforce that a request has a specified HTTP header. You can optionally check to see if the header has a specific value or one of a range of allowed values. If the check fails, the policy terminates request processing and returns the HTTP status code and error message specified by the policy.

這個原則可以幫助我們在`APIM`中檢查特定的`HTTP Header`、並且能夠自訂回應狀態及訊息，如此就可以強化我們在`APIM`通訊上的安全性，尤其是自訂的設計來說，確實能夠有一定程度上的安全性提升，畢竟自訂標頭比較讓有心人難以容易猜到，如果標頭值再進一步以`不可逆`的方式加密，既能夠更加確保安全性，也是一個簡單、方便又實用的功能，範例如下。

```xml
<check-header name="CustomHeader" failed-check-httpcode="413" failed-check-error-message="Not legal reason" ignore-case="true">
    <value>CustomHeaderValue1</value>
    <value>CustomHeaderValue2</value>
</check-header>
```

說明:
- `name`: 要檢查的標頭名稱。
- `failed-check-httpcode`: 當檢查不通過時要回傳的`HTTP Status`狀態碼。
- `failed-check-error-message`: 當檢查不通過時要回傳的`HTTP`自訂訊息。
- `ignore-case`: 檢查標頭值時是否要忽略大小寫，`true`: 是、`false`: 否。

Note:  
1. 大家應該不難注意到，`value是可以包含多個值的!`也就是說，同樣的一個`check-header`可以同時檢查多個值是否符合。
2. 另一個重點是`check-header原則可以使用在inbound、outbound區塊中!`這代表我們可以用來同時檢查`HTTP Request & Response`的標頭資訊，然後轉為我們自訂的內容再送到後端API或是回傳給呼叫端，`有很多時候我們不想後端的URL、或是一些系統資訊洩漏出去`，就可以用這個原則先檢查出來、再進一步另行處理。

## Conclusion
使用`check-header`原則的情境主要可以分兩種來說，一種是想要在`HTTP Request`時檢查自訂標頭，這可能是驗證權杖、也可能是想檢查某些最低的版本號碼等。另一種情境是想要檢查後端API的回傳標頭，如果有特定狀況就可以藉由這個原則進行攔截、再轉為我們希望的自訂內容後回傳，是很方便又實用的功能。

## References
1. [Check HTTP header](https://learn.microsoft.com/en-us/azure/api-management/check-header-policy)