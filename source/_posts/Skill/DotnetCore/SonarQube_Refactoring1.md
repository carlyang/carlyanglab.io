---
title: .NET Core - SonarQube Refactoring in C#(1)
categories:
 - 技術文件
 - .NET Core
 - SonarQube
date: 2021/06/17
updated: 2021/06/17
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [ASP.NET Core, SonarQube, Security Hotspot, Random, CORS]
---
作者: Carl Yang

![](/Teamdoc/image/DotnetCore/Logo.png)

## Purpose
本文主要在說明如何解決`SonarQube - Security Hotspots`對於`C#`撰寫時，檢查到的`Random Object`及`CORS - AllowAnyOrigin`、所可能造成的安全性問題進行處理。
<!-- more -->

## Prerequisites
本文是使用ASP.NET Core 3.1作為開發環境進行練習，請先安裝下列項目。
- 請先安裝ASP.NET Core 3.1 SDK([Download](https://dotnet.microsoft.com/download/dotnet/3.1))

## Introduction
最近使用`SonarQube`檢查`C#`程式碼的品質以及重構時，發現了幾個有趣的安全性問題，可能會造成我們系統有些漏洞沒注意到，導致未來發生嚴重的問題。  
本文所要說明的兩個安全性問題，其實當初在寫的時候還真的沒有去注意到，好在有`SonarQube`幫我們檢查出來，及早修正掉免得到時候造成災情才來彌補，就划不來了。

## `SonarQube - Security Hotspots`
先簡單翻譯成`安全性熱點`(應該有其他更好的翻譯)，也就是我們的程式內可能存在造成安全性問題的地方，而`SonarQube`一旦發現有這些地方就會歸類在`Security Hotspots`，而且是必解的問題。

#### `Random Object`
這個問題我們通常要在特定範圍內取得隨機數字，可能就直接用`Random`物件來取得，如下程式碼。

```csharp
var jitter = new Random();
var number = jitter.Next(1, 100);
```

引用`SonarQube`的說明如下:

>When software generates predictable values in a context requiring unpredictability, it may be possible for an attacker to guess the next value that will be generated, and use this guess to impersonate another user or access sensitive information.

簡單來解釋，就是當我們直接用`Random`物件在`某段可預測的數字範圍間`產生隨機數，那有心人就可以利用猜測的方式猜出下一個數字，進而造成系統意外的問題、甚至可以藉此拿到機密資訊。

再引用`SonarQube`的進一步說明如下:

>As the System.Random class relies on a pseudorandom number generator, it should not be used for security-critical applications or for protecting sensitive data. In such context, the System.Cryptography.RandomNumberGenerator class which relies on a cryptographically strong random number generator (RNG) should be used in place.

也就是說，`C#`的`Random`物件是一種虛擬的隨機數產生器，他並沒有使用加密方法進行隨機數的處理，所以我們應該在有安全性的系統中、使用具有加密能力的物件來產生隨機數，那就是`System.Cryptography.RandomNumberGenerator`。

程式碼範例如下:

```csharp
var jitter = RandomNumberGenerator.GetInt32(0, 100);
```

或者複雜用法如下:

```csharp
using System.Security.Cryptography;
...
var randomGenerator = RandomNumberGenerator.Create();

byte[] data = new byte[16];
randomGenerator.GetBytes(data);
return BitConverter.ToString(data);
```

所以，透過使用`System.Cryptography.RandomNumberGenerator`產生隨機數，才是較為安全的方式。

#### `CORS - AllowAnyOrigin`
`CORS`是瀏覽器為了防止跨網域存取的行為而禁止的動作，所以如果我們前端對後端API傳送Request時，有可能因為網域不同而被判斷為非法行為。  
所以在`ASP.NET Core API`中，我們常會在`Startup.cs`的`ConfigureServices()`加上`CORS`的Code來允許跨網域存取行為。

```csharp
services.AddCors(options =>
{
    options.AddPolicy("CorsPolicy", policy =>
    {
        policy
            .AllowAnyOrigin()
            .AllowAnyHeader()
            .AllowAnyMethod();
    });
});
```

而關鍵點就在`AllowAnyOrigin()`這行，我們`等同於允許所有來源的跨網域請求對API存取`，這是很危險的事情。引用`SonarQube`的說明如下:

>Same origin policy in browsers prevents, by default and for security-reasons, a javascript frontend to perform a cross-origin HTTP request to a resource that has a different origin (domain, protocol, or port) from its own. The requested target can append additional HTTP headers in response, called CORS, that act like directives for the browser and change the access control policy / relax the same origin policy.

主要關鍵點在於`CORS`為了允許跨網域請求，會先送給Client端一個`Preflight Request`、其中會帶上`Access-Control-Allow-Origin: *`這個Header，這樣有心人就可以利用他允許所有的來源欺騙我們的API，進而取得機密資訊。  
所以，我們雖然必須允許Client端的跨網域請求，但也必須是有限度地允許存取，應該要改用`WithOrigins()`來指定允許存取的來源。

```csharp
services.AddCors(options =>
{
    options.AddPolicy("CorsPolicy", policy =>
    {
        policy
            .WithOrigins("https://*.azurewebsites.net")
            .AllowAnyHeader()
            .AllowAnyMethod();
    });
});
```

Note:  
上面的範例也要特別注意，`網路的世界最好都要使用https的安全通訊，否則SonarQube一樣會檢查出安全性問題!`

## 結論
針對以上`Random Object`及`CORS AllowAnyOrigin`造成安全性問題的說明應該是不難懂，但卻是我們在開發的時候常一不注意就犯下的錯誤，如果不是靠著`SonarQube`幫我們檢查出來，我們還真是會在不知不覺間就造成系統的漏洞，當然也還有其他更好的`SonarQube`案例可說明，慢慢地有機會我都會再分享出來可大家參考，也是一種精進技術的方式喔。

## 參考
1. [[Using pseudorandom number generators (PRNGs) is security-sensitive
]](https://rules.sonarsource.com/csharp/type/Security%20Hotspot/RSPEC-2245?search=random)
2. [[Having a permissive Cross-Origin Resource Sharing policy is security-sensitive]](https://rules.sonarsource.com/csharp/type/Security%20Hotspot/RSPEC-5122?search=AllowAnyOrigin)