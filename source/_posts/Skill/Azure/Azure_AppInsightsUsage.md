---
title: Azure - The Event Types & Usage of Application Insights
categories:
 - 技術文件
 - Azure
date: 2020/1/1
updated: 2020/1/1
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, Application Insights, Event Type, Usage]
---
作者: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Purpose
透過傳送遙測資料到Azure上的Application Insights，可以記錄我們應用系統(API、Web...etc)各式各樣的Log，可做為後續分析、監控等等的資料依據。  

本篇文章主要在`紙上談兵`，沒錯!就是紙上談兵，談的是`Application Insights中的Usage功能、及幾種Event Type類型`，因為每個應用系統的遙測資料特性都不太相同，故只針對功能來做說明，至於`怎麼用、怎麼從一堆遙測資料中挖出有用的資訊，就要看各位看倌如何運用了`。
<!-- more -->

## Introduction
Azure的Applicatioin Insights(以下簡稱AppInsights)服務用來收集應用系統端的各式資料，例如CPU使用率、Memory使用率、Event事件、錯誤訊息等，屬於Azure Monitor中的一部分。因此，AppInsights也提供Usage的功能，幫助了解應用系統是如何地被使用、哪一個環節的使用量最高、或者使用者都如何使用我們的應用系統。

## Prerequisites
能夠看到本文，您就具有合適的先決條件了。

## Event Types
Event Types是AppInsights從應用程式端收集而來的各式各樣Logs資料、再加以分類而成的一種資料類型概念，以下介紹各種AppInsights的Event Types。
![](/Teamdoc/image/azure/AppInsights/Usage/event_types1.png)

- Trace: 系統診斷紀錄。
- Request: 系統Request的使用頻率及花費時間。
- Page View: 頁面、畫面、分頁或表單上的使用紀錄。
- Custom Event: 使用者自訂義事件紀錄。
- Exception: 系統例外錯誤紀錄。
- Dependency: `系統的相依性紀錄，也就是與系統相依的外部系統調用紀錄`，例如: Invoke另一個Site上的API以取得資料，這樣的調用動作就會被記錄在這類型中。
- Availability: 系統的可用性紀錄，也就是系統在活期會固定發送這類型的遙測紀錄，表示應用系統是否存活。

## Usage
### Users, Sessions & Events Analysis
![](/Teamdoc/image/azure/AppInsights/Usage/usage1.png)

如上圖所示，打開AppInsights左方的Menu、有一個Usage區塊，Users/Sessions/Events三個功能就位於此，由於這三個功能的內容與操作幾乎相同，僅分析的資料不同，故以下圖合併說明。
![](/Teamdoc/image/azure/AppInsights/Usage/users1.png)

1. 以上圖為例，我們可以進入Users Usage功能後、使用上方的各個條件進行過濾，結果顯示於下方的直方圖。
2. 按下畫面下方的[`View More Insights`]按鈕後可顯示User的概略地點、OS系統、瀏覽器類型等資訊。

Note:  
`下方的資訊必須要先確認遙測資料是否有傳送進來，否則會顯示無法識別(Undefined)`。

以下針對三種Usage說明:
- Users: 統計有多少Users使用系統，這些Users是使用一組匿名IDs(存在瀏覽器cookie中)來統計，所以同一人使用不同瀏覽器會視為兩人。
- Sessions: User工作階段(或稱連線)統計，也就是每一個User進入系統會算做一條Session，而這條Session會在閒置半小時或連續使用超過24小時後被統計一次。
- Events: 統計特定頁面或功能的使用頻率，這個統計則通常在統計User與系統間的互動次數，例如Load Page、使用者按下某個按鈕...等等。

節錄官方文件說明如下:
>- Users tool: How many people used your app and its features. Users are counted by using anonymous IDs stored in browser cookies. A single person using different browsers or machines will be counted as more than one user.
- Sessions tool: How many sessions of user activity have included certain pages and features of your app. A session is counted after half an hour of user inactivity, or after 24 hours of continuous use.
- Events tool: How often certain pages and features of your app are used. A page view is counted when a browser loads a page from your app, provided you have instrumented it.  
A custom event represents one occurrence of something happening in your app, often a user interaction like a button click or the completion of some task. You insert code in your app to generate custom events.

[[MS Doc]](https://docs.microsoft.com/en-us/azure/azure-monitor/app/usage-segmentation)

### Funnels
`幫助我們了解系統各節點的使用比例，包含一系列操作的紀錄`。我們可以依序找出這一系列動作紀錄，然後分析這些動作中那些花費最多時間、使用者最常在哪一個環節結束這次的使用等資訊。

下圖節錄自微軟文件:
![](https://docs.microsoft.com/en-us/azure/azure-monitor/app/media/usage-funnels/funnel1.png)

Note:  
跟上一小節的Users/Sessions/Events不同點在於，`Funnels是以系統各節點為主軸，顯示各節點中User的使用比例`。  
這樣的統計可以幫助我們了解那些節點的使用率最高、或是隨著時間顯示使用率的變化，我們可以藉此找出某些規律性、並加以預先調整。

上圖說明，節錄官方文件如下:
>Funnels features  
The preceding screenshot includes five highlighted areas. These are features of Funnels. The following list explains more about each corresponding area in the screenshot:
>1. If your app is sampled, you will see a sampling banner. Selecting the banner opens a context pane, explaining how to turn sampling off.
>2. You can export your funnel to Power BI.
>3. Select a step to see more details on the right.
>4. The historical conversion graph shows the conversion rates over the last 90 days.
>5. Understand your users better by accessing the users tool. You can use filters in each step.

[[MS Doc]](https://docs.microsoft.com/en-us/azure/azure-monitor/app/usage-funnels)

由上可知幾個重點:
1. 我們必須先針對我們想要知道的問題、設計好Funnels Template，再讓Funnels取樣合適的資料、並顯示於漏斗圖上，例如:我們想知道過去90天內，使用者完成某個系統動作的數目(轉換率)。
2. 我們可以把我們完成的Funnels會盡Power BI中作為報表使用。
3. 我們可以Drill Down更細節的資料。
4. 統計週期為90天(依照我們的設計而不同)。
5. 我們可以篩選不同的步驟。

### User Flow
透過視覺化工具、了解User使用系統的模式。如下圖，我們可以先設定要以哪個Event為進入點，然後觀察User前一步、下一步的巡覽、點按按鈕動作等。
![](https://docs.microsoft.com/en-us/azure/azure-monitor/app/media/usage-flows/00001-flows.png)

節錄官方文件如下:
>1. How do users navigate away from a page on your site?
>2. What do users click on a page on your site?
>3. Where are the places that users churn most from your site?
>4. Are there places where users repeat the same action over and over?

[[MS Doc]](https://docs.microsoft.com/en-us/azure/azure-monitor/app/usage-flows)

由上可以，User Flow可以為我們解答幾種問題。
1. 使用者對系統的巡覽行為。
2. 使用者的Click行為(Interactive)。
3. 使用者最常互動的節點。
4. 那些地方使用者最常重複相同動作。

### Retention
使用者的存留期分析，這個`存留期是指使用者回來使用系統、或是執行特定工作及達成目標的頻率`。透過這個存留期分析，可以知道有多少使用者回來使用系統、那些動作的執行頻率高等，我們可以根據這個分析結果、調整以符合使用者的體驗及商業策略。

![](https://docs.microsoft.com/zh-tw/azure/azure-monitor/app/media/usage-retention/retention.png)

根據上圖，節錄官方文件說明如下:
>1. The toolbar allows users to create new retention reports, open existing retention reports, save current retention report or save as, revert changes made to saved reports, refresh data on the report, share report via email or direct link, and access the documentation page.
>2. By default, retention shows all users who did anything then came back and did anything else over a period. You can select different combination of events to narrow the focus on specific user activities.
>3. Add one or more filters on properties. For example, you can focus on users in a particular country or region. Click Update after setting the filters.
>4. The overall retention chart shows a summary of user retention across the selected time period.
>5. The grid shows the number of users retained according to the query builder in #2. Each row represents a cohort of users who performed any event in the time period shown. Each cell in the row shows how many of that cohort returned at least once in a later period. Some users may return in more than one period.
>6. The insights cards show top five initiating events, and top five returned events to give users a better understanding of their retention report.

[[MS Doc]](https://docs.microsoft.com/en-us/azure/azure-monitor/app/usage-retention)

### Impact
[影響]一詞在這裡表示我們想知道那些系統狀況、會造成使用者的那些行為，這些因 &rarr; 造成的果，就是這個工具的目的。
例如: 某個頁面載入時間很長、導致使用者不想等而離開、或者不同瀏覽器造成使用者停留期長短等。
![](https://docs.microsoft.com/zh-tw/azure/azure-monitor/app/media/usage-impact/0001-impact.png)

節錄官方文件說明如下:
>Impact analyzes how load times and other properties influence conversion rates for various parts of your app. To put it more precisely, it discovers how any dimension of a page view, custom event, or request affects the usage of a different page view or custom event.

[[MS Doc]](https://docs.microsoft.com/en-us/azure/azure-monitor/app/usage-impact)

## 結論:
AppInsights預設提供上述多種分析工具，可以幫助我們從茫茫如大海般的AppInsights Logs，找出有用的資訊，然後針對問題點加以調整，以提供系統體驗及達成商業目標。

## 參考
1. [Users, sessions, and events analysis in Application Insights](https://docs.microsoft.com/en-us/azure/azure-monitor/app/usage-segmentation)
2. [Discover how customers are using your application with Application Insights Funnels](https://docs.microsoft.com/en-us/azure/azure-monitor/app/usage-funnels)
3. [Analyze user navigation patterns with User Flows in Application Insights](https://docs.microsoft.com/en-us/azure/azure-monitor/app/usage-flows)
4. [User retention analysis for web applications with Application Insights](https://docs.microsoft.com/en-us/azure/azure-monitor/app/usage-retention)
5. [Impact analysis with Application Insights](https://docs.microsoft.com/en-us/azure/azure-monitor/app/usage-impact)