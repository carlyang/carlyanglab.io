---
title: Azure - Virtual Network Gateway Settings
categories:
 - 技術文件
 - Azure
date: 2021/6/4
updated: 2021/6/4
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, Virtual Network Gateway, Express Route]
---
Author: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Purpose
最近為了要架`Azure Express Route`的專線，將我們的雲端服務保護在私人網路內，必須使用`Virtual Network Gateway`服務協助雲端的路由加密傳送，它類似VPN的概念、幫助我們透過`Internet`的公開網路進行加密的安全傳輸，當我們想要將私人內網連接上`Azure Public Cloud`內的私有網路時，就可以搭配`Azure Express Route`將不同的網路串連起來。本文主要在說明如何在既有的`Azure Express Route`上啟用`Virtual Network Gateway`服務。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Azure Express Route
- Azure VM

## Introduction
就如前面所說的，`Virtual Network Gateway`服務可協助我們路由加密傳送、可串接兩個不同的網路，雖然`Azure Express Route`服務可以幫我們在雲上連接私人網路，但並不能幫我們處理地、雲兩端網路之間的路由傳遞，所以才需要`Virtual Network Gateway`來幫我們處理這件事，在這裡我們就先簡單`以Azure VM為示範對象，將地端私人網路與Azure VM串聯起來，這樣我們就可以在地端私人網路內使用RDP連接VM操作`。

## ISP線路
在進行設定之前，需要先確認ISP業者的線路正常可通、且連接上`Azure Express Route`並已設定完成，否則在Azure上不管如何設定，路都是不通的。

## Steps
1. 建立`Virtual Network Gateway`
請先在`Azure Portal`上找到`Virtual Network Gateway`服務並按下Create。
![](/Teamdoc/image/azure/VirtualNetworkGateway/step1.png)

2. 接著輸入名稱、選擇要建立在哪個Region、Gateway Type選擇Express Route
![](/Teamdoc/image/azure/VirtualNetworkGateway/step2.png)

3. 如果沒有已設定好的`Virtual Network`可選擇，可以按下`Create virtual network`開啟設定視窗。
![](/Teamdoc/image/azure/VirtualNetworkGateway/step3.png)

4. 設定`Virtual Network`(如果已有先建好的`Virtual Network`，就可以不用新建、直接選用即可)
   - 請輸入名稱
   - Resource Group會預先幫你選好，如果要建在其他的Resource Group可以自行改選。
   - Address Range會先幫你預設好一組。
    Note:  
    `由於我們現在要連接地端的私人網路與Azure，所以務必要確認好這邊切出來的網段是可以通的，否則就算這邊設定成功、地端路由出不來也沒用`，所以請先與地端網管溝通好可用的路由，否則有可能會有衝到IP、或是路由走錯之類的問題。
![](/Teamdoc/image/azure/VirtualNetworkGateway/step4.png)

   - Subnet是根據上面的Address Spaces切出其他的子網段，像我們現在需要連接`Express Route`以及`Azure VM`，所以我們就會需要兩個子網段，一個給`Express Route`用、另一個給`Azure VM`用，所以要多加一組`10.1.0.16/28`子網段，稍後在`Azure VM`設定`Virtual Network`時就可以選用。
   - 請注意，Gateway使用的Subnet名稱固定為`GatewaySubnet`，我們可以直接改掉default這個名稱，讓後面自動選用。
![](/Teamdoc/image/azure/VirtualNetworkGateway/step5.png)

5. 設定好`Virtual Network`後就會回到原本的畫面，`請注意，如果Subnet有指定GatewaySubnet這個名稱，回來主畫面後就不會再要求輸入子網段給Gateway用`，如下圖第一個紅框。
   - 接著就指定Public IP，看是要建新的或是使用現有的都可以，如下圖第二個紅框。
![](/Teamdoc/image/azure/VirtualNetworkGateway/step6.png)

6. 最後就是Create `Virtual Network Gateway`即可。

7. 建完`Virtual Network Gateway`後就是要設定Gateway內的Connection，這個動作就是要將Connection與`Express Route`串接起來。
![](/Teamdoc/image/azure/VirtualNetworkGateway/step7.png)

   - 輸入名稱、`Virtual Network Gateway`會自動幫你選好、最後再選擇要使用的`Express Route`，最後按下OK按鈕就可開始建立。

    Note:  
    這個建立連線的動作可能需要一點時間，請耐心等候。
![](/Teamdoc/image/azure/VirtualNetworkGateway/step8.png)

8. 最後，就是要把`Azure VM`指定使用剛才建立`Virtual Network Gateway`時、同時也建好的`Virtual Network`，要注意Subnet記得使用前面步驟設定好的子網段。

    Note:  
    `Azure VM`的`Virtual Network`的設定，主要就是看是新增VM時可以順便指定(如下圖)，或是現有VM再去更改`Virtual Network`的設定，但這邊離題太遠就不再贅述，簡單示意一下新建VM時的設定畫面。
    ![](/Teamdoc/image/azure/VirtualNetworkGateway/step9.png)

9. 全部建立完成後，就可以試著在內網機器、透過從VM下載的`Private IP` RDP檔進行遠端連線。
![](/Teamdoc/image/azure/VirtualNetworkGateway/step10.png)

## 結論:
透過`Express Route`及`Virtual Network Gateway`兩者，就可以在受保護的私有網路內、由地端存取雲端上的Azure服務。基本上，大部分的Azure都有指定`Virtual Network`的功能，所以我們`可以在Public Cloud上建立具有安全性的私有網路，讓只有從公司內網來的Request存取我們的雲端服務`，其他直接從Internet來的存取動作都被阻擋在外，可以更加保護我們的服務。

## 參考
1. [[What is Azure ExpressRoute?]](https://docs.microsoft.com/en-us/azure/expressroute/expressroute-introduction)
2. [[What is a virtual network gateway?]](https://docs.microsoft.com/en-us/azure/vpn-gateway/vpn-gateway-about-vpngateways#whatis)