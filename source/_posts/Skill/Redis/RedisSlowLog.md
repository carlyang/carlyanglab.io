---
title: Redis - Slow Log
categories:
 - 技術文件
 - Redis
date: 2018/9/8
updated: 2018/9/8
thumbnail: /Teamdoc/image/Redis/RedisTitle1.png
layout: post
tags: 
 - SlowLog
---
作者: Carl Yang

## Redis
![](/Teamdoc/image/Redis/RedisTitle1.png)
<center>([_圖片來源_](https://msdnshared.blob.core.windows.net/media/MSDNBlogsFS/prod.evol.blogs.msdn.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/01/43/96/140720/1031.redis.png))</center>
Redis是memory cache很常用的方式，具有可靠度、快速等優點，也是一種NoSQL的資料庫，很常利用來暫存Server的一些資料，例如使用者登入資訊等等,同時Redis也是一個遵守BSD License開放原始碼的套件。

>Redis is an open source (BSD licensed), in-memory data structure store, used as a database, cache and message broker. It supports data structures such as strings, hashes, lists, sets, sorted sets with range queries, bitmaps, hyperloglogs and geospatial indexes with radius queries. Redis has built-in replication, Lua scripting, LRU eviction, transactions and different levels of on-disk persistence, and provides high availability via Redis Sentinel and automatic partitioning with Redis Cluster.([_Redis_](https://redis.io/topics/introduction))

而Redis是一個鍵值配對(Key-value pairs)的資料結構，使用上簡單易懂、快速上手，也有很多第三方元件支援Redis的存取，內容包括一般的string存取、List類似Queue/stack等等的機制等等，提供非常多現成好用的工具，開發者可依照需求選用適合的功能。

本篇主要在描述Redis中Slow Log的設定方法，方便開發者進行除錯時可查詢Redis Server紀錄，以快速判斷問題點。
<!-- more -->

## Redis Desktop Manager
Redis Desktop Manager是一套方便好用的Redis管理工具，可以至([_RDM_](https://redisdesktop.com/download))下載所需的版本，本文使用的版本為0.9.3.817。
![](/Teamdoc/image/Redis/RedisDesktopManager.png)

## 開始設定及查詢
1. 先打開RDM->連接Redis Server->左邊清單點選Redis Server的Console圖示
![](/Teamdoc/image/Redis/RDM1.png)

2. 輸入下列指令先看看目前Slow Log設定。
```powershell
config get slowlog-log-slower-than
```
![](/Teamdoc/image/Redis/RDM2.png)

預設設定是10000微秒(microseconds)、也就是10毫秒(miliseconds)，<font style="color:red;">**slowlog-log-slower-than參數表示要記錄的slowlog是耗時多久以上的才寫入log**</font>。

3. 輸入下列指令更改設定為500ms。
```
config set slowlog-log-slower-than 5000000
```
設定完成顯示OK。
![](/Teamdoc/image/Redis/RDM3.png)
再查一次已更改設定。
![](/Teamdoc/image/Redis/RDM4.png)

4. 由於Redis有限制寫入log的筆數上限、預設是128筆，故我們還需要加大log筆數上限，以方便查閱，先用下列指令查詢預設的上限值。
```
config get slowlog-max-len
```
![](/Teamdoc/image/Redis/RDM5.png)
預設是只有128筆，如果要做為log查詢的話，最好多加一點比較方便日後查詢，故我們設定為1000筆(建議不要設定太大、避免耗用太多資源)。
```
config set slowlog-max-len 1000
```
![](/Teamdoc/image/Redis/RDM6.png)
再查一次已更改設定。
![](/Teamdoc/image/Redis/RDM7.png)

5. 最後再用下列指令查詢目前所有的slowlog內容。
```
slowlog get
```

6. 也可以指定要查目前最新n筆的slowlog。
```
slowlog get 1000
```

7. 要查看目前slowlog數量，可使用下列指令。
```
slowlog len
```

## 其他
Redis還有很多的命令可以使用，可以參考Redis Labs的文件。([_Redis Documents_](https://redis.io/documentation))，也可以直接查閱Redis commands([_Redis Commands_](https://redis.io/commands))。