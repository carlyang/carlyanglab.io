---
title: Azure - Mock Response of API in APIM
categories:
 - 技術文件
 - Azure
date: 2021/11/15
updated: 2021/11/15
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management, Mock Response]
---
Author: Carl Yang

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
很多時候`APIM`可以很快設計好、但`Backend API`還需要時間開發，但為了讓呼叫端AP可以先行測試正常狀態下的回應(例如: Response Body)，我們就會使用`Mock Response`來處理暫時性的模擬回應，本文主要在說明如何使用`Mock Response`來暫時性地回應並定義(`Definition`)回應主體的`Template`，讓呼叫端可以先拿到正常的資料格式，不用受限於`Backend API`的開發時程。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- API Management

## Introduction
當我們撰寫`Web API`給外部呼叫的`Functions`時，常會有需要使用`Mock`模擬正常回應的時候，這個`Mock`至少包含兩個部分，`Request Format`及`Reaponse ContentType & Body`，一般因為是模擬性質、`Request Format`可以先暫時不用理會`Request Body`長得如何，但`Reaponse ContentType & Body`會希望至少依照未來可參考的格式回傳，這時候我們就可以使用`APIM`中的`Mock Response`來幫助我們處理。

## Response Status
首先我們先在`Operation`設定預設的`Response`，如下圖設定回傳200。

![](/Teamdoc/image/azure/APIMMockResponse/response1.png)

Save之後我們就可以看到這個`Operation`的模擬回應200 HTTP Status。

![](/Teamdoc/image/azure/APIMMockResponse/response2.png)

Note:  
`增加這個設定除了指定模擬回應狀態外，後面還有更重要的目的，讓我們繼續看下去!`

## Mock Response
接著我們在`Inbound`中加入`Mock Response`這個Policy，並且使用預設值即可。

![](/Teamdoc/image/azure/APIMMockResponse/mock1.png)

`Mock Response`的回應值也可以選擇其他項目。
![](/Teamdoc/image/azure/APIMMockResponse/mock2.png)

接著查看我們的`XML Policies`文件就會出現以下標記。
```xml
<policies>
    <inbound>
        <base />
        <mock-response status-code="200" content-type="application/json" />
    </inbound>
    <backend>
        <base />
    </backend>
    <outbound>
        <base />
    </outbound>
    <on-error>
        <base />
    </on-error>
</policies>
```

最後，我們用`Postman`打打看，就會發現拿到`200 OK`的`Response`了。
![](/Teamdoc/image/azure/APIMMockResponse/mock3.png)

## Definitions
還記得前面我們提過`Reaponse ContentType & Body`會希望至少依照未來可參考的格式回傳嗎?!但現在使用`Mock Response`並沒有回傳任何`Body`，也就是這個模擬並沒有回應相對應的資料格式，這時候就必須回到`Operation`內定義回應的`Template`，如下圖。

![](/Teamdoc/image/azure/APIMMockResponse/definition1.png)

再來輸入名稱及`JSON Sample`，就會自動幫我們產生樣板，最後Create即可。

![](/Teamdoc/image/azure/APIMMockResponse/definition2.png)

為了能夠在模擬回應中使用我們新建的`Definition`，我們還需要設定一下，還記得我前面說先在`Operation`設定預設的`Response`嗎?!就是為了在這裡能使用自訂的`Definition`，讓我們來進入剛才設定的`Response`。

![](/Teamdoc/image/azure/APIMMockResponse/definition3.png)

接著新增`Representation`、`Content Type`選擇`application/json`、`Definition`選擇剛新增的`MockTestDefinition1`。

![](/Teamdoc/image/azure/APIMMockResponse/definition4.png)

最後，再用`Postman`試打看看，就會拿到模擬回傳的資料格式了。

![](/Teamdoc/image/azure/APIMMockResponse/definition5.png)

Note:  
其實不是非要用`Definition`的方式建立模擬回應，我們也可以直接在`Operation`設定預設的`Response`的`Sample`欄位輸入我們的`JSON`資料，但是`Definition`具有重用(`re-use`)的特性，`我們可以不必到處輸入同樣的Sample、而是建立一個Definition、用在多個Operations中`。

## 結論:
`本文最主要的目的，是在說明如何將Definition應用在Mock Response中`，當然在`APIM`中其他只要是可以指定Definition的地方都可以使用，所以`Definition`的重用(`re-use`)特性是很方便管理及維護的，但參考[[MS Doc]](https://docs.microsoft.com/en-us/azure/api-management/mock-api-responses?tabs=azure-portal)似乎只有說明直接輸入`Sample`欄位的方式，`Definition`的部分卻未提到，故在這裡分享給大家參考看看。

## 參考
1. [[Tutorial: Mock API responses]](https://docs.microsoft.com/en-us/azure/api-management/mock-api-responses?tabs=azure-portal)