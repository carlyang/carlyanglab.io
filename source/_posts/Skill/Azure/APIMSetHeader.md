---
title: Azure - 如何透過API Management調整標頭
categories:
 - 技術文件
 - Azure
date: 2022/6/3
updated: 2022/6/3
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management]
---

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
暨[[上一篇]](/2022/06/01/Skill/Azure/APIMFindAndReplace/)說明如何隱藏Body資訊後，又發現在標頭的部分有些資訊也是不應該有的，但有些情境也可能需要自訂加入一些標頭，所以本文目的就在說明`如何處理經過APIM的HTTP Request/Response標頭資訊`。
<!-- more -->

## set-header
標頭設定在`APIM`中可說是非常重要的事情，雖然預設`HTTP Request`帶什麼標頭、就會傳給後端服務什麼標頭，但是除了這些之外、我們也可能有些其他的標頭可以在`APIM`處理，可能是移除非必要資訊、加入驗證Toekn等。本文主要是發現，有些後端服務可能會因為標頭而洩漏了本身的URL或是開發版本號，這些資訊如果被有心人取得可能會造成其他的問題，引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/api-management/set-header-policy)如下。

>The set-header policy assigns a value to an existing HTTP response and/or request header or adds a new response and/or request header.  
Use the policy to insert a list of HTTP headers into an HTTP message. When placed in an inbound pipeline, this policy sets the HTTP headers for the request being passed to the target service. When placed in an outbound pipeline, this policy sets the HTTP headers for the response being sent to the gateway’s client.

由上說明可知，`set-header`原則可以使用在`HTTP Request/Response`，我們可以利用這個原則複寫、忽略、附加、刪除這些標頭，範例如下。

```xml
<set-header name="header name" exists-action="override | skip | append | delete">
    <value>value</value>
</set-header>
```

說明:
- `name`: 要處理的標頭名稱。
- `exists-action`: 要進行的動作，`override`是取代現有標頭的值、`skip`不取代現有的標頭值、`append`是將值附加至現有的標頭值、`delete`是移除標頭。
- `value`: 要寫入的值。

舉例來說，有些資訊如`Server`、`X-AspNet-Version`、`X-Powered-By`等，可能洩漏伺服器版本、程式開發版本、發行者等資訊，但`這些資訊如果洩漏出去等於直接告訴駭客你可以從哪個領域尋找漏洞!`所以我們就可以利用`set-header`原則來移除這些標頭。

```xml
<set-header name="Server" exists-action="delete"></set-header>
<set-header name="X-AspNet-Version" exists-action="delete"></set-header>
<set-header name="X-Powered-By" exists-action="delete"></set-header>
```

## Conclusion
有些資訊其實都可以在系統的`Configuration`中設定排除、以避免資訊外洩，但事實上很多時候這些動作都會被開發人員忽略，缺乏安全性而造成的損害不發生則已、一發生就可能很嚴重，安全性的問題多提防一些總是有好無壞!所以在使用`APIM`後就可以針對這些部分統一做些處理及預防，是非常有用的方式。

## References
1. [Set header](https://learn.microsoft.com/en-us/azure/api-management/set-header-policy)