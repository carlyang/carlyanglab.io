---
title: Azure - Usage Quota in APIM
categories:
 - 技術文件
 - Azure
date: 2021/10/18
updated: 2021/10/18
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management, Usage Quota]
---
Author: Carl Yang

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
[[上一篇]](/2021/10/10/Skill/Azure/APIMRateLimit/)介紹的幾種`Policy`都是以限制`呼叫頻率`為目的，但是如果我們希望限制`使用量`呢?本篇就來介紹如何針對`Subscription`及`Key`設定他們的可使用量。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- API Management

## Introduction
`Limit call rate by subscription`及`Limit call rate by key`兩種目的都是限制`呼叫頻率`，但有些情境我們可能無法判斷它頻率會如何，尖峰時很難去估算它頻率會多高，這時候我們就可以考慮改用`使用量`的限制、去限制`Subscription`及`Key`的使用額度。引用[[MS Doc]](https://docs.microsoft.com/en-us/azure/api-management/api-management-sample-flexible-throttling#rate-limits-and-quotas)的說明，`Rate Limit`及`Usage Quota`的差異如下，所以`Rate Limit`用於短期內的頻率、`Usage Quota`則是用於一段時間內的使用額度。

>- Rate limits  
Rate limits are usually used to protect against short and intense volume bursts.

>- Quotas  
Quotas are usually used for controlling call rates over a longer period of time.

> Within Azure API Management, rate limits are typically propagated faster across the nodes to protect against spikes. In contrast, usage quota information is used over a longer term and hence its implementation is different.

## Set usage quota by subscription
顧名思義，就是針對`subscription`設定使用額度，一但超過額度就不再讓它存取`APIM`。  
不過要特別注意一下，與`Limit call rate by subscription`類似，這個`Policy`在整份設定內`只能出現一次`。

> This policy can be used only once per policy document.

我們可以在設計工具中直接編輯`Inbound`的`XML`設定並加入下列標記。`<quota>`就是設定`subscription`的使用額度。
```xml
<policies>
    <inbound>
        <base />
        <set-backend-service base-url="https://xxxxxxxx.azurewebsites.net" />
        <rewrite-uri template="/api/login" />
        <quota calls="10000" bandwidth="40000" renewal-period="3600" />
    </inbound>
    <backend>
        <base />
    </backend>
    <outbound>
        <base />
    </outbound>
    <on-error>
        <base />
    </on-error>
</policies>
```

Note:  
`目前為止還沒有看到可以用視覺化設計工具設定Subscription的Usage Quota`，所以必須進入`Editor`直接撰寫`XML`標記。
- `calls`: 指的是允許的呼叫次數。
- `bandwidth`: 是指`資料量`、單位是`KB`。
- `renewal-period`: 指的是重新累計的時間(秒數)
所以，以口語化來說明上例、就是指定每個訂閱在3600秒的期間內、可以在最大頻寬40000 KB或10000次內呼叫API。

完整的可用`XML`標記範例如下:  
```xml
<quota calls="number" bandwidth="kilobytes" renewal-period="seconds">
    <api name="API name" id="API id" calls="number">
        <operation name="operation name" id="operation id" calls="number" />
    </api>
</quota>
```

Note:  
`<api>`標記是指定這個`Policy`使用在各個API的額度、但全部加總不可超過整個`Quota`的額度。
- `name`: API名稱。
- `id`: API識別Key。
- `calls`: 呼叫次數。
- `operation`: 對應API內的不同操作(`name`、`id`、`calls`都一樣意思)。

## Set usage quota by key
這裡的`Key`也是指我們設定的某個統計值、類似`Limit call rate by key`的`Key`概念，例如:設定某個IP的使用額度。  

方法1: 我們可以透過設計工具直接在`Inbound`中加上`Rate Limit Policy`。  
![](/Teamdoc/image/azure/APIMUsageQuota/usage_quota_key1.png)
![](/Teamdoc/image/azure/APIMUsageQuota/usage_quota_key2.png)
- `Limited by`: 可以選擇呼叫次數或頻寬。
- `renewal-period`: 指的是重新累計的時間(秒數)
- `Counter key`: 這邊指定套用某個IP。
- `Increment condition`: 要用來計算累計的條件，這邊我們指定`Any request`所有的都算。

方法2: 我們可以在設計工具中直接編輯`Inbound`的`XML`設定，加入下列標記。
- `calls`: 指的是允許的呼叫次數。
- `renewal-period`: 指的是重新累計的時間(秒數)。
- `counter-key`: 是我們要用來計算的統計值。
- `increment-condition`: 因為這邊我們指定`Any request`，所以就沒有了，也可以自訂計算條件。

```xml
<policies>
    <inbound>
        <base />
        <set-backend-service base-url="https://xxxxxxxx.azurewebsites.net" />
        <rewrite-uri template="/api/login" />
        <quota-by-key calls="1000" renewal-period="3600" counter-key="@(context.Request.IpAddress)" />
    </inbound>
    <backend>
        <base />
    </backend>
    <outbound>
        <base />
    </outbound>
    <on-error>
        <base />
    </on-error>
</policies>
```

Note:  
這個功能也是不能使用在`Consumption tier`喔!因為`Consumption tier`基本上是拿來開發階段使用的，`APIM`必須開在其他的層級才能使用這個功能。
> This feature is unavailable in the Consumption tier of API Management.

完整的可用`XML`標記範例如下:  
```xml
<quota-by-key calls="number"
              bandwidth="kilobytes"
              renewal-period="seconds"
              increment-condition="condition"
              counter-key="key value"
              first-period-start="date-time" />
```

## 結論:
到此為止，我們主要介紹了兩種控管的方向，`控制呼叫次數`及`控制使用額度`，基本上`APIM`這兩大方向應該能滿足一般性的需求了，不過實務上依然還是要看當時的情境到底如何，才能判斷要採用何種方式。

## 參考
1. [[API Management access restriction policies]](https://docs.microsoft.com/en-us/azure/api-management/api-management-access-restriction-policies)
2. [[Rate limits and quotas]](https://docs.microsoft.com/en-us/azure/api-management/api-management-sample-flexible-throttling#rate-limits-and-quotas)