---
title: Azure - Virtual Machine Inbound/Outbound Rules
categories:
 - 技術文件
 - Azure
date: 2021/6/5
updated: 2021/6/5
thumbnail: https://i.imgur.com/lCrEvb3.png
layout: post
tags: [Azure, Virtual Machine, Inbound/Outbound Rules]
---
Author: Carl Yang

![](/Teamdoc/image/azure/logos/vm.png)

## Purpose
`Azure VM`是非常方便的雲端服務、而且也是`Azure IaaS`很重要的基礎建設，一般會使用到雲端服務的系統，基本上都跟它脫不了關係。我們可以隨時自建一台VM使用、有些服務本身也是以VM為基礎在運作的，所以`Azure VM`的操作非常重要，尤其是當我們要從外界對它存取時，要怎麼設定存取規則、才能夠具有安全性又不影響正常的操作很重要，本文主要在說明如何透過`Azure VM`的`Inbound/Outbound Port Rules`設定，讓我們可以正常地操作它。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Azure VM

## Introduction
`Azure VM`作為`Azure IaaS`的基礎建設，支援各式各樣的作業系統，舉凡Windows到Linux based的系統都有，但進入雲端的世界不得不考量安全性的問題，所以我們也不能讓所有的人都可以隨便進入操作，這時候就要使用到`Inbound/Outbound Port Rules`設定，來幫助我們設定Port的存取規則。

## Inbound/Outbound Port Rules
`Azure VM`的`Inbound/Outbound Port Rules`設定其實很簡單，透過`Azure Portal`操作就可以了，它其實可以視為一種防火牆，哪些Source允進來、哪些Source不允許，哪些Port可以開放、哪些Port不可以開放，都可以透過這個功能設定。  
如下圖所示，進入左方選單的Networking選項 -> 選擇IP Configuration項目 -> Inbound/Outbound port rules，即可看到目前設定的規則。
![](/Teamdoc/image/azure/VirtualMachine/vm1.png)

如果要新增新的規則，就可以按右方的Add按鈕新增，如下圖。
![](/Teamdoc/image/azure/VirtualMachine/vm2.png)

## Adding Rules
1. 新增規則時我們可以指定Source是從哪裡來，這裡就是前面提到的限制存取來源功能。
   - `IP Address`: 指定要套用規則的來源IP。
   - `Service Tag`: Azure服務其實有個Tag功能非常好用，透過Tag的設定我們也可以在這裡指定哪個Tag的服務要套用此規則。
   - `Applicatioin Security Group`: 就是另一種安全性原則群組，我們也可以指定某個群組要套用此規則。

![](/Teamdoc/image/azure/VirtualMachine/vm3.png)

2. 我們也可以指定Source的Port號碼，預設值*表示允許全部。

![](/Teamdoc/image/azure/VirtualMachine/vm4.png)

3. `Destination`就是設定目標，也就是Request的Target，同樣有下列三種設定方式。
   - `IP Address`: 指定要套用規則的來源IP。
   - `Service Tag`: Azure服務其實有個Tag功能非常好用，透過Tag的設定我們也可以在這裡指定哪個Tag的服務要套用此規則。
   - `Applicatioin Security Group`: 就是另一種安全性原則群組，我們也可以指定某個群組要套用此規則。

![](/Teamdoc/image/azure/VirtualMachine/vm5.png)

4. 要特別提到的是`Destination port ranges`，它可以設定此規則要套用的目標Port號碼範圍，引用說明如下:
>Provide a single port, such as 80; a port range, such as 1024-65535; or a comma-separated list of single ports and/or port ranges, such as 80,1024-65535. This specifies on which ports traffic will be allowed or denied by this rule. Provide an asterisk (*) to allow traffic on any port.

![](/Teamdoc/image/azure/VirtualMachine/vm6.png)

5. `Protocol & Action`就是指定要套用此規則的通訊協定、以及此規則的動作是要允許還是拒絕。

![](/Teamdoc/image/azure/VirtualMachine/vm7.png)

6. `Priority & Name & descriptioin`這邊主要是設定優先順序，也就是說當一個Request進來、要套用這些規則時的檢查順序，`數字越小優先順序越高、檢查規則的順序也越前面`。至於Name就不要重複即可、Description只是一點說明，讓進來看到的人容易了解設定此規則的目的。

![](/Teamdoc/image/azure/VirtualMachine/vm8.png)

Note:  
`Outbound port rules`的設定方式幾乎都跟Inbound的設定方式相同，所以就先略過不提。

## Private & Public IP
`Azure VM`的IP基本有兩種，就是透過私人IP或是公開的IP Addresses，`Private IP`是使用另外架設的`Virtual Network`套用才會有這種IP，`Public IP`則是`Azure VM`在建立的時候就會自動給予一個公開的IP，讓外界可以透過Internet經由這個IP存取`Azure VM`。

Note:  
特別說明`Private & Public IP`的目的，主要是讓讀者知道`Azure VM`也是提供私人或公開的兩種存取方式，而在設定規則的時候可能就要注意一下。

## 規則說明
根據下圖的幾條規則，來簡要說明一下各規則的用途。

![](/Teamdoc/image/azure/VirtualMachine/vm9.png)

- `RDP`: 這條規則就是我們在建立`Azure VM`之初、開啟遠端功能時所指定的Port，當然這邊是使用預設Port 3389。
- `iPerf_5201`: 就是我們自訂要允許通過的5201 port，因為我們有需求要使用iPerf工具對這台`Azure VM`測試網速，而且是經由Public IP進來存取`Azure VM`，所以必須建立這條規則讓測試通過。
- `AllowVnetInBound`: 這條是`Azure VM`建立時就預設好的規則，我們可以看到右方Action是Allow，也就是說`Azure VM`其實預設對於走Private IP、套用`Virtual Network`進來的存取都是允許的。
- `AllowAzureLoadBalancerInBound`: 這條也是預設就建好的規則，因為`Azure VM`有Load Balancer的機制，所以必須要有這條規則才能運作。
- `DenyAllInBound`: 這條當然理所當然地必須要預設就建立好的，因為上面Action通通都是Allow、這條當按要Deny啦，不然等於所有存取都允許那還得了!

## 除了`Inbound/Outbound Port Rules`之外
這邊要特別提一下，當我們在建立`iPerf_5201`這條規則時發現，`透過Private IP進來存取其他Port竟然是無法通過的!`
怪了，我前面不是說預設透過`Virtual Network`(Private IP)都是允許的嗎?  

大家不要誤會了，`我前面說的是這邊設定的規則，別忘了Windows還有Defender阿!`它也是會擋存取的，所以如果還是遇到有Port無法存取的話，建議可以另外檢查一下`Azure VM`內的`Windows Defender`的設定喔!

![](/Teamdoc/image/azure/VirtualMachine/vm10.png)

## 結論:
這篇文章主要是在說明`Azure VM`的`Inbound/Outbound Port Rules`設定，其實還有像是不同的IP Configuration也可以設定不同規則、`Application Security Group`及`Load Balancer`等功能，受限篇幅就先不贅述了，基本上，透過`Inbound/Outbound Port Rules`設定已經可以滿足大部分的需求囉。

## 參考
1. N/A