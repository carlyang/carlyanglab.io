---
title: Azure - 如何在Azure Data Factory中呼叫RESTful API
categories:
 - 技術文件
 - Azure
date: 2022/7/7
updated: 2022/7/7
thumbnail: https://i.imgur.com/oHcuJIO.png
layout: post
tags: [Azure, Azure Data Factory]
---

![](/Teamdoc/image/azure/logos/adf.png)

## Purpose
在使用`Azure Data Factory`(下稱`ADF`)過程中，也有可能是要將大量資料送到某個`RESTful API`再做進一步的處理，例如: `Funciton App`、`App Services`等服務都是遵循`RESTful API`格式的服務介面，本文目的就在介紹如何使用`ADF`調用`RESTful API`。
<!-- more -->

## Web Activity
本文是使用`Web` Activity作為呼叫`RESTful API`的操作，引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/data-factory/control-flow-web-activity)如下。

>Web Activity can be used to call a custom REST endpoint from an Azure Data Factory or Synapse pipeline. You can pass datasets and linked services to be consumed and accessed by the activity.

也就是說，我們可以透過各種方式從`datasets`或`linked services`讀取資料後，將資料送到我們另外自訂的`RESTful API`服務，只要這API遵循`RESTful`規格即可，大家可能會疑惑這有什麼優點呢?其實優點可多了。

先從資料面來看，我們可能只是先用`ADF`讀取資料內容，但是一階資料可能尚未經過整理或是未經過另外的商業邏輯運算、還無法提供給分析之用，這時一定需要另一個可以協助商業邏輯的服務來協助處理、再將資料送進分析。

再從服務面來看，透過`Web` Activity把資料送給`RESTful API`來處理，可以說是最被廣泛應用的方式，`Azure`上不論`Function App`或是`App Service`都支援，我們可以專心在撰寫客製化的資料處理邏輯就好。

## Limitation
不過，`Web` Activity還是有少許限制，如下說明。

1. Network Access
>Web Activity is supported for invoking URLs that are hosted in a private virtual network as well by leveraging self-hosted integration runtime. The integration runtime should have a line of sight to the URL endpoint.

也就是說，`Web` Activity也可以調用掛載了`virtual network`的服務，不過必須要先為`ADF`裝載`self-hosted integration runtime`、以在虛擬私有網路上跟掛載`virtual network`的服務溝通，詳細的`self-hosted integration runtime`內容可參考[[MS Doc]](https://learn.microsoft.com/en-us/azure/data-factory/create-self-hosted-integration-runtime?tabs=data-factory)，本文就不再多加描述。

2. Response Size
>The maximum supported output response payload size is 4 MB.

`ADF`只能接受`RESTful API`回應4 MB以內大小的內容。

3. Response Restriction
>REST endpoints that the web activity invokes must return a response of type JSON. The activity will timeout at 1 minute with an error if it does not receive a response from the endpoint. For endpoints that support Asynchronous Request-Reply pattern, the web activity will continue to wait without timeing out (upto 7 day) or till the endpoints signals completion of the job.

這邊要特別注意一下，依照上述微軟的說明，`ADF`只能接受`JSON`格式的回應、而且如果1分鐘內未獲得回應就會`time out`。如果是採用非同步的方式，`ADF`會一直等待最多7天才結束。

4. Authentication
>If your data factory or Synapse workspace is configured with a git repository, you must store your credentials in Azure Key Vault to use basic or client certificate authentication. The service does not store passwords in git.

`Web` Activity調用`RESTful API`也是有可能需要身分驗證的，這要看`RESTful API`是否有這限制，而`Web` Activity則支援基本的使用者帳密、憑證或`AAD`的`Managed Identity`等驗證方式。所以，如果我們的`ADF`有使用`GIT`做版控的話，我們必須把使用者帳密或憑證存放在另一個`Key-Vault`服務中，然後自行取來使用、`不能直接將機密資訊放進GIT喔!`

## Azure Data Factory Studio
接著我們就來設定`Web` Activity吧!

設定其實非常簡單，如下圖。

![](/Teamdoc/image/azure/ADFCallRESTAPI/web_activity1.png)

說明:
- `URL`: 這邊就是你要調用的API位址。
- `Method`: 這是`HTTP Method`。
- `Body`: 這邊支援`JSON`格式的主體內容。
- `Authentication`: 這邊的驗證就要搭配自定的驗證方式，不同的驗證方式需要不同的資訊，請自行輸入。
- `Headers`: 這邊則是可以輸入要帶入的標頭，例如:`APIM`需要的`Subscription Key`等。

## Conclusion
本文主要在說明`Web` Activity的基礎使用方式，如果有需要可能要另外搭配其他的Activity來處理，比方說你要先從`Blob`讀取資料內容、接著才把資料透過`Web` Activity送出等等，這些更複雜的使用情境就必須因地制宜、考量不同的條件做不同的`Pipeline`設計了。

## References
1. [Web activity in Azure Data Factory and Azure Synapse Analytics](https://learn.microsoft.com/en-us/azure/data-factory/control-flow-web-activity)
2. [Create and configure a self-hosted integration runtime](https://learn.microsoft.com/en-us/azure/data-factory/create-self-hosted-integration-runtime?tabs=data-factory)