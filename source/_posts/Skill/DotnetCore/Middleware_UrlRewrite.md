---
title: .NET Core - URL Rewrite
categories:
 - 技術文件
 - .NET Core
date: 2019/01/10
updated: 2019/01/10
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [.NET Core, URL Rewrite]
---
作者: Carl Yang

![](/Teamdoc/image/DotnetCore/Logo.png)

## 什麼是URL Rewriting?
URL重寫是根據一些預先定義好的位址、在Client端發請求至Server時，根據相應的規則導至另一個位址，這樣的機制可以降低Client對於位址的相依性。也就是說，Client端不需要更動位址就可以請求其他位址上的資源，常見的應用如Error Page、存取另一個Web站台、系統暫時性維護網站等情境，根據[微軟文件](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/url-rewriting?view=aspnetcore-2.2)說明節錄如下。
>URL rewriting is the act of modifying request URLs based on one or more predefined rules. URL rewriting creates an abstraction between resource locations and their addresses so that the locations and addresses aren't tightly linked. URL rewriting is valuable in several scenarios to:
>- Move or replace server resources temporarily or permanently and maintain stable locators for those resources.
>- Split request processing across different apps or across areas of one app.
>- Remove, add, or reorganize URL segments on incoming requests.
>- Optimize public URLs for Search Engine Optimization (SEO).
>- Permit the use of friendly public URLs to help visitors predict the content returned by requesting a resource.
>- Redirect insecure requests to secure endpoints.
>- Prevent hotlinking, where an external site uses a hosted static asset on another site by linking the asset into its own content.

URL重寫跟路由概念上很相似，但其實背後的機制是完全不一樣的。路由是根據Client請求的位置、給予該位址上的服務或資源存取，但URL重寫則是轉送請求、也就是將請求轉發至其它位址上的資源，本身並不提供服務或資源。而URL重寫又可區分為重寫(Rewrite)及重導(Redirect)，本文主要在探討.NET Core如何使用Mddleware在Server端收到請求時重寫至另一個位址上的資源。
<!-- more -->

## Prerequisites
本文是使用.NET Core 2.2作為開發環境進行練習，請先安裝下列項目。
- 請先安裝.NET Core 2.2 SDK([Download](https://dotnet.microsoft.com/download))

## 套件
.NET Core是使用Microsoft.AspNetCore.All中的Microsoft.AspNetCore.Rewrite套件，故安裝.NET Core 2.2時就已包含在內。

## 註冊URL Rewrite Middleware
URL重寫是在Startup.cs中的Configure()中註冊使用，註冊時必須給予RewriteOptions並指定規則。最主要有兩種方式，第一種是使用AddRewrite()、第二種是使用AddRedirect()，以下分別為兩種註冊方式。

- URL Rewrite註冊
```csharp
app.UseRewriter(
	new RewriteOptions()
		.AddRewrite(@"First", @"/Home/First", true)
	);
```
	1.參數1(regex): 
	是<font style="color:red;">正則表達式</font>，可直接傳入要判斷的位址或是正則表達式。

	2.參數2(replacement): 
	傳入要重寫的位址。

	3.參數3(skipRemainingRules): 
	表示如果符合此規則、是否要繼續判斷其餘規則。<font style="color:red;">建議是設為true排除其餘的規則判斷，通常判斷符合規則時也應該不用再往下判斷了，這樣可以增加效率、排除不必要的動作。</font>
	
	4.UseRewrite流程如下圖，當Client端發Request給Server後、Rewriter會判斷要重寫的哪個位址並轉送後，取得Response的內容回傳給Client端。

![](/Teamdoc/image/DotnetCore/url_rewrite/urlrewrite4.png)

- URL Redirect註冊
```csharp
app.UseRewriter(
	new RewriteOptions()
		.AddRedirect(@"Second", @"/Error/Index", 302)
	);
```
	1.參數1(regex): 
	是<font style="color:red;">正則表達式</font>，可直接傳入要判斷的位址或是正則表達式。

	2.參數2(replacement): 
	傳入要重寫的位址。

	3.參數3(statusCode): 
	指定符合此規則時要回傳的HTTP Status Code。

	HTTP Status Coded可參考[Wiki](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#3xx_Redirection):
		- 301: 表示該網址已被永久導向目標位址，由於是永久性導向、瀏覽器會為此位址做快取以加快處理速度。
		- 302: 表示該網址已被暫時性地導向目標位址，由於是暫時性導向、瀏覽器不會為此位址進行快取。

	4.UseRedirect流程如下圖，當Client端發Request給Server後、Rewriter會判斷要重導的位址後，直接以3XX HTTP Status Code將位址回傳給Client端，瀏覽器在根據重導位址發請求後再取得Response內容，<font style="color:red;">這時候瀏覽器的URL就會變成新的位址</font>。

![](/Teamdoc/image/DotnetCore/url_rewrite/urlrewrite5.png)

- 其實也可以將兩種都寫在一起。
```csharp
app.UseRewriter(
	new RewriteOptions()
		.AddRewrite(@"First", @"/Home/First", true)
		.AddRedirect(@"Second", @"/Error/Index", 302)
	);
```

Note: <font style="color:red;">不管使用了多少規則，要注意規則盡量不要衝突。</font>

- AddRewrite():
	<font style="color:red;">這種方式是由Server為你將目標位址的內容回傳給你，是使用Server端的資源，</font>在瀏覽器端看不到URL的變化。
	例如:
	Client端請求http://localhost:64593/First 時，Server會去呼叫http://localhost:64593/Home/First 、再將拿到的內容回傳給Client。
	
	1. URL重寫後會看到Home/First.cshtml。
	![](/Teamdoc/image/DotnetCore/url_rewrite/urlrewrite1.png)
	
	2. 但不會看到URL的變化，瀏覽器位址仍然不變。
	```url
	http://localhost:64593/First
	```
	
- AddRedirect():
	<font style="color:red;">這種方式是Server回傳目標位址給瀏覽器，瀏覽器再根據拿到的回傳位址發出第二個請求，使用的是Client端的資源，</font>在瀏覽器端可以看到URL的變化。
	例如:
	Client端請求http://localhost:64593/Second 位址、Server回傳http://localhost:64593/Error/Index ，再由瀏覽器自行重導至新的位址。
	
	1. URL重寫後會看到Error/Index.cshtml。
	![](/Teamdoc/image/DotnetCore/url_rewrite/urlrewrite2.png)
	
	2. 會看到URL的變化。
	```url
	http://localhost:64593/Error/Index
	```

## Method-based rules
除了兩種標準使用方式外，我們也可以自訂方法、實作自己的處理邏輯，再用RewriteOptions.Add()註冊即可。

先加入自定義的Method。
```csharp
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Net.Http.Headers;

namespace NetCoreUrlRewrite.Middlewares
{
    public class ThirdUrlRewriteMiddleware
    {
        public static void ApplyCustomRule(RewriteContext context)
        {
            var request = context.HttpContext.Request;

            if (request.Path.Value.EndsWith(new PathString("/Third")))
            {
                var response = context.HttpContext.Response;
                response.StatusCode = StatusCodes.Status302Found;
                context.Result = RuleResult.EndResponse;
				//Tell Browser to redirect to new URL '/Home/ThirdTest'
                response.Headers[HeaderNames.Location] = request.Path.Value.Replace(@"/Third", @"/Home/ThirdTest");
            }
        }
    }
}
```

註冊自訂方法
```csharp
app.UseRewriter(
	new RewriteOptions()
		.Add(ThirdUrlRewriteMiddleware.ApplyCustomRule)
	);
```

結果會重導至指定的位址。<font style="color:red;">請注意，自訂義方法是Rediret、不是Rewrite。</font>
![](/Teamdoc/image/DotnetCore/url_rewrite/urlrewrite3.png)

Note:
1.由於自訂方法通常是優先權較高的，建議註冊時可以往前一點、可以節省一點操作。
2.關於RuleResult的使用方式，節錄如下。<font style="color:red;">這部份很重要，關係到自訂方法對於Response結果的處理方式。</font>

| RewriteContext.Result              | Action                                                          |
| :--------------------------------- | :-------------------------------------------------------------- |
| RuleResult.ContinueRules (default) | Continue applying rules.                                        |
| RuleResult.EndResponse             | Stop applying rules and send the response.                      |
| RuleResult.SkipRemainingRules      | Stop applying rules and send the context to the next middleware |
	
## 結論
URL重寫其實是很實用的功能，讓我們可以決定在什麼時機、做什麼樣的處理，而URL重寫還有許多不一樣的註冊方式及機制，例如AddRedirectToHttps()可以強制瀏覽器從http導向https，本篇文章僅包含了冰山一角角角.....有興趣的朋友可以參考[RewriteOptions](https://docs.microsoft.com/zh-tw/dotnet/api/microsoft.aspnetcore.rewrite.rewriteoptions?view=aspnetcore-2.2)。
	
## References
1. [URL Rewriting Middleware in ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/url-rewriting?view=aspnetcore-2.2)