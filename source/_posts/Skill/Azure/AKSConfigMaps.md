---
title: AKS - ConfigMaps in Configuration
categories:
 - 技術文件
 - Azure
date: 2023/12/5
updated: 2023/12/5
thumbnail: https://i.imgur.com/5sNBEdu.png
layout: post
tags: [Azure, AKS]
---

![](/Teamdoc/image/azure/logos/aks.png)

## Purpose
在`AKS`中也有許多不同的組態設定給予不同的`Pods`使用，也就是說不同的`Pods`應用可能會需要不同的組態內容，當`Pods`被啟動後就可以各自取得所需的組態內容，它就類似`Windows`的環境變數，我們可以自訂許多變數給不同的服務使用，本文的目的就在為各位介紹`AKS`的`Configuration`設定。
<!-- more -->

## Configuration
`Configuration`有人翻譯為組態、也有人翻譯為配置，其實根本的目的就是自訂一些設定、讓我們可以方便地管理這些東西，而且可以將這些設定與應用系統獨立開來，將來如果這些設定需要更動、應用系統就不需要為了這些變更而重新修改與部署。[官方文件](https://kubernetes.io/docs/concepts/configuration/overview/)對於`Configuration`的最佳做法有一些建議，引述如下。

>- When defining configurations, specify the latest stable API version.

>- Configuration files should be stored in version control before being pushed to the cluster. This allows you to quickly roll back a configuration change if necessary. It also aids cluster re-creation and restoration.

>- Write your configuration files using YAML rather than JSON. Though these formats can be used interchangeably in almost all scenarios, YAML tends to be more user-friendly.

>- Group related objects into a single file whenever it makes sense. One file is often easier to manage than several. See the guestbook-all-in-one.yaml file as an example of this syntax.

>- Note also that many kubectl commands can be called on a directory. For example, you can call kubectl apply on a directory of config files.

>- Don't specify default values unnecessarily: simple, minimal configuration will make errors less likely.

>- Put object descriptions in annotations, to allow better introspection.

說明如下:
1. 主要是因為這些`Configuration`最好是選用穩定版本的API，否則萬一Apply出錯是有可能會導致統崩潰的。
2. 我們這些`Configuration`(YAML)建議也要有版本控管，這樣方便萬一新設定出問題也可以快速回復舊設定。
3. 其實`AKS`就是使用`YAML`進行各項設定的，雖然`K8s`(`AKS`其實就是Azure版的`K8s`)也支援使用`JSON`，但使用`YAML`將讓人更易於使用及理解。
4. 對於這些組態設定適當地加以良好的群組規劃是比較好的方式，也就是說性質相近的設定可以放在同一個群組中、而不是各自一個`YAML`檔，這也更加方便維護與管理。
5. 使用`Configuration`也可以在`YAML`中使用像`kubectl apply`等指令。
6. 在`YAML`內也應該要使用註解來讓組態設定更易於理解。

# ConfigMaps
`ConfigMaps`是`Configuration`中的一種組態設定，它跟環境變數很類似、就是一些`Key-Value pairs`的參數，我們的應用程式可以透過`Key`來取得`Value`使用，引用[官方文件](https://kubernetes.io/docs/concepts/configuration/configmap/)說明如下。

>A ConfigMap is an API object used to store non-confidential data in key-value pairs. Pods can consume ConfigMaps as environment variables, command-line arguments, or as configuration files in a volume.

>A ConfigMap allows you to decouple environment-specific configuration from your container images, so that your applications are easily portable.

事實上，`Key-Value pairs`的參數可以讓我們的應用程式用在像環境變數、指令傳入參數或是一些`Volume`的設定檔中，`最重要的是解偶!`也就是說這些設定可以跟我們的應用程式切開相依性，這樣就不需要因為更動這些設定而不斷重新Build、Deploy我們的`Pods`。

Note:  
`請注意，ConfigMaps並不支援加解密喔!如果想要有安全性功能會建議另外使用Secrets。`

## Add ConfigMaps in AKS Configuration
接下來為各位介紹如何在`AKS`的`Configuration`新增`ConfigMaps`，基本上`K8s`導入`Azure`後有了`AKS`、很多操作都變得方便許多，我們可以先準備好`YAML`內容，然後我們再進入`AKS`的`Configuration` -> `Create` -> `Apply a YAML`如下圖，範例`YAML`如下。

![](/Teamdoc/image/azure/AKSConfigMaps/configmaps1.png)

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: aks-config-test
data:
  # property-like keys; each key maps to a simple value
  key1: "123"
  key2: "456"

  # file-like keys
  key3.properties: |
    status.types=normal,warning,error
    max=10    
  key4.properties: |
    normal=blue
    warning=yellow
    error=red  
```

上方的`YAML`範例用了兩種類屬性及類檔案的方式進行設定，我們貼上`YAML`後按下`Add`鍵就可以新增`ConfigMaps`如下圖。

![](/Teamdoc/image/azure/AKSConfigMaps/configmaps2.png)

接著我們回到`ConfigMaps`清單就可以看到剛才新增的`aks-config-test`變數，然後進入查看就可以發現這個`ConfigMaps`已經包含有四個變數如下圖。

![](/Teamdoc/image/azure/AKSConfigMaps/configmaps3.png)

![](/Teamdoc/image/azure/AKSConfigMaps/configmaps4.png)

## Apply to Pods
上一小節我們順利使用`YAML`設定好`ConfigMaps`，但這僅僅只是新增一個設定而已，我們還需要告訴特定的`Pods`如何使用這個新增的`ConfigMaps`，接著我們使用下列`YAML`範例來指定`Pods`要使用的設定。

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: aks-config-test-pod
spec:
  containers:
    - name: con1
      image: alpine
      command: ["sleep", "3600"]
      env:
        # Define the environment variable
        - name: KEY1 # This is just a name for here, not the ConfigMap's key.
          valueFrom:
            configMapKeyRef:
              name: aks-config-test  # The ConfigMap's name
              key: key1              # The key to fetch.
        - name: KEY2
          valueFrom:
            configMapKeyRef:
              name: aks-config-test
              key: key2
      volumeMounts:
      - name: config
        mountPath: "/config"
        readOnly: true
  volumes:
  # The volumes mounted into container inside that Pod
  - name: config
    configMap:
      # Provide the name of the ConfigMap you want to mount.
      name: aks-config-test
      # An array of keys from the ConfigMap to create as files
      items:
      - key: "key3.properties"
        path: "key3.properties"
      - key: "key4.properties"
        path: "key4.properties"
```

從上方範例我們可以看到用了兩種方式指定`ConfigMap`，第一種是在`containers` -> `env` -> 指定了`key1`及`key2`兩個來自`aks-config-test`設定的key，第二種是以掛載的`Volumes`中指定使用`aks-config-test`中的`key3.properties`及`key4.properties`陣列，如此就可以利用`ConfigMAp`切開變數設定與`Pods`之間的相依性了。

## Conclusion
本文簡單介紹了`ConfigMaps`的概念、並引用官方文件建議的最佳實務做法，讓我們可以思考及規劃我們要如何使用`ConfigMaps`，而且進入`AKS`以後，很多基本功能都能夠透過`Azure Portal`來完成，除非是一些比較細節、特殊的使用方式，否則直接用`AKS`提供的UI介面已經可以滿足一般狀況的需求了，真的推薦大家多使用`AKS`來部署雲端`K8s`的應用。

## References
1. [Configuration](https://kubernetes.io/docs/concepts/configuration/)