---
title: How to batch convert excel files to CSVs?
categories:
 - 技術文件
 - Windows
date: 2019/07/11
updated: 2019/07/11
thumbnail: https://i.imgur.com/oxwR3EB.png
layout: post
tags: [Batch, Excel, CSV]
---
作者: Carl Yang

![](/Teamdoc/image/Windows/Excel-logo-figure.png)

## 目的
工作上很常遇到檔案轉換的問題，尤其是Excel。通常如果只有少數檔案的話，我們可以使用MS Office Excel逐檔轉換，但如果檔案數一多、轉換起來可真是惱人。本篇文章描述在Windows下，如何透過command prompt下指令、調用Excel.Application在背景進行批次轉換工作。
<!-- more -->

## 必要條件
請安裝下列環境，其他環境尚未試過，不保證都可行。
1. Windows 10
2. MS Office Excel 2016

## 撰寫VBScripts
1. 先建立一個ExcelToCsv.vbs文字檔、再將下列程式寫入並存檔。  
    Note:  
    建議將vbs檔案放置於excel檔案的所在資料夾，否則接下來可能會找不到檔案路徑而失敗。 

>程式如下:  [範例來源](https://stackoverflow.com/questions/1858195/convert-xls-to-csv-on-command-line)

```vbscript
if WScript.Arguments.Count < 2 Then
    WScript.Echo "Error! Please specify the source path and the destination. Usage: XlsToCsv SourcePath.xls Destination.csv"
    Wscript.Quit
End If
Dim oExcel
Set oExcel = CreateObject("Excel.Application")
Dim oBook
Set oBook = oExcel.Workbooks.Open(Wscript.Arguments.Item(0))
oBook.SaveAs WScript.Arguments.Item(1), 6
oBook.Close False
oExcel.Quit
WScript.Echo "Done"
```

2. 接著在command prompt中切換至ExcelToCsv.vbs的所在資料夾，並確認欲轉換的Excel檔也都在相同位置。
```cmd
cd D:\test
```

3. 接著輸入以下指令即可開始批次轉換為csv檔案格式。
```cmd
FOR /f "delims=" %i IN ('DIR *.xlsx /b') DO ExcelToCSV.vbs "D:\test\%i" "D:\test\csv\%i.csv"
```

4. 轉換後的檔案內容有可能是其他的編碼，例如，我轉換後變成是Big5(Traditional)。這是因為ExcelToCsv.vbs檔內調用的Excel會自動選擇輸出的編碼格式，但我們會希望編碼格式為UTF-8、以方便資料交換之用，也可避免亂碼的情形。

5. 可以下載[UTFCast Express](https://www.azofreeware.com/2009/04/utfcast-10.html)，這個工具的好用在於它可以一次將多個檔案的編碼格式轉換為UTF-8的格式，使用起來簡單又方便。

## 結論
在工作上很常有批次轉換Excel為CSV的情況，有的時候量少就一個個轉換、也還過得去，但量一多時還一個個轉、就真的會逼死人啦!所以可以批次轉換的話真是很方便又快速。

## 參考
1. [Gthub](https://gist.github.com/tonyerskine/77250575b166bec997f33a679a0dfbe4)