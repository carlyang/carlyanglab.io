---
title: Azure - API Management如何使用受控識別向後端服務進行存取
categories:
 - 技術文件
 - Azure
date: 2022/7/10
updated: 2022/7/10
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management]
---

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
`Managed Identity`是受控於`Azure Active Directory`(下稱`AAD`)的一種身分，這種身分可以是自訂的、也可以由`AAD`所管控，本文目的在於說明如何在`APIM`中使用`Managed Identity`來向後端服務進行存取。
<!-- more -->

## Managed Identity
`Managed Identity`是受控於`AAD`的一種應用服務身分，我們可以把某個服務建立受控識別身分，然後透過`IAM`來允許這個虛擬身分的存取權，詳細可參考[[MS Doc]](https://learn.microsoft.com/en-us/azure/active-directory/managed-identities-azure-resources/overview)，引述如下。

>Managed identities provide an automatically managed identity in Azure Active Directory (Azure AD) for applications to use when connecting to resources that support Azure AD authentication. Applications can use managed identities to obtain Azure AD tokens without having to manage any credentials.

所以由上可知，我們可以直接使用`Managed Identity`、而不需要再去另外產生`Secrets`、`Certificates`、`Credentials`等身分識別資訊，透過`AAD`就可以利用`Managed Identity`自動拿到`Credentials`來進行安全性控管，其實我公司`Key-Vault`服務的部分安全性管控就是使用`Managed Identity`。  

`Managed Identity`又可分為兩種，一種是自動產生的、另一種則是自訂的。

- `System-assigned`: 這是使用系統自動為此服務指定的虛擬身分，大部分都是使用這種、方便又快速，也會自動設定`AAD`中的相關資訊，詳細引述如下。
>- A service principal of a special type is created in Azure AD for the identity. The service principal is tied to the lifecycle of that Azure resource. When the Azure resource is deleted, Azure automatically deletes the service principal for you.
>- By design, only that Azure resource can use this identity to request tokens from Azure AD.
>- You authorize the managed identity to have access to one or more services.
>- The name of the system-assigned service principal is always the same as the name of the Azure resource it is created for. For a deployment slot, the name of its system-assigned identity is <app-name>/slots/<slot-name>.

- `User-assigned`: 這種則是使用者自行指定的身分，基本上`AAD`相關的設定都要手動處理，但是自訂程度較高，詳細引述如下。
>- A service principal of a special type is created in Azure AD for the identity. The service principal is managed separately from the resources that use it.
>- User-assigned identities can be used by multiple resources.
>- You authorize the managed identity to have access to one or more services.

## authentication-managed-identity
`APIM`中要驗證`Managed Identity`是使用`authentication-managed-identity`原則，它會使用`resource`或`client id`幫`APIM`向`AAD`取得`toekn`、然後帶入`Authorization Bearer XXXX`再送到後端API去。

引述微軟文件說明如下:
>Use the authentication-managed-identity policy to authenticate with a backend service using the managed identity. This policy essentially uses the managed identity to obtain an access token from Azure Active Directory for accessing the specified resource. After successfully obtaining the token, the policy will set the value of the token in the Authorization header using the Bearer scheme. API Management caches the token until it expires.

所以，`我們不一定要以APIM的身分去存取後端服務，而是可以透過某個特定身分去存取`。這有麼好處呢?當然有!如果我們某個後端服務它需要知道當下進來存取的身分是誰、並且記錄相關Log，事後需要排除問題時才能夠準確命中問題點，而且這個授權權杖是由`AAD`管控的，我們不需要花大把功夫自建整個驗證服務，範例如下。

```xml
<authentication-managed-identity resource="resource" client-id="clientid of user-assigned identity" output-token-variable-name="token-variable" ignore-error="true|false"/>
```

說明:
- `resource`: 是`AAD`中目標Web API的應用程式識別碼。
- `client-id`: 使用者指派身分識別的用戶端識別碼，`與resource二則一、根據System-assigned或User-assigned受控識別使用`。
- `output-token-variable-name`: 拿到token後要存入的變數名稱，而這個變數是用來後續處理之用，例如存入`Authorization`這個標頭。
- `ignore-error`: 是否要忽略錯誤。

例如我們在`AAD`中自訂了一個`Application ID`，就可以把這個ID帶入`resource`中，範例如下。

```xml
<authentication-managed-identity resource="AD_application_id"/>
```

完整範例如下:

```xml
<authentication-managed-identity resource="{{your application id}}"
   output-token-variable-name="{{custom-variable-name}}" ignore-error="false" />
<set-header name="Authorization" exists-action="override">
   <value>@("Bearer " + (string)context.Variables["{{custom-variable-name}}"])</value>
</set-header>
```

上列範例就是我們使用自訂的`{{your application id}}`向`AAD`取得`Token`後放入變數`{{custom-variable-name}}`，接著我們用內嵌程式碼的方式讀取變數`{{custom-variable-name}}`、放入標頭`Authorization`中，如此就可以使用`Application ID`這個身分對後端API進行存取。

## Conclusion
在應用情境上來說，其實使用特定受控識別身分向後端API驗證的機會不算多，不過如果後端API有些安全性考量、又或者這個後端API是另一個部門所提供的，他們要求必須使用`Managed Identity`來存取時，`authentication-managed-identity`就可以派上用場了，畢竟軟體服務很少由單一部門撰寫，大部分有一定規模的系統基本上都是由多個部門偕同完成的。不過，如果有自己的一套`JWT`驗證服務就也用不上這個功能就是，還是要依照不同情境來做使用才行。

## References
1. [What are managed identities for Azure resources?](https://learn.microsoft.com/en-us/azure/active-directory/managed-identities-azure-resources/overview)
2. [Authenticate with managed identity](https://learn.microsoft.com/en-us/azure/api-management/authentication-managed-identity-policy)