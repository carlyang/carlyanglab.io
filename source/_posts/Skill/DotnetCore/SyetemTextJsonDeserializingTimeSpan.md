---
title: .NET Core - System.Text.Json deserializing with TimeSpan property
categories:
 - 技術文件
 - .NET Core
date: 2021/06/06
updated: 2021/06/06
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [.NET Core, System.Text.Json, Newtonsoft.Json, Deserialization]
---
作者: Carl Yang

![](/Teamdoc/image/DotnetCore/Logo.png)

## Purpose
使用`.NET Core 3.1`中的`System.Text.Json`時發現，序列化`TimeSpan`類型的屬性時正常、反序列化後得到的`JSON`竟然都是`{00:00:00}`，看起來似乎是有問題，於是紀錄一下這篇文章以及解決方法給大家參考。
<!-- more -->

## Prerequisites
本文是使用.NET Core 3.1作為開發環境進行練習，請先安裝下列項目。
- 請先安裝.NET Core 3.1 SDK([Download](https://dotnet.microsoft.com/download/dotnet/3.1))

## Introduction
`System.Text.Json`是`.NET Core`新的處理JSON資料的套件並內建在SDK中，只要有安裝SDK即可呼叫使用，且效能比`Newtonsoft.Json`還要好、占用的記憶體也更少，確實有它更優秀的地方。不過目前來說，`System.Text.Json`還是有一些問題需要解決，微軟後續也會再逐步修正。

## Serialization
接下來我們來做個實驗，比較一下`Newtonsoft.Json`與`System.Text.Json`兩者的序列化結果。

```csharp
public class TimeSpanModel
{
    public TimeSpan ProcessTime { get; set; } = DateTime.Now - DateTime.Today;
}

public class Program
{
    public static void Main(string[] args)
    {
        try
        {
            var result1 = Newtonsoft.Json.JsonConvert.SerializeObject(new TimeSpanModel());

            var result2 = System.Text.Json.JsonSerializer.Serialize(new TimeSpanModel());
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
}
```

- result1的結果如下:

```json
{
  "ProcessTime": "17:05:45.1302475"
}
```

- result2的結果如下:

```json
{
  "ProcessTime": {
    "Ticks": 615455858076,
    "Days": 0,
    "Hours": 17,
    "Milliseconds": 585,
    "Minutes": 5,
    "Seconds": 45,
    "TotalDays": 0.7123331690694444,
    "TotalHours": 17.095996057666667,
    "TotalMilliseconds": 61545585.8076,
    "TotalMinutes": 1025.75976346,
    "TotalSeconds": 61545.5858076
  }
}
```

從序列化後的結果看來，`Newtonsoft.Json`會將`TimeSpan`產出一串時間字串、而`System.Text.Json`則會將`TimeSpan`產出為一個`JSON Object`。

## Deserialization
再來，我們繼續操作反序列化的實驗如下。

```csharp
public class TimeSpanModel
{
    public TimeSpan ProcessTime { get; set; } = DateTime.Now - DateTime.Today;
}

public class Program
{
    public static void Main(string[] args)
    {
        try
        {
            var result1 = Newtonsoft.Json.JsonConvert.SerializeObject(new TimeSpanModel());

            var result2 = System.Text.Json.JsonSerializer.Serialize(new TimeSpanModel());

            var model1 = Newtonsoft.Json.JsonConvert.DeserializeObject<TimeSpanModel>(result1);

            var model2 = System.Text.Json.JsonSerializer.Deserialize<TimeSpanModel>(result2);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
}
```

- model1的結果如下:

![](/Teamdoc/image/DotnetCore/SyetemTextJsonDeserializingTimeSpan/model1.png)

- model2的結果如下:

![](/Teamdoc/image/DotnetCore/SyetemTextJsonDeserializingTimeSpan/model2.png)

這裡我們可以發現，反序列化後的`TimeSpanModel`，`Newtonsoft.Json`得到的結果是正常的、但`System.Text.Json`得到的結果卻是`{00:00:00}`，也就是說`System.Text.Json`並`不能正常處理序列化後的JSON物件!`
  
- 這裡可能有人會想問，那把正常的`Newtonsoft.Json`產出的`JSON`字串丟給`System.Text.Json`反序列化呢?秉持科學實驗精神，我就多做點實驗把result1丟給`System.Text.Json`處理反序列化。

```csharp
var model2 = System.Text.Json.JsonSerializer.Deserialize<TimeSpanModel>(result1);
```

結果如下，抓到`Exception`訊息:
> The JSON value could not be converted to System.TimeSpan. Path: $.ProcessTime | LineNumber: 0 | BytePositionInLine: 33.

![](/Teamdoc/image/DotnetCore/SyetemTextJsonDeserializingTimeSpan/model2-1.png)

所以，簡單但很重要的一點就是，`最好不要混用Newtonsoft.Json及System.Text.Json!`否則難講會出現什麼詭異狀況喔。

## Solution
上面做了這麼多實驗，最後就要來講解決方法啦!  
其實解決方法也很簡單，就是自訂`System.Text.Json`中的`JsonConverter`，在反序列化時自己轉換為`TimeSpan`。

```csharp
public class CustomTimeSpanConverter : JsonConverter<TimeSpan>
{
    public override TimeSpan Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        return TimeSpan.Parse(reader.GetString(), CultureInfo.InvariantCulture);
    }

    public override void Write(Utf8JsonWriter writer, TimeSpan value, JsonSerializerOptions options)
    {
        writer.WriteStringValue(value.ToString(null, CultureInfo.InvariantCulture));
    }
}
```

然後在`TimeSpanModel`的Property加上`[JsonConverter(typeof(CustomTimeSpanConverter))]`即可。

```csharp
public class TimeSpanModel
{
    [JsonConverter(typeof(CustomTimeSpanConverter))]
    public TimeSpan ProcessTime { get; set; } = DateTime.Now - DateTime.Today;
}
```

Note:  
請注意，這裡要自訂的JsonConverter是`繼承並實作System.Text.Json.Serialization.JsonConverter`、不是`Newtonsoft.Json.JsonConverter`喔!

#### Code Explanation
- 當我們在序列化時，`System.Text.Json`會呼叫`CustomTimeSpanConverter`的`Write()`來取得`TimeSpan`序列化後的字串，如下圖。

![](/Teamdoc/image/DotnetCore/SyetemTextJsonDeserializingTimeSpan/result2-1.png)

  結果如下JSON，我們可以看到`產出來JSON中的TimeSpan就會變成跟Newtonsoft.Json產出的結果一樣了`。

```json
{
  "ProcessTime": "08:58:35.6407663"
}
```

- 當我們在反序列化時，`System.Text.Json`會呼叫`CustomTimeSpanConverter`的`Read()`來取得`TimeSpan`物件，如下圖。

![](/Teamdoc/image/DotnetCore/SyetemTextJsonDeserializingTimeSpan/model3.png)

如此一來，我們就可以正常使用`System.Text.Json`序列化/反序列化處理`TimeSpanModel`了。

#### Global JSON Serializer Options
另外，如果不想一個個`Model Property`去加Annotation的話，我們也可以直接在`Startup.cs`的`ConfigureServices()`內設定`JsonSerializerOptions.Converters`，把我們自訂的Converter設定進去，如此整個系統中只要是用`System.Text.Json`進行`TimeSpan`的序列化/反序列化操作，都會套用我們的Converter了。

```csharp
services.AddControllers()
    .AddJsonOptions(options =>
{
    // Configure a custom converter.
    options.JsonSerializerOptions.Converters.Add(new CustomTimeSpanConverter());
});
```

## 結論
`System.Text.Json`是最新的JSON處理套件，目前`.NET Core`也是主推它來處理JSON操作，不過也因為是新開發的套件，雖然效能又更進步了、但支援度仍然還比不上`Newtonsoft.Json`完整。  
目前看到一些消息，這問題本來應該在`.NET 5`修正、不過似乎是還沒修好。  
不過，大家一時間應該也還不會更新版本，畢竟還是有些實際面要考量，所以還在舊版本的`.NET Core`就可以用這個方法解決囉。

## 參考
1. [[#29932]](https://github.com/dotnet/runtime/issues/29932)