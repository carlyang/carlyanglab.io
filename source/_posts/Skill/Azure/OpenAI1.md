---
title: Azure OpenAI Service (1) - 服務簡介及申請步驟
categories:
 - 技術文件
 - Azure
date: 2023/3/5
updated: 2023/3/5
thumbnail: https://i.imgur.com/xt5yNCJ.png
layout: post
tags: [Azure, Azure OpenAI Service]
---

![](/Teamdoc/image/azure/logos/openai.png)

## Purpose
最近火紅的`ChatGPT`大家應該都不陌生，又聽說`Azure OpenAI Service`也支援使用GPT-3模型?!真是快速地令人吃驚，想說來試用看看微軟與`OpenAI`合作推出的`Azure OpenAI Service`，故本文目的在紀錄對`Azure OpenAI Service`的學習過程與心得。  
不過，為了避免大家浪費時間，先告知大家`目前建立Azure OpenAI Service還需要經過一段時間微軟審核通過才行，所以本文撰寫時尚未成功建立服務`，大家如果沒興趣可以先不用往下看，後續等我通過後會再分享給大家。
<!-- more -->

## Azure OpenAI Service
引用[[MS Doc]](https://learn.microsoft.com/en-us/azure/cognitive-services/openai/overview)的說明如下。

>Azure OpenAI Service provides REST API access to OpenAI's powerful language models including the GPT-3, Codex and Embeddings model series. These models can be easily adapted to your specific task including but not limited to content generation, summarization, semantic search, and natural language to code translation. Users can access the service through REST APIs, Python SDK, or our web-based interface in the Azure OpenAI Studio.

由上可知`Azure OpenAI Service`提供了一組REST API以提供`OpenAI`的強大語言模型，包括`GPT-3`、`Codex`及`Embeddings`等模型，而且除了REST API外，也還提供`Python SDK`及`Azure OpenAI Studio`等工具來使用、感覺似乎挺方便的。

## Region
目前`Azure OpenAI Service`適用的區域都在美國及西歐、尚未GA到全球的Region，所以如果想要建立`Azure OpenAI Service`只能先到下列Regions才行，大家可以先進入計價頁面的下拉選單參考[[Pricing]](https://azure.microsoft.com/en-us/pricing/details/cognitive-services/openai-service/)，如下圖。

![](/Teamdoc/image/azure/OpenAI1/region1.png)

## Pricing
各種模型目前的計價如下圖，圖中是以目前美元對台幣的匯率計算，所以是顯示新台幣的價格。

![](/Teamdoc/image/azure/OpenAI1/price1.png)

## GPT-3模型
基本上目前想先試用看看`GPT-3`模型(詳細資訊可參考[[Features overview]](https://learn.microsoft.com/en-us/azure/cognitive-services/openai/overview#features-overview))，也就是可以了解並生成自然語言的模型。不過，`Azure OpenAI Service`似乎尚未支援`GPT-3.5`版本，可能要再等等了，引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/cognitive-services/openai/concepts/models#gpt-3-models)對於`GPT-3`模型的摘要說明如下。

>The GPT-3 models can understand and generate natural language. The service offers four model capabilities, each with different levels of power and speed suitable for different tasks. Davinci is the most capable model, while Ada is the fastest. In the order of greater to lesser capability, the models are:  
>- text-davinci-003
>- text-curie-001
>- text-babbage-001
>- text-ada-001  
>While Davinci is the most capable, the other models provide significant speed advantages. Our recommendation is for users to start with Davinci while experimenting, because it produces the best results and validate the value that Azure OpenAI can provide. Once you have a prototype working, you can then optimize your model choice with the best latency/performance balance for your application.

由上述說明可知，如果希望未來使用功能性最佳的模型、可以使用`Davinci`是功能最豐富的模型，如果希望速度達到最佳的話、則可以使用`Ada`的模型。

Note:  
據了解`ChatGPT目前已經使用GPT-3.5版本了!`想要使用最新版的朋友可能要再等微軟後續的更新了。根據網友在[[Wiki]](https://zh.wikipedia.org/zh-tw/ChatGPT)的分享，`GPT-3已經可以解決70%的心智理論任務，相當於7歲兒童。至於GPT-3.5更是解決了93%的任務，心智相當於9歲兒童`，真是令人好奇`Azure OpenAI Service`又能夠與`Azure`整合應用到什麼程度呢?!

## 申請步驟
首先，當然要先來建立服務啦!我們就直接進入`Azure Portal`建立一個`Azure OpenAI Service`。

1. 搜尋資料名為`Azure OpenAI`。
![](/Teamdoc/image/azure/OpenAI1/open_ai1.png)

2. 接著進入下一步後，你會看到一個`Warning`訊息，點下他進入下一步。
![](/Teamdoc/image/azure/OpenAI1/open_ai2.png)

3. 再來你就會看到一份表單，`告訴你因為目前Azure OpenAI Service僅限於微軟的客戶或合作夥伴才能使用`，所以如果想要試用的話，必須另外填寫這份表單送給微軟審核，所以接下來就耐心把表單內容填完吧!
![](/Teamdoc/image/azure/OpenAI1/open_ai3.png)

4. 填完表單送出後就會告訴你...大約要10個工作天來審核你的資格!天啊，我要昏了...那也沒法，只好等等看了。
![](/Teamdoc/image/azure/OpenAI1/open_ai4.png)

## Conclusion
這篇文主要是紀錄一下最近開始使用`Azure OpenAI Service`的第一步，沒想到還要等10天啊!不過也沒辦法，只能耐心先等等囉!等申請通過後再來分享一下後續的心得，這邊僅先紀錄一下研讀文件的重點及申請步驟給大家參考。

## References
1. [What is Azure OpenAI Service?](https://learn.microsoft.com/en-us/azure/cognitive-services/openai/overview)
2. [ChatGPT](https://learn.microsoft.com/en-us/azure/cognitive-services/openai/concepts/models)