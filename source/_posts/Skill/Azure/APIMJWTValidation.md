---
title: Azure - 如何在API Management中使用JWT Token驗證
categories:
 - 技術文件
 - Azure
date: 2022/6/5
updated: 2022/6/5
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management]
---

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
使用`API Management`(下稱`APIM`)時的身分驗證非常重要，因為許多不同的`API`被整合在一起、安全性也跟著必須要提高，其中最常用的驗證機制非`JWT(JSON Web Token)`莫屬!本文目的在說明如何利用自訂發放的`JWT token`在`APIM`中進行驗證。
<!-- more -->

## JWT
關於`JWT`的使用，推薦拜讀保哥的教學文章、非常地詳細，如果你是舊版的`ASP.NET Core 3 `可參考[如何在 ASP.NET Core 3 使用 Token-based 身分驗證與授權 (JWT)](https://blog.miniasp.com/post/2019/12/16/How-to-use-JWT-token-based-auth-in-aspnet-core-31)，如果是新版的`ASP.NET Core 6`可參考[如何在 ASP.NET Core 6 使用 Token-based 身份認證與授權 (JWT)](https://blog.miniasp.com/post/2022/02/13/How-to-use-JWT-token-based-auth-in-aspnet-core-60)。

由於本文目的在於說明如何在`APIM`中使用`JWT`驗證，請先將發放`JWT`時使用的`Issuer`及`SignKey`記下來，如下列資訊，設定`APIM`時會使用到。

### TokenValidationParameters:
- `ValidIssuer`: 通常應該都會驗證`JWT`的發行者，否則不論哪個來源發的`JWT`都通過那可不得了!
- `IssuerSigningKey`: 這邊則是產生`JWT`時使用的`Signing Key`。

## Validate JWT
想要在`APIM`中使用`JWT`驗證其實不難，一樣是使用`Policy`來處理，範例如下。

```xml
<validate-jwt header-name="Authorization" failed-validation-httpcode="401" require-expiration-time="true">
	<issuer-signing-keys>
		<key>Your signing key converted by Base64</key>
	</issuer-signing-keys>
	<issuers>
		<issuer>Your Issuer name</issuer>
	</issuers>
</validate-jwt>
```

說明:
- `header-name`: 使用`JWT`時帶入`HTTP header`中的名稱，例如: `Authorization Bearer XXXXXX`。
- `failed-validation-httpcode`: 當驗證失敗時要回傳的`HTTP Status`。
- `require-expiration-time`: 是否需要驗證`JWT`過期時間，個人是覺得一定要有過期時間、否則永遠不過期的話就失去使用`Token`驗證的意義了。
- `issuer-signing-keys`: 請特別注意!`這邊必須要把先前記下來的IssuerSigningKey轉成Base64的字串!`使用任何工具都可以，只要`Base64`認得的字串即可。
- `issuers`: 這邊就是先前記下來的`JWT`發行者，通常應該至少會區分測試與正式環境兩種、分別具有不同的發行者，這樣就不會造成一個`JWT`兩環境都可以通過的狀況。

Note:  
大家應該有注意到，`APIM`的`validate-jwt`其實可以包含多組`signing key`及`issuer`，這是為了不同情境或是多種不同的`JWT`發行來源而設計的。

## 進階的Validate JWT
如果希望根據不同的條件、來套用不同的`validate-jwt`原則，我們也可以使用`choose...when..`來先進行判斷、然後再進去該`validate-jwt`進行`Token`驗證(詳細可參考[[MS Doc]](https://learn.microsoft.com/en-us/azure/api-management/policies/authorize-request-based-on-jwt-claims))，範例如下。


Note:  
`JWT`中的宣告(`Claims`)也可以在`validate-jwt`原則中使用，只要設定在`required-claims`區塊即可，就可以驗證類似於角色或群組等的使用情境了。

```xml
<policies>
  <inbound>
    <base />
      <choose>
        <when condition="@(context.Request.Method.Equals("patch=""",StringComparison.OrdinalIgnoreCase))">
          <validate-jwt header-name="Authorization">
            <issuer-signing-keys>
              <key>{{signing-key}}</key>
            </issuer-signing-keys>
            <required-claims>
              <claim name="edit">
                <value>true</value>
              </claim>
            </required-claims>
          </validate-jwt>
        </when>
        <when condition="@(new [] {"post=""", "put="""}.Contains(context.Request.Method,StringComparer.OrdinalIgnoreCase))">
          <validate-jwt header-name="Authorization">
            <issuer-signing-keys>
              <key>{{signing-key}}</key>
            </issuer-signing-keys>
            <required-claims>
              <claim name="create">
                <value>true</value>
              </claim>
            </required-claims>
          </validate-jwt>
        </when>
        <otherwise>
          <validate-jwt header-name="Authorization">
            <issuer-signing-keys>
              <key>{{signing-key}}</key>
            </issuer-signing-keys>
          </validate-jwt>
        </otherwise>
      </choose>    
  </inbound>
  <backend>
    <base />
  </backend>
  <outbound>
    <base />
  </outbound>
  <on-error>
    <base />
  </on-error>
</policies>
```

## Conclusion
本文之所以會使用`JWT`進行身分驗證主要是在`APIM`中、雖然也可以使用`憑證(Certificates)`來驗證，但是`憑證(Certificates)`的申請及花費又是另一種考量，事實上，在我們的情境下，光跑流程申請可能就來不及完成專案需求了，而且憑證每過段期間也要重新更新、又要再重跑一次流程，勞民傷財阿!所以我們自建了一個`JWT`服務負責所有系統的`Token`發放，並且使用`APIM`作為統一的API入口及進行`JWT`驗證，是很有效並安全性架構。

## References
1. [Authorize access based on JWT claims](https://learn.microsoft.com/en-us/azure/api-management/policies/authorize-request-based-on-jwt-claims)
2. [如何在 ASP.NET Core 3 使用 Token-based 身分驗證與授權 (JWT)](https://blog.miniasp.com/post/2019/12/16/How-to-use-JWT-token-based-auth-in-aspnet-core-31)
3. [如何在 ASP.NET Core 6 使用 Token-based 身份認證與授權 (JWT)](https://blog.miniasp.com/post/2022/02/13/How-to-use-JWT-token-based-auth-in-aspnet-core-60)