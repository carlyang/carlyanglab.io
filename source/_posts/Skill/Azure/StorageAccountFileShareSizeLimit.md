---
title: Azure - Storage Account FileShare的資料大小
categories:
 - 技術文件
 - Azure
date: 2022/12/1
updated: 2022/12/1
thumbnail: https://i.imgur.com/8uMcIBL.png
layout: post
tags: [Azure, Storage Account, FileShare]
---

![](/Teamdoc/image/azure/logos/storage_account.png)

## Purpose
[[Azure - Storage Account FileShare]](/2022/11/30/Skill/Azure/StorageAccountFileShare/)簡單介紹了`Storage Account FileShare`並分享如何利用SDK使用程式碼的方式存取`FileShare`中的檔案，不過使用上還是有一些跟檔案相關的限制需要注意，本文目的在說明這些限制避免造成使用上的問題。
<!-- more -->

## Azure Files REST API
首先，作為雲端平台的`IaaS`基礎建設服務，`FileShare SDK`的背後也是透過`RESTful API`與`Storage Account`進行溝通，引述[[MS Doc]](https://learn.microsoft.com/en-us/rest/api/storageservices/file-service-rest-api)如下。

>Azure Files also provides a REST API, which is often called the FileREST API. It provides another method of accessing data stored in Azure file shares. SMB and NFS enable transparent access to remote file shares through native file system APIs, but the FileREST protocol provides a different method for accessing file share data.

所以，`FileREST API`提供另一種`Azure`上檔案的存取機制，了解這些API也可以幫助我們更清楚相關的限制，也因為它是一種API服務、背後其實都是走`HTTP`協定，所以可以提供多種平台的SDK來方便運用，例如:C#、JAVA、Python、JavaScript 和 Go。

## Operations on files
在`FileREST API`中提供了多種的操作，詳細可以參考[[Data APIs]](https://learn.microsoft.com/en-us/rest/api/storageservices/operations-on-files#data-apis)，我們接下來要說明的相關限制，主要在`PUT Range`中(下表僅列出本文相關、詳細請參考原文)。

| API | Available on SMB file shares | Available on NFS file shares |
| --- | --- | --- |
| Put Range | V | X |

Note:  
從表中其實我們也可以觀察到，基本上因為是微軟的平台、`SMB`的相關操作全部都支援、但`NFS`的就沒有支援了。

## PUT Range
接下來就要進入本文的主題，說明`File`各部分有哪些限制。

1. 引述下方說明，我們可以發現其實`FileREST API`都是使用`byte[]`方式在寫入檔案的，而且檔案如果不存在、此操作會直接回傳`404 Not Found`，`這也印證了我上一篇介紹使用SDK時、必須先Create File並指定大小、接著才是開始上傳資料內容`的流程。
>The Put Range operation writes a range of bytes to a file. This operation can be called on an existing file only. It can't be called to create a new file. Calling Put Range with a file name that doesn't currently exist returns status code 404 (Not Found).

```csharp
...略
        //這邊先建立檔案、並且指定該檔案的大小
        await fileShareClient.CreateAsync(content.Length);
...略
        while (true)
        {
...略
            //這邊才真正開始把內容上傳上去
            await fileShareClient.UploadRangeAsync(httpRange, uploadChunk);
...略
        }
```

Note:  
請特別注意，`因為FileSahre新建檔案與上傳內容是分開的動作，所以當上傳時發生失敗、我們進FileShare還是看得到檔案，但是打開檔案內容卻看到全部顯示NUL就是這個原因`，因為實際上只有上傳動作失敗而已、建立檔案的操作是成功的。

2. `單一檔案的大小上限為 4 TiB`，雖然這不太可能碰到、但我也不敢說不可能，畢竟現在光是消費性硬碟基本都TB等級以上的容量，每單位容量的成本也是越降越低，哪天還真的一個檔案被灌到4TiB也不無可能。

3. `每MiB的操作時間平均如果超過10分鐘則會逾時`，引述如下。

>A Put Range operation is permitted 10 minutes per MiB to complete. If the operation is taking longer than 10 minutes per MiB on average, it times out.

4. `每次的PUT Range操作不能超過4MiB，否則該次會失敗並回傳413 (Request Entity Too Large)`，引述如下。

> Each range that's submitted with Put Range for an update operation may be up to 4 MiB in size. If you attempt to upload a range that's larger than 4 MiB, the service returns status code 413 (Request Entity Too Large).

Note:  
前面說的單檔4TiB不太可能，但這邊的單一操作4MiB可就很容易遇到了，我實際在用SDK上傳檔案的時候真是很大部分都超過阿!所以，當我們在上傳資料的時候就需要分批操作、每4MiB丟一次即可，Sample Code如下。

```csharp
/// <summary>
/// Write content to file share in batches.
/// </summary>
/// <param name="fileShareClient"></param>
/// <param name="content"></param>
/// <returns></returns>
private async Task WriteContentAsync(ShareFileClient fileShareClient, Stream content)
{
	const int blockSize = 4000000;

	//http range offset
	long offset = 0;

	await fileShareClient.CreateAsync(content.Length);

	using var reader = new BinaryReader(content);

	//Write in batches
	while (true)
	{
		var buffer = reader.ReadBytes(blockSize);
		if (buffer.Length == 0)
			break;

		var uploadChunk = new MemoryStream();
		uploadChunk.Write(buffer, 0, buffer.Length);
		uploadChunk.Position = 0;

		var httpRange = new HttpRange(offset, buffer.Length);
		await fileShareClient.UploadRangeAsync(httpRange, uploadChunk);

		//Shift the offset by number of bytes already written
		offset += buffer.Length;
	}
}
```

說明:
- `blockSize = 4000000`: 4MiB其實應該是4194304個bytes，但是為了保留點彈性就直接使用4000000 bytes了。
- `BinaryReader`: 這邊是用BinaryReader一次讀N個bytes出來上傳。
- `ploadChunk.Write(buffer, 0, buffer.Length)`: 這邊用另一個`MemoryStream`來準備該次要寫入的資料。
- `UploadRangeAsync`: 這個就是C# SDK提供的非同步方法，其中就是使用`PUT Range`操作。
- `offset`: 最後就是每一個loop都位移N個bytes，以便寫入下一個`HttpRange block`。

## Conclusion
以上就是在`FileShare`的部分、關於檔案大小限制的說明及範例，其實`PUT Range`還有鎖定、清除等部分，簡單來說就是可以鎖定某段Range讓其他人不能更改、或是清除某段Range的資料，不過其他部分遇到機率比較小、目前也沒碰到需要鎖定或清除的需求，基本上都是一個檔一個檔在處理的，本文就不多所著墨了。

## References
1. [Azure Files REST API](https://learn.microsoft.com/en-us/rest/api/storageservices/file-service-rest-api)
2. [Operations on files](https://learn.microsoft.com/en-us/rest/api/storageservices/operations-on-files)
3. [Put Range](https://learn.microsoft.com/en-us/rest/api/storageservices/put-range#remarks)
