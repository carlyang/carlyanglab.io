---
title: Azure - Upgrading Function App from v3 to v4
categories:
 - 技術文件
 - Azure
date: 2022/10/17
updated: 2022/10/17
thumbnail: https://i.imgur.com/zPI1Sv5.png
layout: post
tags: [Azure, Function App]
---

![](/Teamdoc/image/azure/logos/function_app.png)

最近因為`Azure Ftunciton App`在[[MS Doc]](https://learn.microsoft.com/en-us/azure/azure-functions/functions-versions?tabs=azure-cli%2Cwindows%2Cin-process%2Cv4&pivots=programming-language-csharp)說明即將在2022-12-03之後，中止支援Function 2.x & 3.x版本，所以需要將我們所有用舊版的`Function App Rintime`全部升級到4.x版本，其中有些注意事項要處理的可以按照本文說明操作即可。
<!-- more -->

## Purpose
由於`Azure Ftunciton App`即將在2022-12-03以後停止支援2.x & 3.x版本，所以可能以後發生問題就無法再提供官方支援，建議還是能更新就更新吧!畢竟`Azure`服務有時候要靠微軟協助一些問題的調查，不是靠我們開發人員就可以查明的，`而且有些步驟需要手動調整、不是直接部署就可以成功，所以本文主要目的在記錄這些步驟以讓大家參考`。

引文如下。

>Beginning on December 3, 2022, function apps running on versions 2.x and 3.x of the Azure Functions runtime can no longer be supported. Before that time, please test, verify, and migrate your function apps to version 4.x of the Functions runtime.

## Steps
1. 查閱相關版本。
在我們更新前要先了解相關版本的支援，由下圖可知、要升級到4.x版本最低需搭配.NET 6。

![](/Teamdoc/image/azure/AzureFunction/FunctionAppUpgrade/FunctionAppUpgrade_1.png)

2. 更新`Microsoft.NET.Sdk.Functions`至最新穩定版4.1.3(依照當下版本而定)，`如果有必要則須調整Code!`

![](/Teamdoc/image/azure/AzureFunction/FunctionAppUpgrade/FunctionAppUpgrade_2.png)

3. 調整*.csproj檔
這邊我們直接進Project檔編輯XML，不熟悉的人也可以用IDE修改專案屬性。  
要更新專案檔內的`TargetFramework`為`net6.0`、`AzureFunctionsVersion`為`v4`。

```xml
<PropertyGroup>
	<TargetFramework>net6.0</TargetFramework>
	<AzureFunctionsVersion>v4</AzureFunctionsVersion>
</PropertyGroup>
```

### 接下來步驟分為Linux & Windows版的Function App更新步驟:
`請注意，以下皆為AP發佈上去Function App後才進行!`

4. 更新服務版本
Linux:
進入`Function App`的`Configuration` -> `Application settings`。
- 將`FUNCTIONS_EXTENSION_VERSION`改為`~4`。
- 確認`FUNCTIONS_WORKER_RUNTIME`為`dotnet`。

![](/Teamdoc/image/azure/AzureFunction/FunctionAppUpgrade/FunctionAppUpgrade_3.png)

- 進入`Function App`的`Configuration` -> `General settings`，將`.NET Version`改為`.NET 6`。

![](/Teamdoc/image/azure/AzureFunction/FunctionAppUpgrade/FunctionAppUpgrade_4.png)

- 重啟`Function App`，接著等待`Linux container`重啟完成(一般10分鐘內應該可以完成)。

Note:  
要觀察`contianer`的重啟狀況，我們需要查看`docker log`，但`Function App`我們無法直接下`docker`指令，所以我們需要透過工具查看及時的Logs，連結如下(也可以從`Azure Portal`的`Advance Tool`進入)。

```url
https://[FunctionAppName].scm.azurewebsites.net/api/logstream
```

Windows:
進入`Function App`的`Configuration` -> `Application settings`。
- 將`FUNCTIONS_EXTENSION_VERSION`改為`~4`。
- 確認`FUNCTIONS_WORKER_RUNTIME`為`dotnet`。

![](/Teamdoc/image/azure/AzureFunction/FunctionAppUpgrade/FunctionAppUpgrade_3.png)

- Windows版改`SDK`版本為 6.0，有兩種方式
	- azure cli:
		1. az account set -s [Subscription Name]
		2. az functionapp config set --net-framework-version v6.0 -n [FunctionAppName] -g [ResourceGroupName]

	- Resource Explorer:
		- 透過`https://resources.azure.com/`直接調整`XML`內的`netFrameworkVersion`為`v6.0`，詳細可參考下列網址。  
		[[MS Doc]](https://techcommunity.microsoft.com/t5/apps-on-azure-blog/issues-you-may-meet-when-upgrading-azure-function-app-to-v4/ba-p/3288983)

Note:  
以上`FUNCTIONS_WORKER_RUNTIME`為`dotnet`為例，如果是`powershell`則需要改`powerShellVersion`為`~6`。

## Conclusion
本文主要在記錄`Azure Function App`的升級步驟，由於之前不知道的時候，每次這種重大升級就要重建服務、想想真的是很不smart，現在學到正確的方法後以後就不擔心升級囉，希望能幫到大家。

## References
1. [Azure Functions runtime versions overview](https://learn.microsoft.com/en-us/azure/azure-functions/functions-versions?tabs=azure-cli%2Cwindows%2Cin-process%2Cv4&pivots=programming-language-csharp)
2. [Migrating from 3.x to 4.x](https://learn.microsoft.com/en-us/azure/azure-functions/functions-versions?tabs=azure-cli%2Cwindows%2Cin-process%2Cv4&pivots=programming-language-csharp#migrating-from-3x-to-4x)