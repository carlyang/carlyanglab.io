---
title: Azure - API Management中的跨網域請求
categories:
 - 技術文件
 - Azure
date: 2022/6/14
updated: 2022/6/14
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management]
---

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
相信大家對於跨網域請求(`CORS`)都不陌生，既然做為後端API肯定是要提供給各式各樣的來源呼叫，不太可能僅限定某個網站來使用，而且網站也不可能架設在`API Management`(下稱`APIM`)上，所以對於`APIM`來說每個`HTTP Request`一定都是跨域請求，本文目的在於說明如何使`APIM`允許跨域請求。
<!-- more -->

## 跨域請求
跨網域請求(`CORS`)其實是基於瀏覽器的安全性議題，因為對於瀏覽器來說、如果被植入某種惡意的`javascript`程式碼，不斷對某個`Site`進行`DDoS`攻擊是很危險的事情，所以除非服務端有特別允許`CORS`請求，瀏覽器才不會阻擋這些行為。

而服務端也可以使用`CORS`來允許全部或特定來源的跨網域請求，所以`APIM`作為統一的API入口，自然也提供`CORS`原則供使用。

## CORS Policy
在`APIM`中`CORS`原則是屬於`Inbound`區塊中使用的原則，我們可以設定允許的來源、`HTTP Method`、`HTTP Headers`等，詳細可參考[[MS Doc]](https://learn.microsoft.com/en-us/azure/api-management/cors-policy)，範例如下。

```xml
<cors allow-credentials="false | true" terminate-unmatched-request="true | false">
    <allowed-origins>
        <origin>origin uri</origin>
    </allowed-origins>
    <allowed-methods preflight-result-max-age="number of seconds">
        <method>HTTP verb</method>
    </allowed-methods>
    <allowed-headers>
        <header>header name</header>
    </allowed-headers>
    <expose-headers>
        <header>header name</header>
    </expose-headers>
</cors>
```

說明:
- `allow-credentials`: 這個用來設定`preflight request`中是否回應`Access-Control-Allow-Credentials`標頭。
- `terminate-unmatched-request`: 是否允許不符合原則的`HTTP Request`以200 OK回應終止要求。
- `allowed-origins`: 要允許的來源URI。
- `allowed-methods`: 要允許的`HTTP Method`。
- `allowed-headers`: 要允許的`HTTP Header`，我們可以使用自訂的標頭讓`APIM`識別。
- `expose-headers`: 指定可供用戶端存取的標頭。

## Conclusion
`CORS`原則可以增加`APIM`的安全性，並不是所有API的呼叫端都是瀏覽器，如果是後端對後端的呼叫、例如`SignalR`、不同服務間的存取等，這些情境就可以不需要使用`CORS`原則，但有很多情境下就必須要讓瀏覽器能夠正常呼叫，但我們又不能打開`CORS`就讓所有瀏覽器進行跨網域呼叫，這時候就可以用`CORS`原則僅限某些來源的要求通過。另一方面來說，我們也可以限定後端API只允許`APIM`存取、後端API不需要特別設定`CORS`，讓`CORS`這件事統一給`APIM`處理就好，以此簡化後端API的複雜度。

## References
1. [CORS](https://learn.microsoft.com/en-us/azure/api-management/cors-policy)