---
title: Azure - 如何使用Azure Data Factory搬移Blobs檔案
categories:
 - 技術文件
 - Azure
date: 2022/6/7
updated: 2022/6/7
thumbnail: https://i.imgur.com/oHcuJIO.png
layout: post
tags: [Azure, Azure Data Factory]
---

![](/Teamdoc/image/azure/logos/adf.png)

## Purpose
`Azure Data Factory`(下稱`ADF`)可以幫助我們處理大量資料的操作、`Pipeline`設計方便快速，其中提供了很多現成的`Activities`讓我們使用、是很方便的工具。最近有些過舊的`Blobs`檔案放在`Storage Account`上、又不常存取他們，所以想要來將他們做個備份、搬移到`Cool Blobs`去、而且費用也比較便宜，這時候就想到來用`ADF`處理這種大量資料的搬移。
<!-- more -->

## Activities
為了達成這個搬移`Blobs`的需求，我們會使用到`Copy data`、`Get metadata`、`if condition`及`Delete`四種`Activities`，分別說明如下。

1. `Copy data` activity
引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/data-factory/copy-activity-overview)說明如下。

>In Azure Data Factory and Synapse pipelines, you can use the Copy activity to copy data among data stores located on-premises and in the cloud. After you copy the data, you can use other activities to further transform and analyze it. You can also use the Copy activity to publish transformation and analysis results for business intelligence (BI) and application consumption.

也就是說，`ADF`的`Copy data`可以處理從地端到雲端之間的資料存放區，將他們複製各式各樣的目的地，以進一步做為資料分析、商業分析等應用，所以資料的處理是非常重要的，而且大量資料的處理效能也必須考慮進來，設定如下圖。

![](/Teamdoc/image/azure/ADFCopyBlobs/copy_data1.png)

說明:
這邊主要有兩個要注意的地方。
- 第一個是`Recursively`，它可以遞迴式地尋找資料夾及其子資料夾內的`Blobs`，因為我們的`Blobs`不一定只有一層資料夾。
- 第二個要注意的地方，就是`Delete files after completion`，它可以在複製操作結束後刪除來源資料。

2. `Get metadata` activity
引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/data-factory/control-flow-get-metadata-activity)說明如下。

>You can use the Get Metadata activity to retrieve the metadata of any data in Azure Data Factory or a Synapse pipeline. You can use the output from the Get Metadata activity in conditional expressions to perform validation, or consume the metadata in subsequent activities.

`Get metadata`可以幫助我們取得`中繼資料`資訊、也就是資料的描述，在本文的案例中主要是用來確保`Blob`是存在的狀況下才進行後續處理，如果`Blob`不存在就拿不到`metadata`也就不需要再繼續處理該`blob`，設定如下圖。

![](/Teamdoc/image/azure/ADFCopyBlobs/copy_data2.png)

說明:
- 這邊我們會使用自訂參數`DeleteDaysAgo`檢查730天前(也就是兩年前)的`Cool Blobs`。
```
@formatDateTime(addDays(utcNow(),pipeline().parameters.DeleteDaysAgo),'yyyy-MM-dd')
```

![](/Teamdoc/image/azure/ADFCopyBlobs/copy_data3.png)

- 在複製活動結束後，使用`Get metadata`取得兩年之前已經備份到`Cool blob`中的檔案並進行刪除，也就是我們會`定期備份近一年的Blobs、並刪除兩年以前的Cool Blobs`。
- 另一方面來說，如果只使用`Copy data` activity而不串接`Get metadata` activity，在找不到`Blob`的路徑的情況下、反而會收到`Delete` activity報錯，表示找不到該路徑下的`Blob`。

3. `if condition` activity
引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/data-factory/control-flow-if-condition-activity)說明如下。

>The If Condition activity provides the same functionality that an if statement provides in programming languages. It executes a set of activities when the condition evaluates to true and another set of activities when the condition evaluates to false.

`if condition`則是用來判斷某個條件下要進行不同的處理，在這邊是用來判斷自訂參數`IsDelete == true`的情況下，要進一步刪除檔案。

說明:
- 這邊很單純，就是判斷自訂參數`IsDeleteFiles` flag是否有打開、以及`Get metadata`是否存在，如果都是true則進入刪除操作。

```
@and(pipeline().parameters.IsDeleteFiles, activity('GetBlobMetadata').output.exists)
```

![](/Teamdoc/image/azure/ADFCopyBlobs/copy_data4.png)

4. `Delete` activity
引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/data-factory/delete-activity)說明如下。

>You can use the Delete Activity in Azure Data Factory to delete files or folders from on-premises storage stores or cloud storage stores. Use this activity to clean up or archive files when they are no longer needed.

`Delete`活動很單純就是刪除我們指定位置的檔案，當我們的自訂參數`IsDelete == true`時就會進入此活動實際刪除檔案。

![](/Teamdoc/image/azure/ADFCopyBlobs/copy_data5.png)

最後，整個`Pipeline`設計如下圖。

![](/Teamdoc/image/azure/ADFCopyBlobs/copy_data6.png)

再搭配排程`Trigger`就可以定期執行`Pipeline`了，本文案例是設定每日執行一次。

## Conclusion
`ADF`是很強大的`大量資料`處理的工具，可以進行搬移、刪除、轉換等動作，更重要的是它可以整合多種資料來源與目標，不論是從地端到雲端、雲端到雲端都有支援，而且也支援各式各樣大數據分析的`Data Warehouse`，例如`Data Analytics`、`Databricks`、`Machine Learning`、`Power Query`等，甚至也可以將資料送給我們自行開發的`Function App`、再依照不同的商業邏輯進行處理，真的是很好用的工具。

## References
1. [Copy activity in Azure Data Factory and Azure Synapse Analytics](https://learn.microsoft.com/en-us/azure/data-factory/copy-activity-overview)
2. [Get Metadata activity in Azure Data Factory or Azure Synapse Analytics](https://learn.microsoft.com/en-us/azure/data-factory/control-flow-get-metadata-activity)
3. [If Condition activity in Azure Data Factory and Synapse Analytics pipelines](https://learn.microsoft.com/en-us/azure/data-factory/control-flow-if-condition-activity)
4. [Delete Activity in Azure Data Factory and Azure Synapse Analytics](https://learn.microsoft.com/en-us/azure/data-factory/delete-activity)