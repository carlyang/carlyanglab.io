---
title: Azure - Access Restriction with Service Tag
categories:
 - 技術文件
 - Azure
date: 2021/9/13
updated: 2021/9/13
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, Access Restriction, Service Tag]
---
Author: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Purpose
之前的[[Azure Function - Access Restriction]](/2019/08/14/Skill/Azure/Azure_Function_Access_Restriction/)有提到可以在`Function App`、`App Service`等服務內的Networking中、使用`Access Restriction`功能來設訂存取的IP白名單，而設定的類型除了原有的`CIDR(IPv4)`外，還有`IPv6`、`Virtual Network`及`Service Tag`等，本文主要在說明使用`Service Tag`的部分。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Function App

## Introduction
[[Azure Function - Access Restriction]](/2019/08/14/Skill/Azure/Azure_Function_Access_Restriction/)說明如何使用`Access Restriction`功能來設定`CIDR(IPv4)`的白名單，但一個個指定IP其實很麻煩，加上IP是有可能會變的、所以是否有更好的方式來設定白名單呢?答案當然是有、那就是`Service Tag`，廢話不多說直接就接著說明如何使用。

## Service Tag
簡單來說`Service Tag`就是在`Azure`上針對公有雲上的服務，利用一個Tag標籤去對應它所屬的有那些IP，又因為這些IP可以隨著`Azure`的網路變更而更新，所以我們可以不用因為IP的變動、一直更新我們的`Access Restriction`白名單，我們可以到[[Download]](https://www.microsoft.com/en-us/download/details.aspx?id=56519)下載詳細的IP清單來參考。  

Note:  
其實根據`Azure`的不同地點、會有不同的`Service Tag` IP對應清單，`本文是以Azure Public公有雲上的作為說明對象`，其他的可以參考如下。
>- Azure Public
>- Azure US Government
>- Azure China
>- Azure Germany

`Service Tag`的位置其實就在`Access Restriction`的`Add Rule`中，跟原本設定`CIDR(IPv4)`的地方相同。

![](/Teamdoc/image/azure/AccessRestrictionServiceTag/service_tag1.png)
![](/Teamdoc/image/azure/AccessRestrictionServiceTag/service_tag2.png)

接著你就可以選擇`在Azure Cloud公有雲上的其他服務類型、來加入可存取的白名單中`，是不是很簡單呢?


![](/Teamdoc/image/azure/AccessRestrictionServiceTag/service_tag3.png)

## Not including all Service IPs
經過實際測試發現，有些服務的IP其實還沒有被設定進去`Service Tag`，所以如果想要確認要加入白名單的服務IP是否有在`Service Tag`的對應清單中，建議還是可以先到[[Download]](https://www.microsoft.com/en-us/download/details.aspx?id=56519)下載詳細的IP清單來確認看看，不過相信微軟還是會陸續把這些IP對應完成的，如果查不到可以等一段時間再來看看。  
另外，因為不同Location也會有不同的`Service Tag`設定，所以如果你不是在`Azure Cloud`上的服務的話，那就要先另外確認一下你的所屬服務範圍在何地囉(例如:`Azure China`、`Azure US Government`)。

## 結論:
`Service Tag`解決了以往`CIDR`難以維護的設定方式，僅用一組名稱即可以方便地處理白名單問題，真的是還不錯的功能喔!

## 參考
1. [[Set up Azure App Service access restrictions]](https://docs.microsoft.com/en-us/azure/app-service/app-service-ip-restrictions)
2. [[Virtual network service tags]](https://docs.microsoft.com/en-us/azure/virtual-network/service-tags-overview)