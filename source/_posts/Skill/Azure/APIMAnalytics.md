---
title: Azure - API Management的監控分析
categories:
 - 技術文件
 - Azure
date: 2022/5/7
updated: 2022/5/7
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management]
---

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
當我們使用`API Management`(下稱`APIM`)將許多後端API整合在統一的入口後，對於管理來說將會方便很多，例如:Token驗證、白/黑名單等。而且以前一支支後端API分開的狀態下，我們要查閱相關Log來除錯時也必須分開逐一查詢，雖然也不是很困難的操作、但就是比較麻煩也比較慢。`本文的目的就是要來介紹一下APIM的監控工具 - Analytics，它可以提供我們快速查詢的概要資訊，對於日常維護來說是很重要的一環`。
<!-- more -->

## Analytics
首先，我們先從`Azure Portal`進入`APIM`左方的監控區塊，可以看到`Analytics`功能如下圖紅框。

![](/Teamdoc/image/azure/APIMAnalytics/analytics1.png)

剛進入畫面如下圖，上方是查詢時間區間(`最小時間區間是15分鐘`)、下方則是各Tab功能區塊。

- `Timeline`:  
這個功能是依照時間序列依序繪製資料趨勢線，裡面有`Requests`數量、`Data Transfer`資料傳輸量、`Resposne Time`回應時長、`Cache`快取大小等變化趨勢，我們可以很直觀地看到數據的變化。

![](/Teamdoc/image/azure/APIMAnalytics/analytics2.png)

- Geography
這個功能可以讓我們觀察各統計值在不同地理位置的數量，例如: `Total requests`可以讓我們知道不同國家的`Requests`數量，這樣我們就能直觀地知道那裡的需求是最高的。

![](/Teamdoc/image/azure/APIMAnalytics/analytics3.png)

- APIs
這邊則是根據各API計算不同的統計量，像是`Success`、`Unahthorized`、`Failed`要求數量、`回應時間`等，我們可以在這邊看到某段時間內某一支API的多個統計值、了解各API的狀況。

![](/Teamdoc/image/azure/APIMAnalytics/analytics4.png)

- Operations
這個功能類似`APIs`，不過它是根據`APIM`內的不同`Operaion`去計算各統計值，也就是說前一個`APIs`是統計同樣的`API Path`、不同`HTTP Method`會視為同一個，但在這裡則會視為不同個，例如:`HTTP GET`及`HTTP POST`會視為兩個不同的`Operaion`。

![](/Teamdoc/image/azure/APIMAnalytics/analytics5.png)

- Product
這個功能是統計`APIM`內不同的`Product`來計算各統計值，`APIM`的`Product`可以看作一組具有相近特性或商業邏輯的一組`APIs`所組成。

![](/Teamdoc/image/azure/APIMAnalytics/analytics6.png)

- Subscriptions
這個功能則是計算`APIM`內不同的`Subscription`的各統計值，也就是不同的`HTTP Request`如果帶入相同的`Subscription Key`就會被統計在一起。

![](/Teamdoc/image/azure/APIMAnalytics/analytics7.png)

- Users
這個功能則是計算`APIM`內不同的`User`的各統計值，也就是不同`User`的`HTTP Request`會分開來統計。

![](/Teamdoc/image/azure/APIMAnalytics/analytics8.png)

- Requests
這個功能`個人覺得對於日常維護來說很重要、也是我最常使用的功能`，因為它記錄了每個`Request`的概要資訊，例如:`Response code`可以知道該次執行結果、`IP Address`可以查到來源IP等。

Note:  
有次因為公司的`APIM`有鎖`IP Filter`白名單，但從公司內網怎麼打就是回應`403 Forbidden`無法通過，最後就是靠這個功能才發現，原來透過`VPN`連進公司內網再到`Azure`是走公司防火牆後的對外IP、如果是直接在公司內部連`Azure`才是走`VPN`的對外IP出去，真是怎麼也想不到竟然`VPN`狀態卻不是走`VPN`的對外IP出去。

![](/Teamdoc/image/azure/APIMAnalytics/analytics9.png)

## Conclusion
`Analytics`這個功能真的是日常維護很好用的一套分析工具，雖然它沒有提供像`Application Insights`的詳細錯誤資訊，但是卻能夠非常快速地提供不少維運所需的概要資訊，有效幫助我們維運排除故障的需求。不過，還是建議除了使用這個功能的同時，還是要另外紀錄詳細的資訊，可以不用`Application Insights`、但至少也要完整保留一份詳細的Log，畢竟當我們要追查程式錯誤時，`Analytics`這個功能還是愛莫能助的。

## References
1. [Get API analytics in Azure API Management](https://learn.microsoft.com/en-us/azure/api-management/howto-use-analytics)