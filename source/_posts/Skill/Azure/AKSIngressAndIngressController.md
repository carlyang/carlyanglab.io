---
title: AKS - Ingress And Nginx Ingress Controller
categories:
 - 技術文件
 - Azure
date: 2023/11/20
updated: 2023/11/20
thumbnail: https://i.imgur.com/5sNBEdu.png
layout: post
tags: [Azure, AKS]
---

![](/Teamdoc/image/azure/logos/aks.png)

## Purpose
[前一篇](/2023/11/18/Skill/Azure/AKSServices/)為大家介紹了基本的`Service`概念及`AKS`內的建立方法，但`Service`僅作為集群內不同`Pods`互相間溝通之用，所以對於類型為`ClusterIP`的`Service`來說、我們只可以看到集群的內部IP，如果我們想要將`Pods`對外公開、讓外部也能夠來連線，就必須要靠`Ingress`的幫忙了!所以本文主要在說明`Ingress`的概念及`AKS`中建立`Ingress`的方法。
<!-- more -->

## Ingress
`Service`的作用是讓集群內的各`Pods`能夠互相溝通，不用擔心`Pods`的重啟導致內部IP變更等狀況，`Service`會幫我們自動更新與管理這些內部IP的變更，但是`Service`並不能處理由集群外部進來的路由，所以當我們想要讓外部透過如HTTP/HTTPS等協定來與集群內的應用程式互動時，就必須要有`Ingress`來協助處理這些路由、將不同的HTTP/HTTPS Request導向正確的`Service`，引用[[官方文件]](https://kubernetes.io/docs/concepts/services-networking/ingress/#what-is-ingress)說明如下。

>Ingress exposes HTTP and HTTPS routes from outside the cluster to services within the cluster. Traffic routing is controlled by rules defined on the Ingress resource.

![](/Teamdoc/image/azure/AKSIngressAndIngressController/ingress1.png)
<center>[[REF]](https://kubernetes.io/docs/images/ingress.svg)</center>

由上圖可知，當client發起一個Request後，會先經過一個由`Ingress`管理的`Load Balancer`(負責負載平衡)、然後才到下一步的`Ingress`經由設定好的規則(路由處理)將Request導向對應的`Service`，最後就會進入正確的(設定好的規則)`Pods`來處理Requesrt、並回傳Resposne回去給client。

## Install Nginx Ingress Controller

接下來，讓我們用`kubectl`指令實際安裝`Nginx Ingress Controller`試試。

1. 依照下列指令順序先登入azure、切換訂閱及取得Credentials(請先將括號的部分替換為你的相關資訊)，如果已經跑過可以忽略此步驟。

```cmd
az login

az account set --subscription [Subscription ID]

az aks get-credentials --resource-group [AKS Group Name] --name [AKS Name] --overwrite-existing 
```

2. 安裝`Nginx Ingress Controller`，這邊是安裝1.8.2版本，安裝完後如下圖的Output。

```cmd
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.8.2/deploy/static/provider/cloud/deploy.yaml
```

![](/Teamdoc/image/azure/AKSIngressAndIngressController/ingress2.png)

3. 接著我們確認一下安裝後`Nginx Ingress Controller`的目前狀態是否正常Running。

```cmd
kubectl get pods --namespace=ingress-nginx
```

![](/Teamdoc/image/azure/AKSIngressAndIngressController/ingress3.png)

4. (*Optional*)如果我們將來想要刪除`Nginx Ingress Controller`，可以執行以下命令。

```cmd
kubectl delete -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.8.2/deploy/static/provider/cloud/deploy.yaml
```

![](/Teamdoc/image/azure/AKSIngressAndIngressController/ingress4.png)

5. 確認正常運作後安裝的步驟就完成了，我們也可以進入`Azure Portal`的`Services and ingresses`查看，已經可以看到`ingress-nginx-controller`及`ingress-nginx-controller-admission`兩個跑起來了，其中的`ingress-nginx-controller`可以看到他有`External IP`，表示已經可以接收外部從這個IP連進來的連線。

![](/Teamdoc/image/azure/AKSIngressAndIngressController/ingress5.png)

***請注意，以下步驟如果沒有註冊對外的DNS，是無法正常連線到內部的建立的Deployment的，用IP直連也是不行的!***

6. 接著，讓我們先利用httpd建立一個測試用的`Deployment`並指定開放80 Port。

```cmd
kubectl create deployment mytest --image=httpd --port=80
```

7. 再來Expose這個`Deployment`。

```cmd
kubectl expose deployment mytest
```

8. 最後建立`Ingress`並且指定規則為`www.mydns.io/*`對應`mytest`的80 Port。

Note:  
如果DNS是正常有效的話，`Ingress`就可以正常forward給`mytest`、最後回傳`It works!`文字。

```cmd
kubectl create ingress mytest --class=nginx --rule="www.mydns.io/*=mytest:80"
```


## Conclusion
本文簡單說明了`Ingress`的概念與安裝`Nginx Ingress Controller`作為我們`Ingress Load Balancer`，但因為手邊沒有現成可用的`DNS`，所以在最後幾個步驟無法實際跑出來給大家看，但是基本概念與流程都是一樣的，如果各位手邊的環境有可用的`DNS`，照著步驟應該就可以順利跑出來了，其實也還有其他的套件可以協助`Nginx Ingress Controller`的安裝、例如:Helm，不過能夠熟悉`kubectl`指令是最基本的`aks`操作方式，也能夠幫助了解整個細部流程。

## References
1. [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/)
2. [Ingress Controllers](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/)
3. [Managed NGINX ingress with the application routing add-on](https://learn.microsoft.com/en-us/azure/aks/app-routing?tabs=default%2Cdeploy-app-default#enable-web-application-routing-via-the-azure-cli)
4. [Setting up Ingress with NGINX - Step by Step](https://spacelift.io/blog/kubernetes-ingress#setting-up-ingress-with-nginx--step-by-step)
5. [Azure](https://kubernetes.github.io/ingress-nginx/deploy/#azure)