---
title: Azure OpenAI Service (4) - 如何以Line Messaging API與AOAI進行單人對話
categories:
 - 技術文件
 - Azure
date: 2023/3/18
updated: 2023/3/18
thumbnail: https://i.imgur.com/xt5yNCJ.png
layout: post
tags: [Azure, Azure OpenAI Service]
---

![](/Teamdoc/image/azure/logos/openai.png)

## Purpose
[[上一篇]](/2023/03/13/Skill/Azure/OpenAI3/)簡單介紹了一下`Azure OpenAI Service`(下稱`AOAI`)的額度、限制及微調參數，接下來就要撰寫實際專案程式來體驗看看效果，本文的目的以`Line Messaging API`的單人對話為例、串接後端自建的`Line Bot API`來跟`AOAI`進行對話。
<!-- more -->

## Prerequisites
本文範例是以`ASP.NET Core 6.0`撰寫API服務，並部署在`Azure Web App`上，故開始前請先準備具有下列條件的環境。
- Azure Subscription
- Azure OpenAI Service
- Azure Web App
- Azure Application Insights

## Lien Messaging API
為了要實際串接`Azure OpenAI Service`的功能，本文選擇使用`Line Messaging API`來讓使用者可以透過手機App跟`Azure OpenAI Service`進行對話，請先註冊一個`Line Developer`的帳號，如[[連結]](https://account.line.biz/login?redirectUri=https%3A%2F%2Fdevelopers.line.biz%2Fconsole%2F)。

![](/Teamdoc/image/azure/OpenAI4/line_messaging_api1.png)

首先先建立`Provider`，也可以在下個步驟建立。
![](/Teamdoc/image/azure/OpenAI4/line_messaging_api2.png)

接著建立`Channel`(`Provider`欄位可以選擇已建立的、也可選擇新建的)，照著所有步驟一一填寫完成即可。
![](/Teamdoc/image/azure/OpenAI4/line_messaging_api3.png)

最後進入剛建好的`Channel`就可以看到`Messaging API`了。
![](/Teamdoc/image/azure/OpenAI4/line_messaging_api4.png)

接著我們進入`Messaging API`的設定頁，將`Channel Access Token`複製下來，稍後程式會用到。
![](/Teamdoc/image/azure/OpenAI4/line_messaging_api5.png)

Note:  
1. 因為只是單純地串接`Azure OpenAI Service`，所以只使用`Messaging API`的文字功能、簡單先處理`Reply Message`的部分，所以只會使用到`Channel Access Token`。
2. 當我們拿到`Azure OpenAI Service`的回應後，我們會需要將內容回覆給`Messaging API`，這時候需要用到`https://api.line.me/v2/bot/message/reply`這個URL，也請先記下來。
3. 詳細註冊`Line Messaging API`的步驟在此就不詳細說明，有很多的教學文章可以參考。
4. 請注意，因為我們註冊的是`Line Developer`的`Business Account`(也就是常見的官方帳號)，`如果要PUSH Message的話目前每月最多500則，但我們使用的Reply Message則沒有限制`。

## Sample說明
為了要將`Line Messagnig API`平台傳送過來的訊息、轉送到`Azure OpenAI Service`進行訓練及分析，我們會需要一支API來處理中間的介接工作，所以我使用`ASP.NET Core 6.0`建立一支`Azure Web App`服務，讓他在公開網路上為我們處理轉送與Reply的工作，範例如下。

### 完整程式碼範例請參考[[Samples.AzureOpenAI.LineBotAPI]](https://github.com/carlyang920/Samples.AzureOpenAI.LineBotAPI/blob/main/README.md)

1. API如下範例，我們建立一支`LineBotController`及`Message`這個Action，因此等後續部署上去`Azure Web App`後、需要提供一個`Webhook URL`給`Line Messaging API`使用。

```csharp
[ApiController]
[Route("api/[controller]")]
public class LineBotController : Controller
{
..............略

    [HttpPost("Message")]
    public IActionResult SendMessage(LineMessageModel request)
    {
        try
        {
..............略
```

2. 等我們部署上去`Azure Web App`後，就可以提供以下位址給`Line Messaging API`。

```url
https://[Your Web App Name].azurewebsites.net/api/LineBot/Message
```

請將上列位址填入`Messaging API`的`Webhook settings`中，如下圖紅框。

![](/Teamdoc/image/azure/OpenAI4/line_messaging_api6.png)

3. 接下來我們要來撰寫相關程式碼，如下。

```csharp
[HttpPost("Message")]
public IActionResult SendMessage(LineMessageModel request)
{
    try
    {
        ...略

        //這邊Line Messaging API會以陣列方式傳送Events過來，需要逐一處理。
        request.events.ForEach(e =>
        {
            //這邊我們暫時只先處理文字的部分，其他如圖片等事件我們直接略過先不處理。
            if (!e.type.Equals("message", StringComparison.CurrentCultureIgnoreCase)
                || !e.message.type.Equals("text", StringComparison.OrdinalIgnoreCase)) return;

            var text = e.message.text;

            //這邊每個事件都會帶入一串reply Token，是用來在回覆結果時要帶入Body之用，這樣Messaging API才會知道當下是回覆哪個事件。
            var replyToken = e.replyToken;

            //這邊就是將使用者輸入的文字轉送到Azure OpenAI Service進行訓練及分析。
            var answer = _openAIService.RequestAsync<AzureOpenAIModel>(
                    HttpMethod.Post,
                    JsonConvert.SerializeObject(new {prompt = text, max_tokens = 2048})
                )
                .ConfigureAwait(false)
                .GetAwaiter()
                .GetResult();

        ...略

            var replyMessages = new List<ReplyMessage>();

            //Azure OpenAI Service的回覆也是以陣列的方式回傳，我們將這些結果存進List再帶入Messaging API Reply的messages。
            answer?.choices.ForEach(c =>
            {
                replyMessages.Add(new ReplyMessage {type = "text", text = c.text});
            });

            //最後這邊就是將結果回傳給Messaging API，其中notificationDisabled = false是指定不提醒通知User。
            _lineBotService.ReplyAsync<dynamic>(
                    HttpMethod.Post,
                    JsonConvert.SerializeObject(new ReplyLineMessage()
                    {
                        replyToken = replyToken,
                        messages = replyMessages,
                        notificationDisabled = false
                    })
                )
                .ConfigureAwait(false)
                .GetAwaiter()
                .GetResult();
        });
    }
    catch (Exception e)
    {
        ...略

        return BadRequest();
    }

    return Ok();
}
```

4. 由於整個流程都是在雲端上運行，但是一旦發生錯誤往往都只能看到`HTTP Response`的`500 Internal Server Error`，所以我也在專案上安裝了`Application Insights SDK`、啟動應用程式記錄功能，也撰寫Class自訂Log內容上傳進`Application Insights`，方便事後追蹤錯誤。

```csharp
public class MonitorService
{
    //傳送遙測資料物件
    private readonly TelemetryClient _telemetry;

    public MonitorService(
        IConfiguration config
        )
    {
        var config1 = config.Get<ConfigModel>();

        _telemetry = new TelemetryClient(TelemetryConfiguration.CreateDefault())
        {
            //這邊使用InstrumentationKey來指定要傳送的目標Application Insights
            InstrumentationKey = config1.ApplicationInsights.InstrumentationKey
        };
    }

    public void ThrowMessageTrace(string functionName, string title, string message, Dictionary<string, string>? extraInfo)
    {
        extraInfo ??= new Dictionary<string, string>();
        extraInfo.AddValueIfKeyNotExist("CurrentTime", $"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff}");
        extraInfo.AddValueIfKeyNotExist("FunctionName", $"{functionName}");
        extraInfo.AddValueIfKeyNotExist("Title", $"{title}");
        extraInfo.AddValueIfKeyNotExist("Message", $"{message}");

        //這邊是自訂Trace訊息
        _telemetry.TrackTrace($"{message}", extraInfo);
    }

    public void ThrowException(string functionName, Exception ex, Dictionary<string, string>? extraInfo)
    {
        extraInfo ??= new Dictionary<string, string>();
        extraInfo.AddValueIfKeyNotExist("CurrentTime", $"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff}");
        extraInfo.AddValueIfKeyNotExist("FunctionName", $"{functionName}");

        ExceptionHelper.GetExInfo(ex, extraInfo);

        //這邊是自訂Exception訊息
        _telemetry.TrackException(ex, extraInfo);
    }
}
```

5. 還記得我們在[[Azure OpenAI Service (2) - 服務建立及API調用]]((/2023/03/10/Skill/Azure/OpenAI2/))有提到的`Azure OpenAI Service`的URL嗎?範例如下。

```url
https://{your-resource-name}.openai.azure.com/openai/deployments/{deployment-id}/completions?api-version={api-version}
```

有了上面的URL以及`Azure OpenAI Service`的`api-key`後，我們就可以使用下列的`AzureOpenAIService`為我們轉送訊息到服務去進行訓練及分析。

```csharp
public class AzureOpenAIService
{
    private readonly ConfigModel _config;
    private readonly HttpClientService _httpClientService;

    public AzureOpenAIService(
        IConfiguration config,
        HttpClientService httpClientService
    )
    {
        _config = config.Get<ConfigModel>();
        _httpClientService = httpClientService;
    }

    public async Task<TResponse?> RequestAsync<TResponse>(
        HttpMethod method, string? body)
    {
        var headers = new Dictionary<string, string>();

        //這邊要將api-key帶入HTTP Header
        headers.AddValueIfKeyNotExist("api-key", _config.AzureOpenAiConfig.APIKey ?? string.Empty);

        //下方_config.AzureOpenAiConfig.Address就是我們的Azure OpenAI Service URL
        var response = await _httpClientService.SendAsync(
            method,
            _config.AzureOpenAiConfig.Address!,
            null,
            body,
            headers
        );

        response.EnsureSuccessStatusCode();

        var content = await response.Content.ReadAsStringAsync();

        return JsonConvert.DeserializeObject<TResponse>(content);
    }
}
```

6. 還記得我們前面步驟先記下來的`Access Toekn`嗎?它就是用在`LineBotService`這邊帶入`HTTP Header`之用，用以向`Line Messaging API`驗證通行之用。

```csharp
public class LineBotService
{
    private readonly ConfigModel _config;
    private readonly HttpClientService _httpClientService;

    public LineBotService(
        IConfiguration config,
        HttpClientService httpClientService
    )
    {
        _config = config.Get<ConfigModel>();
        _httpClientService = httpClientService;
    }

    public async Task<TResponse?> ReplyAsync<TResponse>(
        HttpMethod method, string? body)
    {
        var headers = new Dictionary<string, string>();

        //這邊要將Access Token帶入Authorization Bearer XXXX中。
        headers.AddValueIfKeyNotExist("Authorization", $"Bearer {_config.LineBotConfig.AccessToken}");

        //這邊就是使用https://api.line.me/v2/bot/message/reply URL來進行回覆。
        var response = await _httpClientService.SendAsync(
            method,
            $"{_config.LineBotConfig.Address}{_config.LineBotConfig.Action}",
            null,
            body,
            headers
        );

        response.EnsureSuccessStatusCode();

        var content = await response.Content.ReadAsStringAsync();

        return JsonConvert.DeserializeObject<TResponse>(content);
    }
}
```

7. 最後，我們就可以進入手機將`LineBot`加入好友並開始進行對話，如下圖。

![](/Teamdoc/image/azure/OpenAI4/line_bot1.png)

## Conclusion
本文簡單先以`Line Messaging API`為例、自建`Azure Web App`服務來串接`Line Bot`與`Azure OpenAI Service`，並且先處理較單純的文字訊息來驗證整個流程的可行性，提供給大家一種應用情境的考量。不過，由於`Line Messaging API`的一些限制，目前使用下來僅`個人對話`的部分比較好用，如果將`Line Bot`加入群組反而衍生一些問題、造成使用上的困擾，所以就先暫時不處理群組訊息的部分，如果以後有更好的解決方案再分享給各位朋友。

## References
1. [Channel access token](https://developers.line.biz/en/reference/messaging-api/#channel-access-token)
2. [Send reply message](https://developers.line.biz/en/reference/messaging-api/#send-reply-message)
3. [Sample](https://github.com/carlyang920/Samples.AzureOpenAI.LineBotAPI)