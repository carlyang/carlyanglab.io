---
title: Azure Function - Access Restriction
categories:
 - 技術文件
 - Azure
date: 2019/08/14
updated: 2019/08/14
thumbnail: https://i.imgur.com/zPI1Sv5.png
layout: post
tags: [Azure, Azure Function, Function App, Access Restriction]
---
作者: Carl Yang

![](/Teamdoc/image/azure/logos/function_app.png)

## Azure Function
或稱為Function App，是Azure平台上提供的一種Serverless服務。所謂的Serverless不是說真的沒有Server，而是指開發人員不需要關注太多AP執行環境上的細節，Azure都已經幫你將環境準備好，開發人員可以專注在自己的商業邏輯開發上就好。而Azure Function就是提供這樣Serverless環境的服務，它也是遵循RESTful API的規格在進行資料交換，```但是RESTful Style是放諸四海皆準的協定，也就是說所有人都知道他要怎麼使用，如果隨便把我們的服務放上雲端，豈不是任何人都可以存取了呢?!本篇文章旨在說明如何透過設定、限制能夠存取Azure Function的遠端來源```。
<!-- more -->

## App Services
其實Azure上還有另一種Serverless服務，稱為App Services。而Azure Function其實就是基於App Services的架構上，多包一層SDK以一個個Function的形式開發及執行。對於開發人員來說易用、易懂，所以本篇的Access Restriciton設定也通用於目前的App Services服務中。

## Access Restriciton
Access Restriciton其實就是一種設定而已，沒有什麼複雜的處理，它就只支援兩種，第一種就是限制IP、第二種就是限制Virtual Networks。前者透過```[IPv4/IPv6]/[Submask]```的設定方式，將一段Range內的IP加入Allow或Deny清單中，後者就是要先建立Azure上的虛擬網路服務、然後針對各式服務進行群組設定、最後再把VNet加入Allow或Deny清單中，但由於VNet為另一種Azure服務，本文就不再多詳細說明VNet的部分。

## 設定步驟
- Step 1 進入Azure Function的Platform Feature &rarr; Networking  
![](/Teamdoc/image/azure/AzureFunction/AzureFunction1.png)

- Step 2 接著進入下方Configure Access Restriction  
![](/Teamdoc/image/azure/AzureFunction/AzureFunction2.png)

- Step 3 按下Add Rule  
![](/Teamdoc/image/azure/AzureFunction/AzureFunction3.png)

- Step 4 接著輸入各欄位，特別注意IP Address Block要使用CIDR的格式，最後按下Add rule即可生效。  
![](/Teamdoc/image/azure/AzureFunction/AzureFunction4.png)

## 關於IP Address Block
IP Address Block是以CIDR的Subnet格式進行設定，這邊幫大家複習一下IP的相關計算。  
Subnet主要分為IP及mask兩部分。以IPv4來說，例如:23.101.10.141/27，每個IP有四個數字、轉成二進制就是
>00010111.01100101.00001010.100 01101

也就是說每個數字是由8個0/1組成，最小為0、最大為255。
而mask: /27指的是```IP的二進位數值、由左到右為1的個數```。所以/27代表的數值就是
>11111111.11111111.11111111.111 00000

為何要有這兩串IP呢?```因為要把上下兩串做AND邏輯運算```，IP每位數 AND 1會保持原本的值、AND 0會變成0，最後轉回10進位的IP值就會發現，因為右方有五位數是被AND掉的(```五個二進位數可表示32個10進位數字```)，再扣掉```23.101.10.128```Network IP及```23.101.10.159```Broadcast IP後，它可以代表30個IP的範圍如下。
>23.101.10.129 ~ 23.101.10.158

所以最後Access Restriction可以依照最後有幾位數0，來判斷有多少IP是符合目前規則的。

## 結論:
透過Subnet的方式，我們可以很方便的設定IP範圍，不用一個個去Key、簡單易懂又方便管理，另外網路上也有很多的計算器可以幫我們計算IP範圍，其實我們連上一小節的計算都可以省掉喔。

## 參考
1. [IP Calculator](http://jodies.de/ipcalc)