---
title: Visual Studio - Remove all unused packages
categories:
 - 技術文件
 - Visual Studio
date: 2019/07/15
updated: 2019/07/15
thumbnail: https://i.imgur.com/gEzvRbT.png
layout: post
tags: [Visual Studio, NuGet Package, Unused]
---
作者: Carl Yang

![](/Teamdoc/image/VS/2019/NuGetPackingWithStaticFiles/VSWinIcon_100x.png)

## 目的
我們常使用很多的第三方元件來加速開發，所以也有許多元件間的相依性需要注意，如果沒有注意很可能就造成錯誤。所以元件間的相依性是很重要的，但一堆東西參考來、參考去，很難每一個相依性都顧好，甚至系統內存了一大堆沒用到的元件，卻總是被參考進來一起建置。這不但可能降低系統效能，也會使發佈後的Size變得很肥大。

本篇說明如何使用```Resharper```的```Refactor```功能，一次移除系統內所有未使用到的參考元件。
<!-- more -->

## Resharper
非常好用的工具，但也是授權軟體、需要支付費用才可使用。

## 使用步驟
1. 首先我們在方案上按右鍵，拉到下方選擇[```Refactor```] &rarr; [```Remove Unused References```]。
![](/Teamdoc/image/VS/2019/RemoveUnusedPackages/RemoveUnusedPackages1.png)

2. 接著再按右下角的[```Analyze Used References```]按鈕，它就會幫你偵測整個方案中已安裝的元件。
![](/Teamdoc/image/VS/2019/RemoveUnusedPackages/RemoveUnusedPackages3.png)

3. 分析完以後會告訴你那些沒有用到、那些有用到，如下圖。
![](/Teamdoc/image/VS/2019/RemoveUnusedPackages/RemoveUnusedPackages4.png)
<div style="text-align:center;">
[圖片來源](https://d3nmt5vlzunoa1.cloudfront.net/dotnet/files/2016/02/analyse_references_nuget.png)
</div>

4. 最後勾選你要移除的未使用元件，Resharper就會自動幫你移除並修改相關的設定，例如:Web.config、packages.config、*.csproj等等。
![](/Teamdoc/image/VS/2019/RemoveUnusedPackages/RemoveUnusedPackages5.png)
<div style="text-align:center;">
[圖片來源](https://d3nmt5vlzunoa1.cloudfront.net/dotnet/files/2016/02/remove_unused_references_nuget.png)
</div>

![](/Teamdoc/image/VS/2019/RemoveUnusedPackages/RemoveUnusedPackages6.png)

## 結論
在開發時元件之間很常有許多的相依性，不管是相依Framework、或是相依第三方元件都在所難免，當開發越來越多的功能、方案的相依性也越複雜，使用Resharper能夠幫助快速地整理這些資訊，也避免不可預期的錯誤發生。

## 參考
1. [Optimize NuGet references](https://blog.jetbrains.com/dotnet/2016/02/22/resharper-ultimate-10-1-eap-3/)