---
title: Azure Function - 如何使用(1)
categories:
 - 技術文件
 - Azure
date: 2019/11/14
updated: 2019/11/14
thumbnail: https://i.imgur.com/zPI1Sv5.png
layout: post
tags: [Azure, Azure Function, Function App]
---
作者: Carl Yang

![](/Teamdoc/image/azure/logos/function_app.png)

## Azure Function
或稱為Function App，是Azure平台上提供的一種Serverless服務。所謂的Serverless不是說真的沒有Server，而是指開發人員不需要關注太多AP執行環境上的細節，Azure都已經幫你將環境準備好，開發人員可以專注在自己的商業邏輯開發上就好。
<!-- more -->

## App Services
其實Azure Function就是基於App Services的架構上，多包一層SDK以一個個Function的形式開發及執行。

## 環境
請先準備好以下開發環境。
  - .NET Core 2.2 SDK
  - Visual Studio 2019
  - 安裝NuGet Package - Microsoft.NET.Sdk.Functions
  - 安裝NuGet Package - Microsoft.Azure.Functions.Extensions
  - 安裝npm install -g azure-functions-core-tools(如果要在local端測試驗證，需要此套件初始化相關設定檔，不一定要安裝)

## 開始
1. 新增Azure Functions專案
![](/Teamdoc/image/azure/AzureFunction/AzureFunction_Dev1.png)

2. 在專案按右鍵 &rarr; 新增Azure Function
![](/Teamdoc/image/azure/AzureFunction/AzureFunction_Dev2.png)

3. 填入Function Class名稱
![](/Teamdoc/image/azure/AzureFunction/AzureFunction_Dev3.png)

4. 選擇Http Trigger作為範例
![](/Teamdoc/image/azure/AzureFunction/AzureFunction_Dev4.png)

5. 接著IDE就會幫我們建好對應的Class，如下:
    ```csharp
    public static class TestFunction
    {
        [FunctionName("TestFunction")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            string name = req.Query["name"];

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            name = name ?? data?.name;

            return name != null
                ? (ActionResult)new OkObjectResult($"Hello, {name}")
                : new BadRequestObjectResult("Please pass a name on the query string or in the request body");
        }
    }
    ```

 - 說明:
     - 我們可以在Function本體中開始撰寫我們自己要處理的程式邏輯。
     - FunctionName: 是我們公開給外部呼叫的API ROUTE，例如: /api/TestFunction。
     - HTTP Trigger: 是基於RESTful Style的觸發Function，他可以讓外部透過HTTP Request、觸發我們的這個Function幫我們進行某項工作。
     - HTTP Trigger: 有幾個參數需要設定。
       - AuthorizationLevel: 他可以讓我們指定驗證層級，預設為Function。
      ```csharp
      public enum AuthorizationLevel
      {
          Anonymous = 0,
          User = 1,
          Function = 2,
          System = 3,
          Admin = 4
      }
      ```

       - methods: HTTP methods，及GET/POST/PUT/DELETE等等，這裡我們使用GET及POST，表示接受這兩種HTTP的呼叫。
       - Route: 預設值為null，可以讓我們另外指定Function的Route。
       - HttpRequest: 就是逤進來的Request內容，它就是Microsoft.AspNetCore.Http下的HttpRequest，可以讓我們處理進來的要求。
       - ILogger: Azure Function的Log工具，我們可以將發生錯誤時的資訊、透過它寫入Azure紀錄中，方便我們日後查找問題。

6. 接著將它發佈到Azure上(這裡我簡單引用微軟文件上的圖片，發佈流程其實都一樣，但我個人的Azure有些不方便放上來)。
![](https://docs.microsoft.com/en-us/azure/includes/media/functions-vstools-publish/functions-visual-studio-publish-profile.png)

![](https://docs.microsoft.com/en-us/azure/includes/media/functions-vstools-publish/functions-visual-studio-publish.png)

![](https://docs.microsoft.com/en-us/azure/azure-functions/media/functions-develop-vs/functions-vstools-app-settings.png)

![](https://docs.microsoft.com/en-us/azure/azure-functions/media/functions-develop-vs/functions-vstools-app-settings2.png)

7. 記得要去Azure上啟動Azure Functions。
![](/Teamdoc/image/azure/AzureFunction/AzureFunction_Dev5.png)

8. 我們也可以直接在Azure上測試它。
![](/Teamdoc/image/azure/AzureFunction/AzureFunction_Dev6.png)

9. ```其中有個地方要特別注意，就是我們發佈上去的Function URL需要帶入參數code```，它是一組自動產生的Key，我們必須要在呼叫端調用Function時設定這個值，否則是無法成功呼叫Azure Function的。
例如: /api/TestFunction?code=baodt0prW00Aue29oitA0xoZw3w1QTfwpF/dEi9gUE6UNzM1HZVhPg==
![](/Teamdoc/image/azure/AzureFunction/AzureFunction_Dev7.png)

## Debug Locally
當我們在開發時，免不了需要在Local跑看看、並測試一下正不正常，如果你現在是全新安裝Azure Function專案的話，應該會自動產生host.json及local.setting.json兩個檔案，而這兩個檔就是我們在local按下F5後、試跑需要的相關設定，應該不會有問題。  

但如果你是承接版控上的Repo，就有可能沒有這兩個檔案，因為預設版控會ignore掉這兩個檔，```這是合理的!```，因為你只在local端使用的東西，本就不需要也不應該放上去版控，但這卻會造成local debug時的錯誤、而沒辦法順利跑起來。  

這時候就要回到我們準備環境時有提到要安裝的東西。
>npm install -g azure-functions-core-tools

請確認已安裝好NPM環境，再執行上面這個指令、以安裝azure-functions-core-tools。

裝完後重開powershell、切換到專案目錄底下、再執行以下命令，它就會根據專案設定，自動幫你建立這兩個檔了。
>func init
或
>func init --force

Note:  
1. ```請特別注意，這個動作有可能連專案檔.csproj都被重新初始化```，導致原本安裝的一些套件設定被清空(我自己就發生了)，所以執行指令前先備份一下專案檔，之後再蓋回來就可以了。
2. 我的code因為本來就在版控Repo上，所以我可以直接Revert專案檔就好(```這時候就知道版控的重要了!!```)。

## 結論:
Azure Functions讓我們可以專注在開發自己需要的程式碼，不用擔心Runtime環境是否Ready(事實上都已經準備好)，可以方便又快速地發佈我們的服務。

## 參考
1. [Develop Azure Functions using Visual Studio](docs.microsoft.com/en-us/azure/azure-functions/functions-develop-vs)