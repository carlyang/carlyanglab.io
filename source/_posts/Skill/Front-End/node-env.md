---
title: 前端開發環境建置
categories:
 - 技術文件
 - Front-End
date: 2018/6/3 02:28:00
updated: 2018/6/15
thumbnail: /Teamdoc/image/node_env/NodeLogo.png
layout: post
tags: 
 - Node.js 
 - NPM
---
作者: Carl Yang

# [Front-End] 前端開發環境建置

## 建置前端Node.js的開發/執行環境，主要步驟如下:

 1. 安裝Nod.js 
 2. 使用NPM還原專案packages 
 3. Run local端的開發Server
<!-- more -->
根據Wiki對於Node.js開宗明義的定義:(詳細內容可參考Wiki內容)
>Node.js是一個能夠在伺服器端運行Javascript的開放原始碼、跨平台JavaScript執行環境。([_Wiki_](https://zh.wikipedia.org/wiki/Node.js))

如同開發專案時，
需要安裝相關的Framework及伺服器一樣，
前端開發也需要建立一個執行環境、以利開發作業進行。

以下開始依序說明各步驟:

 - ### 安裝Node.js:
可至官方網站下載適合系統的安裝檔，本例是使用([_8.11.2 LTS_](https://nodejs.org/en/))版本，
請先下載安裝程式。

<center>下載完成後即可執行安裝、一般狀況直接下一步直到結束即可。</center>
![](/Teamdoc/image/node_env/install_Node_1.png)
<center>版權宣告</center>
![](/Teamdoc/image/node_env/install_Node_2.png)
<center>預設安裝路徑為C:\Program Files\nodejs\，您也可以自訂安裝位置。</center>
![](/Teamdoc/image/node_env/install_Node_3.png)
<center>選取安裝項目(本例使用預設安裝)</center>
![](/Teamdoc/image/node_env/install_Node_4.png)
<center>按下Install就會依照設定、進行安裝。</center>
![](/Teamdoc/image/node_env/install_Node_5.png)
<center>安裝中</center>
![](/Teamdoc/image/node_env/install_Node_6.png)
<center>安裝完成</center>
![](/Teamdoc/image/node_env/install_Node_7.png)

<center>安裝完成後可CMD輸入指令查看版號。
`$ node -v`
</center>
![](/Teamdoc/image/node_env/node_version.png)

<center>Node.js中已包含了NPM套件管理工具，[NPM](https://www.npmjs.com/)是安裝/還原第三方套件的工具，安裝Node後可使用下列命令查看版本，未來與套件相關的使用都會用到它。
`$ npm -v`
</center>
![](/Teamdoc/image/node_env/npm_version.png)

 - ### 使用NPM還原專案packages:
套件的安裝及還原都需使用命令列方式進行，
當下達npm i指令後、即會根據當下目錄中的_package.json_安裝參考的各式套件，
並將下載下來的這些套件放進_node_modules_資料夾中。

還原所有相關套件、使用npm i(install的簡寫):
`$ npm i`
![](/Teamdoc/image/node_env/npm_use_1.png)

安裝特定套件，使用npm i [套件名稱] -g，如下例安裝hexo:
`$ npm i hexo-cli -g`
(* -g是global的簡寫，也就是指定此套件是否要全域安裝，如果沒有此參數則僅會安裝到當下專案目錄中。 *)
![](/Teamdoc/image/node_env/npm_use_2.png)

 - ### Run local端的開發Server
各家套件大多都有自己的樣板可以快速建立，
命令的使用上就需要參考各家不同的使用方式了，
這邊以hexo為例、簡要示範如何在local端run開發server。

如上方安裝完hexo-cli套件後，可執行hexo指令新增專案，如下例新增testHexo、hexo會自動幫我們在當下目錄中建立新的專案目錄[testHexo]。
`$ hexo init testHexo`
![](/Teamdoc/image/node_env/npm_use_3.png)

接著切換工作目錄進入testHexo、要先還原Hexo所使用到的相關套件，
`$ cd testHexo`
`$ npm i`
![](/Teamdoc/image/node_env/npm_use_4.png)

以上都成功完成後、就可run local server開始進行開發，而Hexo預設的port為4000。
`hexo s -p 8080`
(* s是Server的簡寫，表示要run hexo local server。-p則是指定local server要使用哪個port，這邊指定為8080，如不指定則預設為4000。 *)
![](/Teamdoc/image/node_env/npm_use_5.png)

這時就可以打開瀏覽器測試看看是否成功。
![](/Teamdoc/image/node_env/npm_use_6.png)

 - ### 備註:
Hexo是最近練習架設blog所使用的套件，
而前端的開發還有其他像webpack(模組整合工具)等等都是必備的前端套件，
此篇僅先就基本的node.js相關環境建置為主題說明。
