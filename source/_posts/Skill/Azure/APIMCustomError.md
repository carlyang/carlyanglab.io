---
title: Azure - 如何自訂APIM錯誤並回傳狀態及訊息
categories:
 - 技術文件
 - Azure
date: 2022/4/15
updated: 2022/4/15
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management]
---

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
`API Management`(以下簡稱`APIM`)是Azure上作為雲端微服務架構上的`Getway`(匣道器)，可以進行API版本控管、流量監控、權限管控等功能。雖然`APIM`已經內建了許多功能可以使用，但有些情境下仍然需要進行一些自訂的操作，本文目的在說明`如何自訂錯誤並回傳自訂的狀態及訊息給呼叫端`。
<!-- more -->

## Set-Body Policy
有使用過`APIM`的朋友應該會知道，`APIM`是以`XML`的方式進行設定，所以內建提供的許多功能也可以透過編輯`XML`來進行，而`set-body`就是其中一種用來處理`HTTP Request Body`的policy，引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/api-management/set-body-policy)如下。

>Use the set-body policy to set the message body for incoming and outgoing requests. To access the message body you can use the context.Request.Body property or the context.Response.Body, depending on whether the policy is in the inbound or outbound section.

也就是說，這個policy可以使用在`Inbound`及`Outbound`兩個區塊中，如果用在`Inbound`就可以根據傳上來的內容進行一些操作及判斷、再往後傳給`Backend`服務，如果是用在`Outbound`就可以處理後再轉傳給呼叫端。  

## On-Error Policy
另一個本篇要使用的功能就是`on-error`，它是`APIM`用來`Error handling`的功能、本身嚴格來說其實也不算是一般的policy(個人認為可以視為policies的`error handling policy`)，可以套用在許多policies的錯誤處理上，例如: `ip filter`等，引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/api-management/api-management-error-handling-policies)如下。

>By providing a ProxyError object, Azure API Management allows publishers to respond to error conditions, which may occur during processing of requests. The ProxyError object is accessed through the context.LastError property and can be used by policies in the on-error policy section. This article provides a reference for the error handling capabilities in Azure API Management.

那既然是`Error Handling`之用，像是回傳的`Header`、`body`內容等當然也是可以處理的，基本可使用的回傳範例如下。

```xml
<policies>
    <inbound>
        <base />
    </inbound>
    <backend>
        <base />
    </backend>
    <outbound>
        <base />
    </outbound>
    <on-error>
        <set-header name="ErrorSource" exists-action="override">
            <value>@(context.LastError.Source)</value>
        </set-header>
        <set-header name="ErrorReason" exists-action="override">
            <value>@(context.LastError.Reason)</value>
        </set-header>
        <set-header name="ErrorMessage" exists-action="override">
            <value>@(context.LastError.Message)</value>
        </set-header>
        <set-header name="ErrorScope" exists-action="override">
            <value>@(context.LastError.Scope)</value>
        </set-header>
        <set-header name="ErrorSection" exists-action="override">
            <value>@(context.LastError.Section)</value>
        </set-header>
        <set-header name="ErrorPath" exists-action="override">
            <value>@(context.LastError.Path)</value>
        </set-header>
        <set-header name="ErrorPolicyId" exists-action="override">
            <value>@(context.LastError.PolicyId)</value>
        </set-header>
        <set-header name="ErrorStatusCode" exists-action="override">
            <value>@(context.Response.StatusCode.ToString())</value>
        </set-header>
        <base />
    </on-error>
</policies>
```

## Custom Error
綜合以上所述，我們接下來就利用這兩個功能來根據傳進來的`JSON Body`、解析其內容與值並判斷是否回傳自訂的`HTTP Status`和`Message`。

首先，範例如下:
```xml
<policies>
    <inbound>
        <base />
        <set-body>@{
            var body = context.Request.Body.As<string>();
            //Get the rest of path string excepting suffix
            var path = context.Request.OriginalUrl.Path.Trim('/').Substring(context.Api.Path.Trim('/').Length);
            
            //Only Error Test path needs to check
            if (path.StartsWith("/api/error/test")){
                var source = (string)JObject.Parse(body)["ErrorName"];

                if (source.ToUpper() == "TESTERROR")
                {
                    throw new Exception("Error Happened");
                }
            }

            return body;
        }</set-body>
    </inbound>
    <backend>
        <base />
    </backend>
    <outbound>
        <base />
    </outbound>
    <on-error>
        <base />
        <choose>
            <when condition="@(context.LastError.Message.Contains("Error Happened")==true)">
                <return-response>
                    <set-status code="451" reason="Unavailable For Legal Reasons" />
                    <set-header name="Content-Type" exists-action="override">
                        <value>application/json</value>
                    </set-header>
                    <set-body>{ "statusCode": 451, "message": "Unavailable For Legal Reasons" }</set-body>
                </return-response>
            </when>
        </choose>
    </on-error>
</policies>
```

說明:
1. `set-body`可以接受用內嵌程式碼的方式進行處理，我們只要把我們自己的C# Code寫在`@{}`中即可。
2. `context.Request.OriginalUrl.Path`用來抓取當下進來的`Request`的`Suffix`路徑、也就是要轉傳到`Backend`的某支API的位置，以用來判斷是否要進入`on-error`。
3. 當路徑是`/api/error/test`時，我們就可以丟出Exception以讓`on-error`抓取錯誤並判斷處理。
4. `on-error`是當發生Exception時都會進入，但在這裡我們只想要判斷`Error Happened`這個錯誤而已，所以我還利用`choose`...`when`判斷式來判斷錯誤訊息。
5. 最後就是使用`set-status`自訂回傳狀態、`set-header`自訂回傳的`content-type`及`set-body`自訂回傳的`JSON Body`。

## Conclusion
能夠內嵌程式碼對於自訂這部分來說真的很好用，畢竟服務能提供的是大部份的共用功能，但是比較細部的一些邏輯還是需要自訂才行，`APIM`提供這樣的一些彈性操作是非常好的設計，畢竟各式各樣的應用都不同、很難不自訂操作。不過要注意`不是所有的Policies都可以內嵌程式碼喔!`這邊是使用`set-body`才行，還是要依照不同設計去看看是否適用才行。

## References
1. [API Management policy samples](https://learn.microsoft.com/en-us/azure/api-management/policies/)
2. [Set body](https://learn.microsoft.com/en-us/azure/api-management/set-body-policy)
3. [Error handling in API Management policies](https://learn.microsoft.com/en-us/azure/api-management/api-management-error-handling-policies)