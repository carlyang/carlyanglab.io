---
title: .NET Core - SonarQube Refactoring in C#(2)
categories:
 - 技術文件
 - .NET Core
 - SonarQube
date: 2021/06/19
updated: 2021/06/19
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [ASP.NET Core, SonarQube, Cognitive Complexity, Duplicated Block]
---
作者: Carl Yang

![](/Teamdoc/image/DotnetCore/Logo.png)

## Purpose
本文主要在說明如何解決`SonarQube`對於`C#`撰寫時，檢查到的`Cognitive Complexity`及`Duplicated Blocks`的解決方法，可能不是最佳解、但也可做為參考方式。
<!-- more -->

## Prerequisites
本文是使用ASP.NET Core 3.1作為開發環境進行練習，請先安裝下列項目。
- 請先安裝ASP.NET Core 3.1 SDK([Download](https://dotnet.microsoft.com/download/dotnet/3.1))

## Introduction
最近使用`SonarQube`檢查`C#`程式碼的品質以及重構時，發現了幾個有趣的複雜度問題，可能會造成我們系統效能不彰、背地裡做了太多沒有意義的動作。  
本文所要說明的兩個複雜度問題趕專案時其實很難兼顧、尤其每個需求來的都又快又急、開發時程不太夠的情況下，好在有`SonarQube`幫我們檢查出來，及早修正掉免得到時候造成災情才來彌補，就划不來了。

## `Cognitive Complexity`
暫且翻譯為中文`認知複雜度`，它指的是當我們在撰寫Code的時候，我們可能又是`foreach`、又是一堆`if`，然後通通擠在一個`Function`(或稱一件工作)內，這個時候因為`迴圈的Looping`、加上好幾個`if條件`造成程式理解上的困難，且很可能多做了很多多餘的動作，只為了最後判斷結果，這樣的程式結構對於其他人更是難以維護，引用`SonarQube`文件的說明如下。

>Cognitive Complexity is a measure of how hard the control flow of a method is to understand. Methods with high Cognitive Complexity will be difficult to maintain.

雖然只有上列短短幾句說明，但事實上我在重構程式的時候更是發現了很多不必要的動作，例如以下範例。

```csharp
foreach (var groupKey in mergeGroup.Keys)
{
    //...
    foreach (var record in content.Select((data, index) => new { data, index }))
    {
        if (preRow.Any())
        {
            //...
            foreach (var columnName in mergeGroup[groupKey])
            {
                if (groupKey == 1)
                {
                    var result = string.IsNullOrEmpty(columnName) ? string.Empty : columnName;
                }
                if (groupKey == 2)
                {
                    //...
                }
            }
            
            if (canBeMerge)
            {
                foreach (var columnName in mergeGroup[groupKey])
                {
                    //...
                }
            }
            else
            {
                    //...
            }
        }
        else
        {
            //...
        }
    }
}
```

以上範例去除掉一些不必要的Code，`foreach`及`if判斷式`都是真實上線系統的程式碼，主要可以分為以下幾點說明:
1. 細數`foreach`迴圈至少就有三層。
2. 再連`if判斷式`也算進來，複雜度至少就有五層。
3. 最內層還有這段Code  
   `var result = string.IsNullOrEmpty(columnName) ? string.Empty : columnName;`
   裡面的`?`也是`if判斷式`的一種，加進來變成第六層。

最後`SonarQube`的判斷結果，`Cognitive Complexity`高達`89`。沒錯，複雜度的計算不是一個`foreach`或`if`只+1、而是依據它往內幾層累加計算，層數越多`加分`越多，如下圖。

![](/Teamdoc/image/DotnetCore/SonarQube/SonarQube_Refactoring2/complexity1.png)

#### Resolving
基本上，這個複雜度問題很難有一套制式的方式去解決，也就是說並不是現在提供我的解法，其他人一定能通用。  
因為會有這樣的狀況出現應該都跟商業邏輯脫不了關係，也因為商業邏輯的複雜性、導致程式的`巢狀結構`很多層。
所以，要解決這種狀況可能有幾個方向可以思考。
- 必須要更深入理解及分析商業邏輯，然後將各`foreach`或`if判斷式`拆分開來。
- 適時地應該`跳出迴圈`(加`break;`)，或許`根本不需要進入內層迴圈`。
- 嘗試用`switch`取代`if判斷式`，因為`if判斷式`造成的複雜度比`switch`高。
- 思考將`整個Finction拆分成數個小Functions`的可能性，也可以有效減少複雜度。
- 設計使用`擴充方法`，將`if判斷式`放入擴充方法內、再回到原本`if判斷式`的地方改用擴充方法，例如下面這個範例。

```csharp
public static class DictionaryExtension
{
    public static Dictionary<string, string> AddValueIfKeyNotExist(this Dictionary<string, string> dic, string key, string value)
    {
        if (!dic.ContainsKey(key)) dic.Add(key, value);
        return dic;
    }
}
```

最後，概念上的解決方法可以表示如下(`這段Code只是示意，並不是可以Run的程式結構`)，最主要是想表達拆分後的結果。

```csharp
public void OperateA()
{
    var mergeGroup = GetData();//...Get data records

    var mergheGroupKeys = OperateB(mergeGroup);

    var groupKeys = OperateC(mergheGroupKeys);
}
public IEnumerable<MergheGroupKey> OperateB(IEnumerable<...> mergeGroup)
{
    var result;

    foreach (var groupKey in mergeGroup.Keys)
    {
        result = ...;//...
    }

    return result;
}
public IEnumerable<GroupKey> OperateC(IEnumerable<...> mergheGroupKeys)
{
    var result;

    foreach (var key in groupKeys)
    {
        switch(key)
        {
            case "1":
                //...
                break;
            case "2":
                //...
                break;
            default:
                result = null;
                break;
        }
    }

    return result;
}
```

## `Duplicated Blocks`
這個指的是程式的重複區塊>=2，也就是說相同的一段程式不應該類似`Copy & Paste`的方式重複使用，不僅維護困難、也造成程式流程的複雜度，最後就是一個小調整要到處修改很多地方的Code，就像學寫程式一定都聽過的一句警語 - `Don't repeat yourself`。

引用`SonarQube`的說明如下:

>When two methods have the same implementation, either it was a mistake - something else was intended - or the duplication was intentional, but may be confusing to maintainers. In the latter case, one implementation should invoke the other.

也就是說`相同的實作程式碼、不應該重複出現在不同的Functions，而是應該要實作在一個Function內、但在很多地方共用這個實作Function`，而且`SonarQube`判斷重複的行數有一定行數才會顯示`Duplicated Blocks`、並非一行code就重複，至於多少行判斷重複就要看當下的`SonarQube`設定了，現在就讓我們以發送Mail為例來說明，如下案例。

```csharp
public abstract class SampleAService
{
    protected void NotifyMail()
    {
        var subject = "SampleA mail subect";
        var receiver = "userSampleA@email.com";
        var content = "This is SampleA email";

        var mailInfo = new MailInfo()
        {
            SystemName = "SystemA",
            Subject = subject,
            From = "systemA@email.com",
            Receivers = receiver,
            Content = content,
            CreatedBy = "SampleA"
        };

        //...Send mail actually
    }
}
public abstract class SampleBService
{
    protected void NotifyMail()
    {
        var subject = "SampleB mail subect";
        var receiver = "userSampleB@email.com";
        var content = "This is SampleA email";

        var mailInfo = new MailInfo()
        {
            SystemName = "SystemB",
            Subject = subject,
            From = "systemB@email.com",
            Receivers = receiver,
            Content = content,
            CreatedBy = "SampleB"
        };

        //...Send mail actually
    }
}
```

由上面的案例來看，我們可以發現`SampleAService`及`SampleBService`都需要發送通知信(`NotifyMail()`)，差別只在於一些傳送值的不同，像這種情況`SonarQube`就會告訴你有程式區塊重複了，你應該要抽出來共用Function。

這邊直接用繼承方式改寫如下:

```csharp
public abstract class SampleBaseService
{
    protected void NotifyMail(
        string systemName,
        string subject, 
        string from, 
        string receiver, 
        string content, 
        string createdBy
        )
    {
        var mailInfo = new MailInfo()
        {
            SystemName = systemName,
            Subject = subject,
            From = from,
            Receiver = receiver,
            Content = content,
            CreatedBy = createdBy
        };

        //...Send mail actually
    }
}
public abstract class SampleAService : SampleBaseService
{
    protected void Notify()
    {
        NotifyMail(
            "SystemA",
            "SampleA mail subect", 
            "systemA@email.com", 
            "userSampleA@email.com", 
            "This is SampleA email", 
            "SampleA"
        );
    }
}
public abstract class SampleBService : SampleBaseService
{
    protected void Notify()
    {
        NotifyMail(
            "SystemB",
            "SampleB mail subect", 
            "systemB@email.com", 
            "userSampleB@email.com", 
            "This is SampleB email", 
            "SampleB"
        );
    }
}
```

`SampleAService`及`SampleBService`共同繼承了抽象類別`SampleBaseService`，如此就可以共用`NotifyMail()`、減少`Duplicated Blocks`的情況。  
但重複的情況還有很多種，無法一一列舉出來，像我就遇過判斷`HTML`重複的狀況，這就要看是怎樣重構`HTML`的結構、抽出來變成共用HTML Components...等作法。

## 結論
本篇主要在說明`Cognitive Complexity`及`Duplicated Blocks`兩種狀況，會造成系統不易維護、難以理解及調整的狀況，我們也可以藉此提醒自己盡量不要踏入這樣的陷阱中。  
但是，基本上這兩種情況還是需要因地制宜、找出當下最合適的優化方式來處理，而不是照本宣科使用本文所列示方式調整，本文僅僅只能就概念上或思考解決方向提供給各位朋友參考。

## 參考
1. [[Cognitive Complexity of methods should not be too high]](https://rules.sonarsource.com/csharp/RSPEC-3776?search=Cognitive%20Complexity)
2. [[Methods should not have identical implementations]](https://rules.sonarsource.com/csharp/tag/duplicate/RSPEC-4144)