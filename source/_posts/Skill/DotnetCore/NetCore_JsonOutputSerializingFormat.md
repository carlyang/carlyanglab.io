---
title: .NET Core - JSON Serializer for HTTP Response
categories:
 - 技術文件
 - .NET Core
date: 2019/12/25
updated: 2019/12/25
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [.NET Core, .NET Core 2.2, JSON Serializer Settings]
---
作者: Carl Yang

![](/Teamdoc/image/DotnetCore/Logo.png)

## JSON Serializer
.NET Core中針對HTTP Request/Response的反序列化/序列化是使用Json.Net處理，所以有些相關的功能也是通用的，本篇主要說明如何在HTTP Response的時候，設定序列化後的格式。
<!-- more -->

## Camel Case
.NET Core預設是使用Camel Case格式進行HTTP Response的JSON序列化，也就是首字母小寫的格式。因此，如果使用像JsonResult()或OkObjectResult()等類型進行回傳的話，就會得到下列結果。

Data Model:
```csharp
public class ResponseModel
{
    public bool IsSuccess { get; set; } = false;
    public string Result { get; set; }
}
```
Serialized JSON:
```csharp
{
    "isSuccess": true,
    "result": "Success"
}
```

我們可以發現，原本Model中的屬性首字母都是大寫，經過序列化後都變成了小寫，這可能造成Client端收到Body反序列化時無法正確Mapping到Model上(如果沒有特別處理大小寫格式的話)。

`通常我們會希望自訂的Response Model屬性長怎樣、回應就會長怎樣`，但事實上.NET Core預設的格式卻是Camel Case格式，那要如何做才能按照我們所希望的處理呢?

## IContractResolver
根據以上的描述，直覺地就會想到、用Json.Net的IContractResolver指定格式就可以處理，事實上也的確如此。我們可以在StartUp的ConfigureServices()註冊MVC Services，如下方Code。
```csharp
services.AddMvcCore().AddJsonOptions(opt =>
{
    opt.SerializerSettings.ContractResolver = new DefaultContractResolver()
    {
        NamingStrategy = new DefaultNamingStrategy()
    };
});
```
透過指定ContractResolver.NamingStrategy = new `DefaultNamingStrategy`就可以依照Mode屬性的大小寫進行序列化操作，並且`作用於全域的HTTP Response`上。

## AddMvc() v.s. AddMvcCore()
不過註冊後實際使用發現，透過AddMvcCore()註冊的回應會失敗、收到`HTTP Status 406 - Not Acceptable錯誤`，如下圖右上角的狀態。 
![](/Teamdoc/image/DotnetCore/http/postman1.png)

再仔細看下方回傳的Headers會發現，原本應該有的Content-Type不見了!所以收到回應時，不知道該如何處理Body內容。
這是因為`AddMvcCore()`比起`AddMvc()`少了`AddJsonFormatters`動作，所以kestrel不會在HTTP Response的Hesders加上Content-Type([詳細請參考](https://blog.yowko.com/aspdotnet-core-addmvc-addmvccore/))，也使得Client端最後無法識別回傳內容而顯示406錯誤。

## 安裝Microsoft.AspNetCore.Mvc.FormattersJson
讀者應該會發現，在我們要使用`AddJsonFormatters()`解決這個問題時，卻沒有這個方法可使用，這是因為沒有安裝`Microsoft.AspNetCore.Mvc.FormattersJson`套件，所以要解決以上的問題，就來安裝一下這個套件吧!

PM CMD:
```
Install-Package Microsoft.AspNetCore.Mvc.Formatters.Json
```
IDE:
![](/Teamdoc/image/DotnetCore/http/formatter1.png)

安裝完以後，接著再回到ConfigureServices()加入註冊如下。
```csharp
services.AddMvcCore().AddJsonFormatters();
```

最後，我們再用Postman試打看看，就成功收到HTTP Status 200了。
![](/Teamdoc/image/DotnetCore/http/postman2.png)

總結一下最後註冊的Code。
```csharp
services.AddMvcCore().AddJsonFormatters();
services.AddMvcCore().AddJsonOptions(opt =>
{
    opt.SerializerSettings.ContractResolver = new DefaultContractResolver()
    {
        NamingStrategy = new DefaultNamingStrategy()
    };
});
```

## 結論
.NET Core的優點就在於可以讓我們把要用到的東西載入就好，藉此減少Package的大小、提升Runtime的效率，不過缺點就是要弄清楚需要用的東西在哪個套件中。

再回到原本的Serialize問題，大多數情況應該都會希望HTTP回應的資料，要與我們的設計完全相同，而通常屬性我們不會設定首字母小寫，除非是程式內部的Functions、才會以動詞開頭並首字母小寫(`其實也有點難理解為何.NET Core要預設為Camel Case格式，明明直接使用Json.NET就是DefaultNamingStrategy，透過.NET Core的ActionResult預設格式反而不一樣，何況它內部也是用Json.Net進行處理的`)，不過至少有個變通的方法可以解決就是了。

## 參考
1. [ASP.NET Core 中 AddMvc() 與 AddMvcCore() 的差別](https://blog.yowko.com/aspdotnet-core-addmvc-addmvccore/)
2. [ASP.NET Core WebAPI 回應 406 Not Acceptable](https://blog.yowko.com/aspdotnetcore-webapi-406-not-acceptable/)