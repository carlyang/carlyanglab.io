---
title: Azure - 如何在Azure Data Factory中調用Store Procedure
categories:
 - 技術文件
 - Azure
date: 2022/8/8
updated: 2022/8/8
thumbnail: https://i.imgur.com/oHcuJIO.png
layout: post
tags: [Azure, Azure Data Factory]
---

![](/Teamdoc/image/azure/logos/adf.png)

## Purpose
`Azure Data Factory`(下稱`ADF`)既然是處理大量資料的工具，自然也就少不了針對`Azure SQL DB`的操作，其中執行`Store Procedure`的需求一定是免不了的，所以本文目的就在介紹`Store Procedure` Activity的使用與設定。
<!-- more -->

## Store Procedure Activity
引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/data-factory/transform-data-using-stored-procedure)如下。

>You use data transformation activities in a Data Factory or Synapse pipeline to transform and process raw data into predictions and insights. The Stored Procedure Activity is one of the transformation activities that pipelines support. This article builds on the transform data article, which presents a general overview of data transformation and the supported transformation activities.

由上可知，`Store Procedure` Activity主要是用來轉換資料之用，也就是說我們可以將來源資料送進某隻`Store Procedure`、讓它幫助我們處理資料、就如同我們平常透過`Store Procedure`操作資料庫一樣。

而`Store Procedure` Activity可以幫助我們調用下列服務中的預存程序。

>- Azure SQL Database
>- Azure Synapse Analytics
>- SQL Server Database. If you are using SQL Server, install Self-hosted integration runtime on the same machine that hosts the database or on a separate machine that has access to the database. Self-Hosted integration runtime is a component that connects data sources on-premises/on Azure VM with cloud services in a secure and managed way. See Self-hosted integration runtime article for details.

其中比較特殊的是`SQL Server Database`、這是指地端的`SQL Server`。但是`ADF`是雲端服務無法直接存取地端內網中的`SQL Server`，所以必須另外先建立[self-hosted integration runtime](https://learn.microsoft.com/en-us/azure/data-factory/create-self-hosted-integration-runtime?tabs=data-factory)。

## Configuration
接下來就讓我們以`Azure SQL DB`為例，開始建立`Store Procedure` Activity。

1. 建立`Linked Service`
進入`Azure Data Factory Studio`後進入左方的`Manage` -> `Linked services` -> `New`，如下圖。
![](/Teamdoc/image/azure/ADFStoreProcedure/sp_activity1.png)

接著選擇`Azure SQL Database`，如下圖。
![](/Teamdoc/image/azure/ADFStoreProcedure/sp_activity2.png)

輸入相關必填資訊，如下圖。
![](/Teamdoc/image/azure/ADFStoreProcedure/sp_activity3.png)

說明:
- `Name`: 要建立的`Linked services`名稱，不可與其它的重複。
- `Connect via integration runtime`: 這邊使用預設的即可。
- `Account selection method`: 這邊指的是你要在訂閱中直接挑選或是另外手動輸入相關資訊，使用訂閱的方式好處是會列出訂閱中你可以存取的DB名稱、直接挑選就好，手動輸入的話就必須要一個個填入完整資訊了(例如`XXX.database.windows.net`)，基本上差異不是很大。
- `Server name`: 如果選擇從訂閱中挑選就會出現這欄位讓你挑選。
- `Fully qualified domain name`: 如果選擇手動輸入就會出現這欄位讓你Key完整的domain。
- `Database name`: 就是`SQL Server`中你要存取的DB名稱。
- `Authentication type`: 驗證方式，基本上從`ADF`存取DB都是用`SQL authentication`、也就是`Azure SQL master內建立的使用者帳密`。
- `User name`: 登入使用者名稱。
- `Password`: 這邊可以選擇直接輸入密碼，或是改去`Azure Key-Vault`拿`Secret`密鑰來用(使用`Azure Key-Vault`可以確保機密資訊更不容易被盜用)。

2. 設定 Store Procedure Activity
接著我們拉一個`Store Procedure` Activity到UI上然後開始設定，先進入General tab，如下圖。
![](/Teamdoc/image/azure/ADFStoreProcedure/sp_activity4.png)

說明:
- `Name`: 活動名稱。
- `Description`: 說明及描述。
- `Timeout`: 執行的timeout時間(`最大可到7天、預設是12小時`)，它設定的格式是`D.HH:MM:SS`，`要注意它的格式設定比較不同!`
- `Retry`: 重試次數，預設0次表示不重試。
- `Retry interval (sec)`: 重試間隔秒數，預設30秒。
- `Secure output`: 勾選時表示不將執行結果存進log，以確保資料安全性。
- `Secure input`: 勾選時表示不將輸入參數存進log，以確保資料安全性。

再來進入Settings tab設定，如下圖。
![](/Teamdoc/image/azure/ADFStoreProcedure/sp_activity5.png)

說明:
- `Linked service`: 下拉選擇我們剛建立連結的`Azure SQL Database`。
- `Description`: 下拉選擇其中我們要執行的SP。
- `Store procedure parameters`: SP的輸入參數，如果沒有直接留空即可。

最後可再搭配`Trigger`設定排程時間，就可以週期性地執行`Pipeline`囉!

## Conclusion
本文說明了如何在`ADF`內使用`Store Procedure` Activity來執行SP，基本上關於資料的處理應該都跑不掉執行`Store Procedure`的動作，所以`Store Procedure` Activity也是非常實用的功能。

## References
1. [Transform data by using the SQL Server Stored Procedure activity in Azure Data Factory or Synapse Analytics](https://learn.microsoft.com/en-us/azure/data-factory/transform-data-using-stored-procedure)
2. [Create and configure a self-hosted integration runtime](https://learn.microsoft.com/en-us/azure/data-factory/create-self-hosted-integration-runtime?tabs=data-factory)