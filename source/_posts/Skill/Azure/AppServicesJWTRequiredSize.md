---
title: Azure - JWT Signing Key Required Size
categories:
 - 技術文件
 - Azure
date: 2024/1/12
updated: 2024/1/12
thumbnail: https://i.imgur.com/OTE99ml.png
layout: post
tags: [Azure, App Services]
---

![](/Teamdoc/image/azure/logos/web_app.png)

## Purpose
最近更新`.NET 6`的套件`Microsoft.AspNetCore.Authentication.JwtBearer`到`6.0.26`版本後發現，原本使用的`SymmetricSecurityKey`竟然不能使用了!於是就有了本文來分享一下其原因及解決方法。
<!-- more -->

## Issue
一開始是因為更新套件到新版`6.0.26`後，突然報以下錯誤訊息。

```
IDX10720: Unable to create KeyedHashAlgorithm for algorithm 'HS256', the key size must be greater than: '256' bits, key has '160' bits.
```

但很奇怪，這次僅是更新套件版本而已，並沒有改什麼Code或是資料。

## Root Cause
接著就是一段一段追查Code，先是發現錯誤發生在下面這一行，但是這一行是因為傳入的的變數`sec(JwtSecurityToken)`造成的。

```csharp
var token = new JwtSecurityTokenHandler().WriteToken(sec);
```

於是我又往內追到產出`JwtSecurityToken`的部分，最後發現原因是下面這一段Code。

```csharp
private JwtSecurityToken GenJwtSecurityToken(string name, string signingKey, int expiredMinutes)
{
    var claims = new[]
    {
        new Claim(JwtRegisteredClaimNames.Sub, name),
        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
        new Claim(JwtRegisteredClaimNames.Iat, DateTime.Now.ToString(CultureInfo.InvariantCulture)),
    };
    var privateKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(signingKey));
    var credentials = new SigningCredentials(privateKey, SecurityAlgorithms.HmacSha256);

    return new JwtSecurityToken(
        issuer: _config.Jwt.Issuer,
        audience: _config.Jwt.Issuer,
        claims: claims,
        expires: DateTime.Now.AddMinutes(expiredMinutes),
        signingCredentials: credentials);
}
```

最後，我們發現是因為我們使用的是`SecurityAlgorithms.HmacSha256`演算法、`但是new SymmetricSecurityKey時傳入的Key卻不足256 bits`。  
這是因為在`6.0.26`版以後，會強制要求使用不同演算法時、傳入的Key長度要符合該演算法的`最低長度要求`!詳細可以參考[[IDX10720]](https://github.com/AzureAD/azure-activedirectory-identitymodel-extensions-for-dotnet/wiki/IDX10720)

引用說明如下:

```
The numbers and name of the algorithm can vary (HS256 requires 256 bits, HS384 requires 384 bits, HS512 requires 512 bits.).
```

更精確來說，是因為`Microsoft.AspNetCore.Authentication.JwtBearer 6.0.26`版使用了新的`Microsoft.IdentityModel 6.30.1`版本、而該版本則修正了以前沒有要求演算法對應的最小長度要求，但是以前沒有要求這個最小長度的狀況下，`反而會造成JWT的安全性下降`!

## Solution
所以，其實解法很簡單，以我們使用`SecurityAlgorithms.HmacSha256`演算法來說，就是把傳入的Key拉長到256 bits(以Unicode來說就是32個字)就可以解決這個問題了。

## Conclusion
`Microsoft.AspNetCore.Authentication.JwtBearer`是我們很常使用產生`JWT`的套件，以前真的沒有注意到這個會造成安全性下降的問題，好在微軟有修正這個安全性問題，不然以後發生狀況可就難處理了。

## References
1. [IDX10720](https://github.com/AzureAD/azure-activedirectory-identitymodel-extensions-for-dotnet/wiki/IDX10720)
