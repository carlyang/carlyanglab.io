---
title: Storage Account - Blob Container
categories:
 - 技術文件
 - Azure
date: 2018/12/30
updated: 2018/12/30
thumbnail: https://i.imgur.com/8uMcIBL.png
layout: post
tags: [File Upload, Blob]
---
作者: Carl Yang

![](/Teamdoc/image/azure/logos/storage_account.png)

## Blob Storage
檔案的儲存是個非常大眾化的資料儲存方式，舉凡文字、影像、影片等等，而Azure也提供檔案儲存的服務-Blob儲存體，他屬於Azure Storage Account中的一環，當有需要分享各式檔案至外部、內部又沒有可用資源如File Server時，就可以考慮將檔案放到雲端服務中，以供外部存取。

## IND 4.0中的檔案儲存
工業4.0中的機台資料量很大，如果都用RDB、NonSQL DB等方式儲存，既耗資源、也占用空間，且機台資料的收集是為了未來深入分析之用，如果能夠直接放上雲端、就可以利用雲端龐大的運算能力，為我們的資料進行大數據分析、ML等等應用。本篇文章旨在說明如何將機台收集來的資料、以檔案的方式放上Azure blob Storage，以共後續使用。
<!-- more -->

## 安裝元件
請先安裝下列Azure SDK
- Microsoft.Azure.Storage.Blob

Note: 
請注意另有WindowsAzure.Storage，為舊版的StorageAccount SDK，其整合了Blob、Table、Queue等等，但從9.4.0版以後，就切開為Microsoft.Azure.Storage.Blob/Microsoft.Azure.Storage.Queue/Microsoft.Azure.Storage.File三個SDK，本篇使用Microsoft.Azure.Storage.Blob 9.4.1版本。

## Blob Container
- Step 1 建立Blob Container
進入Storage Account -> Blobs -> [+Container] -> 輸入Container Name -> 選擇Public access level為Container，並記下剛新增的Blob Name、稍後程式會需要此資訊。
![](/Teamdoc/image/azure/blob/blobsetting1.png)

- Step 2 Connection String
回到Storage Account -> 進入Access Keys -> 記下Connection String，稍後程式會需要此資訊。
![](/Teamdoc/image/azure/blob/blobsetting2.png)

## 程式範例

- 建立要傳入的Blob Configuration Class
```csharp
public class BlobConfigInfo
{
	public string AzureStorageConnectionString { get; set; }
	public string BlobName { get; set; }
	public string ProxyUrl { get; set; }
}
```

- 建立要使用的Proxy DelegatingHandler Class
```csharp
public class BlobProxyHandler : DelegatingHandler
{
	public int CallCount { get; private set; }

	private readonly IWebProxy _proxy;

	private bool _firstCall = true;

	public BlobProxyHandler() : base()
	{

	}

	public BlobProxyHandler(HttpMessageHandler httpMessageHandler) : base(httpMessageHandler)
	{

	}

	public BlobProxyHandler(IWebProxy proxy)
	{
		this._proxy = proxy;
	}

	protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
	{
		CallCount++;
		if (_firstCall && this._proxy != null)
		{
			var inner = (HttpClientHandler)this.InnerHandler;
			inner.Proxy = this._proxy;
		}
		_firstCall = false;
		return base.SendAsync(request, cancellationToken);
	}
}
```

Note:
<font style="color:red;">目前Blob SDK並沒有直接開放屬性或其他方式讓外部指定Proxy URL，只能透過繼承DelegatingHandler、覆寫其SendAsync、並在該Function中指定Proxy。</font>

- 拆解傳入的Connection String以取得Account Name、Account Key等資訊
```csharp
private void SetConnInfo(string azureStorageConnectionString)
{
	if (string.IsNullOrEmpty(azureStorageConnectionString)) return;

	foreach (var element in azureStorageConnectionString.Split(';'))
	{
		_dictConnInfo.Add(
			element.Substring(0, element.IndexOf("=", StringComparison.Ordinal)), 
			element.Substring(element.IndexOf("=", StringComparison.Ordinal) + 1)
			);
	}
}
```

- 定義及初始化相關物件，如BlobConfigInfo、_cloudStorageAccount、_cloudBlobContainer
```csharp
private readonly CloudStorageAccount _cloudStorageAccount;
private readonly CloudBlobContainer _cloudBlobContainer;

private readonly Dictionary<string, string> _dictConnInfo = new Dictionary<string, string>();

public BlobService(BlobConfigInfo blobInfo)
{
	var blobInfo1 = blobInfo;
	CloudBlobClient cloudBlobClient;

	if (!CloudStorageAccount.TryParse(blobInfo1.AzureStorageConnectionString, out _cloudStorageAccount)) return;

	//To depart information from connection string into dictionary
	SetConnInfo(blobInfo1.AzureStorageConnectionString);

	//If no proxy setting, don't use custom DelegatingHandler.
	if (!string.IsNullOrEmpty(blobInfo.ProxyUrl))
	{
		var delegatingHandler = new BlobProxyHandler(new WebProxy(blobInfo1.ProxyUrl, true));

		cloudBlobClient = GenerateCloudBlobClient(delegatingHandler);
	}
	else
	{
		cloudBlobClient = _cloudStorageAccount.CreateCloudBlobClient();
	}

	//Use blob client to generate blob container instance
	_cloudBlobContainer = cloudBlobClient.GetContainerReference(blobInfo1.BlobName);
}
```

- 撰寫上傳檔案Function
```csharp
public async Task CreateAsync(string fileName, string jsonData)
{
	// Get block blob(file) reference for uploading
	var cloudBlockBlob = _cloudBlobContainer.GetBlockBlobReference(fileName);

	using (var sourceData = new MemoryStream(Encoding.ASCII.GetBytes(jsonData)))
	{
		await cloudBlockBlob.UploadFromStreamAsync(sourceData);
	}
}
```

- 撰寫查詢檔案Function
```csharp
public IEnumerable<BlobInfo> Query(string prefix = null, DateTime? startModifiedTime = null, DateTime? endModifiedTime = null)
{
	var list = _cloudBlobContainer.ListBlobs(prefix: prefix, useFlatBlobListing: true).Cast<CloudBlockBlob>().ToList();

	if (null != startModifiedTime && null != endModifiedTime)
		list.RemoveAll(b => b.Properties.LastModified?.DateTime < startModifiedTime || b.Properties.LastModified?.DateTime > endModifiedTime);

	return list.Select(b => new BlobInfo
	{
		BaseUri = b.ServiceClient.BaseUri.ToString(),
		Uri = b.Uri.ToString(),
		Name = b.Uri.Segments.Last(),
		LastModifiedTime = b.Properties.LastModified?.DateTime
	}).ToList();
}
```

- 撰寫檔案下載Function
```csharp
public async Task<string> DownloadAsStringAsync(string fileName)
{
	// Get block blob(file) reference for uploading
	var cloudBlockBlob = _cloudBlobContainer.GetBlockBlobReference(fileName);

	return await cloudBlockBlob.DownloadTextAsync();
}
```

- 撰寫檔案刪除Function
```csharp
public async Task DeleteAsync(string fileName)
{
	// Get block blob(file) reference for uploading
	var cloudBlockBlob = _cloudBlobContainer.GetBlockBlobReference(fileName);

	await cloudBlockBlob.DeleteAsync();
}
```