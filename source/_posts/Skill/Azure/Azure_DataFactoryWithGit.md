---
title: Azure - Data Factory's Version Control with Azure DevOps GIT
categories:
 - 技術文件
 - Azure
date: 2020/6/23
updated: 2020/6/23
thumbnail: https://i.imgur.com/oHcuJIO.png
layout: post
tags: [Azure, Azure Data Factory]
---
Author: Carl Yang

![](/Teamdoc/image/azure/logos/adf.png)

## Purpose
本文目的在描述`如何利用Azure Data Factory的GIT Configuration進行版本控制`。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Azure Data Factory Service
- Azure DevOps Account & GIT Repository

## Introduction
Azure Data Factory可以讓我們快速地進行資料移轉作業，不論是Data Lake to SQL DB、Blob Storage to SQL DB等，但是當我們設計好相關的Pipelines、Datasets、Triggers、LinkedServices之後，這些JSON組態卻不知道如何進行管理，以前都只能手動複製這些JSON組態、再另外放進版控內，現在不用這麼麻煩了，只要使用它的GIT Configuration功能就可以直接同步進版控。

##### GIT Configuration
1. 首先我們先進入Azure Data Factory的編輯工具，然後進入左手邊的`工具箱`圖示、名為`Manage`功能，再按下Set up code repository，即會彈出設定視窗。
![](/Teamdoc/image/azure/AzureDataFactory/config1.png)

1. 各欄位說明如下:
- Repository Type: 選擇GIT版控服務的類型，這裡是選Azure Devops GIT，讀者也可以選擇其他的GitHub服務。
- Azure Devops Account: 這邊我是選擇Azure DevOps的Office 365帳戶類型。
- Project Name: Azure DevOps內設定的專案名稱。
- GIT Repository Name: 指定Azure DevOps內保存庫的名稱。
- Collaboration Branch: 指定版控分支。
- Root Folder: 基本上保留預設值就好，如果要指定其他子資料夾可在此處修改。
- import existing Data Factory resources to repository: 請記得打勾才會同步設定進去版控。
- Branch to import resource into: 也是指定分支、或是可以建立新的分支。
![](/Teamdoc/image/azure/AzureDataFactory/config2.png)

最後，按下左下角的`Apply`按鈕就會開始同步進版控了。

## 結論:
`Azure Data Factory是一種管線串接、流程設計的工具，幫助我們方便快速地進行資料移轉作業，所以它並不是撰寫程式碼、而是以JSON的組態設定方式進行，所以我們同步進版控的資料，就是這一些JSON組態`，未來如果有需要就可以利用這些JSON組態再還原回去即可。
早期僅能用手動複製的方式進行版控，現在已經有自動化的設定功能可使用，真的方便很多。另外，`其實Azure Data Factory的GIT Configuration功能不是只為了版本控管、真正目的是為了DevOps的CI/CD而設計，只要設計好CI/CD流程，就可以自動化建置及佈署，這才是整合版控的真正目的`。

## 參考
1. N/A