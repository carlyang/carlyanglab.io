---
title: Azure - vNet Restriction for Storage Account
categories:
 - 技術文件
 - Azure
date: 2020/1/17
updated: 2020/1/17
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, vNet, Networking, Virtual Networks]
---
Author: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Purpose
由於使用vNet會將服務置於一個虛擬私人網域內，可以達到安全性保護的目的、外部無法存取，`本文以Azure Storage Account為例，介紹如何指定服務使用已建立的vNet、在私人網域內相互溝通，並與外網Internet隔離`。
<!-- more -->

## Introduction
vNet是Azure上各種資源透過網路互相存取的基礎，透過Azure vNet的功能，我們能夠建立私人的虛擬網路，並讓所有的相關服務使用相同的vNet互相溝通，`我們可以將數個Azure資源規劃在相同的虛擬私人網域中，具有隔離、保護及安全性，外部無法隨便存取內部的資源服務`。

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Two vNets located at different regions
- Two VMs located at different regions
- One Storage Account

Note:  
`上列兩個vNet要先做好Peering，才能夠從另一個vNet連接過來`，詳細設定可參考文件  
[[Azure - Virtual Network Peering]](/2020/01/13/Skill/Azure/Azure_VirtualNetwork/)。

## Configuration
關於上述的先決條件，我們已經有兩個vNets及兩台VMs、分別建立在不同的Regions上(`本文是使用East Asia及South East Asia兩個Regions`)，還有一個Storage Account建立在South East Asia，接著我們只要完成下列設定，就可以將Storage Account限制在允許的vNets內的服務可以存取它。

##### 進入`Storage Account` &rarr; `Settings` &rarr; `Firewall and virtual networks`，如下圖所示。
![](/Teamdoc/image/azure/VirtualNetwork/vnets1.png)

- Allow access from:  
選擇Selected networks。
- Virtaul networks:  
我們按下`Add existing network`連結、右方會出現設定視窗。
- 全選Virtual networks。
- 接著全選Subnets，按下Add按鈕新增設定。
- 最後記得按左上角的Save按鈕儲存設定，才會生效。
![](/Teamdoc/image/azure/VirtualNetwork/vnets2.png)

  Note:  
  1. 新增vNet可以選擇多個Region的資源，代表允許來源位於其他Region。
  2. 如果只希望允許位於相同vNet的資源存取的話，只要新增自己本身所處Region的vNet即可。
  3. Subnet必須要選擇才可Add，因為vNet會根據Subnets的設定給予服務一組私有IP，`而vNet如果有設定多個網段(Subnets)時，也可以逐一選擇是否開放存取`。
  4. Firewall:  
  設定允許通過防火牆的設定，你可以把目前的Client IP加入允許清單，方便測試，也可以勾選Exceptions增加防火牆例外規則，不過接下來我們還要進行測試，故先預設勾選第一個[`Allow trusted Microsoft services to access this storage account`]即可。

## Tests
1. 設定完成後，先用Storage Explorer從外網嘗試存取Blob，顯示錯誤訊息訊息[`The request is not authorized to perform this operation.`]，表示無法從Internet存取Storage Account。
![](/Teamdoc/image/azure/VirtualNetwork/test1.png)

2. 接著我們進入位於South East Asia的VM，執行自行撰寫的測試程式，使用`Microsoft.Azure.Storage.Blob SDK`實際寫入Blob驗證是否能存取、測試完會刪除測試的Blob，結果顯示測試OK，代表我們在相同vNet中的VM服務可以存取Storage Account。
![](/Teamdoc/image/azure/VirtualNetwork/test2.png)

3. 接著我們再遠端進入East Asia的VM執行測試程式，如下圖所示測試OK，表示不同的vNet也可以存取Storage Account。
![](/Teamdoc/image/azure/VirtualNetwork/test3.png)

4. 我們再`回到Storage Account移除East Asia的vNet`，再遠端測試一次，結果顯示測試失敗，錯誤訊息為[`The request is not authorized to perform this operation.`]。
![](/Teamdoc/image/azure/VirtualNetwork/test4.png)

由以上測試就可以知道，當我們指定好要允許存取的vNet後，就會根據我們希望的方式進行服務的存取。

Note:  
`請注意，Azure服務不是每個都能夠設定vNet，詳細可使用的服務請參考`[[MS doc]](https://docs.microsoft.com/en-us/azure/virtual-network/virtual-network-service-endpoints-overview)。

## 結論:
透過設定vNet我們可以很輕鬆地對服務進行保護，且可以如同我們在地端所使用的Intranet一般。而`可以使用vNet的服務也包含App Services類型，舉凡Web App、API App、Function App等，都可以進行設定`，方便又快速。

## 參考
1. [Virtual Network service endpoints](https://docs.microsoft.com/en-us/azure/virtual-network/virtual-network-service-endpoints-overview)