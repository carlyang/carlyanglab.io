---
title: Azure OpenAI Service (3) - 額度、限制及微調參數
categories:
 - 技術文件
 - Azure
date: 2023/3/13
updated: 2023/3/13
thumbnail: https://i.imgur.com/xt5yNCJ.png
layout: post
tags: [Azure, Azure OpenAI Service]
---

![](/Teamdoc/image/azure/logos/openai.png)

## Purpose
[[上一篇]](/2023/03/10/Skill/Azure/OpenAI2/)簡單說明微軟申請通過後如何建立`Azure OpenAI Service`、部署`GPT-3`模型及調用它的`REST API`，目前為止我們已經可以使用`Postman`這類工具試打`Azure OpenAI Service`來觀察回應結果，但還有些額度、限制及相關微調要用的參數，我們也有必要事先了解以幫助我們更快上手，故本文目的就在說明額度、限制及這些微調參數的作用。
<!-- more -->

## Quotas & Limits
[[上一篇]](/2023/03/10/Skill/Azure/OpenAI2/)雖然我們已建立`Azure OpenAI Service`、而且也可以呼叫`REST API`來取得回應，但是在使用上還是有一些額度的限制，現在就讓我們先來了解一下這部分，如下表。

| 限制 | 數量 |
| ---- | ---- |
| OpenAI resources per region | 2 | 
| Requests per minute per model | Davinci-models (002 and later): 120  <br> ChatGPT model: 300 <br> All other models: 300|
| Tokens per minute per model | Davinci-models (002 and later): 40,000  <br> ChatGPT model: 120,000 <br> All other models: 120,000 |
| Max fine-tuned model deployments | 2 |
| Ability to deploy same model to multiple deployments | Not allowed |
| Total number of training jobs per resource | 100 |
| Max simultaneous running training jobs per resource | 1 |
| Max training jobs queued | 20 | 
| Max Files per resource | 50 |
| Total size of all files per resource | 1 GB | 
| Max training job time (job will fail if exceeded) | 120 hours |
| Max training job size (tokens in training file) x (# of epochs) | **Ada**: 40-M tokens <br> **Babbage**: 40-M tokens <br> **Curie**: 40-M tokens <br> **Cushman**: 40-M tokens <br> **Davinci**: 10-M |
###### Ref: [[MS Doc]](https://learn.microsoft.com/en-us/azure/cognitive-services/openai/quotas-limits#quotas-and-limits-reference)

說明:
- `OpenAI resources per region`: 我們在每個`Azure Region`上建立的服務數量上限只能有2個。
- `Requests per minute per model`: 我們每分鐘內、每個模型呼叫的數量，不同模型有不同的上限，像我們使用的`Davinci`模型最高只能120個。
- `Tokens per minute per model`: 我們每分鐘內、每個模型使用的`Tokens`數量。(`Token`指的是分析時拆分開來的字詞數量，例如: `hamburger`會拆分成`ham`、`bur`、`ger`三個)
- `Max fine-tuned model deployments`: 我們同時最多能部署的模型數量上限只有2個。
- `Ability to deploy same model to multiple deployments`: 這邊不允許相同模型部署至不同的部署(deployments)。
- `Total number of training jobs per resource`: 因為每次的分析訓練都會占用一個數量，這裡限制最多每個資源上同時訓練100個工作。
- `Max simultaneous running training jobs per resource`: 最多只能有1個simultaneous job。
- `Max training jobs queued`: 已排入Queue的simultaneous job數量最多只能有20個。
- `Max Files per resource`: 每個資源使用的檔案數上限為50個。
- `Total size of all files per resource`: 每個資源使用的所有檔案大小上限為1 GB。
- `Max training job time`: 訓練工作最多只能跑120個小時，超過就會failed。
- `Max training job size`:  這邊是指每個模型訓練時用的`Tokens`數量，像我們使用的`Davinci`模型可以有1000萬個。

Note:  
以上的額度及限制`只是目前的狀況，考量到未來如果用戶進入正式環境或是要上線營運的話，微軟還是可以根據需求動態調整的!`所以，目前的`Standard S0`應該只是暫時性的，雲端服務的好處就是可以`on-demands`設計。

## Parameters
除了上述的額度及限制外，我們也有一些微調參數可以使用，讓我們得到的結果`可能可以更符合需求`，說明如下。

| 參數名稱 | 類型 | 必&#47;選填 | 預設值 | 說明 |
|:--:|:--:|:--:|:--:|:--|
| `prompt` | string or array | 選填 | &nbsp;&nbsp;<&#92;&#124;endoftext&#92;&#124;>&nbsp;&nbsp; | 可以是字串、字串清單或`Token`清單，<&#92;&#124;endoftext&#92;&#124;>則是模型執行時辨識的分隔符號。 |
| `max_tokens` | integer | 選填 | 16 | 每次完成時回傳的Token數量上限，我們可以指定`Davinci-text`上限為2048(大部分都是)，少數如`davinci-codex`是4096。 |
| `temperature` | number | 選填 | 1 | 指取樣時的溫度，值越高風險越高、準確率也會下降，值越低越明確、但重複性越高，建議與`top_p`二擇一使用就好。 |
| `top_p` | number | 選填 | 1 | 取樣時只考慮前p高的機率質量Tokens，例如:設定0.1就是取前10%高機率的的Tokens，也就是數值越低代表越高，建議與`temperature`擇一使用。 |
| `n` | integer | 選填 |  1 | 每個提示要產生的完成項數量，設定越高消耗的Tokens數量會越多。 |
| `stream` | boolean | 選填 | False | 是否要串流回部分進度，也就是說打開的話、在Tokens額度夠的狀況下服務會不斷串流結果回來，最後以`data: [DONE]`訊息終止。| 
| `logprobs` | integer | 選填 | null | 包含最可能是Tokens之 logprob 的對數機率以及所選Tokens。 例如:設定10的時候、API會傳回10個最可能的Tokens清單，回應中可能包含多達logprobs+1個元素，而且這個參數不能與 gpt-35-turbo搭配使用。 |
| `echo` | boolean | 選填 | False | 除了完成之外，還回顯提示。這個參數不能與gpt-35-turbo搭配使用。 |
| `stop` | string or array | 選填 | null | 指定服務的回應在具有某些Token時停止(最多四個)，傳回的文字不會包含這些Tokens。 |
| `presence_penalty` | number | 選填 | 0 | 介於 -2.0 和 2.0 之間的數字，正值越大越會增加新主題的回應，越不會回應舊的已回應的內容(這邊指的是Token是否已出現在先前的回應文字中)。 |
| `frequency_penalty` | number | 選填 | 0 | 介於 -2.0 和 2.0 之間的數字，正值越大越會增加新主題的回應，越不會回應舊的已回應的內容(這邊指的是Token已出現在先前的回應文字中的頻率)。 |
| `best_of` | integer | 選填 | 1 | `best_of`會完成伺服器端的多格完成項、並傳回「最佳」的結果(每個Token具有最低對數機率的)，使用這個參數時不能開啟`stream`。搭配`n`時`best_of`會控制待選完成項的數目、`n`則指定傳回的數目且`best_of 必須大於 n`。此參數有可能會快速消耗Tokens配額，也不能與gpt-35-turbo搭配使用。 |
| `logit_bias` | map | 選填 | null | 在完成時出現指定Tokens的可能性、也接受json物件。此物件會將Tokens對應至 -100 ~ 100 的相關偏差值，我們可以在GPT-2或GPT-3使用此工具將文字轉換成識別碼。確切的效果會因模型而異，但介於-1~1之間的值應該會降低或提高選用的可能性、而 -100或100之類的值應該會導致禁止選用或獨佔選取相關Tokens，例如: 我們可以送出{"50256": -100}以防止產生<&#92;&#124;endoftext&#92;&#124;>這個Token。 |

### 下方我們就以`temperature`來觀察看看有什麼差別(先用`0.1`再用`1`來比較結果)，

Question1:
```json
{
    "prompt": "請問如何減肥?",
    "max_tokens":2048,
    "temperature":0.1
}
```

Answer1:
```json
{
    "id": "cmpl-6tZ3ocqzJ6dkSFtoio4OcbjQNYJzg",
    "object": "text_completion",
    "created": 1678700832,
    "model": "text-davinci-003",
    "choices": [
        {
            "text": "\n\n1. 運動：每天做一些有氧運動，例如跑步、游泳、健身操等，可以有效減少脂肪和熱量。\n\n2. 飲食：減少攝取高熱量食物，多吃蔬菜水果，減少油脂和糖分的攝取，增加蛋白質的攝取，可以有效減少脂肪和熱量。\n\n3. 均衡飲食：每天攝取的熱量應與消耗的熱量相等，以達到減肥的目的。\n\n4. 健康生活：多喝水，多休息，控制壓力，保持正常的作息時間，可以有效減少脂肪和熱量。",
            "index": 0,
            "finish_reason": "stop",
            "logprobs": null
        }
    ],
    "usage": {
        "completion_tokens": 391,
        "prompt_tokens": 17,
        "total_tokens": 408
    }
}
```

由第一道問題的回覆我們可以發現，`當我們降低temperature的值、服務的回答中相同字詞會出現比較多次、但看起來比較準確`，例如:`熱量`這個詞就出現了六次，接著我們把`temperature`調高到最大1來試試看。

Question2:
```json
{
    "prompt": "請問如何減肥?",
    "max_tokens":2048,
    "temperature":1
}
```

Answer2:
```json
{
    "id": "cmpl-6tZ67lPdNTg3mu9pyP2TvBvoIAzTh",
    "object": "text_completion",
    "created": 1678700975,
    "model": "text-davinci-003",
    "choices": [
        {
            "text": "\n\n減肥最基本的原則是「減少攝取熱量、多做運動」：\n\n1. 選擇健康的飲食。選擇富含營養的飲食，攝取正確的熱量，以及減少飽和脂肪酸、糖和食盐的攝取量。例如可多選擇以蔬菜、豆類、果類、貝類、雞肉等主食多吃，鮮果也可以替代甜食。\n\n2. 多運動。多運動不僅可以提高新陳代謝，還可以增加體能，並維持健康的身體。\n有氧運動（如散步、跑步、游泳、自行車）可以有助於減少脂肪；而重量訓練則可以增加肌肉量，提高新陳代謝率。每天適量地運動，對減肥有很大的幫助。\n\n3. 睡眠要充足。睡眠不足會使身體新陳代謝出現問題，從而影響減肥效果，因此睡眠充足是減肥過程中勿可缺少的因素。",
            "index": 0,
            "finish_reason": "stop",
            "logprobs": null
        }
    ],
    "usage": {
        "completion_tokens": 598,
        "prompt_tokens": 17,
        "total_tokens": 615
    }
}
```

由上面調整`temperature`參數值後再拿到的回答，`熱量`這個詞就只出現兩次、重複性降低，但這個例子的文字其他選項也都或多或少跟`熱量`這個詞有關，但出現次數卻減少、也比較有可能不準確。

Note:  
`temperature`參數越高代表回答越有創意、但是卻不一定是正確的答案，`temperature`參數越低則是越具有高準確性的答案，但是比較適合已經有明確答案的問題。

## Parameters in Azure OpenAI Studio with GPT-3 model
接下來讓我們回到`Azure OpenAI Studio`的Playground來試試看參數效果、可以較容易看出差別，如下圖紅框。

Note:  
為了實際擷取畫面說明給大家，我們是在`Azure OpenAI Studio`中使用`Playground`功能跑`GPT-3`模型，所以`開始前請注意，並不是前一小節介紹的所有參數都可以使用喔!`

![](/Teamdoc/image/azure/OpenAI3/azure_openai_studio1.png)

1. 改`max_tokens`為10會發現回答的字數變很少，話根本還沒講完。

![](/Teamdoc/image/azure/OpenAI3/azure_openai_studio2.png)

2. 使用`stop sequence`指定某個Token時停止回應並回傳，如下圖就會停在`高`這個字，因為接下來就是`熱量`兩個字。

![](/Teamdoc/image/azure/OpenAI3/azure_openai_studio3.png)

3. 我們設定`frequency_penalty`為最大值2，會發現相同的文字會降低很多、甚至是沒有，例如下圖的`減肥`兩個字就完全沒有在回應中出現了(如果沒設定`frequency_penalty`為2則會出現多個重複的字詞)。

![](/Teamdoc/image/azure/OpenAI3/azure_openai_studio4.png)

4. `presence_penalty`值設定為最大2，我們同樣的問題問兩遍、它會多出第五點新主題來做為新的答案，如下圖。

![](/Teamdoc/image/azure/OpenAI3/azure_openai_studio5.png)

6. `best_of`這個參數目前一直試不出來、似乎結果沒有太大差異。如果按照文件的說明，它應該是會在伺服器端完成多個項目、並傳回最佳結果，所以我一直收到同樣的回答，可能事實上它已經是`最佳解`了，如果有新發現我再更新上來。

7. `Pre-response`及`Post-response`則是在送出及收到回應後附加上一段自訂文字，讓我們方便識別對話框中那些是我們自行Key-in的、那些是服務的回答內容。

![](/Teamdoc/image/azure/OpenAI3/azure_openai_studio6.png)

## Conclusion
本文說明了`Azure OpenAI Studio`的相關額度及限制，也列出所有相關可以使用的參數，最後使用`Azure OpenAI Studio`實際調整參數後觀察回傳結果，大部分都可以看出差異、僅`best_of`這個參數試不出來，可能後續找到方法或是有更好的樣本可以試出來，再為大家更新一下。

## References
1. [Azure OpenAI Service quotas and limits](https://learn.microsoft.com/en-us/azure/cognitive-services/openai/quotas-limits)
2. [Azure OpenAI Service REST API reference](https://learn.microsoft.com/en-us/azure/cognitive-services/openai/reference)