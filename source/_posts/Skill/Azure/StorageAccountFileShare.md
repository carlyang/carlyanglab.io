---
title: Azure - Storage Account FileShare
categories:
 - 技術文件
 - Azure
date: 2022/11/30
updated: 2022/11/30
thumbnail: https://i.imgur.com/8uMcIBL.png
layout: post
tags: [Azure, Storage Account, FileShare]
---

![](/Teamdoc/image/azure/logos/storage_account.png)

目前不管是公司或是家庭使用，大都離不開`NAS`這種產品了，因為儲存需求越來越高、儲存量也越來越大，加上儲存技術不斷地進步，每單位儲存容量的成本也不斷下降，所以`檔案分享`這功能的需求也是越來越高，而`Azure FileShare`就是雲端檔案儲存及分享的解決方案。
<!-- more -->

## Purpose
目前對於檔案的儲存及分享大都離不開`SMB`及`NFS`兩種通訊協定，我自己也有一台`NAS`作為家中儲存及分享之用，但是一旦離開家中的區域就變得很難使用，雖然大部分`NAS`都具有遠端存取功能，但那個使用起來可真是不太能接受阿!  
所以，如果能夠在雲端平台上提供檔案分享的功能，不僅有更大的容量、也有較佳的網路頻寬(通常受限於上傳頻寬)，本文的目的就在介紹`如何使用SDK來存取FileShare中的檔案`。

## FileShare
`FileShare`主要支援`SMB`、`NFS`及`Azure Files REST API`幾種協定，我們可以在地端Server掛載一個`SMB`的檔案分享路徑、直接存取`Azure`上的檔案資料，引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/storage/files/storage-files-introduction)如下。

>Azure Files offers fully managed file shares in the cloud that are accessible via the industry standard Server Message Block (SMB) protocol, Network File System (NFS) protocol, and Azure Files REST API. Azure file shares can be mounted concurrently by cloud or on-premises deployments. SMB Azure file shares are accessible from Windows, Linux, and macOS clients. NFS Azure file shares are accessible from Linux clients. Additionally, SMB Azure file shares can be cached on Windows servers with Azure File Sync for fast access near where the data is being used.

由上可知，`FileShare`還可以使用`Azure File Sync`功能自動同步檔案上去雲端，不過這部分不在本文的範圍、就先略過不提，有興趣的朋友可以參考[[What is Azure File Sync?]](https://learn.microsoft.com/en-us/azure/storage/file-sync/file-sync-introduction)。

## Prerequisites
請先準備好以下開發環境。
  - Visual Studio 2022
  - .NET 6
  - Install Azure.Storage.Common 12.13.0
  - Install Azure.Storage.Files.Shares 12.12.1

## Samples
只要安裝好套件，就可以開始使用下列的`Functions`建立/刪除`FileShare`的檔案。

Functions說明:
    內部函式:
1. `CreateHierarchyFolders(string dirPath)`: 依照輸入的路徑建立資料夾。
2. `DeleteHierarchyFolders(string dirPath)`: 依照輸入的路徑刪除資料夾。
3. `WriteContentAsync(ShareFileClient fileShareClient, Stream content)`: 將內容寫入檔案。

公開函式:
1. `UploadAsFileAsync(string dirPath, string fileName, byte[] content)`: 用byte[]陣列上傳資料。
2. `UploadAsFileAsync(string dirPath, string fileName, Stream content)`: 用Stream上傳資料。
3. `FileExistsAsync(string dirPath, string fileName)`: 檢查檔案是否存在。
4. `DeleteFileAsync(string dirPath, string fileName)`: 刪除檔案。
5. `CreateFolderAsync(string dirPath, bool isRecursive = false)`: 建立資料夾，如果isRecursive = true表示要依照輸入的階層逐一建立資料夾。
6. `FolderExistsAsync(string dirPath)`: 檢查資料夾是否存在。
7. `DeleteFolderAsync(string dirPath, bool isRecursive = false)`: 刪除資料夾，如果isRecursive = true表示要依照輸入的階層逐一刪除資料夾。

Note:  
1. 因為`FileShare`資料夾階層的建立或刪除，必須由內而外進行，也就是不能直接建立下層資料夾、而是必須先建好上層才能再建下層。反之，刪除的話則是需先刪掉下層再刪除上層，所以才有`CreateHierarchyFolders`及`DeleteHierarchyFolders`兩個內部函式在建立或刪除時處理階層的動作。。
2. 詳細範例可參考[[Github Sample]](https://github.com/carlyang920/AzureStorageFileShare)。

```csharp
/// <summary>
/// The library for accessing Azure storage file share.
///
/// Note:
/// Please make sure to initialize one instance for one file share.
/// </summary>
public class FileShareService
{
    /// <summary>
    /// The file share client to handle file share operations.
    /// </summary>
    private readonly ShareClient _shareClient;

    /// <summary>
    /// Use this to create file share client with each share name.
    ///
    /// Note:
    /// Please make sure one share client instance to handle one share name.
    /// </summary>
    /// <param name="connectionString"></param>
    /// <param name="shareName"></param>
    public FileShareService(
        string connectionString,
        string shareName
    )
    {
        _shareClient = GetShareClient(connectionString, shareName);
    }

    #region private functions

    /// <summary>
    /// Generate file share client instance.
    /// </summary>
    /// <param name="connectionString">Azure Storage connection string</param>
    /// <param name="shareName">File share name</param>
    /// <returns></returns>
    private static ShareClient GetShareClient(
        string connectionString,
        string shareName
    )
    {
        var shareClient = new ShareClient(connectionString, shareName.ToLower());

        shareClient.CreateIfNotExists();

        return shareClient;
    }

    /// <summary>
    /// Create hierarchy folders
    /// </summary>
    /// <param name="dirPath"></param>
    private void CreateHierarchyFolders(string dirPath)
    {
        void CreateFolder(string p)
        {
            var dirClient = _shareClient.GetDirectoryClient(p);
            dirClient.CreateIfNotExistsAsync().ConfigureAwait(false).GetAwaiter().GetResult();
        }

        var stackPath = string.Empty;

        dirPath.Split('/').Where(d => !string.IsNullOrEmpty(d)).ToList().ForEach(d =>
        {
            if (string.IsNullOrEmpty(d)) return;

            stackPath += $"/{d}";

            CreateFolder(stackPath);
        });
    }

    /// <summary>
    /// Delete hierarchy folders
    /// </summary>
    /// <param name="dirPath"></param>
    private void DeleteHierarchyFolders(string dirPath)
    {
        void DeleteFolder(string p)
        {
            var dirClient = _shareClient.GetDirectoryClient(p);
            dirClient.DeleteIfExistsAsync().ConfigureAwait(false).GetAwaiter().GetResult();
        }

        var stackPath = dirPath;
        var splitList = dirPath.Split('/').Where(d => !string.IsNullOrEmpty(d)).ToList();

        for (var i = splitList.Count - 1; i >= 0; i--)
        {
            if (!string.IsNullOrEmpty(stackPath))
                DeleteFolder(stackPath);

            stackPath = stackPath?.Replace($"{splitList[i]}", string.Empty).TrimEnd('/');
        }
    }

    /// <summary>
    /// Write content to file share in batches.
    /// </summary>
    /// <param name="fileShareClient"></param>
    /// <param name="content"></param>
    /// <returns></returns>
    private async Task WriteContentAsync(ShareFileClient fileShareClient, Stream content)
    {
        const int blockSize = 4000000;

        //http range offset
        long offset = 0;

        await fileShareClient.CreateAsync(content.Length);

        using var reader = new BinaryReader(content);

        //Write in batches
        while (true)
        {
            var buffer = reader.ReadBytes(blockSize);
            if (buffer.Length == 0)
                break;

            var uploadChunk = new MemoryStream();
            uploadChunk.Write(buffer, 0, buffer.Length);
            uploadChunk.Position = 0;

            var httpRange = new HttpRange(offset, buffer.Length);
            await fileShareClient.UploadRangeAsync(httpRange, uploadChunk);

            //Shift the offset by number of bytes already written
            offset += buffer.Length;
        }
    }

    #endregion

    #region public functions

    /// <summary>
    /// Save content with bytes into file share folder
    /// </summary>
    /// <param name="dirPath">Directory path</param>
    /// <param name="fileName">File name</param>
    /// <param name="content">Data bytes</param>
    public async Task UploadAsFileAsync(string dirPath, string fileName, byte[] content)
    {
        dirPath = dirPath.Replace("\\", "/");

        CreateHierarchyFolders(dirPath);

        var dirClient = _shareClient.GetDirectoryClient(dirPath);
        
        using var ms = new MemoryStream(content);

        // Get a reference to a file and upload it
        await WriteContentAsync(dirClient.GetFileClient(fileName), ms);
    }

    /// <summary>
    /// Save content with Stream into file share folder
    /// </summary>
    /// <param name="dirPath">Directory path</param>
    /// <param name="fileName">File name</param>
    /// <param name="content">Data stream</param>
    public async Task UploadAsFileAsync(string dirPath, string fileName, Stream content)
    {
        dirPath = dirPath.Replace("\\", "/");

        CreateHierarchyFolders(dirPath);

        var dirClient = _shareClient.GetDirectoryClient(dirPath);

        var data = await new StreamReader(content).ReadToEndAsync();

        if (content.CanSeek) content.Seek(0, SeekOrigin.Begin);
        
        using var ms = new MemoryStream(Encoding.UTF8.GetBytes(data));

        await WriteContentAsync(dirClient.GetFileClient(fileName), ms);
    }

    /// <summary>
    /// Check file exists or not
    /// </summary>
    /// <param name="dirPath">Directory path</param>
    /// <param name="fileName">File name</param>
    public async Task<bool> FileExistsAsync(string dirPath, string fileName)
    {
        dirPath = dirPath.Replace("\\", "/");

        var result = false;

        var dirClient = _shareClient.GetDirectoryClient(dirPath);

        if (!await dirClient.ExistsAsync()) return false;

        // Get a reference to a file and upload it
        var fileShareClient = dirClient.GetFileClient(fileName);

        if (await fileShareClient.ExistsAsync())
        {
            result = true;
        }

        return result;
    }

    /// <summary>
    /// Delete file
    /// </summary>
    /// <param name="dirPath">Directory path</param>
    /// <param name="fileName">File name</param>
    public async Task<bool> DeleteFileAsync(string dirPath, string fileName)
    {
        dirPath = dirPath.Replace("\\", "/");

        var result = false;

        var dirClient = _shareClient.GetDirectoryClient(dirPath);

        if (!await dirClient.ExistsAsync()) return false;

        // Get a reference to a file and upload it
        var fileShareClient = dirClient.GetFileClient(fileName);

        if (await fileShareClient.ExistsAsync())
        {
            result = await fileShareClient.DeleteIfExistsAsync();
        }

        return result;
    }

    /// <summary>
    /// Create folder
    /// </summary>
    /// <param name="dirPath">Directory path</param>
    /// <param name="isRecursive">Create folder by hierarchy</param>
    public async Task<bool> CreateFolderAsync(string dirPath, bool isRecursive = false)
    {
        dirPath = dirPath.Replace("\\", "/");

        if (isRecursive)
        {
            CreateHierarchyFolders(dirPath);

            return true;
        }

        var dirClient = _shareClient.GetDirectoryClient(dirPath);

        var dir = await dirClient.CreateIfNotExistsAsync();

        return dir.Value != null;
    }

    /// <summary>
    /// Check folder exists or not
    /// </summary>
    /// <param name="dirPath">Directory path</param>
    public async Task<bool> FolderExistsAsync(string dirPath)
    {
        dirPath = dirPath.Replace("\\", "/");

        var dirClient = _shareClient.GetDirectoryClient(dirPath);

        return await dirClient.ExistsAsync();
    }

    /// <summary>
    /// Delete folder
    /// </summary>
    /// <param name="dirPath">Directory path</param>
    /// <param name="isRecursive">Delete folder by hierarchy</param>
    public async Task<bool> DeleteFolderAsync(string dirPath, bool isRecursive = false)
    {
        dirPath = dirPath.Replace("\\", "/");

        if (isRecursive)
        {
            DeleteHierarchyFolders(dirPath);

            return true;
        }

        var dirClient = _shareClient.GetDirectoryClient(dirPath);

        return await dirClient.DeleteIfExistsAsync();
    }

    #endregion
}
```

## Conclusion
本文介紹了如何利用SDK處理`FileShare`的檔案基本操作，不過這邊只介紹了如何建立檔案及資料夾，`FileShare`本身主要的優點還是在`分享`及`同步`等部分，我們可以利用`SMB`在Windows建立遠端磁碟、也可以利用`Azure File Sync`同步檔案進地端的Windows File Server，是非常好用的工具。

## References
1. [What is Azure Files?](https://learn.microsoft.com/en-us/azure/storage/files/storage-files-introduction)
2. [What is Azure File Sync?](https://learn.microsoft.com/en-us/azure/storage/file-sync/file-sync-introduction)
3. [Why Azure Files is useful](https://learn.microsoft.com/en-us/azure/storage/files/storage-files-introduction#why-azure-files-is-useful)
4. [Github Sample](https://github.com/carlyang920/AzureStorageFileShare)
