---
title: Azure - Azure Data Factory的自我裝載整合執行階段
categories:
 - 技術文件
 - Azure
date: 2023/3/3
updated: 2023/3/3
thumbnail: https://i.imgur.com/oHcuJIO.png
layout: post
tags: [Azure, Azure Data Factory]
---

![](/Teamdoc/image/azure/logos/adf.png)

## Purpose
`Azure Data Factory`(下稱`ADF`)可以幫助我們搬移、轉換大量資料，但基本上它是一種雲端服務，如果我們想要它幫我們處理地端與雲端間的大量資料呢?其實是可以的!本文目的就在說明如何設定讓`ADF`可以從雲端上呼叫地端私人網路內的某台機器，藉以取得資料後再同步進雲端儲存空間。
<!-- more -->

## Integration Runtime
`Integration Runtime`(下稱`IR`)的目的就是在整合不同網路環境，讓相關的資料流程可以在不同網路環境之間作用，引述[[MS Doc]]()如下。

>An integration runtime provides the bridge between activities and linked services. It's referenced by the linked service or activity, and provides the compute environment where the activity is either run directly or dispatched. This allows the activity to be performed in the closest possible region to the target data store or compute service to maximize performance while also allowing flexibility to meet security and compliance requirements.

所以，`IR`可以視作一種橋接器，負責橋接兩種不同的網路環境，例如:公司內網與`ADF`。但是`想要整合公司內網與雲端網路，單靠IR是無法達成的，還必須安裝`[[Self-hosted integration runtime]](https://learn.microsoft.com/en-us/azure/data-factory/concepts-integration-runtime#self-hosted-integration-runtime)，引述如下。

>A self-hosted IR is capable of:
>- Running copy activity between a cloud data stores and a data store in private network.
>- Dispatching the following transform activities against compute resources in on-premises or Azure Virtual Network: HDInsight Hive activity (BYOC-Bring Your Own Cluster), HDInsight Pig activity (BYOC), HDInsight MapReduce activity (BYOC), HDInsight Spark activity (BYOC), HDInsight Streaming activity (BYOC), ML Studio (classic) Batch Execution activity, ML Studio (classic) Update Resource activities, Stored Procedure activity, Data Lake Analytics U-SQL activity, Custom activity (runs on Azure Batch), Lookup activity, and Get Metadata activity.

## Self-hosted integration runtime
引用[[MS Doc]](https://learn.microsoft.com/en-us/azure/data-factory/create-self-hosted-integration-runtime?tabs=data-factory)的概念圖如下。

`ADF`會透過`Self-hosted IR`與地端Server通訊，如下圖Control Channel。

![](/Teamdoc/image/azure/ADFSelfHostedIntegratonRuntime/high-level-overview.png)

步驟:
1. 先進入`ADF Integration Runtime` -> `New`。
![](/Teamdoc/image/azure/ADFSelfHostedIntegratonRuntime/self_hosted_IR1.png)

2. 再選擇`Self-hosted`項目。
![](/Teamdoc/image/azure/ADFSelfHostedIntegratonRuntime/self_hosted_IR2.png)

3. 再來照著指示建立好`Self-hosted IR`後會看到如下畫面，將`Key1`、`Key2`複製下來等下會用到。
![](/Teamdoc/image/azure/ADFSelfHostedIntegratonRuntime/self_hosted_IR3.png)

4. 接著到[[下載]](https://www.microsoft.com/en-us/download/details.aspx?id=39717)最新版安裝檔。

等待安裝完成。
![](/Teamdoc/image/azure/ADFSelfHostedIntegratonRuntime/self_hosted_IR4.png)

5. 安裝完成後貼上稍早複製下來的其中一個`Key`，按下`Register`即可向`ADF`註冊一個私人網路端點，
![](/Teamdoc/image/azure/ADFSelfHostedIntegratonRuntime/self_hosted_IR5.png)

6. 接著進入`設定` -> `來自內部網路的遠端存取` -> `啟用而不使用SSL/TLS憑證(基本)`。
![](/Teamdoc/image/azure/ADFSelfHostedIntegratonRuntime/self_hosted_IR7.png)

7. 接下來就可以在`ADF`上進入剛建立好的`Self-hosted IR`查看剛註冊的節點資訊，後續就可以建立各種活動來與地端的節點溝通了。
![](/Teamdoc/image/azure/ADFSelfHostedIntegratonRuntime/self_hosted_IR6.png)

Note:  
關於安裝的`Self-hosted IR Configuration Manager`目前僅支援`Windows`、`Windows Server`相關系統，如下所列。
- Windows 8.1
- Windows 10
- Windows 11
- Windows Server 2012
- Windows Server 2012 R2
- Windows Server 2016
- Windows Server 2019
- Windows Server 2022

## Conclusion
建立了`Self-hosted IR`後就可以在`ADF`上直接與地端節點溝通，真的是很方便的工具，而且它是以加密通訊方式與`ADF`溝通，不用擔心機密資訊被洩漏出去。使用這個工具的最大優點，主要是能夠在`ADF`直接調用地端Server上的微服務，例如:`DAL API`存取資料，我們只要在身分驗證的部分進行檢查即可，

## References
1. [Integration runtime in Azure Data Factory](https://learn.microsoft.com/en-us/azure/data-factory/concepts-integration-runtime)
2. [Create and configure a self-hosted integration runtime](https://learn.microsoft.com/en-us/azure/data-factory/create-self-hosted-integration-runtime?tabs=data-factory)