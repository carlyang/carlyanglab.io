---
title: Azure - 如何刪除Azure Subscription
categories:
 - 技術文件
 - Azure
date: 2022/12/26
updated: 2022/12/26
thumbnail: https://i.imgur.com/DW9eM8H.jpg
layout: post
tags: [Azure, Subscription]
---

![](/Teamdoc/image/azure/logos/subscription.png)

## Purpose
最近又因為一些安全性問題，被質疑使用公司`MSDN`建立的免費`Azure`訂閱是不安全的，因為在這訂閱上建立的服務可以存取公司訂閱中的各項服務。所以，只好乖乖來把訂閱刪除...
<!-- more -->

## Subscription
有使用`Azure`的朋友應該都會需要一點開發者使用的免費額度，對於日常開發來說是有必要的。但是為了公司系統安全、避免無意中被有心人當作跳板，駭進公司`Production`環境的服務中做壞事、盜取機密資訊，所以`最好的方式就是把自己的免費訂閱刪光光`，既然都刪光不存在了、也就沒有安全性疑慮了....  

好吧，言歸正傳，你懂的!  
在做刪除這件事時發現，`咦!我還真沒想過要刪除訂閱這件事耶!要怎麼刪除訂閱阿@@...`  
於是就找了一下，其實還滿簡單的、步驟如下。

1. 先進左邊`Menu` -> `Subscription` -> `Cancel subscription`，直接按到最後送出就好。

Note:  
Cancel之前建議還是先把所有資源全部手動刪完、再來這邊取消訂閱會比較好。

![](/Teamdoc/image/azure/SubscriptionDeletion/subscription1.png)

2. 取消成功後原本的`Cancel subscription`按紐會變成`Reactivate`(微軟真的很不希望你刪除啊!為何要刪呢...俺也不想阿=.=)，然後這還不是真正刪除。你還要按下右上方的`Delete`按鈕才會真的刪除。

![](/Teamdoc/image/azure/SubscriptionDeletion/subscription2.png)

3. 按下刪除按鈕後`接著你會發現...還是刪不掉阿!`(相信我，微軟真的不希望你刪掉它...)然後就可以看到下圖的訊息，請你等三天才能真的刪除。

![](/Teamdoc/image/azure/SubscriptionDeletion/subscription3.png)

於是，三天後我再回來按`Delete`按鈕就成功刪除了...藍瘦香菇(泣)

Note:  
同事有人三天後還是無法刪除，只能開Case找微軟幫忙，然後還要簽署一份保證聲明回傳之類的文件，最後查明是有某些內部資源刪不掉、導致訂閱也無法刪除，只能請微軟支援從後台刪除。所以步驟1才會`建議先手動清空所有資源再取消&刪除訂閱，不然一取消就無法手動刪資源了`。

## Conclusion
這篇文章主要是紀錄一下刪除訂閱的步驟，用了`Azure`這麼久還真是第一次要自己刪訂閱，結果發現自己完全沒想過要刪訂閱這件事，真的很傷心阿...