---
title: Azure - Event Grid Security
categories:
 - 技術文件
 - Azure
date: 2020/1/5
updated: 2020/1/5
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, Event Grid, Security, Authentication]
---
Author: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Purpose
Event Grid整合多種以事件為基礎的服務，透過Pub/Sub機制傳遞訊息，基本上在Azure平台上以事件為基礎的服務或資源，都可以透過Event Grid的整合並發送，`本文主要在說明透過Event Grid整合的這些事件服務，如何進行安全驗證`。
<!-- more -->

## Introduction
Event Grid整合多種以事件為基礎的服務，如下圖。我們可以選擇要訂閱那些事件服務，當收到這些事件後要轉送到哪個服務後續處理，這樣的`好處有統一的集中處理中心、也可以在Event Grid進行安全性驗證、過濾等，開發者也不用個別撰寫程式去發送其他的服務`。
![](https://docs.microsoft.com/en-us/azure/event-grid/media/overview/functional-model.png)

### Cross Regions
Event Grid服務可以跨域發佈，它可以運行在多個Fault Domain上、以及多個Availability Zones。

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Create Azure Event Grid Service

## Event Sources
如上一小節的圖示，Event Source相對於Event Grid來說，就是各式各樣具有事件基礎的服務，我們也可以發現就連Resource Group發生的事件都可以整合進來，對於監控來說是很方便的。

## Event Handler
Event Grid所要發送的對象，整合在Event Handler中，也就是透過Handler我們可以改變Publish對象，以進行其他的操作。

## Concepts
關於Event Grid有些概念如下引述。
>- Events - What happened.
>- Event sources - Where the event took place.
>- Topics - The endpoint where publishers send events.
>- Event subscriptions - The endpoint or built-in mechanism to route events, sometimes to more than one handler. Subscriptions are also used by handlers to intelligently filter incoming events.
>- Event handlers - The app or service reacting to the event
[[MS Doc]](https://docs.microsoft.com/en-us/azure/event-grid/overview#concepts)

## Security & Authentication
Event Grid的安全性驗證分為以下三個部分。
- WebHook event delivery:  
WenHook是我定設定的Endpoints，當新的事件就序時、Event Grid就會發送HTTP POST Request至該端點，並且將事件內含於Request種一起轉發，而這個過程中的驗證機制主要有兩種。
  1. ValidationCode Handshake:  
  `這種方式需要開發者自行撰寫相關的程式碼進行驗證`，如果以有自行開發的Source Code就可以選擇這種方式進行。`Event Grid會對Endpoint發送帶有validationCode屬性的要求、然後Endpoint會根據這個然後Endpoint會根據這判斷是否合法，如果合法就會再返回給Event Grid、表示已接受次要求`，再來就可以真正發送事件。
  2. ValidationURL handshake:  
  在有些情況下我們可能無法取得對方服務的Source Code，也就是說無法處理ValidationCode屬性，這時候就無法完成Handshake，故可以改用validationUrl屬性的方式，發送HTTP GET Request與Endpoint服進行Handshake。而`這種方式有5分鐘的有效期限，且較適合用手動方式(AwaitingManualAction)進行`。

  以下為JSON的Handshake範例:
```json
[{
"id": "2d1781af-3a4c-4d7c-bd0c-e34b19da4e66",
"topic": "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
"subject": "",
"data": {
"validationCode": "512d38b6-c7b8-40c8-89fe-f46f9e9622b6",
"validationUrl": "https://rp-eastus2.eventgrid.azure.net:553/eventsubscriptions/estest/validate?id=512d38b6-c7b8-40c8-89fe-f46f9e9622b6&t=2018-04-26T20:30:54.4538837Z&apiVersion=2018-05-01-preview&token=1A1A1A1A"
},
"eventType": "Microsoft.EventGrid.SubscriptionValidationEvent",
"eventTime": "2018-01-25T22:12:19.4556811Z",
"metadataVersion": "1",
"dataVersion": "1"
}]
```
- Event subscriptions:  
如果無法使用Webhook的驗證，那Event Grid就必須具有該資源的寫入權限，如此才能夠訂閱或處理Handler發送，故Event Grid需在Event Source的部分需具有`Microsoft.EventGrid/EventSubscriptions/Write`權限。
- Custom topic publishing:  
自訂主題的驗證可以有`Shared Access Signature (SAS)`或`key authentication`，但`key authentication較適合單純的程式碼撰寫、且需要具有公開的Webhook連結才行，故根據官方文件的建議，自訂主題的部分還是使用SAS驗證較為合適`，SAS產生範例如下連結。
![](https://docs.microsoft.com/en-us/azure/event-grid/security-authentication#sas-tokens)

HTTP:
```
aeg-sas-token: r=https%3a%2f%2fmytopic.eventgrid.azure.net%2feventGrid%2fapi%2fevent&e=6%2f15%2f2017+6%3a20%3a15+PM&s=a4oNHpRZygINC%2fBPjdDLOrc6THPy3tDcGHw1zP4OajQ%3d
```

Sample:
```csharp
static string BuildSharedAccessSignature(string resource, DateTime expirationUtc, string key)
{
    const char Resource = 'r';
    const char Expiration = 'e';
    const char Signature = 's';

    string encodedResource = HttpUtility.UrlEncode(resource);
    var culture = CultureInfo.CreateSpecificCulture("en-US");
    var encodedExpirationUtc = HttpUtility.UrlEncode(expirationUtc.ToString(culture));

    string unsignedSas = $"{Resource}={encodedResource}&{Expiration}={encodedExpirationUtc}";
    using (var hmac = new HMACSHA256(Convert.FromBase64String(key)))
    {
        string signature = Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(unsignedSas)));
        string encodedSignature = HttpUtility.UrlEncode(signature);
        string signedSas = $"{unsignedSas}&{Signature}={encodedSignature}";

        return signedSas;
    }
}
```

## 結論:
Event Grid具有整合、轉發、過濾等功能，可以簡化我們在事件處理上的工作，其中的驗證機制就是很重要的一環，特定情境下就可以選用不同的驗證來進行處理。

## 參考
1. [What is Azure Event Grid?](https://docs.microsoft.com/en-us/azure/event-grid/overview)
2. [Event Grid security and authentication](https://docs.microsoft.com/en-us/azure/event-grid/security-authentication)