---
title: Azure - Data Encryption of Storage Account Blob
categories:
 - 技術文件
 - Azure
date: 2023/1/12
updated: 2023/1/12
thumbnail: https://i.imgur.com/8uMcIBL.png
layout: post
tags: [Azure, Storage Account]
---

![](/Teamdoc/image/azure/logos/storage_account.png)

`Storage Account`儲存體帳戶在`Azure`雲端架構上規劃屬於`IaaS`(`Infrastructure as a Service`)的部分、也就是基礎設施之一，提供儲存大量檔案、非結構化資料等，甚至`Data Lake`標榜大數據運算使用目的的服務、底層也是使用`Storage Account`，所以資料加密當然也是不可或缺的一部分，對於資料安全性一定是必要項目。
<!-- more -->

## Purpose
`Storage Account` Server-side的`Blob Encryption`就相當於我們`Windows`的`Bitlocker`，能夠針對磁碟上的資料進行加解密處理，但是`Storage Account`上就沒有什麼磁碟概念，主要就是針對資料儲存在`Storage Account`中的加解密功能，它是使用`256-bit AES encryption`加解密演算法來進行，引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/storage/common/storage-service-encryption#about-azure-storage-service-side-encryption)如下。

>## About Azure Storage service-side encryption
>Data in Azure Storage is encrypted and decrypted transparently using 256-bit AES encryption, one of the strongest block ciphers available, and is FIPS 140-2 compliant. Azure Storage encryption is similar to BitLocker encryption on Windows.

本文的目的就在說明`Storage Account` Server-side的`Blob Encryption`所使用的`Key`如何利用`Key-Vault`保存及存取。

## Data Encryption
事實上，`Storage Account`預設就已經針對儲存資料進行加解密了，只是我們如果沒特別注意、大概也不太容易發現這件事，就像我們如果啟動`Windows`的`Bitlocker`、使用上也沒有特殊的感受，最多是發現系統變慢了一些。  
但是實際上資料的加密對於公有雲來說，可是一件一等一的大事，不論是為了符合安規、還是客戶對安全性的要求，這都是必要項目。  
所以，從`2017/10/20之後，寫入儲存體的每個Blob都會自動加密`，當我們取出時也會自動解密，我們預設不需要特別著墨些甚麼。如果是2017/10/20之前的Blob則需要重寫一遍才會加密存入。

## Key-Vault
`Key-Vault`服務是`Azure`提供的安全性服務，可以用來儲存機密性的`Key`、`Secrets`、`Certificates`等資料，具有加密、存取管控、安全傳輸等特性，可以讓我們安心儲存機密資訊的服務，`所以Storage Blob加密時所用的Key也可以使用Key-Vault來儲存`，如下步驟。

1. 我們可以建立一個`Key`、然後輸入加密時要用的Key值。
![](/Teamdoc/image/azure/StorageAccountEncryption/kv1.png)

2. 然後進入`Key`的詳細資訊，將代表該`Key`的URL複製下來。
![](/Teamdoc/image/azure/StorageAccountEncryption/kv2.png)

3. 最後將URL貼到`Storage Account -> Encryption -> Encryption Type選擇Customer-managed keys -> Encryption key選擇Enter key URI -> 貼上Key URI欄位 -> Save`這樣就可以了，如下圖。

Note:
這邊手動使用URL有個好處，`就是可以指定Key-Vault內Key的Version`。

![](/Teamdoc/image/azure/StorageAccountEncryption/kv3.png)

## Conclusion
能夠使用自訂的加解密Key來增加`Storage Blob`的安全性是很方便的功能，對於高度想要自訂Key的使用者是不錯的功能。不過，使用`Microsoft-managed keys`卻可以自動更新Key版本、也能夠自動產生新的Key值，安全性來說也比較好，所以青菜蘿蔔個人所好、就看各自的使用者需求來決定如何運用了。

## References
1. [Azure Storage encryption for data at rest](https://learn.microsoft.com/en-us/azure/storage/common/storage-service-encryption)
