---
title: AKS - How To Use Deployments & Pods of Workloads in Azure Portal?
categories:
 - 技術文件
 - Azure
date: 2023/11/15
updated: 2023/11/15
thumbnail: https://i.imgur.com/5sNBEdu.png
layout: post
tags: [Azure, AKS]
---

![](/Teamdoc/image/azure/logos/aks.png)

## Purpose
`AKS`的日常維護作業也是非常重要的一環，而`Azure Portal`也提供一套能簡化我們對`AKS`維運工作的一系列強大功能，本文目的就是要來介紹`Workloads`功能中常用的`Deployments`及`Pods`，它能夠讓我們快速方便地查閱`AKS`內部的部署、`Pods`等資訊，我們就不用使用複雜的`kubectl`指令查找，在一些緊急狀況需要快速排除時就能夠發揮作用。
<!-- more -->

## Workloads
在`K8s`的概念中我們有提到，`Workloads`的概念是由一組`Pods`所構成、用來共同完成某些工作目的，而`Pods`則可以包含多個`Containers`，這些`Containers`可能是互相搭配、也可能是各自獨立運作來完成這些工作，所以`Workloads`可以視為`AKS`內的一個工作負載機制，所有跟`Pods`相關的資訊都可以來這邊查詢，位置如下圖紅框。

![](/Teamdoc/image/azure/AKSWorkloadsDeploymentsAndPods/workloads1.png)

進入後我們可以看到如下圖幾個tab，接下來就要介紹幾個我們日常維運常會用到的幾個功能。

![](/Teamdoc/image/azure/AKSWorkloadsDeploymentsAndPods/workloads2.png)

## Deployments
這個功能是讓我們查閱目前`AKS`的所有部署相關資訊，`它是以Namespace的部署資訊為單位顯示、而每個部署可能包含一或多個Pods!`我們可以選擇其中一個部署進入查看`Overview`詳情。

- `Overview`: 如下圖紅框，我們可以看到有`Pods`顯示所有相關資訊、以及`Replica sets`顯示副本資訊。

![](/Teamdoc/image/azure/AKSWorkloadsDeploymentsAndPods/deployment1.png)

- `YAML`: 其實就是`AKS`的YAML文件，一般維運是不太需要用到、但有些特定狀況可能會需要來這邊進行設定的編輯修改。

- `Events`: 這是`AKS`內部所發的一些通知資訊，可以讓我們了解一些已知的狀況。

- `Insights`: 接著我們可以進入左方的`Insights`，如下圖我們可以得知目前的狀態是否正常、使用的CPU、Memory百分比等資訊。

![](/Teamdoc/image/azure/AKSWorkloadsDeploymentsAndPods/deployment2.png)

- `Live logs`: 這個功能則是提供即時的logs資訊，`請注意，這是當下選定Pod後開始錄製的紀錄，也就是最新最即時的狀況。因為進入container的世界後，我們常需要了解當下AKS內部發生了什麼問題以幫助我們快速排除問題!`

![](/Teamdoc/image/azure/AKSWorkloadsDeploymentsAndPods/deployment3.png)

## Pods
`Pods`則是`AKS`的管理的最小應用程式單位，`AKS`會根據我們設定的`YAML`管理一或多個`containers`，所以這個功能能讓我們查訊這些詳細的`Pods`資訊。


如下圖顯示`Pods`的Namespace、目前狀態、重啟次數、目前存活天數、`Pod`的IP、目前運行的`Node`等資訊。

![](/Teamdoc/image/azure/AKSWorkloadsDeploymentsAndPods/pods1.png)

接著我們進入其中一個`Pod`查看詳細內容，基本上功能與`Deployments`大同小異，只差在沒有`Insights`功能(這邊就不再詳述)。

![](/Teamdoc/image/azure/AKSWorkloadsDeploymentsAndPods/pods2.png)

如上圖紅框:
- `Containers`: 就是`AKS`為我們根據`Pod`設定啟動的`Containers`資訊，同樣有目前狀態、重啟次數、使用的image等資訊。

- `Volumes`: 這個有玩過`docker`的朋友應該都知道，如果`Container`需要存取外部的某些資源時(例如D槽、分享資料夾等)、需要為它掛載`Volume`，這邊就是用來顯示這些資訊讓我們方便查閱。

- `Conditions`: 這邊則是`Pod`相關的一些特定條件記錄，不過目前來說較少用到。

## Conclusion
本文的目的主要在介紹`AKS`中`Workloads`的常用功能、也就是`Deployments`及`Pods`，事實上`Workloads`還有提供其他好用的功能讓我們使用，後續有機會再為各位朋友介紹。

## References
1. N/A