---
title: Azure - 如何自訂IP Filter的錯誤處理
categories:
 - 技術文件
 - Azure
date: 2022/4/17
updated: 2022/4/17
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management]
---

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
上一篇[Azure - 如何自訂APIM錯誤並回傳狀態及訊息](/2022/04/15/Skill/Azure/APIMCustomError/)為各位說明了如何利用`set-body`取得資訊並判斷及回傳自訂錯誤，但並不是所有Policy都可以使用內嵌程式碼的方式取得資訊，這時候也就無法自訂錯誤回傳的內容、例如常用的`IP Filter`，本文目的在說明`如何利用on-error捕捉exception的特性、自訂IP Filter的錯誤回傳，而非使用預設的狀態`。
<!-- more -->

## IP Filter
`IP Filter`可以幫助我們在每一個`HTTP Request`進來的時候檢查它的來源IP，如果`不在白名單中或是在黑名單中`就會擋回去並回傳`403 Forbidden`(預設)。其設定方法有分為指定`特定IP`或是指定`某段IP Range`，範例如下。

- 下列範例會`允許`(action="allow")IP為`10.1.1.2`或是IP位於`10.1.2.1 ~ 10.1.2.3`之間的來源要求通過檢查。
```xml
<ip-filter action="allow">
    <address>10.1.1.2</address>
    <address-range from="10.1.2.1" to="10.1.2.3" />
</ip-filter>
```

- 下列範例會`拒絕`(action="forbid")IP為`10.1.1.2`或是IP位於`10.1.2.1 ~ 10.1.2.3`之間的來源要求通過檢查。
```xml
<ip-filter action="forbid">
    <address>10.1.1.2</address>
    <address-range from="10.1.2.1" to="10.1.2.3" />
</ip-filter>
```

Note:  
請注意，`IP Filter`是屬於`Inbound`區塊的Policy、不能使用在其他區塊，因為它本身就是過濾呼叫端進來的`HTTP Request`，如果你把它放在如`Outbound`區塊中，當儲存的時候就會得到如下錯誤訊息，告訴你不允許放在此區塊。

![](/Teamdoc/image/azure/APIMCustomError4IPFilter/ipfilter1.png)

## On-Error Policy
接下來就是本文的重點-`on-error`，它本身可以支援大部分policy的`Exception Catch`、基本上只要發生錯誤都可以進到`on-error`進行錯誤處理(目前還沒有遇過不會進去的，如果以後真的有遇到沒進去的再更新給大家)，詳細內容可以參考[[MS Doc]](https://learn.microsoft.com/en-us/azure/api-management/api-management-error-handling-policies)。

除了[[上一篇]](/2022/04/15/Skill/Azure/APIMCustomError/)為各位介紹的`on-error`的部分外，本文要另外介紹`on-error`對於其他的policy有一些預先定義好的錯誤資訊，例如接下來要用到的`Reason - CallerIpNotAllowed`，其他詳細資訊可參考[[MS Doc]](https://learn.microsoft.com/en-us/azure/api-management/api-management-error-handling-policies#predefined-errors-for-policies)。

| Source | Condition | Reason | Message |
|--------|---------- |--------|---------|
| ip-filter | Caller IP is not in allowed list | `CallerIpNotAllowed` | Caller IP address {ip-address} is not allowed. Access denied.

所以，我們就可以運用`on-error`捕捉錯誤的特性、加上判斷 `CallerIpNotAllowed`確認當下的錯誤是來自於`IP Filter`所拋出的例外，範例如下。

```xml
<policies>
    <inbound>
        <base />
        <set-backend-service id="apim-generated-policy" backend-id="carltestaf1" />
        <ip-filter action="allow">
            <address>120.36.57.66</address>
        </ip-filter>
    </inbound>
    <backend>
        <base />
    </backend>
    <outbound>
        <base />
    </outbound>
    <on-error>
        <base />
        <choose>
            <when condition="@(context.LastError.Reason=="CallerIpNotAllowed")">
                <return-response>
                    <set-status code="451" reason="Unavailable For Legal Reasons" />
                    <set-header name="Content-Type" exists-action="override">
                        <value>application/json</value>
                    </set-header>
                    <set-body>{ "statusCode": 451, "message": "Unavailable For Legal Reasons" }</set-body>
                </return-response>
            </when>
        </choose>
    </on-error>
</policies>
```

說明:
1. `IP Filter`我們設定只允許`120.36.57.66`這個IP通過，當不是這個IP進來時就會拋出`401 Forbidden`錯誤。
2. `on-error`的部分我們使用`choose...when`條件判斷式檢查`context.LastError.Reason=="CallerIpNotAllowed"`，如果是的話就進一步處理。
3. 最後就是使用`set-status`自訂回傳狀態、`set-header`自訂回傳的`content-type`及`set-body`自訂回傳的`JSON Body`。

我們可以用`Postman`試打看看，就可以拿到我們自訂的回傳錯誤，如下圖。

![](/Teamdoc/image/azure/APIMCustomError4IPFilter/ipfilter2.png)

## Conclusion
本文主要是利用`IP Filter`會主動拋出`401 Forbidden`例外錯誤的特性、使用`on-error`來自訂錯誤處理，我想這應該是必備的技巧，因為很多時候我們可能對於`401 Forbidden`這個共通的`HTTP Status`很熟悉，但是因為API內外架構的不同，我們希望外層檢查回傳`401 Forbidden`、內層檢查使用自訂的回傳錯誤，這樣就可以很方便地識別問題點在哪裡了。

## References
1. [Error handling in API Management policies](https://learn.microsoft.com/en-us/azure/api-management/api-management-error-handling-policies)
2. [Restrict caller IPs](https://learn.microsoft.com/en-us/azure/api-management/ip-filter-policy)