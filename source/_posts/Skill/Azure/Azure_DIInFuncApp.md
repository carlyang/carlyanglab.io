---
title: Azure - How to use Dependency Injection in Function App
categories:
 - 技術文件
 - Azure
date: 2021/7/5
updated: 2021/7/5
thumbnail: https://i.imgur.com/zPI1Sv5.png
layout: post
tags: [Azure, Dependency Injection, Function App]
---
Author: Carl Yang

![](/Teamdoc/image/azure/logos/function_app.png)

## Purpose
`Function App`作為`Azure Serverless`的服務，可以讓我們專注於商業邏輯的開發、只需要知道一點服務上的相關設定，就可以快速產出大部分情況下所需的相關處理。而到了`.NET Core`時代`Function App`自然也是要支援的，尤其是`.NET Core`中的`Dependency Injection`更是重要，所以本文主要是在說明如何在`Function App`中使用相依注入的功能。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Azure Function App v3 Service

## Introduction
原本撰寫`Function App`只要專注在商業邏輯就好，不過過程中發現雖然`Function APP SDK`都把共同的機制在外層包好好的，但這樣也意味著有些強大的`.NET Core`功能都不能使用，基本上我們只能針對`Function`內部開發而已，其他像`Middleware`等都無法使用(`因為Function App的URL路由幾乎都由它內部在控制，外部僅能指定名稱`)。  
不過好在`Dependency Injection`功能倒是有開放出來給我們使用，它就是`FunctionsStartup`。

## Installation
往下之請務必先將專案安裝下列三個套件。
- Microsoft.NET.Sdk.Functions
- Microsoft.Azure.Functions.Extensions
- Microsoft.Extensions.Http(這個套件是因為我們範例中有使用`HttpClientFactory`所以要用到)

Note:  
因為`Function App SDK`版本變更挺快的，個人是覺得很容易遇到SDK新舊差異造成錯誤，所以如果Build不過可以安裝新版看看、或者更新`Azure Functions Core Tools`，這邊主要在說明`Dependency Injection`的部分，開發環境相關的就先略過不提。

## Sample
廢話不多說，就直接來上Code吧!

```csharp
[assembly: FunctionsStartup(typeof(Startup))]
namespace Sample.FunctionApp
{
    public class Startup : FunctionsStartup
    {
        private IServiceProvider ServiceProvider { get; set; }

        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services.AddHttpClient<MyServices>();

            builder.Services.AddSingleton<MyServices>();

            ServiceProvider = builder.Services.BuildServiceProvider();
        }
    }
}
```

[[範例參考]](https://github.com/carlyang920/AzureFunctionApp.git)

- `FunctionsStartup(typeof(Startup)`: 這行是告訴`Function App`啟動的類別是哪一個Class，這邊就沿用`.NET Core`的`Startup.cs`。
- `public class Startup : FunctionsStartup`: 為了能夠使用Configure進行相關的`DI`設定，我們必須先繼承`FunctionsStartup`並覆寫`Configure()`，我們才能夠自行決定`DI`的相關設定。
- `AddHttpClient` & `AddSingleton`: 這裡就是自訂要注入什麼`Instance`、是否用`HttpClientFactory`來調用`HttpClient`並協助管理底層的`HTTP 連線`、避免耗盡的情況。

執行之後我們就可以`Functions`的建構子拿到我們要的`Instance`、並且是由`.NET Core DI`幫我們管理這些`Instance`的生命週期，如下圖所示。

![](/Teamdoc/image/azure/AzureFunction/Azure_DIInFuncApp/di1.png)

Note:  
過程中發現，由於`DI`不能用在`Static class`，所以如果是用`Function App`預設的樣板建立的Functions，會預設帶上`static`的關鍵字，請務必要拿掉否則`DI`是不起作用的。

## 結論:
`Function App`讓我們可以專注時做商業邏輯、不需要大量的Server部署及管理，這種`Serverless`的概念對於敏捷、快速特別要求的地方是很有用的。  
雖然目前`Function App`除了`Dependency Injection`外，`.NET Core`其它比較重要的部分尚未看到，也就是說它也不是完全支援`.NET Core`的所有功能，所以一些單純的應用就可以使用它來快速疊代、倒也不失為一種好方法喔!

## 參考
1. [[Use dependency injection in .NET Azure Functions]](https://docs.microsoft.com/en-us/azure/azure-functions/functions-dotnet-dependency-injection)