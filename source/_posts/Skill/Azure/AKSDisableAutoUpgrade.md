---
title: AKS - How to disable Auto-upgrade version?
categories:
 - 技術文件
 - Azure
date: 2023/12/15
updated: 2023/12/15
thumbnail: https://i.imgur.com/5sNBEdu.png
layout: post
tags: [Azure, AKS]
---

![](/Teamdoc/image/azure/logos/aks.png)

## Purpose
最近使用`AKS`時發現常會不定時服務中斷，接著就是各系統不斷報錯，但遍查問題發生當下並沒有對`AKS`進行什麼特殊操作，於是開始懷疑`AKS`本身是否有什麼問題，本文的目的就在說明此次狀況的原因及排除方法。
<!-- more -->

## Root Cause
這邊就先直接破題，當我們發現各系統呼叫`AKS`不斷失敗的那段時間(大約維持20~40分鐘不等)，剛好就是`AKS`在進行升級的時段。

Note:  
每次的升級並不一定會造成服務中斷，主要是要看該次升級是更動哪個部分，所以不是每次`AKS`升級都會造成服務中斷。

## Solution
上述的原因其實排查花了不少時間，最後是經由微軟幫忙從後台查看`AKS`紀錄才發現、原來中斷時是因為`AKS`本身的升級所造成。

由於問題是發生在正式環境上，這環境可不是鬧著玩的，三不五時就來個中斷服務，我們光是故障電話就接不完了，所以就要想辦法來`關閉Automatic upgrade功能`!

方法1: 這個方法最簡單，進入`Azure Portal`關閉`Automatic upgrade`就好，如下。

![](/Teamdoc/image/azure/AKSDisableAutoUpgrade/auto_upgrade1.png)

接著再選擇`Disable`即可。

![](/Teamdoc/image/azure/AKSDisableAutoUpgrade/auto_upgrade2.png)

方法2: 使用`Azure CLI`指令。
`主要就是在設定--auto-upgrade-channel為none。`

```cmd
az login

az account set --subscription [you subscription id]

az aks get-credentials --resource-group myResourceGroup --name myAKSCluster

az aks update --resource-group myResourceGroup --name myAKSCluster --auto-upgrade-channel none
```

## Conclusion
其實這個自動升級的功能，對於測試環境還滿方便的，我們其實可以隨時保持新版並發現可能的相容性問題，但是在正式環境是萬萬不可能開放這個功能的，對使用者來說系統穩定性一定高於軟體的新舊版差異，但這個自動升級的功能又常不小心就開在那裏沒關閉，所以本文也是提醒自己要多注意才是。

## References
1. [Use cluster auto-upgrade with an existing AKS cluster](https://learn.microsoft.com/en-us/azure/aks/auto-upgrade-cluster#use-cluster-auto-upgrade-with-an-existing-aks-cluster)
2. [az aks](https://learn.microsoft.com/en-us/cli/azure/aks?view=azure-cli-latest#az-aks-update)