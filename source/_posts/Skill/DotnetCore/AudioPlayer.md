---
title: .NET Core - Audio Player
categories:
 - 技術文件
 - .NET Core
date: 2019/03/31
updated: 2019/03/31
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [.NET Core, Audio Player]
---
作者: Carl Yang

![](/Teamdoc/image/DotnetCore/Logo.png)

## Audio Library for .NET Core
現今播放音樂的需求可謂無處不在，舉凡midi、mp3、wav...等等，可能今天按一個按鈕、就會要求也要有特定音效。而在.NET的世界裡，就有這麼一個音效檔處理的Library - NAudio，可以幫助處理相關的操作，包括多種檔案格式的播放、音檔合併、錄音...等等，其作者為[Mark Heath](https://mvp.microsoft.com/en-us/PublicProfile/5002551?fullName=Mark%20%20Heath)，是一位非常資深的微軟MVP。早從.NET Framework 2.0時代、他就開發NAudio供大家使用，而如今.NET Core平台出現、他也致力將原本的Framework版本、改寫成.NET Standard版，以供如.NET Core的開發者使用。同時，NAudio也是開源軟體的一員，詳細可參考[Github](https://github.com/naudio/NAudio)。
<!-- more -->

## License
NAudio使用Ms-Pl開源協議，簡單地說、它允許免費地被使用在商業用途，但其版權宣告也必須內含在軟體系統內，如果有人更改其程式碼，就必須將其也開源出來。

## 版本
NAudio在1.8.5版本前僅支援.NET Framework，由於其關聯到Windows很多的相關底層元件，故無法直接被使用在.NET Core的環境。但由於跨平台需求與日俱增，Mark Heath也開始致力於改寫為Standard 2.0版本，目前已經開放兩版preview版本在[Nuget](https://www.nuget.org/packages/NAudio)上，相信不久後就會正式發布Stable版本。

## 串流播放
音效檔播放其實就是串流播放，NAudio中統一由WaveStream進行處理，我們也可以透過繼承、來複寫WaveStream的行為，例如:我們希望有循環播放的功能，我們可以實作LoopStream如下[範例來源](https://github.com/naudio/NAudio/blob/master/NAudioDemo/LoopStream.cs)，透過開關EnableLooping屬性達成循環播放的功能。

Note:<br/>
範例程式碼有經過一點改寫，不完全仿照Github上的內容。

```csharp
public class LoopStream : WaveStream
{
    private readonly WaveStream _sourceStream;

    /// <summary>
    /// Creates a new Loop stream
    /// </summary>
    /// <param name="sourceStream">The stream to read from. Note: the Read method of this stream should return 0 when it reaches the end
    /// or else we will not loop to the start again.</param>
    public LoopStream(WaveStream sourceStream)
    {
        _sourceStream = sourceStream;
        EnableLooping = false;
    }

    /// <summary>
    /// Use this to turn looping on or off
    /// </summary>
    public bool EnableLooping { get; set; }

    /// <inheritdoc />
    /// <summary>
    /// Return source stream's wave format
    /// </summary>
    public override WaveFormat WaveFormat => _sourceStream.WaveFormat;

    /// <inheritdoc />
    /// <summary>
    /// LoopStream simply returns
    /// </summary>
    public override long Length => _sourceStream.Length;

    /// <inheritdoc />
    /// <summary>
    /// LoopStream simply passes on positioning to source stream
    /// </summary>
    public override long Position
    {
        get => _sourceStream.Position;
        set => _sourceStream.Position = value;
    }

    public override int Read(byte[] buffer, int offset, int count)
    {
        var totalBytesRead = 0;

        while (totalBytesRead < count)
        {
            var bytesRead = _sourceStream.Read(buffer, offset + totalBytesRead, count - totalBytesRead);
            if (bytesRead == 0)
            {
                if (_sourceStream.Position == 0 || !EnableLooping)
                {
                    // something wrong with the source stream
                    break;
                }
                // loop
                _sourceStream.Position = 0;
            }
            totalBytesRead += bytesRead;
        }
        return totalBytesRead;
    }
}
```

## Player Design
本文主要示範.mp3及.wav的範例程式碼，我們先建立Interface規範相同的Player行為。

```csharp
public interface IAudioPlayer : IDisposable
{
    bool EnableLooping { get; set; }

    void Play();
    void Stop();
    void Pause();
}
```

主要有一個控制循環播放的屬性-EnableLooping、及Play(播放)、Stop(停止)、Pause(暫停)。

## Audio Player
因為不同檔案格式需要使用不同的File Reader進行解析、讀取、最後使用串流播放，差異只有在File Reader的不同，故我們建立一個共用的Audio Player實作IAudioPlayer，並在Constructor傳入不同的WaveStream類型，以進行播放。

```csharp
public class AudioPlayer : IAudioPlayer
{
    private readonly WaveStream _reader;
    private readonly LoopStream _loop;
    private readonly WaveOutEvent _waveOut;

    public bool EnableLooping
    {
        get => _loop.EnableLooping;
        set => _loop.EnableLooping = value;
    }

    public AudioPlayer(WaveStream stream)
    {
        _reader = stream;
        _loop = new LoopStream(_reader);
        _waveOut = new WaveOutEvent();
    }

    ~AudioPlayer()
    {
        Dispose();
    }

    public void Play()
    {
        if (_waveOut.PlaybackState == PlaybackState.Stopped)
        {
            _loop.Position = 0;
            _waveOut.Init(_loop);
        }

        _waveOut.Play();
    }

    public void Stop()
    {
        _waveOut.Stop();
    }

    public void Pause()
    {
        _waveOut.Pause();
    }

    public bool IsStop()
    {
        if(_waveOut.PlaybackState == PlaybackState.Stopped)
        {
            return true;
        }

        return false;
    }

    public void Dispose()
    {
        _reader?.Dispose();
        _loop?.Dispose();
        _waveOut?.Dispose();
    }
}
```

Note:<br/>
1. 請特別注意使用WaveOutEvent在.NET Core環境進行音效播放的操作，舊版的IWavePlayer雖然仍可調用，但實際上仍不支援。
2. _waveOut.PlaybackState: 可以透過這個屬性判斷目前的播放狀態。
3. _waveOut.Init(_loop): 是用來初始化串流處理的功能，如果重複呼叫會造成錯誤，請務必確認播放或暫停狀態下，不要調用Init()。
4. 外部使用上請務必記得使用Dispose()來釋放占用的相關資源，如Stream等等。

## MP3 & WAV Player
前一小節提到，player都是使用WaveStream進行串流處理，只差在File Reader不同，以下為兩種常見的Reader範例。

```csharp
public class Mp3Player : AudioPlayer
{
    public Mp3Player(string filePath) : base(new Mp3FileReader(filePath))
    {

    }

    ~Mp3Player()
    {
        Dispose();
    }
}
```

```csharp
public class WavPlayer : AudioPlayer
{
    public WavPlayer(string filePath) : base(new WaveFileReader(filePath))
    {
        
    }

    ~WavPlayer()
    {
        Dispose();
    }
}
```

Note:<br />
由上範例可知，我們都繼承了AudioPlayer，僅在Constructor使用不同的File Reader讀取不同格式的檔案，並將其串流至WaveStream後、統一交給父類別AudioPlayer處理。

## 播放/停止/暫停

- Play
```csharp
_player = new Mp3Player(@".\AudioFiles\alarm.mp3");
_player.Play();
```

- Stop
```csharp
_player = new Mp3Player(@".\AudioFiles\alarm.mp3");
_player.Play();
_player.Stop();
```

- Pause
```csharp
_player = new Mp3Player(@".\AudioFiles\alarm.mp3");
_player.Play();
_player.Pause();
```

- Play Looping
```csharp
_player = new Mp3Player(@".\AudioFiles\alarm.mp3");
_player.EnableLooping = true;
_player.Play();
```

Note:<br />
透過EnableLooping屬性，我們可以方便地切換循環播放功能。

## 結論
NAudio的功能非常豐富，本文僅介紹了其中的冰山一角，有興趣的朋友可以參考作者[Mark Heath](https://markheath.net/category/naudio)的部落格、或是[GitHub](https://github.com/naudio/NAudio)中的Sample Code，相信會有更多收穫。
