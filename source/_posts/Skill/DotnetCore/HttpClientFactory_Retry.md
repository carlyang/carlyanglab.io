---
title: .NET Core - HttpClient Factory & Polly
categories:
 - 技術文件
 - .NET Core
date: 2019/10/24
updated: 2019/10/24
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [.NET Core, HttpClient Factory, Polly]
---
作者: Carl Yang

![](/Teamdoc/image/DotnetCore/Logo.png)

## HttpClient Factory
HttpClient是目前最新的Http Request/Response處理元件，其中有些撰寫上的設計及使用原則可以參考本篇[[Should I make HttpClient reusable?]](http://ktcinno.tw.kingston.corp/teamdoc/2018/08/28/Skill/WebAPI/HttpClientReusing/)。  
但最近發現一個更好用的HttpClientFactory、搭配Microsoft.Extensions.Http.Polly套件，完成HttpClient共用連線、重試策略等功能。
<!-- more -->

## 概念
> .Net進行Http Request/Response目前最新的元件是HttpClient，他也支援許多非同步的方法，提供更好的應用情境。但他也存在一些缺點，例如:`沒有重複使用HttpClient instance造成效能降低、Socket連線耗盡、DNS不會即時更新等問題`。  
> 而HttpClientFactory就是為了解決上述的問題衍生出的解決方案，`每個HttpClient instance都透過HttpClientFactory獲取、然後底層的HttpMessageHandler也由HttpClientFactory管理及重用、避免效能及連線耗盡`，可由下面這張圖說明。
![](https://docs.microsoft.com/zh-tw/dotnet/architecture/microservices/implement-resilient-applications/media/image3.5.png)

- `透過HttpClientFactory取得的ClientService，使用時都是從Pool取得HttpMessageHandler`，也就是說如果某個Http Connection已經存在，那HttpClientFactory就會給相同的連線、讓外部重用，如果不存在才會建立新的連線。  
- 這樣的好處就是HttpClient可以不用每次都要花時間重新建立連線、且透過重用連線也能夠降低Socket使用的連線數。
- `當Http使用完連線、HttpClient被回收之後，Socket連線並不會被馬上刪除，而是進入TIME_WAIT狀態，直到240秒後由Windows自行回收。`試想想，如果每一個Http Request/Response都新建連線、用完再自行等待4分鐘後由系統回收才能真正移除，當一台Server上短時間內需要大量的Http存取時，很容易就會爆炸了。

## 安裝元件
1. 本文是以.Net Core 2.2為實驗環境。
2. .NET Core自2.1以後因已預設參考Microsoft.AspNetCore.App，已內建 Microsoft.Extensions.Http plugin，而HttpClientFactory就位於此plugin內，故無須額外安裝套件，使用時直接透過DI注入即可。
3. .NET Framework 4.7.2以後的使用者則可以安裝Microsoft.Extensions.DependencyInjection來使用，可參考[[在 ASP.NET MVC 5 中使用 ASP.NET Core Dependency Injection 與 HttpClientFactory]](https://blog.yowko.com/aspnet-core-di-httpclientfactory-in-aspnet-mvc-5/)。
4. 為了實現重試策略，需安裝Microsoft.Extensions.Http.Polly套件，這個套件是以[[Polly GitHub]](https://github.com/App-vNext/Polly)為基礎、wrapping而成。

## 使用方法
1. 直接注入HttpClient  
我們可以透過DI直接註冊HttpClient、然後在Constructor注入HttpClient instance使用。

先在ConfigureServices中註冊。
```csharp
public void ConfigureServices(IServiceCollection services)
{
    services.AddHttpClient();
}
```

再注入Constructor使用
```csharp
public class ClientService
{
    private HttpClient _httpClient;

    public ClientService(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    // ...
}
```

2. 具名註冊  
我們也可以在ConfigureServices中指定名稱註冊。
```csharp
public void ConfigureServices(IServiceCollection services)
{
    services.AddHttpClient("NamedHttpClient", h =>
    {
        h.BaseAddress = new Uri("http://namedhttpclient.test.com/");
    });
}
```

再於Constructor中注入HttpClientFactory以取得HttpClient Instance。
```csharp
public class ClientService
{
    private IHttpClientFactory _httpClientFactory;

    public ClientService(IHttpClientFactory httpClientFactory)
    {
        _httpClientFactory = httpClientFactory;
    }

    public Task<HttpResponseMessage> PostAsync()
    {
        var httpClient = _httpClientFactory.CreateClient("NamedHttpClient");
    }
}
```

3. 自訂類型  
我們也可以註冊自訂類型的方式，使用HttpClientFactory，於ConfigureServices中註冊自訂類別。
```csharp
public void ConfigureServices(IServiceCollection services)
{
    services.AddHttpClient<ClientService>();
}
```

然後在自訂類別中注入HttpClient instance。
```csharp
public class ClientService
{
    private HttpClient _httpClient;

    public ClientService(HttpClient httpClient)
    {
        _httpClient = httpClient;

        _httpClient.BaseAddress = new Uri("http://namedhttpclient.test.com/");
    }

    // ...
}
```

最後再注入自訂類別使用。
```csharp
public class ClientController
{
    private ClientService _clientService;

    public ClientController(ClientService clientService)
    {
        _clientService = clientService;
    }

    // ...
}
```

## 重試策略
- 重試策略是指當我們進行Http Request/Response時，偶而會遇到斷線、網路不穩、對方站台回應過慢...等等狀況，這時候有可能只是暫時性的、過陣子就會恢復正常，我們可以適當地加以重試、以完成工作。
- 而Polly幫我們實作了各式各樣重試模式，例如:一般重試間隔、指數輪詢(exponential backoff)、斷路器模式(Circuit Breaker)等，我們可以選用適合的模式進行重試。

1. 一般重試間隔  
一般我們會指定每次重試間的等待時間、直到該次HttpClient Timeout。
```csharp
public static void ConfigureServices(IServiceCollection services)
{
    services.AddHttpClient<ClientService>()
            .AddPolicyHandler(GetRetryPolicy());
}

private static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
{
    return HttpPolicyExtensions
        //For HttpRequestException/5XX/408 errors
        .HandleTransientHttpError()
        //For 404 NoFound
        .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.NotFound)
        //Retry N times and latency M seconds
        .WaitAndRetryAsync(
            3,
            retryTimes => TimeSpan.FromSeconds(30))
        );
}
```

Note:  
`使用Retry Policy是在註冊時，利用AddPolicyHandler()將Polly的Retry Policy同時註冊進HttpClientFactory中。`

2. 指數輪詢(exponential backoff)  
這個方式是指透過指述的方式、逐次拉長每次重試間的等待時間，直到重試次數結束或HttpClient Timeout為止。
```csharp
public static void ConfigureServices(IServiceCollection services)
{
    services.AddHttpClient<ClientService>()
            .AddPolicyHandler(GetRetryPolicy());
}

private static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
{
    return HttpPolicyExtensions
        //For HttpRequestException/5XX/408 errors
        .HandleTransientHttpError()
        //For 404 NoFound
        .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.NotFound)
        //Retry N times and latency M seconds
        .WaitAndRetryAsync(
            9,
            retryTimes => TimeSpan.FromSeconds(Math.Pow(2, retryTimes)),
            OnRetryAsync
        );
}
```

Note:  
- HandleTransientHttpError()是指定當發生5XX系列的Http錯誤時要套用此Retry Policy。
- OrResult()是指定當發生404的Http錯誤時要套用此Retry Policy。你也可以指定其他的Http錯誤進行套用。
- WaitAndRetryAsync()則是指定Retry次數及等待時間。

3. 斷路器模式(Circuit Breaker)  
此模式可以指定當Retry幾次後、就休息一段時間，然後再繼續Retry。以下方程式碼為例，引用微軟文件的描述如下。

[[MS Doc]](https://docs.microsoft.com/zh-tw/dotnet/architecture/microservices/implement-resilient-applications/implement-circuit-breaker-pattern)
>斷路器原則設定為在重試 Http 要求時，若連續五次失敗，則會中斷或開啟網路。 發生此情況時，網路會中斷 30 秒：在這段期間，斷路器會立即使呼叫失敗，而不是實際發出
```csharp
public static IAsyncPolicy<HttpResponseMessage> GetCircuitBreakerPolicy()
{
    return HttpPolicyExtensions
        .HandleTransientHttpError()
        .CircuitBreakerAsync(5, TimeSpan.FromSeconds(30));
}
```
Note:  
斷路器模式主要在狀態的切換，根據[[Polly GitHub]](https://github.com/App-vNext/Polly/wiki/Polly-and-HttpClientFactory)的說明，它屬於Retry內部的一種狀態Handling，特定條件可重發Request、特定條件又可退回一般Retry機制。
![](https://user-images.githubusercontent.com/9608723/39209461-b9d004ec-47fd-11e8-8e16-0a378a9ee871.PNG)
```csharp
services.AddHttpClient(/* etc */)
    .AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
    {
        TimeSpan.FromSeconds(1),
        TimeSpan.FromSeconds(5),
        TimeSpan.FromSeconds(10)
    }))
    .AddTransientHttpErrorPolicy(builder => builder.CircuitBreakerAsync(
        handledEventsAllowedBeforeBreaking: 3,
        durationOfBreak: TimeSpan.FromSeconds(30)
    ));
```

4. Jitter重試  
所謂的Jitter重試，其實就是指定等待時間的一個隨機尾數，讓Retry間的間隔時間變成在一個範圍內隨機跳動、有長有短，`可以避免因為所有的等待間隔相同、造成同一時間點產生大量的Http Request`。
```csharp
private static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
{
    async Task OnRetryAsync(
        DelegateResult<HttpResponseMessage> result, 
        TimeSpan timespan, 
        int retryCount, 
        Context ctx
        )
    {
        await Task.Run(() =>
        {
            Console.WriteLine(string.Empty);
            Console.WriteLine($"FunctionName: [{nameof(Program)}] {nameof(GetRetryPolicy)}()");
            Console.WriteLine($"Result: {JsonConvert.SerializeObject(result)}");
            Console.WriteLine($"Timespan: {timespan}");
            Console.WriteLine($"CurrentTime: {DateTime.Now:yyyy-MM-dd HH:mm:ss.fff}");
            Console.WriteLine($"RetryCount: {retryCount}");
            Console.WriteLine($"Context: {JsonConvert.SerializeObject(ctx)}");

            EventLogHelper.WriteErrorEventLog(
                JsonConvert.SerializeObject(
                    new
                    {
                        FunctionName = $"[{nameof(Program)}] {nameof(GetRetryPolicy)}()",
                        Result = JsonConvert.SerializeObject(result),
                        Timespan = timespan,
                        CurrentTime = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff}",
                        RetryCount = retryCount,
                        Context = JsonConvert.SerializeObject(ctx)
                    }
                )
            );
        });
    }

    var jitter = new Random();

    return HttpPolicyExtensions
        //For HttpRequestException/5XX/408 errors
        .HandleTransientHttpError()
        //For 404 NoFound
        .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.NotFound)
        //Retry N times and latency M seconds
        .WaitAndRetryAsync(
            9,
            retryTimes => TimeSpan.FromSeconds(Math.Pow(2, retryTimes)) + TimeSpan.FromMilliseconds(jitter.Next(0, 100)),
            OnRetryAsync
        );
}
```

Note:
- 上面範例使用一個jitter隨機0~100的毫秒數，加上原先的等待秒數，最後真正的等待時間就會是前後100毫秒內的隨機數。
- 這裡掛了一個內部Function - OnRetryAsync()，是為了在每次的Retry失敗時、將錯誤資訊紀錄進Event Log中，以便事後追查。

## 結論:
透過HttpClientFactory可以重用連線、降低Socket耗盡的風險、以及自動更新DNS資訊等等，讓我們不必再自行實作HttpClient的相關機制，真的是非常好的東西，有興趣的朋友不妨查閱微軟文件，有更多的範例可供參考。

## 參考
1. [實作復原應用程式](https://docs.microsoft.com/zh-tw/dotnet/architecture/microservices/implement-resilient-applications/)
2. [在 .NET Core 與 .NET Framework 上使用 HttpClientFactory](https://blog.yowko.com/httpclientfactory-dotnet-core-dotnet-framework/)
3. [Using Polly with HttpClient factory from ASPNET Core 2.1](https://github.com/App-vNext/Polly/wiki/Polly-and-HttpClientFactory)