---
title: .NET Core - SignalR Retry Policy
categories:
 - 技術文件
 - .NET Core
date: 2021/12/15
updated: 2021/12/15
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [.NET Core, SignalR, Retry Policy]
---
作者: Carl Yang

![](/Teamdoc/image/DotnetCore/Logo.png)

## Purpose
本文目的主要在說明如何使用自訂的`Retry Policy`、不受原生`SignalR`重連失敗四次後就不再繼續的限制。
<!-- more -->

## Prerequisites
本文是使用.NET Core 3.1作為開發環境進行練習，請先安裝下列項目。
- 請先安裝.NET Core 3.1 SDK([Download](https://dotnet.microsoft.com/download/dotnet/3.1))

## Introduction
相信`SignalR`大家都已經不陌生了，透過`Push`機制發送Message不僅即時、也避免不斷`Pulling`浪費資源的問題，而`WebSocket`更是建立在通用的80或443 Port上，通透性也較不受防火牆等網路限制。  
雖然在建立`SignalR`連線時可以設定自動斷線重連，但預設它卻只會重連四次、如果都失敗就會停止。在一些特殊情境如系統維護作業時間，常常都會超過它重連的四次的時間，維護結束後都還要回來手動重啟才能再重連，於是就`希望它能夠無限嘗試重連、直到重連成功為止!`

## Reconnect Automatically
首先，讓我們先介紹自動重連的方法，就是在一開始建立`SignalR`連線時指定讓它自動重連。  
如下段Code，我們只要在建立連線之初、指定`WithAutomaticReconnect()`就可以開啟自動重連功能。

```csharp
var _connection = new HubConnectionBuilder()
    .WithUrl(url)
    .WithAutomaticReconnect()
    .Build();
```

Note:  
根據[[微軟文件]](https://docs.microsoft.com/en-us/aspnet/core/signalr/dotnet-client?view=aspnetcore-3.1&tabs=visual-studio#handle-lost-connection)所述，開啟自動重連後、預設它會等待0秒、2秒、10秒、30秒後重連，總共就是重連四次的意思，引述如下。

> Without any parameters, WithAutomaticReconnect() configures the client to wait 0, 2, 10, and 30 seconds respectively before trying each reconnect attempt, stopping after four failed attempts.

## Custom Retry Policy
為了能夠克服四次重連後停止的限制，我們就必須自己定義`Retry Policy`、然後丟進`WithAutomaticReconnect()`中即可，有注意到引述中的開頭`Without any parameters`嗎?它其實就已經在告訴我們可以自訂傳入參數了。  

讓我們先建立自訂的`Retry Policy`如下，先實作`Interface: IRetryPolicy`、然後撰寫`Function: NextRetryDelay`內容，這邊我們就直接每次都回傳`TimeSpan`並設定30~100秒隨機等待秒數，如此`SignalR`連線就會無限地每次等待`TimeSpan`的指定秒數後重新連線。

```csharp
using System;
using System.Security.Cryptography;
using Microsoft.AspNetCore.SignalR.Client;

namespace IND4.API.Hubs
{
    public class InfinitelyRetryPolicy : IRetryPolicy
    {
        public TimeSpan? NextRetryDelay(RetryContext retryContext)
        {
            return new TimeSpan(0, 0, RandomNumberGenerator.GetInt32(30, 100));
        }
    }
}
```

接著再把剛自訂的`InfinitelyRetryPolicy`丟進`WithAutomaticReconnect()`即可。

```csharp
var _connection = new HubConnectionBuilder()
    .WithUrl(url)
    .WithAutomaticReconnect(new InfinitelyRetryPolicy())
    .Build();
```

Note:  
必須特別說明一下，`以上方法僅能用於斷線重連的情況，如果一開始啟動SignalR就連不上目標的話(例如:Server未啟動)、是不會觸發自動重連的，這時候還是必須人工介入檢查Server到底發生什麼狀況並排除才行`。

## 結論
其實撰寫本文的起因，是因為公司常需要周末進行Server更新，而`SignalR`又放在`Docker`上跑`Container`，所以Server重啟後沒意外的話`Container`也就跟著自動重啟了。但`SignalR Server`端可能需要很久時間才會重啟，畢竟光是Windows更新就不少時間了，還要再加上其他人工作業等時間(也可能是系統重新佈署)，只有四次重連實在是不太夠用，所以才希望能無限自動重連，提供給大家參考看看。

## 參考
1. [[ASP.NET Core SignalR .NET Client]](https://docs.microsoft.com/en-us/aspnet/core/signalr/dotnet-client?view=aspnetcore-3.1&tabs=visual-studio)