---
title: Powershell - Start/Close Windows Services
categories:
 - 技術文件
 - Windows
date: 2021/06/28
updated: 2021/06/28
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [Windows, Powershell, Kill Process]
---
作者: Carl Yang

![](/Teamdoc/image/Windows/windows_logo.png)

## Purpose
有些時候Windows被裝了一些背景執行的服務，有時候造成系統反應變慢、有時會莫名其妙不能連網站，這時候如果去`Task Manager`可能會查到這些`Process`名稱，但要怎麼關掉它呢?每次關掉後又自動起來怎麼辦?本文就是要說明如何使用`Powershell`指令，一鍵處理這樣的情況。
<!-- more -->

## Prerequisites
本文是使用Windows 10環境進行練習，請先具備下列項目。
- 請確認系統具有Powershell

## Introduction
最近`WFH`的人相信也不少，對於公司願意為了員工著想、開放在家上班真的是感激在心，尤其是上班地點身處熱區的我、更是心安不少，畢竟家有老小、三餐要靠我養家餬口，出了什麼事可是全家遭殃呢!(當然...所有人都是一樣的憂心，不只有我一個)  

前因說完，其實本文重點就在`WFH`這裡，因為公司開放`WFH`所以有些安全性問題都會有所限制，例如，公司在我們的NB上裝了某某VPN軟體，雖然讓我們可以方便連入公司內網處理公事，也限制了一些公有雲的資源存取，要關掉這些背景的`Windows Services`又要我們輸入帳密，但我們正常當然拿不到這組帳密囉。  

但是手動找出這些`Windows Services`其實不麻煩、`Task Manager`打開觀察看看應該都找得出來，問題在於這些就算手動砍了它還是會自動重跑，關不掉、砍不掉、更沒有帳密可循正常流程關閉。  

但我又很多機會需要聯外網處理公有雲的東西，這時候被`VPN`卡住真的造成很多困擾，一下連上DB卻不能執行`SQL`，一下可以列出`Storages`卻無法打開內容...諸如此類的怪現象，各位可能會說，那把執行檔更名或刪除就好啦!  

哈哈，這樣只對一半，我想做的是按下Enter鍵直接關閉所有相關`Windows Services`、再跑一次就重開，既像開電燈一樣，我要開就開、要關也是一個動作搞定、隨時切換，這時候就要來寫一下`Powershell`指令囉。

## `Powershell`
其實撇開`Powershell`指令囉嗦的部分先不談，有時候它其實還滿好用的，學到深處應該也是能做到很多事情(不過我不是專家XD)，以下就是我的指令:

```powershell
Get-ChildItem -Path "[folder path]\*.exe" -File -Recurse | ForEach-Object {
    if ($_.Name.StartsWith('_')) {
		$_ | Rename-Item -NewName $_.Name.TrimStart('_')
		Start-Service -Name $_.Name.TrimStart('_').Replace(".exe", "")
	} else {
		$_ | Rename-Item -NewName ('_' + $_.Name)
		$_ | taskkill /f /im $_.Name.TrimStart('_')
	}
}
```

- `Get-ChildItem`: 取得某路徑下的所有`符合Pattern`的檔案，`-File`是只取檔案類型、`-Recurse`則是向下找所有子資料夾。
- `Pipe`: 就是`|`符號，它是管線的意思，可以讓我們串接一連串指令動作、達成我們最後要的結果，再這裡就是把前面拿到的檔案、丟給後面指令接續處理。
- `ForEach-Object`: 其實就是迴圈，一個個處理前一個指令丟進來的檔案。
- `$_`: 這個其實就是一個匿名物件，代表一個檔案及其相關資訊，所以這裡會用`$_.Name`來取得檔名。
- `Rename-Item`: 就是改檔名，因為`Windows Services`會自己砍掉又重啟，所以在砍掉`Process`前要先改名，讓它找不到就無法重啟了。
- `taskkill`: 它其實是`CMD`指令、並非`Powershell`的，但我用`Powershell`的`Stop-Process`竟然砍不掉process，所以只好改用`taskkill`砍，後面`/f`是強制關閉、`/im`是指定process名稱(可以用`wildcard: *`，但比較危險)。


## `XXX.ps1`
.ps1檔就是`Powershell`的Script檔，我們可以把多個指令寫成一個ps1檔、然後再執行檔案就可以批次處理囉。

[[Sample]](https://github.com/carlyang920/SampleScripts/blob/master/Powershell/KillWindowsService.ps1)

## 結論
有些人可能覺得這篇文很無聊，其實我也是這樣覺得XD。但仔細想想，總比讓人每次都只會手動工作管理員內`End Task`、然後改檔名 & 重開機吧!我也可以記錄一下加深印象，而且有些軟體真的很討厭，可能會嚴重影響工作順暢度呢，所以有需要的人可以參考看看囉。

## 參考
1. [[Kill Windows Services]](https://github.com/carlyang920/SampleScripts/blob/master/Powershell/KillWindowsService.ps1)