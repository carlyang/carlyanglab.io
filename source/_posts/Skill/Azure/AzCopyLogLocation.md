---
title: Azure - AzCopy Log Location
categories:
 - 技術文件
 - Azure
date: 2024/5/21
updated: 2024/5/21
thumbnail: https://i.imgur.com/8uMcIBL.png
layout: post
tags: [Azure, Blob, AzCopy]
---
作者: Carl Yang

![](/Teamdoc/image/azure/logos/storage_account.png)

## Purpose
`AzCopy`是批次處理大量`Storage Blobs`的利器、也是絕大多數推薦的工具，微軟也是主推使用這個工具來處理大量的`Blobs`檔案，不論是單/雙向同步、地/雲端同步等都非常好用，而且微軟也不斷在更新優化`AzCopy`的功能以達到更快、更穩定的`Blobs`檔案處理操作，而`AzCopy`在運作時會產生自己的Log檔案，如果太久沒清可能會造成磁碟空間不足的問題，所以本文的目的就在說明如何解決這個狀況(`本文皆以Windows的操作為例`)。
<!-- more -->

## AzCopy Log Position
事實上，每次`AzCopy`的操作都一定會儲存本身的Logs檔案、並且檔名會以每次Run都不重複的`Job ID`命名，以方便事後我們可以依照`Job ID`來查詢追蹤相關的Logs內容，也作為Debug的重要線索，所以要使用`AzCopy`學習如何查閱相關Logs也是很重要的一環，`因為AzCopy只是一隻.exe執行檔，一旦跑完Process就消滅了，事後要再去追查Process是不太可能的事情，所以為了排除故障、了解當下發生什麼狀況，就必須依靠這些Logs的線索了`。

但是，`AzCopy`的Logs如果沒有去整理它，它只會不斷增加、不會減少，預設的Logs儲存位置如下。

```cmd
%USERPROFILE%\.azcopy
```

由上面可以知道，`AzCopy`的預設Logs位置是放在`執行當下的User帳號資料夾之下的.azcopy資料夾!`所以要特別注意看是誰跑的Logs檔案，不過這只是預設的儲存位置。

而這次遇到的問題就是Logs的數量太過龐大，而這個`%USERPROFILE%`位置事實上就是使用到windows系統的安裝磁碟，我們將`AzCopy`做成Schedule Job後定時執行它、而Logs都一直放在預設的儲存位置，導致過了幾年後發現系統槽已經快要被用光光了!

## Custom Log Location
在說明自訂Logs位置之前須要先說明一下`AzCopy`的Logs檔案結構，基本上有下量兩種檔案。

- `\.azcopy\plans\*.steV[Version No]`: 執行計畫相關Logs。
- `\.azcopy\*.log`: `AzCopy`Logs本身的紀錄。

所以，自訂Logs位置也需要分開兩種的位置設定來處理，Plans相關Logs位置設定可參考[[微軟文件]](https://learn.microsoft.com/en-us/azure/storage/common/storage-use-azcopy-configure?toc=%2Fazure%2Fstorage%2Fblobs%2Ftoc.json#change-the-location-of-plan-files)，我們可以使用下列指令進行設定。

```powershell
PowerShell:$env:AZCOPY_JOB_PLAN_LOCATION="<value>"
In a command prompt use:: set AZCOPY_JOB_PLAN_LOCATION=<value>
```

由上可知，`AzCopy`每次執行會去讀取`AZCOPY_JOB_PLAN_LOCATION`所設定的路徑來儲存Plans Logs檔案。

另一種則是一般的AzCopy Logs檔案，其中會包含Info or Error等資訊，自訂Logs位置可參考[[微軟文件]](https://learn.microsoft.com/en-us/azure/storage/common/storage-use-azcopy-configure?toc=%2Fazure%2Fstorage%2Fblobs%2Ftoc.json#change-the-location-of-log-files)，我們可以使用下列指令進行設定。

```powershell
PowerShell:$env:AZCOPY_LOG_LOCATION="<value>"
In a command prompt use:: set AZCOPY_LOG_LOCATION=<value>
```

Note:  
請注意，`經過實測上列指令設定變數的方式，僅作用在當次視窗的AzCopy執行，也就是說如果重開PS/CMD、或是像我們自己寫AP啟動Process的方式，上列設定的變數值就會失效了!`  
所以建議是將`AZCOPY_JOB_PLAN_LOCATION`及`AZCOPY_LOG_LOCATION`設定為全域環境變數，`且全域環境變數設定後需要重啟AzCopy才能讀取新的設定值`，如下圖。

![](/Teamdoc/image/azure/AzCopyLogLocation/env1.png)

經過設定並重啟`AzCopy`後，我們就可以觀察到Logs已經會自動寫到自訂的位置了。

![](/Teamdoc/image/azure/AzCopyLogLocation/loc1.png)

## AzCopy Jobs Clean
經過上述的設定，應該已經可以將Logs移到另一個自訂位置了，但還記得我們一開始想要自訂Logs位置的原因嗎?`是因為太久沒清理，造成Logs一直堆積，最後占用太多的磁碟空間!`所以，上面小節我們先將Logs移到我們自己方便管理的位置(例如:磁碟空間較大的位置)，這小節就要來介紹如何使用`AzCopy`的`Jobs Clean`指令清除不需要保留的Logs檔案，盡可能節省不必要的磁碟空間使用，如下指令。

```cmd
azcopy jobs clean --with-status=completed
```

上列指令真的很簡單，就是啟動`AzCopy Jobs Clean`指令、並且指定只清除`status為completed狀態的Job Logs`，而completed狀態就是每次執行的`AzCopy Job`最後是`成功完成`的結果，這是因為我們留Logs基本上是為了某天發生錯誤時、可以保留當下的錯誤資訊、方便我們日後追蹤查閱，如果該次Job已經都成功完成了，其實Logs也不需要再留著了，如果硬要留反而會造成我們在查閱Error Logs時、檔案過多的困擾。

Note:  
1. 請注意，`上列指令我們可以改寫成.ps1檔，然後利用Windows的Task Scheduler設定定期執行，如此就不用時常手動清理Jobs`，至於設定`Task Scheduler`的方法這邊就不多囉嗦了，相信網上已經有很多教學文可以參考。
2. 另外，執行`AzCopy Jobs Clean`指令它本身也會產生一個Log檔，但是觀察下來這個Log檔只會有1KB左右，所以占用的磁碟非常小、有空時再手動刪除即可。
3. 其實經過觀察，`AzCopy`的Logs檔案還有其它種的資料，例如:檔名以`-scanning.log`結尾的檔案，這種是無法用`AzCopy Jobs Clean`指令清除的，這些可能還是必須要一段時間就手動進去清理，但它也比原本的Logs檔案小很多、也比較不用擔心累積的問題(基本上一大段時間進去清一下即可)。

## Conclusion
本文主要在介紹如何用全域環境變數的方式自訂`AzCopy`的Logs位置，並且利用`AzCopy Jobs Clean`指令、搭配`Task Scheduler`定期清理不需要的`AzCopy` Logs，雖然是很簡單的操作、實用性卻是不錯，千萬不要經年累月累積了一大堆Logs才發現磁碟空間不足以後才要來清，到時候可能因為磁碟空間不足、光是要清理就等大半天的窘境。

## References
1. [Change the location of plan files](https://learn.microsoft.com/en-us/azure/storage/common/storage-use-azcopy-configure?toc=%2Fazure%2Fstorage%2Fblobs%2Ftoc.json#change-the-location-of-plan-files)
2. [Change the location of log files](https://learn.microsoft.com/en-us/azure/storage/common/storage-use-azcopy-configure?toc=%2Fazure%2Fstorage%2Fblobs%2Ftoc.json#change-the-location-of-log-files)