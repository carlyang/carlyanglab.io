---
title: Redis - Lua Scripting
categories:
 - 技術文件
 - Redis
date: 2018/9/11
updated: 2018/9/11
thumbnail: /Teamdoc/image/Redis/RedisTitle1.png
layout: post
tags: 
 - Lua Script
---
作者: Carl Yang

## Redis
![](/Teamdoc/image/Redis/RedisTitle1.png)
<center>([_圖片來源_](https://msdnshared.blob.core.windows.net/media/MSDNBlogsFS/prod.evol.blogs.msdn.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/01/43/96/140720/1031.redis.png))</center>
Lua是一種輕量級的Scripting Language，詳細內容可參考[Lua](https://www.lua.org/about.html)，而Redis從2.6.0版以後支援Lua Scripting的功能，開發者可以透過撰寫複雜的Lua Script處理存放於Redis Cache中的資料、並回傳結果給呼叫端。本篇簡單介紹基本的Lua Script撰寫、如何使用redis-cli及StackExchange.Redis傳送給Redis Server執行等方式。
<!--more-->

### Hello World!
當Client將Lua Script內容傳送至Redis Server後，Redis會將該段Script轉譯成一個function、並命名為f_[SHA Code]、例如:f_31a5b4a25ce0d53e57e6d46c955a16493ae9899e，底線後的就是自動產生的SHA代碼，故執行Lua Script時可以有兩種方式、EVAL或EVALSHA指令。
1. EVAL: 這個指令可以直接將Lua內容傳送給Redis Server執行。
2. EVALSHA: 這個指令可帶入SHA代碼執行已建立的Lua function。

<font style="color:red;">**Note:**
**由於Redis執行Lua Script時會確保同時不會有其他Script或命令在執行、也就是以Single thread的方式在存取鍵值資料，有可能在執行大量Lua計算的時候、因為獨佔資源而造成其他Thread發生Timeout錯誤，故在使用Lua時務必確保在任何時候僅有一個Thread在處理，盡量不要將需要大量運算時間的工作交由Lua處理、或是改由獨立的Redis Server來運算。**
**而什麼程度才可視為大量的運算，則需要視當下情況判斷，相關文件可參考[Atomicity](https://gist.github.com/fxn/4261018)，文中改為使用SHA1調用Lua Function即可避免Race Conditioin的問題。**
</font>

- #### redis-cli
```
EVAL "return 'Hello world!'" 0
```
結果:
"Hello world!"

先使用SCRIPT LOAD建立Lua function、取得SHA代碼後再執行
```
SCRIPT LOAD "return 'Hello world!'"
```
結果: 取得已建好的Function SHA代碼
"9a3fdf49135ad384c54e462ac90fc0395600638c"

```
EVALSHA 9a3fdf49135ad384c54e462ac90fc0395600638c 0
```
結果:
"Hello world!"

- #### StackExchange.Redis
```csharp
var sha = RedisServer.ScriptLoad(
	LuaScript.Prepare(@"
		return 'Hello world!'
	")
).Hash;

if (RedisServer.ScriptExists(sha))
	result = RedisDb.ScriptEvaluate(sha);
```
結果:
"Hello world!"

### 基本鍵值計算
Lua Script可以使用redis.call()或redis.pcall()執行特定的command，兩者差異在於redis.call()如果發生錯誤、會回傳Lua類型的Error，如果redis.pcall()發生錯誤、則會回傳Lua的表格錯誤格式，以下為鍵值計算的簡單範例。

- #### redis-cli
```
EVAL "
redis.call('set', 'A', 2)
redis.call('set', 'B', 3)
redis.call('set', 'C', 4)

local A = redis.call('get', 'A')
local B = redis.call('get', 'B')
local C = redis.call('get', 'C')
return A + B + C
" 0
```
結果:
9

- #### StackExchange.Redis
```csharp
var sha = RedisServer.ScriptLoad(
	LuaScript.Prepare(@"
		redis.call('set', 'A', 2)
		redis.call('set', 'B', 3)
		redis.call('set', 'C', 4)

		local A = redis.call('get', 'A')
		local B = redis.call('get', 'B')
		local C = redis.call('get', 'C')
		return A + B + C
	")
).Hash;

if (RedisServer.ScriptExists(sha))
	result = RedisDb.ScriptEvaluate(sha);
```
結果:
9

### List值計算
除了上一小節基本的鍵值計算外，我們也很常用到List儲存同質資料，以下列出如何利用Lua存取List中的值以及加總計算。

- #### redis-cli
```
EVAL "
--The total summary
local sum = 0
--Get total number of list
local items = redis.call('LRANGE', 'ListA', 0, -1)

--summary all values
for i=1,#items
do
    sum = sum + items[i]
end

return sum
" 0
```
結果:
6

- #### StackExchange.Redis
```csharp
var result = RedisDb.ScriptEvaluate(
	LuaScript.Prepare(@"
		--The total summary
		local sum = 0
		--Get total number of list
		local items = redis.call('LRANGE', 'ListA', 0, -1)

		--summary all values
		for i=1,#items
		do
			sum = sum + items[i]
		end

		return sum
	")
);
```
結果:
6

### List中的JSON Parse及Properties存取
我們很常將整串JSON字串存入Redis Cache以方便後續處理，這時候就必須要先解譯為JSON物件後、再取得特定的property值來加以計算或處理，而在Redis中也提供了JSON物件的處理元件: cjson，透過它的encode()及decode()我們就可以很方便地在Lua中處理JSON格式的資料。

- #### redis-cli
```
EVAL "
--The total summary of property 'p1'
local sum = 0
--Get total number of list
local items = redis.call('LRANGE', 'ListB', 0, -1)

--summary all values
for i=1,#items
do
    sum = sum + cjson.decode(items[i]).p1
end

return sum
" 0
```
結果:
9

- #### StackExchange.Redis
```csharp
var result = RedisDb.ScriptEvaluate(
	LuaScript.Prepare(@"
		--The total summary of property 'p1'
		local sum = 0
		--Get total number of list
		local items = redis.call('LRANGE', 'ListB', 0, -1)

		--summary all values
		for i=1,#items
		do
			sum = sum + cjson.decode(items[i]).p1
		end

		return sum
	")
);
```
結果:
9

### 大量資料的操作
Lua中對於元素的存取也有效能的差異，上一小節中for i=1,#items的方式在少量資料時較無明顯感覺，但如果是幾千甚至上萬筆的資料時、效能差異就會禿顯出來，這時候可用下列ipairs的方式逐一取值及處理。

- #### redis-cli
```
EVAL "
--The total summary of property 'p1'
local sum = 0
--Get total number of list
local items = redis.call('LRANGE', 'ListC', 0, -1)

--summary all values
--This way to read mass data is more efficient
for index, value in ipairs(items)
do
    sum = sum + cjson.decode(items[index]).p1
end

return sum
" 0
```
結果:
12

- #### StackExchange.Redis
```csharp
var result = RedisDb.ScriptEvaluate(
	LuaScript.Prepare(@"
		--The total summary of property 'p1'
		local sum = 0
		--Get total number of list
		local items = redis.call('LRANGE', 'ListC', 0, -1)

		--summary all values
		--This way to read mass data is more efficient
		for index, value in ipairs(items)
		do
			sum = sum + cjson.decode(items[index]).p1
		end

		return sum
	")
```
結果:
12