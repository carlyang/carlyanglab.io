---
title: AKS - Secrets in Configuration
categories:
 - 技術文件
 - Azure
date: 2023/12/7
updated: 2023/12/7
thumbnail: https://i.imgur.com/5sNBEdu.png
layout: post
tags: [Azure, AKS]
---

![](/Teamdoc/image/azure/logos/aks.png)

## Purpose
[[上一篇]](/2023/12/05/Skill/Azure/AKSConfigMaps/)說明了`Configuration`中一般性用途的`ConfigMaps`，大部分的應用情境類似於我們常用的環境變數、一種`Key-value pairs`的設定，但如果是要儲存一些具有安全性疑慮的資料呢?例如:資料庫連線、通行令牌、密鑰...等，`ConfigMaps`就是不很適合了，而這時候我們就可以改使用`Secrets`來儲存這些機密資訊，本文主要的目的就在為各位說明什麼是`Secrets`以及如何在`AKS`中使用`Azure Key Vault`存取。
<!-- more -->

## Secrets
`Secrets`是用來保護敏感資訊的一種儲存方式，你可以啟用靜態加密、限制存取權限、限制可以使用的`Pods`或是直接使用外部儲存服務，而使用`Secrets`的好處就是不用在應用程式中儲存敏感資訊、可以避免這些機密無意中外洩造成損害，例如:資料庫連線被放在`appsettings.json`連帶上傳至版控，事實上，這些敏感資訊都應該集中管理及保護而不是各自儲存，引用說明如下。

>A Secret is an object that contains a small amount of sensitive data such as a password, a token, or a key. Such information might otherwise be put in a Pod specification or in a container image. Using a Secret means that you don't need to include confidential data in your application code.

>Because Secrets can be created independently of the Pods that use them, there is less risk of the Secret (and its data) being exposed during the workflow of creating, viewing, and editing Pods. Kubernetes, and applications that run in your cluster, can also take additional precautions with Secrets, such as avoiding writing sensitive data to nonvolatile storage.

>Secrets are similar to ConfigMaps but are specifically intended to hold confidential data.

## Use YAML to create a secret in AKS
接下來我們先用一個最基本`Opaque`類型(其他類型可參考[[官方文件]](https://kubernetes.io/docs/concepts/configuration/secret/#secret-types))的`Secret`來做為範例如下。

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: secrettest1
type: Opaque
data:
  username: bXl0ZXN0bmFtZQ==
  password: MTIzNDU2
```

說明:  
- `apiVersion`: API版本號。
- `kind`: 資源類型，這邊就是`Secret`。
- `metadata` -> `name`: 資源名稱。
- `type`: 這邊指的是`Secret`內的類型，而`Opaque`則是使用者自訂義的類型。
- `data`-> `username`: 我們自訂一個username的變數及其值。
- `data`-> `password`: 我們自訂一個username的變數及其值。

Note:  
`請注意，Secret儲存的值需要以Base64的字串格式儲存，所以請先將值轉成Base64資料再儲存，例如本範例中username、password的值。`否則您可能會收到下列錯誤訊息。

>Failed to create the secret 'secrettest1'. Error: BadRequest (400) : Secret in version "v1" cannot be handled as a Secret: illegal base64 data at input byte 8

接著我們將上面的`YAML`範例使用`Azure Portal`的`Configuration` -> `Secrets` -> `Apply a YAML`功能來新增，如下圖位置。

![](/Teamdoc/image/azure/AKSSecrets/secret1.png)

![](/Teamdoc/image/azure/AKSSecrets/secret2.png)

接著貼上我們準備好的`YAML`、按下`Add`按鈕新增。

![](/Teamdoc/image/azure/AKSSecrets/secret3.png)

回到清單後就可以看到我們剛新增的`Secret`，接著我們再進入`Secret`查看內容。

![](/Teamdoc/image/azure/AKSSecrets/secret4.png)

進入後我們可以看到`Secret`內含有兩個資料(password & username)，預設看到的是Base64的字串、點選右方的小眼睛可以切換查看明文。

![](/Teamdoc/image/azure/AKSSecrets/secret5.png)

## Use Azure Key Vault to store Secret

<font color="#660000">***2023-12-10 更新資訊: 經過幾日的研究發現，目前以下的範例在AKS中使用Azure Key Vault仍有些問題無法解決，強烈建議有需要存取Azure Key Vault的應用程式，可在程式內安裝[[Azure Key Vault SDK]](https://learn.microsoft.com/en-us/dotnet/api/overview/azure/key-vault?view=azure-dotnet)、再搭配[[AKS - How To Use Managed Identity?]](/2023/11/10/Skill/Azure/AKSManagedIdentity/)這篇文章的設定，即可順利存取相關外部受Key Vault保護的Secrets。***</font>

開始使用`Azure Key Vault`之前必須要先了解一些相關的套件，後續才能知道會用到那些功能。

- `Secrets Store CSI Driver`: `CSI Driver`的全名是`Container Storage Interface`，它可以讓外部的存儲服務提供者以套件的方式在`AKS`使用、而不需要直接與`K8s`的核心溝通，如此一來，只要透過使用這個`Interface`雙方都識別的接口即可讓`Container`內部存取外部資源。所以`Secrets Store CSI Driver`就是整合`AKS`的`Secrets`及存儲服務、並以`Volume`的方式掛載給`Pods`使用的套件，引用[[官方文件]](https://secrets-store-csi-driver.sigs.k8s.io/#kubernetes-secrets-store-csi-driver)如下。

>The Secrets Store CSI Driver secrets-store.csi.k8s.io allows Kubernetes to mount multiple secrets, keys, and certs stored in enterprise-grade external secrets stores into their pods as a volume. Once the Volume is attached, the data in it is mounted into the container’s file system.

了解什麼是`Secrets Store CSI Driver`之後，我們就不難理解`CSI Driver`事實上就是`K8s`專門設計來讓外部存儲服務提供者開發插件介接之用的，只要符合`Interface`的規範、各家服務都可以提供對應的套件、以安裝進`AKS`使用，而`Secrets Store CSI Driver`則是專指用來存取`Secrets`之用的套件。

- `Azure Key Vault Provider for Secrets Store CSI Driver`: 顧名思義，這個就是`Azure Key Vault`提供來讓`AKS Pods`掛載`Volume`並存取`Azure Key Vault`內已建好的各個`Secrets`套件。  
這裡我們會需要注意一些版本資訊，以確保對應的`K8s`版本與`Azure Key Vault Provider`可以搭配使用，相關的版本資訊可參考[[官方文件]](https://azure.github.io/secrets-store-csi-driver-provider-azure/docs/)。

接下來讓我們回到`AKS`的部分，我們可以先使用下列指令列出目前`AKS`已安裝的套件，並檢查是否有安裝`Azure Key Vault Provider`。

```cmd
az aks addon list -g [Resource group] -n [AKS]
```

如果已經有安裝好套件的話，應該可以看到下列項目，`這邊也要注意一下enabled要是true表示才有打開作用中!`

```json
{
    "api_key": "azureKeyvaultSecretsProvider",
    "enabled": true,
    "name": "azure-keyvault-secrets-provider"
}
```

如果沒有看到的話表示還沒有安裝，我們可以使用下列指令來安裝套件。

```cmd
az aks enable-addons --addons azure-keyvault-secrets-provider -g [Resource group] -n [AKS]
```

接著我們可以試著用下列指令列出目前`kube-system`命名空間中的`Pods`具有相關標籤的清單，表示目前`azureKeyvaultSecretsProvider`作用中的對象。

```cmd
kubectl get pods -n kube-system -l 'app in (secrets-store-csi-driver,secrets-store-provider-azure)'
```

結果應該會類似下列清單。

```
NAME                                     READY   STATUS    RESTARTS   AGE
aks-secrets-store-csi-driver-25r2f       3/3     Running   0          2d13h
aks-secrets-store-csi-driver-n449x       3/3     Running   0          16d
aks-secrets-store-csi-driver-pp2cd       3/3     Running   0          16d
aks-secrets-store-csi-driver-wnqgz       3/3     Running   0          2d13h
aks-secrets-store-provider-azure-ljxj5   1/1     Running   0          16d
aks-secrets-store-provider-azure-qkddf   1/1     Running   0          2d13h
aks-secrets-store-provider-azure-x52kt   1/1     Running   0          16d
aks-secrets-store-provider-azure-zfr5b   1/1     Running   0          2d13h
```

為了能夠讓`AKS`具有存取`Azure Key Vault`的權限，我們使用`Managed Identity`的方式、將`AKS Node`的受控身分識別加入`Azure Key Vault`的允許清單內，詳細的設定方式可參考另一篇[[AKS - How To Use Managed Identity?]](/2023/11/10/Skill/Azure/AKSManagedIdentity/)，這邊就不再多敘述。

- ***由於手邊POC環境沒有可用的Azure Key Vault，以下範例先提供[[微軟文件]](https://learn.microsoft.com/en-us/azure/aks/csi-secrets-store-identity-access)中的範例、僅作為示意之用，可能無法成功執行。***

再來我們可以建立`SecretProviderClass`，YAML範例如下、`請注意將<client-id>、<key-vault-name>、<tenant-id>先替換為您實際使用的Key Vault資訊`、再使用`kubectl apply`部署下列YAML。

[[範例來源]](https://learn.microsoft.com/en-us/azure/aks/csi-secrets-store-identity-access#configure-managed-identity)
```yaml
# This is a SecretProviderClass example using user-assigned identity to access your key vault
apiVersion: secrets-store.csi.x-k8s.io/v1
kind: SecretProviderClass
metadata:
  name: azure-kvname-user-msi
spec:
  provider: azure
  parameters:
    usePodIdentity: "false"
    useVMManagedIdentity: "true"          # Set to true for using managed identity
    userAssignedIdentityID: <client-id>   # Set the clientID of the user-assigned managed identity to use
    keyvaultName: <key-vault-name>        # Set to the name of your key vault
    cloudName: ""                         # [OPTIONAL for Azure] if not provided, the Azure environment defaults to AzurePublicCloud
    objects:  |
      array:
        - |
          objectName: secret1
          objectType: secret              # object types: secret, key, or cert
          objectVersion: ""               # [OPTIONAL] object versions, default to latest if empty
        - |
          objectName: key1
          objectType: key
          objectVersion: ""
    tenantId: <tenant-id>                 # The tenant ID of the key vault
```

最後就在我們`Pods`的YAML以`Volume`方式掛載即可，如以下範例。

[[範例來源]](https://learn.microsoft.com/en-us/azure/aks/csi-secrets-store-identity-access#configure-managed-identity)
```yaml
# This is a sample pod definition for using SecretProviderClass and the user-assigned identity to access your key vault
kind: Pod
apiVersion: v1
metadata:
  name: busybox-secrets-store-inline-user-msi
spec:
  containers:
    - name: busybox
      image: registry.k8s.io/e2e-test-images/busybox:1.29-4
      command:
        - "/bin/sleep"
        - "10000"
      volumeMounts:
      - name: secrets-store01-inline
        mountPath: "/mnt/secrets-store"
        readOnly: true
  volumes:
    - name: secrets-store01-inline
      csi:
        driver: secrets-store.csi.k8s.io
        readOnly: true
        volumeAttributes:
          secretProviderClass: "azure-kvname-user-msi"
```

## Conclusion
本文位各位介紹`AKS`中的`Secrets`概念及使用方式，尤其是對於機密資訊的保護、使用`Secrets`更為合適，而且最重要的是這些機密資訊與應用程式間的相依性可以被良好地隔離開來，`Pods`的生命週期也不再影響`Secrets`的儲存與使用，而且將這些機密資訊集中化管理並加密保護，對於我們整體的系統安全性將會更高、是非常好的功能，建議大家可以適當地加以利用。

## References
1. [Secrets](https://kubernetes.io/docs/concepts/configuration/secret/)
2. [Azure Key Vault Provider for Secrets Store CSI Driver](https://azure.github.io/secrets-store-csi-driver-provider-azure/docs/)
3. [Connect your Azure identity provider to the Azure Key Vault Secrets Store CSI Driver in Azure Kubernetes Service (AKS)](https://learn.microsoft.com/en-us/azure/aks/csi-secrets-store-identity-access)
4. [Upgrade an existing AKS cluster with Azure Key Vault provider for Secrets Store CSI Driver support](https://learn.microsoft.com/en-us/azure/aks/csi-secrets-store-driver#upgrade-an-existing-aks-cluster-with-azure-key-vault-provider-for-secrets-store-csi-driver-support)