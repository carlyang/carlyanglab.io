---
title: IoT Edge - Azure物聯網邊際運算 with Docker
categories:
 - 技術文件
 - Azure
date: 2018/6/24
updated: 2018/6/24
thumbnail: https://i.imgur.com/d1Wvf0c.png
layout: post
tags: 
 - IoT Edge
 - Docker
---
作者: Carl Yang

![](/Teamdoc/image/azure/logos/iot_hub.png)

# IoT Edge
IoT Edge是微軟Azure為物聯網而產生的一套解決方案，目的在解決雲端運算中對於大量資料計算的負荷，將原本集中於雲端中心的大量資料、模型、服務等，重新建置於終端裝置、並使用這些更貼近於真實資料來源的節點進行運算。
此法可降低網路延遲的影響、提高雲端資源的再利用性，因為大部分的資料預處理已轉移至終端裝置資源上，故雲端上的資源使用率可降低、可以更專注於商業邏輯、大數據、機器學習等等的相關運算。
<!-- more -->
IoT Edge需要與IoT Hub搭配使用、差異在於使用的Device不同，本篇使用的是IoT Edge Device、目前仍在Preview版本，關於IoT Hub相關操作請參考: [IoT Hub - 物聯網解決方案](/2018/06/18/Skill/Azure/IoT_Hub/)

提到IoT Edge就不得不先提到Edge Computing(邊際運算/邊界運算)的概念，根據Wiki的定義如下:
>Edge computing is a method of optimizing cloud computing systems "by taking the control of computing applications, data, and services away from some central nodes (the "core") to the other logical extreme (the "edge") of the Internet" which makes contact with the physical world.[(_Wiki_)](https://en.wikipedia.org/wiki/Edge_computing)

這其實是一種合久必分的概念，雲端運算盛行後、開發者將商業邏輯、大數據分析、機器學習等等大量的計算分析轉移至雲端上，造成雲端的負荷非常吃重，且雲端資料中心並非隨處可見，以Azure來說離台灣最近的資料中心在香港，很可能受到網路傳輸延遲的影響，造成服務無法即時反應的狀況。
故將雲端運算的部分操作轉移至終端裝置後，可降低雲端運算的負荷、降低網路傳輸延遲的影響，進而提高雲端中心的使用率。

Azure IoT Edge目前仍是預覽版本，相關的Sample可參考[_Github_](https://github.com/Azure/iot-edge)

## Iot Edge Device
##### 概觀
Azure IoT Edge Device主要可分為三個部分，如下:
- IoT Edge Device in IoT Hub: 必須要先在IoT Hub內建立Edge Device、取得唯一的連線字串後續才能開始使用。
- Module: Edge Device所要使用的模組，此模組可參考微軟在[_Github_](https://github.com/Azure/iot-edge)上發佈的Open Source Code、或是自行載入Edge的NuGet套件(後面會詳述)自行撰寫。
- Azure雲端管理介面

下面這是IoT Edge的整個運作流程圖:
主要由中間的Edge Device上安裝Runtime所需的Module，再由左方的各式Sensors(或其他種各式資料來源)取得原始資料後進行處理並上傳至右方的IoT Hub中。
![](/Teamdoc/image/azure/iotedge/IoTHubFlow.png)
<center>(資料來源: [微軟文件](https://docs.microsoft.com/zh-tw/azure/iot-edge/how-iot-edge-works))</center>

##### 環境:
1.根據[微軟文件](https://docs.microsoft.com/zh-tw/azure/iot-edge/quickstart)，系統需要以下支援版本:
- Windows 10 Fall Creators Update，或
- Windows Server 1709 (組建 16299)，或
- x64 型裝置上的 Windows IoT 核心版 (組建 16299)
- .NET Core 2.0 SDK

2.自行安裝Docker CE或EE，本文是以Docker CE為實作環境。
- [Docker CE](https://store.docker.com/editions/community/docker-ce-desktop-windows)

##### 步驟:
1.建立 IoT 中樞
2.註冊 IoT Edge 裝置
3.建立Azure Container Registry(ACR)
3.Docker Build & Push image
4.指定Edge Device的模組
5.Docker Pull image & Run container
<font style="color:red;">Note:<br/>本文是以Docker作為虛擬化工具的使用為主，如希望參考完整的微軟範例、可自行至[微軟IoT Edge](https://docs.microsoft.com/zh-tw/azure/iot-fundamentals/)深入了解。</font>

開始建立步驟:
1.建立 IoT 中樞
關於如何建立IoT中樞(IoT Hub)在前篇文章已有說明，可自行參考[IoT Hub - 物聯網解決方案](/2018/06/18/Skill/Azure/IoT_Hub/)

2.註冊 IoT Edge 裝置
- 進入IoT Hub選取左方的IoT Edge(preview)->左上角Add Edge Device->填入相關的裝置名稱、啟用、儲存。
![](/Teamdoc/image/azure/iotedge/IoTHub1.png)

- 儲存後即可看到途中的[IoTEdgeTest]裝置名稱。
![](/Teamdoc/image/azure/iotedge/IoTHub2.png)

- 進入裝置詳細資料、並複製<font style="color:red;">**連接字串-主要金鑰**</font>，後續模擬裝置需要使用此字串連線。
![](/Teamdoc/image/azure/iotedge/IoTHub3.png)<br/>Note:
<font style="color:red;">這邊的連接字串中含有IoT Hub Host Address、DeviceId、SharedAccessKey等等資訊，用來讓終端裝置知道要連到哪一個IoT Hub中的哪一台DeviceId。</font>

3.建立Azure Container Registry(ACR)
建立ACR的目的是為了將自行開發的程式、Build成Docker image再放入私有的Container Registry中，以讓我們能夠在Device上下載安裝，建立步驟如下:
- 進入所有服務->容器服務
![](/Teamdoc/image/azure/iotedge/ACR1.png)

- 新增ACR、及填入相關資訊，並完成幾個步驟後即建立完成(詳細ACR建立步驟可參考[建立容器登錄](https://docs.microsoft.com/zh-tw/azure/container-registry/container-registry-get-started-portal))。
![](/Teamdoc/image/azure/iotedge/ACR2.png)

- 進入ACR服務點選左方的存取原則，取得登入伺服器、[管理使用者]啟用、使用者名稱、密碼等資訊，並另外儲存<font style="color:red;">**於Edge Device的部署階段使用**</font>。
![](/Teamdoc/image/azure/iotedge/ACR3.png)

4.程式撰寫
- 首先建立一個console專案名為IoTHubTest(過程省略)
- 安裝NuGet套件
	- Microsoft.Azure.Devices
	- Microsoft.Azure.Devices.Client
	- Newtonsoft.Json(未附圖)
![](/Teamdoc/image/Azure/IoTHubConsole3.png)

- 建立IotHubDeviceService類別、並撰寫相關Function
```csharp
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Client;
using Newtonsoft.Json;

namespace IoTHubTest.Services
{
    public class IotHubDeviceService
    {
        private string IoTHubServiceUrl = string.Empty;
        private string strDeviceId = string.Empty;
        private string strTestIotHubConnString = string.Empty;

        public IotHubDeviceService(string hubUrl, string deviceId, string hubConnString)
        {
            this.IoTHubServiceUrl = hubUrl;
            this.strDeviceId = deviceId;
            this.strTestIotHubConnString = hubConnString;
        }

        public async Task SendAsync<T>(T objData)
        {
            try
            {
                string strDeviceKey = string.Empty;
                Device objIoTHubDevice = null;

                //取得Device Info
                objIoTHubDevice = await GetDeviceInfoAsync(strDeviceId, strTestIotHubConnString);

                //Get device's key after registering.
                if (null != objIoTHubDevice)
                {
                    //取得Device Key以作為最後要送出時帶給DeviceClient的Key
                    strDeviceKey = objIoTHubDevice.Authentication.SymmetricKey.PrimaryKey;
                }
                else
                {
                    Console.WriteLine("Cannot get IoT Hub device's information.");
                    return;
                }

                //將資料送給IoTHubServiceUrl所指定的IoT Hub
                await SendMessageAsync(IoTHubServiceUrl, JsonConvert.SerializeObject(objData), strDeviceId, strDeviceKey);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private async Task<Device> GetDeviceInfoAsync(string strDeviceId, string strTestIotHubConnString)
        {
            Device objIoTHubDevice = null;
            RegistryManager objRegistryMgr = RegistryManager.CreateFromConnectionString(strTestIotHubConnString);

            try
            {
                //Get Device Information
                objIoTHubDevice = await objRegistryMgr.GetDeviceAsync(strDeviceId);

                //If device id not exists in IoT Hub, add it...
                if (null == objIoTHubDevice)
                {
                    objIoTHubDevice = await objRegistryMgr.AddDeviceAsync(new Device(strDeviceId));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return objIoTHubDevice;
        }

        private async Task SendMessageAsync(string strIoTHubUrl, string strMessage, string deviceId, string strDeviceKey)
        {
            try
            {
                DeviceClient deviceClient = DeviceClient.Create(strIoTHubUrl,
                    new DeviceAuthenticationWithRegistrySymmetricKey(deviceId, strDeviceKey));

                await deviceClient.SendEventAsync(new Microsoft.Azure.Devices.Client.Message(Encoding.UTF8.GetBytes(strMessage)));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
```

IotHubDeviceService的建構子帶入三個IoT Hub必要資訊，此資訊可由IoT Hub->Shared access policies->POLICY->Connection string—primary key獲得。
```
public IotHubDeviceService(string hubUrl, string deviceId, string hubConnString)
```
![](/Teamdoc/image/azure/iotedge/IoTHub4.png)

使用DeviceId、Connection String取得IoT Hub上的裝置資訊。
```
private async Task<Device> GetDeviceInfoAsync(string strDeviceId, string strTestIotHubConnString)
```

取得裝置資訊。
```
private async Task<Device> GetDeviceInfoAsync(string strDeviceId, string strTestIotHubConnString)
```

接收外部傳入的物件並JSON序列化後傳給SendMessageAsync()送出，其中先取得Device Key以作為最後送出Message時要帶入的Key值。
```
private async Task SendMessageAsync(string strIoTHubUrl, string strMessage, string strDeviceId, string strDeviceKey)
```

Program.cs為Console程式的Entry Point，我們需要在這裡撰寫如何使用IotHubDeviceService送出訊息。
```csharp
using System;
using System.Runtime.Loader;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Devices.Client;
using Microsoft.Azure.Devices.Client.Transport.Mqtt;
using Newtonsoft.Json;

using Microsoft.Azure.Devices.Shared;
using IoTHubTest.Services;

namespace IoTHubTest
{
    public class Program
    {
        private static IotHubDeviceService service = null;

        private static string EdgeHubConnectionString = @"[IoT Edge Device Connection String]";
        private static string IoTHubServiceUrl = @"[IoT Hub URL]";
        private static string strDeviceId = @"[Device Id]";
        private static string strTestIotHubConnString = @"[IoT Hub Connection String]";

        public static void Main(string[] args)
        {
            try
            {
                // The Edge runtime gives us the connection string we need -- it is injected as an environment variable
                string connectionString = EdgeHubConnectionString;

                Console.WriteLine($"Connecting to Azure IoT Hub...");
                Console.WriteLine($"Connection String = [{connectionString}]");

                // Cert verification is not yet fully functional when using Windows OS for the container
                //bool bypassCertVerification = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
                //if (!bypassCertVerification) InstallCert();
                Init(connectionString).Wait();

                var objSensor = new SensorTest()
                {
                    Humidity = 75.0,
                    Temperature = 35.5
                };

                service = new IotHubDeviceService(IoTHubServiceUrl, strDeviceId, strTestIotHubConnString);
                service.SendAsync(objSensor).Wait();

                // Wait until the app unloads or is cancelled
                var cts = new CancellationTokenSource();
                AssemblyLoadContext.Default.Unloading += (ctx) => cts.Cancel();
                Console.CancelKeyPress += (sender, cpe) => cts.Cancel();
                WhenCancelled(cts.Token).Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        /// <summary>
        /// Initializes the DeviceClient and sets up the callback to receive
        /// messages containing temperature information
        /// </summary>
        private static async Task Init(string connectionString, bool bypassCertVerification = false)
        {
            try
            {
                Console.WriteLine("Connection String {0}", connectionString);

                MqttTransportSettings mqttSetting = new MqttTransportSettings(TransportType.Mqtt_Tcp_Only);
                // During dev you might want to bypass the cert verification. It is highly recommended to verify certs systematically in production
                if (bypassCertVerification)
                {
                    mqttSetting.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                }
                ITransportSettings[] settings = { mqttSetting };

                // Open a connection to the Edge runtime
                DeviceClient ioTHubModuleClient = DeviceClient.CreateFromConnectionString(connectionString, settings);
                await ioTHubModuleClient.OpenAsync();
                Console.WriteLine("IoT Hub module client initialized.");

                // Read TemperatureThreshold from Module Twin Desired Properties
                var moduleTwin = await ioTHubModuleClient.GetTwinAsync();
                var moduleTwinCollection = moduleTwin.Properties.Desired;
                try
                {
                    IoTHubServiceUrl = moduleTwinCollection["IotHubUrl"];
                    strDeviceId = moduleTwinCollection["DeviceId"];
                    strTestIotHubConnString = moduleTwinCollection["IoTHubKeyName"];
                }
                catch (ArgumentOutOfRangeException e)
                {
                    Console.WriteLine($"Configuration properties not exist. Exception = {e.Message}");
                }

                // Attach callback for Twin desired properties updates
                await ioTHubModuleClient.SetDesiredPropertyUpdateCallbackAsync(OnDesiredPropertiesUpdate, null);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        /// <summary>
        /// To handle desired properties; change
        /// </summary>
        /// <param name="desiredProperties"></param>
        /// <param name="userContext"></param>
        /// <returns></returns>
        private static Task OnDesiredPropertiesUpdate(TwinCollection desiredProperties, object userContext)
        {
            try
            {
                Console.WriteLine("Desired property change:");
                Console.WriteLine(JsonConvert.SerializeObject(desiredProperties));

                IoTHubServiceUrl = desiredProperties["IotHubUrl"];
                strDeviceId = desiredProperties["DeviceId"];
                strTestIotHubConnString = desiredProperties["IoTHubKeyName"];

                var objSensor = new SensorTest()
                {
                    Humidity = 75.0,
                    Temperature = 35.5
                };

                service = new IotHubDeviceService(IoTHubServiceUrl, strDeviceId, strTestIotHubConnString);
                service.SendAsync(objSensor).Wait();
            }
            catch (AggregateException ex)
            {
                foreach (Exception exception in ex.InnerExceptions)
                {
                    Console.WriteLine("Error when receiving desired property: {0}", exception);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine();
                Console.WriteLine("Error when receiving desired property: {0}", ex.Message);
            }

            return Task.CompletedTask;
        }

        public static Task WhenCancelled(CancellationToken cancellationToken)
        {
            var tcs = new TaskCompletionSource<bool>();
            cancellationToken.Register(s => ((TaskCompletionSource<bool>)s).SetResult(true), tcs);
            return tcs.Task;
        }

    }
    public class SensorTest
    {
        public double Humidity { get; set; }
        public double Temperature { get; set; }
    }


}
```

在Main()中調用Init()並傳入前面我們設定IoT Edge Device後取得的連接字串。
```
Init(connectionString).Wait();
```

由於僅是測試之用，在Init()中的bypassCertVerification = false不使用MqttTransportSettings作驗證。
```
private static async Task Init(string connectionString, bool bypassCertVerification = false)
```

在Init()中使用DeviceClient取得Edge Device資訊後，會拿到一組moduleTwinCollection，這是我們在稍後於Azure上設定要帶給裝置的參數值，以讓裝置可以找到要傳Message的IoT Hub。moduleTwinCollection的設定會在稍後的Desired Properties中說明。
```csharp
var moduleTwinCollection = moduleTwin.Properties.Desired;
try
{
	IoTHubServiceUrl = moduleTwinCollection["IotHubUrl"];
	strDeviceId = moduleTwinCollection["DeviceId"];
	strTestIotHubConnString = moduleTwinCollection["IoTHubKeyName"];
}
catch (ArgumentOutOfRangeException e)
{
	Console.WriteLine($"Configuration properties not exist. Exception = {e.Message}");
}
```

Desired Properties: 
這是一組管理者於Azure管理平台上，設定Edge Device所套用的Module中的自訂參數設定。
為什麼要透過這個東西遠端設定呢?因為<font style="color:red;">Desired Properties可以讓我們動態地透過Azure管理平台更新終端裝置內部的設定值，以方便遠端管理裝置</font>。
由於終端裝置的數量可能非常龐大，光靠人力現場設定裝置是不太可行的方法，除非裝置的地理位置非常集中、或是數量很少才沒有這種距離限制。
為了要能夠在Edge Device設定動態接收Azure所傳來的參數更新，我們必須掛上一個Callback Function、在這裡就是將我們的實作OnDesiredPropertiesUpdate()透過Edge Device Module的SetDesiredPropertyUpdateCallbackAsync()掛上去，便能動態地接收IoT Edge的更新要求。
```csharp
await ioTHubModuleClient.SetDesiredPropertyUpdateCallbackAsync(OnDesiredPropertiesUpdate, null);
```
Note:由於尚未進入Edge Device的Module設定，故這裡僅先提到裝置部分的設定，Azure IoT Edge上的Desired Properties設定稍後小節會做說明。

在OnDesiredPropertiesUpdate這個Callback fnction中，每當收到Desired Properties後、我們就假設一筆SensorTest的更新、並依照最新設定重新初始化IotHubDeviceService物件後送出Message。
Note:<font style="color:red;">程式設計師可以自行修改這邊的邏輯，這裡僅是為了測試之用</font>。
```csharp
IoTHubServiceUrl = desiredProperties["IotHubUrl"];
strDeviceId = desiredProperties["DeviceId"];
strTestIotHubConnString = desiredProperties["IoTHubKeyName"];

var objSensor = new SensorTest()
{
	Humidity = 75.0,
	Temperature = 35.5
};

service = new IotHubDeviceService(IoTHubServiceUrl, strDeviceId, strTestIotHubConnString);
service.SendAsync(objSensor).Wait();
```

這支Console使用CancellationToken接收自外部的終止指令。
```csharp
public static Task WhenCancelled(CancellationToken cancellationToken)
```

5.Docker Build & Push image
接下來我們使用前一小節建立好的Console來搭建我們的Edge Module，從這裡開始我們會使用Docker Commands來Build & Push Module上去ACR，請注意先安裝好Docker才能夠繼續進行。Docker的相關資訊可參考[Docker Doc](https://docs.docker.com/docker-for-windows/)

##### Build with DockerFile
- 先在專案根目錄建立Dockerfile.windows-amd64，內容就是使用指令的方式告訴Docker要如何Build我們的專案。
Note:<font style="color:red;">請確認環境有安裝.NET Core 2.0 SDK，否則Dockerfile中的dotnet相關指令會有錯誤</font>。
```
FROM microsoft/dotnet:2.0-sdk AS build-env
WORKDIR /app

COPY *.csproj ./
RUN dotnet restore

COPY . ./
RUN dotnet publish -c Release -o out

FROM microsoft/dotnet:2.0-runtime
WORKDIR /app
COPY --from=build-env /app/out ./
ENTRYPOINT ["dotnet", "IoTEdgeTest.dll"]
```

- Docker Build:先切到專案根目錄，再使用docker build指令，flag:-t表示我們要tag image的名稱、-f 指定我們要用哪個Dockerfile來build。
```
cd D:\IoTEdgeTest
docker build -t iotedgetestcontainerregistry.azurecr.io/ioteedgetestmodule -f .\Dockerfile.windows-amd64 .
```

- Log in ACR:先取得先前我們建立ACR時獲得的登入資訊，再用下列指令登入。flag:-u表示username、-p表示登入密碼。
```
docker login -u IoTEdgeTestContainerRegistry -p jMtSjq9lWDSl7NGH4/TdqblmnUsz5OFV iotedgetestcontainerregistry.azurecr.io
```

- Push image to ACR:最後再將image push上ACR即完成push作業。
```
docker push iotedgetestcontainerregistry.azurecr.io/ioteedgetestmodule:latest
```

接下來我們就可以進入ACR查看我們剛push的image，並記下這個ImageURL為[iotedgetestcontainerregistry.azurecr.io/ioteedgetestmodule:latest]、稍後在Edge Device要pull image時會用到。
![](/Teamdoc/image/azure/iotedge/ACR4.png)

6.指定Edge Device與模組
- 建立一台IoT Edge Device，如下圖箭頭順序。
![](/Teamdoc/image/azure/iotedge/IoTEdgeDevice1.png)

- 進入裝置詳細資料、選擇上方的Set Module。
![](/Teamdoc/image/azure/iotedge/IoTEdgeDevice2.png)

- 選擇新增。
![](/Teamdoc/image/azure/iotedge/IoTEdgeDevice3.png)

- 輸入各欄位值，Desired Properties即可在此用JSON的方式建立，或是稍後進入Module另外設定，前步驟push所記錄的image url也需在這設定。接著直接下一步、直到Submit送出即可建立Module。
Note:Module中尚有Route關設定，後續文章再做說明。
![](/Teamdoc/image/azure/iotedge/IoTEdgeDevice4.png)
```JSON
{
	"properties.desired": {
		"IotHubUrl":"SelfTestIoTHub.azure-devices.net",
		"DeviceId":"IoTEdgetest",
		"IoTHubKeyName":"HostName=SelfTestIoTHub.azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey=6rfo66C3f2HwdN79bL/9Eom6Gdr5BA2tBcuMNIxKEtI="
	}
}
```

- 最後回到Iot Edge Device中即可看到已設定的Module、及另外Azure IoT Edge自行建立的必要image: $edgeAgent & $edgeHub(此兩module是Azure為了Edge Device與Cloud間雙向通訊之用)。
![](/Teamdoc/image/azure/iotedge/IoTEdgeDevice5.png)

- Module建立後再調整Desired Properties如下圖，在Device TWIN中調整設定即可。
![](/Teamdoc/image/azure/iotedge/IoTEdgeDevice7.png)
![](/Teamdoc/image/azure/iotedge/IoTEdgeDevice8.png)

7.Docker Pull image & Run container
接下來就是到終端裝置上進行安裝，也就是透過Docker將image pull下來後再run即可開始與Azure IoT Hub溝通與上傳資料。
- Log in ACR: 登入ACR以pull image
```
docker login -u IoTEdgeTestContainerRegistry -p jMtSjq9lWDSl7NGH4/TdqblmnUsz5OFV iotedgetestcontainerregistry.azurecr.io
```

- Pull Image
```
docker pull iotedgetestcontainerregistry.azurecr.io/ioteedgetestmodule:latest
```

- Run Image:
```
docker run -it iotedgetestcontainerregistry.azurecr.io/ioteedgetestmodule:latest
```
最後進入IoT Hub查看Daily Message已有資料、線圖也有出來，表示已有資料上傳至IoT Hub。
![](/Teamdoc/image/azure/iotedge/IoTEdgeDevice6.png)
