---
title: Azure - 筆記一下Azure Cache for Redis的Cluster相關概念
categories:
 - 技術文件
 - Azure
date: 2021/4/29
updated: 2021/4/29
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, Cache, Redis, Cluster]
---
Author: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Purpose
Redis目前來說已經是很普及的快取機制，微軟Azure當然也是不弱人後，本文的目的主要在介紹如何設定Azure Redis Cluster以及相關的操作。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Azure Cache for Redis - Premium Tier以上層級

## Introduction
Redis Cluster是透過多個Nodes所組成的叢集快取，援引微軟文件的說明，有下列幾個優點。  

>- The ability to automatically split your dataset among multiple nodes.
>- The ability to continue operations when a subset of the nodes is experiencing failures or are unable to communicate with the rest of the cluster.
>- More throughput: Throughput increases linearly as you increase the number of shards.
>- More memory size: Increases linearly as you increase the number of shards.  
[[MS Doc]](https://docs.microsoft.com/en-us/azure/azure-cache-for-redis/cache-how-to-premium-clustering)

也就是說至少有以下幾個優點: 自動資料分割儲存進不同節點、不受其中少數節點失效的影響、強化存取效能及更多的記憶體使用量。

## Master-Slave Model
Redis是使用`Master-Slave Model`的方式進行服務的Failover，例如有A、B、C三個Master nodes，則A1、B1、C1則為Slave Nodes，Slave nodes會有三個Master Nodes中的資料複本([Redis Cluster Tutorial](https://redis.io/topics/cluster-tutorial))，假設當B Node失效時、B1會立即切換為Master Node繼續服務，但如果B及B1都同時失效時、則整個Redis Cluster將無法繼續提供服務，援引文件說明如下。
>Node B1 replicates B, and B fails, the cluster will promote node B1 as the new master and will continue to operate correctly.
>However, note that if nodes B and B1 fail at the same time, Redis Cluster is not able to continue to operate.

## Redis Cluster - Premium Tier+
在Azure Redis中必須要是Premium Tier以上才有支援Redis Cluster，可參考下表。  
![](/Teamdoc/image/azure/Redis/redis_feature1.png)  
[[REF]](https://azure.microsoft.com/en-us/pricing/details/cache/)

## SLA
Azure Redis的服務可用性，只有Standard層級以上才有開始保證99.9%以上的水準。  
![](/Teamdoc/image/azure/Redis/redis_feature2.png)  
[[REF]](https://azure.microsoft.com/en-us/pricing/details/cache/)

## Maximum number of client connections
請注意，`建置Redis Cluster並無法增加用戶端的可用連線數`，必須提高Azure Redis的服務層級才能夠增加可用連線數，援引微軟文件的說明如下。
>The Premium tier offers the maximum number of clients that can connect to Redis, with a higher number of connections for larger sized caches. Clustering does not increase the number of connections available for a clustered cache.

以Premium為例、各層級用戶端的可用連線數如下表。  
![](/Teamdoc/image/azure/Redis/redis_feature3.png)  
[[REF]](https://azure.microsoft.com/en-us/pricing/details/cache/)

## Network Performance
如下圖所示，網路效能也是隨著層級越高、效能越高、網路延遲也越低。  
![](/Teamdoc/image/azure/Redis/redis_feature4.png)  
[[REF]](https://azure.microsoft.com/en-us/pricing/details/cache/)

## Data Persistence
Azure Redis的資料持續性指的是資料的備份/還原功能，如果有開啟該功能則Redis會將資料備份至Azure儲存體帳戶及受控磁碟上，避免資料遺失造成無法挽救的狀況。  
Data Persistence當然也是承接Redis本身的備份功能，故也是具有以下兩種備份機制。
- RDB: `備份的資料是經過壓縮過的`。
- AOF: `備份資料是直接Append上去的`。

## Azure Redis設定
1. 建立Premium Tier層級的Reids服務。  
![](/Teamdoc/image/azure/Redis/setting1.png)  

2. 進入Advanced tab、Enable Cluster、指定Shard分區數(分區數越多、可用量越多)。  
![](/Teamdoc/image/azure/Redis/setting2.png)  
Note:  
請注意，`開啟Cluster後以建好的Redis是無法再關閉Cluser功能的，僅能調整分區數量`。

3. 設定Data Persistence。  
在這裡可以選擇RDB(壓縮)方式、或是ADF(Append)方式進行備份，`RDB僅能指定一組Storage Account及備份頻率，AOF則是能夠指定兩組Storage Account`。
![](/Teamdoc/image/azure/Redis/setting3.png)  

4. 事後我們也可以透過Azure Portal中的Cluster Size重新調整分區數目。  
![](/Teamdoc/image/azure/Redis/setting4.png) 

5. 請注意預設Azure Redis是只開放SSL安全加密連線的，所以要透過6380 Port連線，我們也可以改成自己想要的Port。  
![](/Teamdoc/image/azure/Redis/setting5.png) 

6. 用Another Redis Desktop Manager試加一筆Key/Value正常。  
![](/Teamdoc/image/azure/Redis/setting6.png) 

## 結論:
透過Redis Cluster的設定，就能夠提供更快、更穩、更完整的雲端快取服務，本文僅是簡單筆記一下Azure Redis Cluster的基礎概念及設定，未來再詳細介紹其它的部分。

## 參考
1. [[Redis Cluster Tutorial]](https://redis.io/topics/cluster-tutorial)
2. [[Azure Redis Tiers & Pricing]](https://azure.microsoft.com/en-us/pricing/details/cache/)
3. [[About Azure Cache for Redis]](https://docs.microsoft.com/en-us/azure/azure-cache-for-redis/cache-overview#choosing-the-right-tier)