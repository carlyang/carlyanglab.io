---
title: AKS - The concepts of K8s
categories:
 - 技術文件
 - Azure
date: 2023/11/02
updated: 2023/11/02
thumbnail: https://i.imgur.com/5sNBEdu.png
layout: post
tags: [Azure, AKS]
---

![](/Teamdoc/image/azure/logos/aks.png)

## Purpose
微軟自從2014年宣布擁抱開源之後，真的是擁抱地非常徹底，尤其是在`Azure`上的開源服務導入更是越來越完整。而這幾年來盛行的`docker`以及後來火熱的`Kubernetes(簡稱K8s)`也沒有錯過，而微軟導入`K8s`之後的服務就稱為`AKS`、取其`Azure K8s`之意。所以在了解`AKS`之前就必須要先了解`K8s`的一些概念，本文目的主要先介紹一點最基礎的`K8s`概念。
<!-- more -->

## Kubernetes
簡稱`K8s`，這是由其名去掉前後的K與s兩字後、中間的8個字母以數字8取代而得，而`K8s`本身是一套`Container`的管理平台，目的是處理容器的自動化部署、負載平衡、容器的擴充/修復/復原...等很多方面的管理作業。

根據[What Kubernetes is not](https://kubernetes.io/docs/concepts/overview/#what-kubernetes-is-not)的說明，節錄以下說明。

>Kubernetes is not a traditional, all-inclusive PaaS (Platform as a Service) system. Since Kubernetes operates at the container level rather than at the hardware level, it provides some generally applicable features common to PaaS offerings, such as deployment, scaling, load balancing, and lets users integrate their logging, monitoring, and alerting solutions. However, Kubernetes is not monolithic, and these default solutions are optional and pluggable. Kubernetes provides the building blocks for building developer platforms, but preserves user choice and flexibility where it is important.

由以上可知，`K8s`跟傳統`PaaS`不完全相同、但因為它也不是基於硬體層面上運行而是基於軟體層面上運行的平台，所以跟`PaaS`具有一些相同的特徵與功能，例如:部署、擴展、負載平衡、紀錄、監控、通知等等，基本上`K8s`所要處理的對象只有一個:`Container`(最小單位是`pod`後面會說明)，而建構起來的平台則是一種開發者平台並保留了很大的彈性，個人認為這是跟傳統`PaaS`以服務為導向的設計不同的地方。

## Node
`K8s`的Node分為`master node`及`worker node`。`master node`為早期的名稱、目前已改為`control plane`，主要負責相關的管理工作。`worker node`則是代表不同台的`Host`，這個`Host`可以是一台實體機器、也可以是一台`VM`，每個`worker node`都有獨立的一套OS，如下圖所示。

![](/Teamdoc/image/azure/AKSConceptsK8s/k8s1.png)
(Ref:[K8s](https://kubernetes.io/images/docs/components-of-kubernetes.svg))

由上圖可知，各`Worker Node`是透過`control plane`的`api server`進行溝通，所以`control plane`就是依此對多台(`含control plane建議至少三台Nodes`)`Worker Node`管理`Container`的相關作業。

## Container & pod
`Container`就是我們的應用程式，比方說我們包好一包`docker image`後，在`docker`上實際跑起來的一隻`instance`。而`pod`則是`K8s`所管理的最小單位，每個`pod`可以包含一或多個`Container`，但是`K8s`的處理對象是一個一個的`pod`，我們可以在`pod`內規劃多個容器，讓`K8s`每個處理的`pod`可以同時啟動一過多個`Container`，所以`pod`可視為一組容器的集合、每個`pod`可以運行在不同的`worker node`上。

Note:  
每個`pod`對應的是一個應用程式，而同一個`pod`中的容器則會共享相同的網路資源。

## Workload
`Workload`是一個在`K8s`上跑的應用程式，而這個`Workload`則是由一組`pod`所構成，他們可能是相互溝通合作來達成某項任務、也可能是由一組相依元件所組成，引用文件說明如下。

>A workload is an application running on Kubernetes. Whether your workload is a single component or several that work together, on Kubernetes you run it inside a set of pods. In Kubernetes, a Pod represents a set of running containers on your cluster.

## Conclusion
本文主要是在了解幾個`K8s`的專有名詞以及他們的重要概念，這是因為整個`AKS`都是圍繞在`K8s`的平台解決方案並進行擴充，例如:`Microsoft cloud defender`、`Microsoft Entra ID`的整合、`Azure Monitor`...等，所以要使用`AKS`就必須先理解`K8s`的相關概念才行。

## References
1. [Kubernetes](https://kubernetes.io/docs/concepts/overview/)