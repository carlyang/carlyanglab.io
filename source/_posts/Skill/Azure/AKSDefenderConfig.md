---
title: AKS - How to set log analytics workspace of Microsoft Defender container
categories:
 - 技術文件
 - Azure
date: 2023/11/29
updated: 2023/11/29
thumbnail: https://i.imgur.com/5sNBEdu.png
layout: post
tags: [Azure, AKS]
---

![](/Teamdoc/image/azure/logos/aks.png)

## Purpose
對於當前火紅的`Kubernetes`群集`Azure`也沒有置身事外，已經推出`Azure`雲端平台上的`PaaS`服務`Azure Kubernetes Service`，簡稱`AKS`，微軟也一直在努力整合`Azure IaaS`與`AKS`的相關功能，`Microsoft Defender`就是其中一項整合功能，它可以在`AKS`上提供一些弱點建議，幫助我們更方便地建設安全的雲端服務環境。  
但是當我們開始跑`AKS`的`Microsoft Defender`後發現，它的`Container`一直在重啟、似乎有錯誤不斷發生，但又找不出發生錯誤的原因，故本文的目的就在說明如何解決`Microsoft Defender`不斷重啟報錯的問題。
<!-- more -->

## `Microsoft Defender` always restart
如下圖所示，我們可以在`AKS Insights`功能查詢相關log，就可以看到`microsoft-defender-publisher`這個`Container`的restart次數多到可怕，雖然沒有觀察到有造成其他服務的異常狀況，但也不能放任這種情況持續下去，於是我們就連絡了微軟支援一起來調查這個問題。

![](/Teamdoc/image/azure/AKSDefenderConfig/defender1.png)

## Root Cause
經過幾次的排查，最後發現其實`microsoft-defender-publisher`它會需要一個`log analytics workspace`來儲存相關監控log，我們可以在`Azure Portal`上搜尋`Resource Explorer`，然後輸入搜尋你的`AKS Cluster`名稱，如下圖。

![](/Teamdoc/image/azure/AKSDefenderConfig/defender2.png)

接著，可以在下圖的階層&右方的`defender` -> `logAnalyticsWorkspaceResourceId`屬性找到`ResourceID`，這個就是`microsoft-defender-publisher`它[預設]要參考使用的`log analytics workspace`。  
但是這個預設的資源並不存在，所以`microsoft-defender-publisher`啟動錯誤找不到它就報錯、錯誤後就failed並嘗試重啟，如此就形成不斷restart的狀況。

![](/Teamdoc/image/azure/AKSDefenderConfig/defender3.png)

## Solution
1. 為了解決這個資源不存在的問題，當然我們就需要為他建立或是綁定一個`log analytics workspace`，我們可以先手動建立一個、或者選定一個已經存在的來使用。然後我們先進入`log analytics workspace` -> `Overview`、並打開右上角的`JSON View`、然後複製它的`ResourceID`出來，如下圖。

![](/Teamdoc/image/azure/AKSDefenderConfig/defender4.png)

2. 接著，把`ResourceID`替換掉下方的`<workspace-id>`、並且另存成*.json檔。

```json
{"logAnalyticsWorkspaceResourceId": "<workspace-id>"}
```

3. 再來我們要透過`az`指令來更改`AKS`內`microsoft-defender-publisher`所綁定的
`log analytics workspace`，關鍵在於使用`--defender-config`來指定完整的`JSON`檔路徑。

Note:  
`[AKS Resource Group]` & `[AKS Name]` & `[JSON file full path]`要記得改成你對應的資訊喔!

```cmd
az aks update --enable-defender --resource-group [AKS Resource Group] --name [AKS Name] --defender-config [JSON file full path]
```

4. 最後，等一段時間更新完後會如下圖所示，顯示出一個`AKS`的資訊，如此就代表更新完成，如此就不會再出現不斷重啟的錯誤了。

![](/Teamdoc/image/azure/AKSDefenderConfig/defender5.png)

## Conclusion
雖然過程中`microsoft-defender-publisher`不斷重啟並沒有觀察到有造成其它的危害，或是服務異常的狀況，但是畢竟都處於同一個`AKS`服務內實在不能冒險置之不理，因為一旦`AKS`掛掉，影響的就是全面性的服務Crash，希望能夠幫助到大家。

## References
1. [How to enable Microsoft Defender for Containers components](https://learn.microsoft.com/en-us/azure/defender-for-cloud/defender-for-containers-enable?tabs=aks-deploy-portal%2Ck8s-deploy-asc%2Ck8s-verify-asc%2Ck8s-remove-arc%2Caks-removeprofile-api&pivots=defender-for-container-aks)
2. [az aks](https://learn.microsoft.com/en-us/cli/azure/aks?view=azure-cli-latest)