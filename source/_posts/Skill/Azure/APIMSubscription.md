---
title: Azure - How to use subscription to limit to access APIM
categories:
 - 技術文件
 - Azure
date: 2021/9/15
updated: 2021/9/15
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management, Subscription]
---
Author: Carl Yang

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
經過[[Azure - Policies of API Management]](/2020/01/04/Skill/Azure/Azure_ApiManagement/)的介紹可以了解到，我們可以使用`API Management`來做為`Azure`上眾多微服務的`Gateway`、統一APIs的入口，也可以利用它達到隱藏API的真正路由、提高API的安全性，而且因為這些`HTTP Request`已經集中化管理在`API Management`中，我們就可以利用這一個特性來進行一些管控，而本文就是說明如何利用`API Management`建立的`Subscription Key`來限制可存取的對象。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- API Management

## Introduction
[[Azure - Policies of API Management]](/2020/01/04/Skill/Azure/Azure_ApiManagement/)一文介紹了一些基本的`API Management`服務的設計及使用本篇就來進一步說明如何使用`Subscription Key`來限制可存取的對象，而利用`Subscriptioni Key`也可以一定程度上針對不同的角色分類、方便管理及賦予適當的權限。

## Subcription
首先，`必須要先說明，這邊的Subscription跟我們的Azure訂閱無關喔!`請大家不要搞混了。這邊的訂閱指的是我們在`API Management`中建立的訂閱，當建立完成後會有兩組Key、爾後想要存取`API Management`就必須要在`HTTP Header`中帶入這個Key才行，設定如下圖。

![](/Teamdoc/image/azure/APIMSubscription/subscription1.png)

Note:  
- 如上圖建立完TestUser後就會有`Primary Key`及`Secondary Key`，基本上只要使用其中一組即可。
- Scope的話可以看到，我們可以設定它的作用範圍僅限於API的部分，如此就不能進行其他如`Administrator`的操作權限了。

## Header or URL Parameter
當我們在`API Management`建立一組API設定時，我們就可以指定每個`HTTP Request`必須要有`Subscription Key`，而驗證方式有兩種、從`Header帶入`或是從`URL Parameter帶入`。
- `Header`帶入: 必須在`HTTP Header`中帶入`Ocp-Apim-Subscription-Key`這個Key、Value則帶入`Subscription Key`值。
- `URL Parameter`帶入: 在URL尾巴帶入`?subscription-key=[Your Subscription Key]`即可。

![](/Teamdoc/image/azure/APIMSubscription/apim_setting1.png)

Note:  
- 實務上，還是建議不要直接在`URL Parameter`帶入這種跟安全性有關的Key比較好，雖然`HTTP Header`其實也有方法可以抓到Key，但總比直接放在URL被人家看光光來得好。
- 另外，`請注意，這個Header Name或URL Parameter Name是可以自訂的喔!`所以我們也可以自訂另一個讓人看不懂、也不容易猜到的Key名稱，這樣就會更安全囉!

## 結論:
透過`API Management`本身的功能，就能夠達成基本的驗證功能，而且我們也可以透過這個`Subscription Key`進行分類、管控、甚至可以記錄Log來分析某個`Subscription Key`的期間用量、讓我們可以適當地調整APIs服務的Scale，例如: `Scale Up/Down`、`Scale in/Out`等，而且基於`Subscription Key`、`API Management`也還有提供其他的進階功能，就待下篇再繼續介紹給各位。

## 參考
1. [[Subscriptions in Azure API Management]](https://docs.microsoft.com/en-us/azure/api-management/api-management-subscriptions)