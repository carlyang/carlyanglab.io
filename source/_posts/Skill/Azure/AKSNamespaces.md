---
title: AKS - Namespaces
categories:
 - 技術文件
 - Azure
date: 2023/11/13
updated: 2023/11/13
thumbnail: https://i.imgur.com/5sNBEdu.png
layout: post
tags: [Azure, AKS]
---

![](/Teamdoc/image/azure/logos/aks.png)

## Purpose
`AKS`是微軟將`K8s`引進Azure平台作為擁抱`docker`的新服務，所以內部的機制也跟`K8s`相同，其中有個好用的`Namespaces`可以幫助我們區分`AKS`中的各種資源，本文的目的就是介紹`Namespaces`的概念及`AKS`上的使用。
<!-- more -->

## Namespaces
`Namespaces`的最主要功能就是我們可以對於`AKS`內的各種資源進行不同的規劃，方便我們管理與維護這些東西，引述官方文件如下:

>In Kubernetes, namespaces provides a mechanism for isolating groups of resources within a single cluster. Names of resources need to be unique within a namespace, but not across namespaces. Namespace-based scoping is applicable only for namespaced objects (e.g. Deployments, Services, etc) and not for cluster-wide objects (e.g. StorageClass, Nodes, PersistentVolumes, etc).

上述說明很清楚地提到`Namespaces`提供了一套機制，這個機制可以用來群組、識別、隔離同一個`AKS Cluster`中的多個資源，這些資源可能是Deployment、Services...等。請注意!`同一個Namespcae中的資源名稱不能重複、但不同Namespace中的資源名稱則可以相同，也就是說Namespace所作用的對象只有在同個內的這些資源而已!`如此才能夠具有`Isolating`的特性，對於我們日常管理維護才能夠安全方便地處理。

## Features
官方文件也提供幾個如何使用`Namespaces`時機及參考，引述如下:

>Namespaces are intended for use in environments with many users spread across multiple teams, or projects. For clusters with a few to tens of users, you should not need to create or think about namespaces at all. Start using namespaces when you need the features they provide.

根據上述說明可知，今天如果我們的人員分布在多個不同的團隊、部門或專案中，就可以使用`Namespaces`來加以適當地規劃這些資源的運用，例如:某個POC專案需要的Deployments、Pods等，可以將這些資源規劃在一個`Namespace`中，等POC結束後就可以移除。不過，官方也建議如果人數不多、甚至只有數十人的環境中，是可以不用考慮使用`Namespaces`的。我個人其實不是很認同這個說法，因為就算人少、但可能專案很多，考慮不同專案能夠在同一個Cluster中的資源良好規劃，使用`Namespaces`有其必要性，並不能只看人多人少來考量是否使用。

>Namespaces provide a scope for names. Names of resources need to be unique within a namespace, but not across namespaces. Namespaces cannot be nested inside one another and each Kubernetes resource can only be in one namespace.

上述說明了同一個`Namespace`中的資源名稱不可重複、但如果不同`Namespace`就可以。另外，`Namespace不可巢狀建立、也就是Namespace內部能夠含有另一個Namespace!`

`K8s`中基本預設的`Namespace`引用[官方文件](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/#initial-namespaces)如下:

>default
>- Kubernetes includes this namespace so that you can start using your new cluster without first creating a namespace.

>kube-node-lease
>- This namespace holds Lease objects associated with each node. Node leases allow the kubelet to send heartbeats so that the control plane can detect node failure.

>kube-public
>- This namespace is readable by all clients (including those not authenticated). This namespace is mostly reserved for cluster usage, in case that some resources should be visible and readable publicly throughout the whole cluster. The public aspect of this namespace is only a convention, not a requirement.

>kube-system
>- The namespace for objects created by the Kubernetes system.

## Azure Portal
一般要在`K8s`操作`Namespaces`跑不掉使用指令如下取得清單、還有其它的複雜指令等，但`Azure Portal`提供我們好用的`GUI`功能、讓我們可以方便快速地維護`Namespaces`。

```
kubectl get namespace
```
`Namespaces`功能位置如下圖。

![](/Teamdoc/image/azure/AKSNamespaces/namespaces1.png)

進入後我們可以看到幾個`AKS`預設的`Namespaces`如default、kube-node-lease、kube-public、kube-system等，這些都是原本`K8s`就有的，而其他三個則是`AKS`另外內建的`Namespaces`。

![](/Teamdoc/image/azure/AKSNamespaces/namespaces2.png)

原本`K8s`中要使用的Create指令、我們可以在`AKS`上用`Create`按鈕輕鬆建立，如下圖。

![](/Teamdoc/image/azure/AKSNamespaces/namespaces3.png)

如果要刪除則勾選其中一個`Namespace`再點選上方`Delete`按鈕即可。

![](/Teamdoc/image/azure/AKSNamespaces/namespaces4.png)

## Conclusion
本文主要在說明`Namespaces`的概念及作用，還有幾個可參考使用的時機點，更重要的是它能夠幫助我們良好地規劃`AKS Cluster`中的各種資源，方便我們良好地規劃及維運，尤其是`Azure Portal`提供了簡化的功能讓我們容易上手、方便又快速，但不可否認地有些進階的狀況還是跑不掉使用`kubrctl`指令、畢竟指令才能較完整地操作`AKS`，建議各位朋友還是要學習一下指令的使用方法喔!

## References
1. [Namespaces](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/)