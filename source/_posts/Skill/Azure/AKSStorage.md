---
title: AKS - Concepts of Storage
categories:
 - 技術文件
 - Azure
date: 2023/11/25
updated: 2023/11/25
thumbnail: https://i.imgur.com/5sNBEdu.png
layout: post
tags: [Azure, AKS]
---

![](/Teamdoc/image/azure/logos/aks.png)

## Purpose
當我們的應用程式啟動之後可能會需要存取一些實體檔案資料，而這些存取的資料儲存機制就顯得非常重要，因為`container`內部的資料儲存都只是暫時性的，它會隨著`container`的生命週期而存活/清除。而`AKS`對於資料儲存也跟`docer`一般使用`Volume`，不管是單一檔案的存取、還是多個`Pods`間要共享的檔案等，本文主要目的在於說明`AKS`內常用的`Storage`種類及概念，讓大家能先有個基本的理解。
<!-- more -->

## Storage
`Storage`意指`AKS`內的儲存體，它可以用來給我們的應用程式儲存取擷取資料之用，有以下幾個核心概念。
- Volumes
- Persistent volumes
- Storage classes
- Persistent volume claims

接下來引用[微軟文件](https://learn.microsoft.com/en-us/azure/aks/concepts-storage)的`Storage`架構圖如下。

![](/Teamdoc/image/azure/AKSStorage/storage1.png)
<center>[[REF]](https://learn.microsoft.com/en-us/azure/aks/media/concepts-storage/aks-storage-options.png)</center>

由上圖我們可以看到，在`AKS`的`Node`中需要先為`Pod`宣告一個`Persistent volume claims`，這樣`Pod`啟動後才會知道有這麼一個磁碟區可用，然後我們會在`AKS`(`Node`外部)建立真正的`Persistent volumes`，而`Persistent volumes`則可以對應到許多外部的雲端儲存服務，例如:`Azure Managed Disk`、`Azure Files`等這些的外部儲存體。

## Volumes
由於`Pod`對於`AKS`來說只是一個暫時可存取的資源，它會因為它的生命週期而生老病死、也可能因為資源配置的關係被`AKS`配置到不同的機台上，所以我們需要一個外部磁碟來提供給`Pod`使用，這樣我們只要在`Pod`組態中設定好可以用的`Volumes`，不管之後`Pod`如何地重配置，都能夠正常存取`Volumes`。

在`AKS`上提供這些磁碟服務的當然就不會僅僅只是傳統的實體硬碟，而是`Azure`雲端平台上的`IaaS`基礎建設，這些可用磁碟服務列舉如下。

- Azure Disk: 這類服務就如同我們建立`Azure VM`時另外附加的`Data Disk`(不是暫時的`OS Disk`)，只要我們沒有刪除它就可以持續儲存在`Azure`上。同時它也依照效能高低區分為一般硬碟(`HDD`)、或是固態硬碟()`SSD`)，也根據是否有進階的一些資料保護機制區分為`Ultra`、`Premium`及`Standard`。
  - Ultra Disks
  - Premium SSDs
  - Standard SSDs
  - Standard HDDs

- Azure Files: 這就是我們常用的`Azure Storage Account`(儲存體帳戶)中的`File Storage`，它可用來最佳化檔案的存取與分享、增進存取效能，也可以整合雲端與地端之間的檔案同步(`Hybrid Cloud`)，它也根據不同的進階保護機制分為`Premium`及`Standard`層級，`所以我們可以適當地mount Azure File來讓Pod即時處理來自地端分享的檔案資料!`
  - Azure Premium storage backed by high-performance SSDs
  - Azure Standard storage backed by regular HDDs

- Azure NetApp Files: 這是`Azure`為企業級資料所打造的簡易操作、高效能且安全資料傳輸的儲存服務，並且也是合規儲存體之一。
  - Ultra Storage
  - Premium Storage
  - Standard Storage

- Azure Blob Storage: 就是我們`Azure Storage Account`中的`Blob`，它是以`Binary`的形式為我們儲存檔案資料，但這邊要注意的是`AKS`的`Storage`只能支援`Block Blob`，也就是說一次一個`Blob`的存取，它不支援其他像`Append Blob`可以不斷附加資料上去。
  - Block Blob

而`Volumes`也區分為三個類型如下:
- emptyDir: 這個是`Pod`一啟動時就會同時建立的暫存資料夾，當`Pod`刪除就會跟著一併消失。
- secret: 這個`Volume`類型是用來注入給`Pod`機敏資料之用，例如:DB連線、帳號密碼等。
- configMap: 就是Key-value pairs的資料型態，類似於我們.NET的appsettings.json組態設定。

## Persistent volumes
一般來說如果建立`PodS`的`Volume`都是會跟著它的生命周期而存滅，但為了能夠長時間保存的資料、我們不能使用這種`Volume`，所以我們才需要`Persistent volumes`這種能夠持續性保存資料的磁碟，我們只要在`AKS`內宣告`Persistent volumes`並指定對應為外部如`Azure Files`、`Azure Blob Storage`等儲存體，就不用擔心資料因為`Pod`生命週期而遺失，如下圖所示。

![](/Teamdoc/image/azure/AKSStorage/storage2.png)
<center>[[REF]](https://learn.microsoft.com/zh-tw/azure/aks/media/concepts-storage/persistent-volumes.png)</center>

## Storage classes
`Storage classes`是用來定義`Persistent volumes`的記憶體層級、它能夠為我們提供不同的`Azure Storage`的tier，例如`Standard`、`Premium`等，詳細如下圖。

Note:  
`請注意，如果沒有為Persistent volumes指定儲存類型，則預設會是managed-csi。`

![](/Teamdoc/image/azure/AKSStorage/storage3.png)

## Persistent volume claims
`Persistent volume claims`就是我們要先告訴`Pod`現在有一個`Persistent volume claims`並且指派了某個`Azure Storage`給它，這個動作就稱為掛載(`Volume Mount`)，我們必須要在`Persistent volume claims`中宣告它的`Storage classes`、存取模式及磁碟區大小，這樣`Pods`才能夠知道要如何使用這個`Persistent volume`，如下圖說明。

![](/Teamdoc/image/azure/AKSStorage/storage4.png)
<center>[[REF]](https://learn.microsoft.com/zh-tw/azure/aks/media/concepts-storage/persistent-volume-claims.png)</center>

## Conclusion
本文說明了關於`AKS`的`Storage`相關概念，也由於是`AKS`雲端服務，所以跟各為介紹的都是以`Azure Storage`儲存體服務為主，這是因為上了雲端平台，所有的基礎建設都是`IaaS`、也就是`基礎建設即服務`，所以`AKS`並不像`K8s`會去掛載實體的磁碟或資料夾，例如:`D:\Share`、`\\ShareFolder\dir1`等，但是像`Azure Files`也提供如`Server Message Block, SMB`通訊協定的支援，可以讓我們跟使用實體磁碟一樣地方便、容易上手。

## References
1. [Storage options for applications in Azure Kubernetes Service (AKS)](https://learn.microsoft.com/en-us/azure/aks/concepts-storage)