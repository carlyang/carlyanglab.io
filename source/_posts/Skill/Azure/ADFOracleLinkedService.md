---
title: Azure - How to use Linked Service to connect to Oracle DB
categories:
 - 技術文件
 - Azure
date: 2024/2/22
updated: 2024/2/22
thumbnail: https://i.imgur.com/oHcuJIO.png
layout: post
tags: [Azure, Azure Data Factory]
---

![](/Teamdoc/image/azure/logos/adf.png)

## Purpose
`Azure Data Factory`(下稱`ADF`)既是處理大量資料的工具，當然不能只處理`MS SQL`的資料，所以`ADF`也能夠提供`Oracle DB`的`Linked Service`讓我們連線進行處理，本文的目的在於說明如何建立`Oracle DB`的`Linked Service`並連線處理資料。
<!-- more -->

## Prerequisite
在開始前請先準備好下列Oracle資料庫連線資訊，
1. DB URL
2. Port
3. SID/Service Name
4. Username
5. Password

## Linked Service
1. 首先，我們先進`ADF`的`Linked Service`然後進入新增視窗如下圖，輸入`Oracle`搜尋 -> 點選`Oracle`選項 -> 按下左下角的`Continue`按鈕進行下一步。

![](/Teamdoc/image/azure/ADFOracleLinkedService/oracle1.png)

2. 接下來就要填入連線相關的資訊如下。
- `Name`: 這欄位只要跟現有的`Linked Service`名稱不要重複即可，不過建議還是要有可讀性較好。
- `Connect via Integration Runtime`: 這個是`ADF`的整合執行環境，他有雲端的環境、也可以安裝在地端的`Self-hosted Integration Runtime`，裝在哪裡就會從哪裡進行連線，而這邊保持預設的`AutoResolveIntegrationRuntime`、讓他從`Azure`上進行連線即可，因為本文所要連線的`Oracle DB`也在Public Cloud上。
- `Host`: 就是DB URL。
- `Port`: 就是DB開放的連接埠號碼。
- `Connection Type`: 這邊請確認是要用SID還是Service Name，`Oracle`中這兩個是不同的東西。
- `SID/Service Name`: 就是Oracle DB的SID或服務名稱。
- `Username`: 使用者帳號。
- `Password`: 使用者密碼。

![](/Teamdoc/image/azure/ADFOracleLinkedService/oracle2.png)

3. 全部輸入成後右下角的`Test Connection`就會被Enable，我們可以在真正建立`Linked Service`前先測試連線是否正常。

![](/Teamdoc/image/azure/ADFOracleLinkedService/oracle3.png)

4. 如果連線失敗就會有對應的錯誤代碼及錯誤訊息，我們就可以根據這個資訊進行故障排除。

![](/Teamdoc/image/azure/ADFOracleLinkedService/oracle4.png)

Note:  
`請注意，目前ADF對於Oracle尚不支援TNS或TCPS的連線方式，基本上僅有ODBC foc Oracle可用!`

***Update!! 2024/04/24 經過2個月不斷地研究，終於找到利用TNS使用TCPS連線的方法，有需要的朋友可參考[[Azure - How to use TNS to connect to Oracle DB]](/2024/04/24/Skill/Azure/ADFOracleUseTNS)。***

## Conclusion
自從微軟擁抱開源以後，很多各式各樣的軟體服務都能夠被納入使用，`Azure`目前除了自家的`MS SQL`外、也新增了`Oracle DB`服務，而`ADF`這種專門處理大量資料的服務當然也必須要支援才行，這樣`ADF`的優勢也就更加地強大、可以處理異質資料庫間的資料轉換問題，真的是很好用的工具，不過目前一些進階的`Oracle`連線方式如TNS或TCPS等尚不支援，還是有些不足的地方就是了。

## References
1. [Copy data from and to Oracle by using Azure Data Factory or Azure Synapse Analytics](https://learn.microsoft.com/en-us/azure/data-factory/connector-oracle?tabs=data-factory)