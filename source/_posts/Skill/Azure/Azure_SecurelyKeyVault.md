---
title: Azure - The Reasons to Use Key Vault
categories:
 - 技術文件
 - Azure
date: 2024/1/20
updated: 2024/1/20
thumbnail: https://i.imgur.com/OlN82Li.png
layout: post
tags: [Azure, Key Vault]
---
Author: Carl Yang

![](/Teamdoc/image/azure/logos/key_vault.png)

## Purpose
最近在公司分享了一些使用`Key Vault`服務的一些心得，也介紹了`Key Vault`服務對於安全性上的一些保護，對於注重機敏資料安全性的公司來說，這是很重要的考量，而如何在複雜的網路環境中做到安全地儲存及取用更加地關鍵，尤其是當資料及應用上雲之後，又牽涉到雲端與地端間的安全傳輸方法。所以本文將這次的一點分享整理出來，讓大家在評估的時候可以有個參考。
<!-- more -->

## Features
`Key Vault`的特性概括有以下幾點。

 - `Centralization`: 集中化管理及維護，這是指公司內必定會有許多的機敏資料，例如:金鑰、憑證、Secrets...等，但傳統的方式管理及維護都較分散，可能因為不同系統、不同專案等因素導致分別使用不同的儲存方式，甚至很多都還是人工處理的方式。  
 所以，`Key Vault`能夠幫助我們將這些機敏資訊集中存放，不僅便於管理維護、也避免了敏感資訊外洩的風險。

 - `Securely store`: `Key Vault`提供了許多安全地存取機制，例如:機敏資料的加密儲存、外部存取權限的管控及加密傳輸等方面，所以如何達成`安全地儲存`對於`Key Vault`來說就是最重要的任務，詳細的安全機制會在下一小節說明。

 - `Monitor access and use`: 既然`Key Vault`是一個雲端服務、那就一定要能被監控，否則用戶還是會擔心機敏資料放進公有雲服務後的隱私性，所以`Key Vault`同樣可以透過`Azure Monitor`的機制進行監控，我們可以使用`Log Analytics`查詢存取紀錄、TLS Version(加密傳輸版本)、分析使用量...等，讓我們可以確實掌握這些機敏資訊的狀態。

 - `Simplified administration`: 以往對於這些機敏資訊最常見就是放在一個只有極少數人能夠存取的私有位置，等到需要用時知道位置的人就可以去私有位置取得，例如:Key、資料庫連線、憑證檔(.pfx)...等，但這些私有位置可能是某個Share Folder、某個人的電腦D槽、甚至被放在某人的行動存儲裝置上，完全沒有統一一致的管理方法，也無法簡化管理作業，所以使用`Key Vault`來儲存這些機敏資訊將讓管理者透過這個服務來簡化、一致性管理作業，不再需要擔心散落各地的問題。

 - `Integrate with other Azure services`: 理所當然地，使用了`Key Vault`服務不只有享受到服務本身提供的好用功能，它還提供了與其它雲服務整合的特性，例如:`Azure VM`的磁碟加密Bitlocker Encryption Key(BEK)可以另外儲存在`Key Vault`服務中，而不是自行儲存。

## Securely store
關於`Key Vault`服務如何安全地提供機敏資訊的儲存及取用，可以分為如下三方面來說明。

1. `Internal encryption`: `Key Vault`內部儲存這些機敏資訊的加密方式。
   - Software encrypted: 這是軟體加密、也是預設的加密方式，一般來說都是使用此選項即可。
   - HSM: 硬體安全性模組，這是屬於硬體加密，它可以自動幫我們處理HSM的布建、設定、修補和維護等作業，當某個HSM分割區失效時，也會自動幫我們將資料轉移到另一個可用區上。

2. `External authentication`: 外部存取`Key Vault`的授權及驗證，主要有下列三種方式。
  - Managed Identity(recommended): 使用Azure受控識別(`Managed Identity`)身分進行存取，我們可以先賦予某個Azure服務一個身分，然後在`Key Vault`這邊的`Access Policy`設定存取權限，該服務才可以進來存取資訊，而這也是微軟建議的方式，`因為對於機敏資訊的存取，對象應該要盡可能地越少越安全!`而`Managed Identity`的方式能夠做到最小化的識別能力，也就是只允許單一服務的存取。

![](/Teamdoc/image/azure/Azure_SecurelyKeyVault/accesspolicy1.png)

  - Service principal and certificate: 這是使用`Azure Entra ID`的服務主體及憑證來設定存取權限，但服務主體往往都還需要搭配`RBAC(Role-based access control)`的群組進行設定，這樣一來一將服務主體加入某個群組後，將會`同時允許一群人或服務的存取權限`，不僅增加管理的複雜度、也增加了風險，因為你不一定能夠完全掌控每個群組內的人或服務`應該被允許具有存取Key Vault的權限!`
  - Service principal and secret: 這是使用`Azure Entra ID`的服務主體及Secrets來設定存取權限，但跟上一點相同疑慮都是會牽涉到`RBAC`的管理及維護，以及這一群人或服務的權限問題。
  
3. `Encryption of data in transit`: 這是指資料傳輸過程的加密，可以避免傳輸過程中資料被截取並解譯，目前大部分的方式都是透過TLS通訊協定來達成，但是TLS有版本差異、其安全性也差異很大，尤其是目前TLS 1.1的版本已經被證明是可以被破解的，所以建議都至少是TLS 1.2版本以上，`而使用Key Vault雲服務的好處就是可以支援最新、更安全的TLS 1.3版本`，我們只要注意呼叫端與`Key Vault`溝通時使用的TLS版本即可。  
Note:  
基本上`Azure`服務都會提供較新、目前主流的穩定版本，所以都支援TLS 1.1、1.2、1.3等版本，但還是要看Host是使用何種版本來跟服務溝通，相關的設定方法可參考[[微軟文件]](https://learn.microsoft.com/en-us/windows-server/security/tls/tls-registry-settings?tabs=diffie-hellman#tls-dtls-and-ssl-protocol-version-settings)。

## Network Restriction
上一小節說明的是有關機敏資料的安全存取及傳輸，接下來要說明的是關於`Key Vault`的網路存取限制，這不同於資料的角度、而是以網路層的角度來考量存取限制。  
以往在企業內部我們想要使用網路層的存取限制，不外乎是防火牆、Proxy代理伺服器、DMZ...等方式，這些都是非常有效且專業的方式，但是缺點就是這些資源全都需要人力來維護，不管是日常作業上Patch或是機房異動等都可能需要隨著調整，但是使用`Key Vault`服務後這些都可以被簡化了，我們可以透過簡單的設定讓服務幫我們過濾這些IP流量。

1. Firewall and virtual networks:
  - CIDR(Classless Inter-Domain Routing): CIDR是一種簡化的IP網段表示法。  
    透過雲服務提供的`Networking`功能，我們可以設定IP的黑名單或白名單來允許或拒絕流量進入服務，如下圖。

![](/Teamdoc/image/azure/Azure_SecurelyKeyVault/networking1.png)

  - Azure vNET: 就是Virtual Network，我們可以將服務綁定某個虛擬網路，這樣一來只有在相同vNET中的服務才能夠來存取。

2. Private endpoint connections: 這是透過建立私有端點並綁定vNET後讓兩端點的服務能夠互通的方法，這是因為Azure vNET無法跨Region，所以必須另外各自建立端點再透過這個端點來進行連線，詳細可參考[[微軟文件]](https://learn.microsoft.com/en-us/azure/private-link/private-endpoint-overview)。

## Soft Delete
關於虛刪除(`Soft Delete`)功能其實就類似我們Windows的資源回收桶，它可以防止我們誤刪資料、即便不小心刪掉也可以從這邊復原回來，但`Key Vault`的`Soft Delete`又有些特徵與資源回收桶不同，說明如下。

- `Recovery of the deleted vaults`: `Soft Delete`功能可以讓我們復原機敏資料以避免誤刪的狀況。

- `Include Key Vault & Objects`: `Soft Delete`功能不只有作用在`Keys`、`Secrets`、`Certifictes`這些機敏資料上，`Key Vaule`本身也受`Soft Delete`保護喔!也就是說我們如果誤刪了整個`Key Vault`服務，在一定存留期內還是可以復原的。

- `Retain Days: 7~90 days`: 目前`Soft Delete`的存留期可以自訂範圍為7至90天，超過天數的資料才會被真正`Purge`。

- `Cannot be disabled`: 這個功能只要打開過就無法在被關閉，所以在`Key Vault`服務的建立之初就必須要決定要開還是要關，但通常都是會開的才對，否則一旦誤刪後果可能就很嚴重。

- `Management`: 目前`Soft Delete`內資料的管理與復原可以透過`Azure Portal`或`Azure CLI`(Ex. az keyvault secret recover)來完成，`Azure Portal`位置如下圖。  
   Note:  
   目前`Azure`已公告`Key Vault`建立之初就可關閉`Soft Delete`功能的選項、將在2025年2月取消，也就是說明年2月以後所建立的`Key Vault`將一律套用`Soft Delete`功能。

![](/Teamdoc/image/azure/Azure_SecurelyKeyVault/softdelete1.png)

## Objects
`Key Vault`中儲存的機敏資料統稱為`Objects`，分為三類如下所列。

1. `Keys`: Cryptographic keys，也就是支援某些演算法的金鑰資料。
  - Support: 目前支援RSA、EC演算法。
  - 可以選擇使用軟體加密或HSM保護。

2. `Secrets`: App name & secret value，可透過`Key-value pairs`的方式儲存的機敏資料都可，例如:資料庫連線。
  - Max size: 每個最大25k bytes
  - Automatic encrypt & decrypt: 存取時`Key Vault`會自動幫我們加解密。
  - 同一個`Key Vault`中都必須具有唯一識別值。

3. `Certificates`: 儲存憑證檔(.pfx)。
  - X.509 certificates: 只要符合X.509的憑證都可以。

## Logs
為了能夠監控`Key Vault`的存取可以適當地儲存日常紀錄，主要有下列兩種方式。

- `Blob`: 我們可選擇將Logs儲存至`Storage Account`的`Blob`中，以供我們需要時查閱，位置如下圖。

![](/Teamdoc/image/azure/Azure_SecurelyKeyVault/logs1.png)

- `Logs Analytics`: 我們先進入`Diagnostic settings`中選擇將相關Logs存進`Log Analytics workspace`、然後使用`Kusto Query`語法進行查詢，查詢功能位置如下圖。

  Note:  
  選擇將Logs存進`Log Analytics workspace`之前請先確認`Key Vault`已綁定`ApplicationInsights`。

![](/Teamdoc/image/azure/Azure_SecurelyKeyVault/logs2.png)

## 結論:
`Key Vault`作為安全性的目的，能夠提供全方位的機敏資料儲存及管理，也能夠將這些機敏資料集中起來、不再散落各地，更重要的是它可以提供簡化、一致性的管理方式，讓我們方便管理這些資訊，而且`Azure`也提供[[Azure Key Vault SDK]](https://learn.microsoft.com/en-us/dotnet/api/overview/azure/key-vault?view=azure-dotnet)讓開發者可以撰寫相關的程式碼、透過`Key Vault API`存取需要的資料，這樣一來`可以將機敏資訊與應用程式的相依性隔離開來，如果需要更新資訊時就不需要重新部署應用程式、而更新應用程式也不會將這些機敏資訊一同帶上去，進一步降低外洩的風險`，真的建議大家可以考慮使用，

## 參考
1. [Azure Key Vault basic concepts](https://learn.microsoft.com/en-us/azure/key-vault/general/basic-concepts)
2. [Azure Key Vault SDK](https://learn.microsoft.com/en-us/dotnet/api/overview/azure/key-vault?view=azure-dotnet)