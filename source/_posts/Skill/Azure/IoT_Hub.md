---
title: IoT Hub - 物聯網解決方案
categories:
 - 技術文件
 - Azure
date: 2018/6/18
updated: 2018/09/08
thumbnail: https://i.imgur.com/d1Wvf0c.png
layout: post
tags: 
 - IoT Hub
---
作者: Carl Yang

![](/Teamdoc/image/azure/logos/iot_hub.png)

# IoT Hub(IoT中樞)
物聯網時代來臨，資訊領域內各行各業無不大興土木、趨之若鶩地想將各種舊時代的裝置連上網際網路，期待有更突破性的產品推出，如Big Data、Machine Learning、Artificial Intellegence...等等，想要利用雲端時代的資訊科技進行更智慧化的分析及應用。
當然，微軟Azure也不例外，Azure也提供[[_IoT Hub_](https://docs.microsoft.com/zh-tw/azure/iot-fundamentals/)]服務(或稱IoT中樞)以連結終端裝置到雲端服務間的通訊，其可作為端對端的IoT應用程式(SaaS)、與平台服務(PaaS)間的橋樑，即時將終端裝置產生的資料送上Azure以進行後續的商業邏輯、大數據分析、機器學習等等應用。

談到IoT不免俗地也必須對它做個定義:
>The Internet of Things (IoT) is the network of physical devices, vehicles, home appliances and other items embedded with electronics, software, sensors, actuators, and connectivity which enables these things to connect and exchange data, creating opportunities for more direct integration of the physical world into computer-based systems, resulting in efficiency improvements, economic benefits and reduced human exertions.
>([_Wiki_](https://en.wikipedia.org/wiki/Internet_of_things))

根據上述定義，簡單來說就是將我們現實世界中的各種物品加上網路連結的能力、再透過雲端服務的加持，以進行資料交換、資料分析、新領域應用等，造就出更多創新的產品及應用，所以IoT的第一步就是要連結這些終端裝置與雲端服務，以達到資料交換的目的。

而IoT Hub就是在提供這樣的一個物聯網解決方案，當終端裝置的這些資料送上雲端後，再透過一定的ETL處理程序就能夠進一步分析與應用。當然IoT Hub也不只有資料交換的功能，它還支援從裝置到雲端以及從雲端到裝置間的雙向通訊，例如追蹤裝置狀態、接收裝置上傳的資料等等，也可以透過Azure遠端監控終端裝置目前的狀態。

## <font style="color:red;">2018-09-08 更新Proxy設定</font>:
IoT Hub SDK Proxy設定: GitHub上微軟對於SDK的Proxy有增加支援，程式設定如下，主要是在CreateFromConnectionString中帶入<font style="color:red;">ITransportSettings[]</font>的設定，可以使用AMQT、MQTT、HTTP等等。
```csharp
DeviceClient = DeviceClient.CreateFromConnectionString(
	ConfigSettings.IoTHubConnectionString,
	ConfigSettings.DeviceId,
	new ITransportSettings[]
	{
		new Http1TransportSettings
		{
			Proxy = new WebProxy(ConfigSettings.ProxyAddress)
		}
	}
);
```
<!-- more -->
Note:
本文將簡介如何使用Azure IoT SDK將終端裝置的資料送進IoT Hub，相關測試code資訊如下:
- Console Application
- .Net Framework 4.6.1
	
NuGet Package(必要):
- Microsoft.Azure.Devices
- Microsoft.Azure.Devices.Client

## 建立IoT Hub
#### 步驟1:登入Azure Portal -> 所有資源 -> IoT中樞
![](/Teamdoc/image/azure/IoTHub1.png)

#### 步驟2:建立IoT中樞
![](/Teamdoc/image/azure/IoTHub2.png)

#### 步驟3:設定詳細資訊
Note:
1.Resource Group可以自行指定新建或使用現有的。
2.Region是指定服務要建在哪個Data Center，通常都是選擇離自己最近的地方，這邊是選擇東亞。
3.IoT Hub Name可以自行指定，但這邊設定的名稱會作為後續終端裝置中連線字串中的URL。
![](/Teamdoc/image/azure/IoTHub3.png)

#### 步驟4:建立尚未真正建立IoT Hub前可以瀏覽相關Size及Scale，預設為S1、金額為751.36TWD，由於目前僅是測試、不希望收費扣除每月額度，故需要另外調整如下一個步驟。
![](/Teamdoc/image/azure/IoTHub4.png)

#### 步驟5:點選下方按鈕[Previous: Size and scale]
![](/Teamdoc/image/azure/IoTHub5.png)

#### 步驟6:選擇F1: Free tier(此步驟可依您的裝置數量多寡自行選擇不同選項)
![](/Teamdoc/image/azure/IoTHub6.png)

>Note:
>如果是選擇Free Tier則每日限制上傳訊息數量為8000筆，以目前測試來說綽綽有餘。

#### 步驟7:最後點選下方Review + create -> Create即可建立IoT Hub
![](/Teamdoc/image/azure/IoTHub7.png)

#### 步驟8:建立服務需要一點時間，右上角通知視窗會顯示進行中，稍待即會建立完成。
![](/Teamdoc/image/azure/IoTHub8.png)

#### 步驟9:成功部署後可以點選[前往資源]進入IoT 中樞，另外也可以選擇[釘選到儀表板]方便日後查閱。
![](/Teamdoc/image/azure/IoTHub9.png)

#### 步驟10:部署完成後進入、目前沒有任何Message、圖中也沒有任何線條，待後續模擬裝置的console執行後才會上傳資料。
![](/Teamdoc/image/azure/IoTHub10.png)

#### 步驟11:設定共用存取原則，以給予不同裝置有不同的權限。在此為了方便測試、給予最大權限iothubowner。
<font style="color:red;">**請複製右方的[連接字串-主要金鑰]，後續模擬裝置需要此字串連接Iot Hub。步驟3中所建立的IoT Hub Name即會顯示在此字串中並組成URL讓終端裝置連接。**</font>
![](/Teamdoc/image/azure/IoTHub11.png)

#### 步驟12:回到IoT Hub首頁，右上角主機名稱也會顯示完整的URL、例如xxxx.azure-devices.net
![](/Teamdoc/image/azure/IoTHub12.png)


## 建立console專案
#### 步驟1:使用Visual Studio 2017建立Console專案。(詳細內容不再贅述)
專案結構如圖左方案總管，所有IoT Hub相關程式碼位於IotHubDeviceService.cs中、SensorTest.cs是要傳送給IoT Hub的模擬資料。
![](/Teamdoc/image/azure/IoTHubConsole1.png)

#### 步驟2:確認所使用的.NET Framework版本，本例使用.NET Framework 4.6.1版本。

>Note:
>請務必確認所使用的版本是否>= .NET Framework 4.5，否則IoT Hub SDK是無法使用的。
![](/Teamdoc/image/azure/IoTHubConsole2.png)

#### 步驟3:安裝NuGet套件。
>NuGet Package(必要安裝):
>- Microsoft.Azure.Devices
>- Microsoft.Azure.Devices.Client

![](/Teamdoc/image/azure/IoTHubConsole3.png)

## 撰寫程式
#### 取得IoT Hub裝置資訊
- 先使用GetDeviceInfoAsync()取得裝置資訊。
- 如果null != objIoTHubDevice，接著取得DeviceKey作為後續傳送訊息之用。
```csharp
Device objIoTHubDevice = null;

objIoTHubDevice = await GetDeviceInfoAsync(strDeviceId, strTestIotHubConnString);

//Get device's key after registering.
if (null != objIoTHubDevice)
{
	strDeviceKey = objIoTHubDevice.Authentication.SymmetricKey.PrimaryKey;
}
else
{
	Console.WriteLine("Cannot get IoT Hub device's information.");
	return;
}
```

#### 上個步驟中所使用的GetDeviceInfoAsync()如下。
- RegistryManager.GetDeviceAsync(): 先檢查目前的DeviceId是否已存在IoT Hub中。
- RegistryManager.AddDeviceAsync(): 如果沒有DeviceId則新增進IoT Hub。
```csharp
private async Task<Device> GetDeviceInfoAsync(string strDeviceId, string strTestIotHubConnString)
{
	Device objIoTHubDevice = null;
	RegistryManager objRegistryMgr = RegistryManager.CreateFromConnectionString(strTestIotHubConnString);

	try
	{
		//Get Device Information
		objIoTHubDevice = await objRegistryMgr.GetDeviceAsync(strDeviceId);

		//If device id not exists in IoT Hub, add it...
		if (null == objIoTHubDevice)
		{
			objIoTHubDevice = await objRegistryMgr.AddDeviceAsync(new Device(strDeviceId));
		}
	}
	catch (Exception e)
	{
		Console.WriteLine(e);
		throw;
	}

	return objIoTHubDevice;
}
```

#### 回到第一段程式碼取得strDeviceKey後
- 我們先建立模擬資料物件objSensor、並假定濕度 - 75.0、溫度 - 35.5。
- 再帶入主機名稱:xxxx.azure-devices.net
- 由於IoT Hub服務目前僅接受JSON格式的資料，故我們使用JsonConvert.SerializeObject將訊息序列化後帶入，最後再帶入DeviceId、前步驟取得的DeviceKey。
```csharp
objSensor = new SensorTest()
{
	Humidity = 75.0,
	Temperature = 35.5
};

await SendMessageAsync(IoTHubServiceUrl, JsonConvert.SerializeObject(objSensor), strDeviceId, strDeviceKey);
```

#### 上個步驟中所使用的SendMessageAsync()如下。
- 先透過DeviceClient.Create()取得DeviceClient物件，並帶入strIoTHubUrl、strMessage、strDeviceId、strDeviceKey四項資訊。
- 最後使用DeviceClient.SendEventAsync()傳送訊息到IoT Hub上。
```csharp
private async Task SendMessageAsync(string strIoTHubUrl, string strMessage, string strDeviceId, string strDeviceKey)
{
	try
	{
		DeviceClient deviceClient = DeviceClient.Create(strIoTHubUrl,
			new DeviceAuthenticationWithRegistrySymmetricKey(strDeviceId, strDeviceKey));

		await deviceClient.SendEventAsync(new Microsoft.Azure.Devices.Client.Message(Encoding.UTF8.GetBytes(strMessage)));
	}
	catch (Exception e)
	{
		Console.WriteLine(e);
		throw;
	}
}
```

## 檢視IoT Hub資料
#### 步驟1:接著我們回到Azure Portal的IoT Hub，點選Device Explorer，就會看到我們剛才新增進來的DeviceId，以及目前狀態、最近一次活動時間等資訊。
![](/Teamdoc/image/azure/IoTHub13.png)

#### 步驟2:回到IoT Hub首頁，我們即可看到剛才上傳的Message、以及右方圖形內已有折線出現，表示已成功上傳模擬裝置資料。
![](/Teamdoc/image/azure/IoTHub14.png)


## 完整程式Sample
```csharp
using System;
using System.Text;
using System.Threading.Tasks;
using IoThubTest.Models;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Client;
using Newtonsoft.Json;

namespace IoThubTest.Services
{
    public class IotHubDeviceService
    {
        private const string IoTHubServiceUrl = @"Your IoT Hub URL";
        private const string strDeviceId = @"Your IoT device name";
        private const string strTestIotHubConnString = @"Your IoT Hub connection string";

        public async Task SendAsync()
        {
            string strDeviceKey = string.Empty;
            SensorTest objSensor;

            try
            {
                Device objIoTHubDevice = null;

                objIoTHubDevice = await GetDeviceInfoAsync(strDeviceId, strTestIotHubConnString);

                //Get device's key after registering.
                if (null != objIoTHubDevice)
                {
                    strDeviceKey = objIoTHubDevice.Authentication.SymmetricKey.PrimaryKey;
                }
                else
                {
                    Console.WriteLine("Cannot get IoT Hub device's information.");
                    return;
                }

                objSensor = new SensorTest()
                {
                    Humidity = 75.0,
                    Temperature = 35.5
                };

                await SendMessageAsync(IoTHubServiceUrl, JsonConvert.SerializeObject(objSensor), strDeviceId, strDeviceKey);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private async Task<Device> GetDeviceInfoAsync(string strDeviceId, string strTestIotHubConnString)
        {
            Device objIoTHubDevice = null;
            RegistryManager objRegistryMgr = RegistryManager.CreateFromConnectionString(strTestIotHubConnString);

            try
            {
                //Get Device Information
                objIoTHubDevice = await objRegistryMgr.GetDeviceAsync(strDeviceId);

                //If device id not exists in IoT Hub, add it...
                if (null == objIoTHubDevice)
                {
                    objIoTHubDevice = await objRegistryMgr.AddDeviceAsync(new Device(strDeviceId));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return objIoTHubDevice;
        }

        private async Task SendMessageAsync(string strIoTHubUrl, string strMessage, string strDeviceId, string strDeviceKey)
        {
            try
            {
                DeviceClient deviceClient = DeviceClient.Create(strIoTHubUrl,
                    new DeviceAuthenticationWithRegistrySymmetricKey(strDeviceId, strDeviceKey));

                await deviceClient.SendEventAsync(new Microsoft.Azure.Devices.Client.Message(Encoding.UTF8.GetBytes(strMessage)));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
```