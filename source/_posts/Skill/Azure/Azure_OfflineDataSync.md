---
title: Azure - Offline Data Sync in Azure Mobile Apps Client
categories:
 - 技術文件
 - Azure
date: 2020/1/5
updated: 2020/1/5
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, Offline Data Sync, Azure Mobile Apps]
---
Author: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Purpose
Offline Data Sync(離線資料同步)是Azure Mobile SDK中的一項功能，`本文主要在說明Offline Data Sync中的幾個重要概念`。
<!-- more -->

## Introduction
Offline Data Sync(離線資料同步，以下簡稱ODS)是Azure Mobile SDK中的一項功能，`可以讓我們在行動裝置處於離線狀態時仍然能夠進行資料更新`，它是一種client-server間資料同步(或稱交換)的功能。  
當`行動裝置離線時，它會將資料存在local store存放區、當回到線上時，會自動與Azure Mobile Service Backend同步這些資料，如果有Conflicts發生也能夠主動通知使用者進行調整`。

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Create Azure Mobile Service

## Benefits
根據官方文件說明，它具有下列優點。
>- Improve app responsiveness by caching server data locally on the device
>- Create robust apps that remain useful when there are network issues
>- Allow end users to create and modify data even when there is no network access, supporting scenarios with little or no connectivity
>- Sync data across multiple devices and detect conflicts when the same record is modified by two devices
>- Limit network use on high-latency or metered networks

也就是說，因為`它能夠利用Local Store Cache，持續回應使用者、線下資料更新及同步、線上資料同步、衝突偵測及限制高延遲或計量網路的使用`。

## Local Store
Local Store其實就是`使用SQLite資料庫進行Local資料的存取，而SQLite就是一個Local端的檔案，用來記錄相關的資料及CRUD等更新`，當重新連上網路後就會降這些資料同步回去Azure Mobile App Backend。

## SDKs
ODS是Azure Mobile client SDK的一項功能，它提供以下介面進行Table存取。
- IMobileServiceSyncTable:  
`提供一組介面供調用端存取Local Store內的資料，以及相關CRUD的操作`。

## Sync Context
SDK使用IMobileServiceClient來進行Local Store的Create、Update、Delete等操作，它內部會維護一份Operation Queue，紀錄一系列的操作步驟，最後上線時在同步到Cloud上。應用程式可以透過`IMobileServicesSyncContext.InitializeAsync(localstore)`來初始化該元件。

## How does it work?
應用程式可以透過push來將Local端所做的變更同步到雲端上、也可透過pull來將最新的資料從雲端上同步回Local Store，以下就各種動作說明。
- Push:  
用來將一系列的CUD操作同步到Azure Mobile App Backend上，`這個動作無法只同步單一資料表、只能將全部Operation一起同步上去`。
- Pull:  
將雲端上最新的資料同步下來Local Store，它也`可以透過自訂Query同步部分的資料下來`。
- Implicit Pushes:  
當Local Store的資料有變更時，會`嘗試先進行Push、爾後再Pull最新的資料`，這樣的Implicit Pushes可以最小化Conflicts，盡量避免衝突。
- Incremental Sync:  
Local Store的資料都會給予一個時間戳記(timestamp)，當我們`Pull資料時只會更新筆這個時間戳記更晚的資料下來，而這個時間戳記會被記錄於updatedAt中`。
- Purging:  
當有特定情況發生時，可能會想要清除Local Store內的變更，這時候就可以用`IMobileServiceSyncTable.PurgeAsync()`來清除Local Store內的變更。

## 結論:
離線資料同步的功能在行裝置環境是非常好用的工具，因為手持裝置的環境可能變來變去，也可能因為訊號減弱而臨時斷線，所以有這種能夠自動同步資料的機制，就較不用擔心資料遺失的狀況發生。

## 參考
1. [Offline Data Sync in Azure Mobile Apps](https://docs.microsoft.com/en-us/azure/app-service-mobile/app-service-mobile-offline-data-sync)