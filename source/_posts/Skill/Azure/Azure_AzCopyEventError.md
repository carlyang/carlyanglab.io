---
title: Azure - AzCopy Error Log in Windows Event Viewer
categories:
 - 技術文件
 - Azure
date: 2021/4/27
updated: 2021/4/27
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, AzCopy, Event Log]
---
Author: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Introduction
在使用AzCopy時總是有個困擾的問題，就是當HTTP Request耗時超過3秒、就會寫入Windows Event Error Log，雖然網路況可能有快有慢，但在查其他問題時總會被這個錯誤所困擾，再查Github Issues也是有些類似的問題，如下。

<!-- more -->

[[#1505]](https://github.com/microsoft/AzureStorageExplorer/issues/1505)  
這個Issue已經被關閉了，但下方還是有些人反映問題仍存在。

[[#469]](https://github.com/Azure/azure-storage-azcopy/issues/469)  
這也是類似的問題，下方開發者有回覆已經解決，但細看我的狀況是HTTP HEAD時發生的SLOW，似乎跟這個傳送大型檔不太一樣。

>Event Error Sample如下:  
```
The description for Event ID 0 from source D:\[Path]\azcopy.exe cannot be found. Either the component that raises this event is not installed on your local computer or the installation is corrupted. You can install or repair the component on the local computer.

If the event originated on another computer, the display information had to be saved with the event.

The following information was included with the event: 

==> REQUEST/RESPONSE (Try=1/10.0554548s[SLOW >3s], OpTime=10.0554548s) -- REQUEST ERROR
   HEAD https://[XXX].blob.core.windows.net/[container]?[SAS Token]
   User-Agent: [AzCopy/10.10.0 Azure-Storage/0.13 (go1.15; Windows_NT)]
   X-Ms-Client-Request-Id: [3fcbfa79-4a1a-4138-74eb-48a9896ae1f2]
   X-Ms-Version: [2019-12-12]
   --------------------------------------------------------------------------------
   ERROR:
-> github.com/Azure/azure-pipeline-go/pipeline.NewError, /home/vsts/go/pkg/mod/github.com/!azure/azure-pipeline-go@v0.2.3/pipeline/error.go:157
HTTP request failed

Head "https://[XXX].blob.core.windows.net/[container]?[SAS Token]": net/http: TLS handshake timeout
```

## 自訂選項
基本上，這種寫入Event Error的錯誤，應該要可以由外界自訂開關，但由上面兩篇中的回覆發現，AzCopy的開發者並不贊成這麼做，而是應該忠實反映網路太慢的問題，並由調用端檢查網路頻寬等部分，並進行適當的調整。

## 進度更新
1. 2021-04-27:  
有查到一個option [--log-level]似乎可以使用，但實際跑完發現，它是用來設定寫入AzCopy本身的log檔類型，有Info、Debug、ERROR...etc，並無法控制Event Log的寫入行為。  
Note:  
AzCopy的Log會存在User Profile的.azcopy隱藏資料夾內。

## 參考
1. [[AzCopy Github]](https://github.com/Azure/azure-storage-azcopy)