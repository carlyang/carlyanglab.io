---
title: Azure - VM網路速度測試工具
categories:
 - 技術文件
 - Azure
date: 2021/6/1
updated: 2021/6/1
thumbnail: https://i.imgur.com/lCrEvb3.png
layout: post
tags: [Azure, VM, Network, iPerf]
---
Author: Carl Yang

![](/Teamdoc/image/azure/logos/vm.png)

## Purpose
最近為了要架Azure Express Route的MPLS專線，將我們的雲端服務保護在私人網路內，免不了要驗證一下專線的網路速度，所以就開了Azure VM來進行測試。  
而測試的時候使用iPerf這個工具、來啟動Client-Server的TCP通訊，並送資料取得網速資訊。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Azure VM

## Introduction
iPerf是一套廣為人使用的網路速度測量工具、也是一套指令化的工具，它並沒有GUI介面可供使用，所以對一般人來說可能沒有那麼友善，但它的功能可是很強大的。它支援Windows / Mac OS X / Linux / Android / iOS這幾種OS，也支援通訊協定TCP / UDP / SCTP。

## Client - Server
### iPerf Server
假設我們想測試自己的電腦到Azure VM間的網路速度，首先我們要在目標端(也就是Azure VM)先啟動iPerf Server監聽某個Port的流量，讓後面啟動的Client能夠送資料過來測試，啟動Server需使用下列的指令。
```cmd
.\iperf3.exe -s
```

`-s`就是啟動Server的意思。

啟動後如下圖所示，預設就會開始監聽5201 Port
![](/Teamdoc/image/azure/iPerf/iPerf_Server_1.png)


如果不想要用預設Port，我們也可以透過下列指令指定Port Number
```cmd
.\iperf3.exe -s -p 5202
```

`-p`就是指定要監聽的Port Number。

啟動後如下圖所示，就會開始監聽5202 Port
![](/Teamdoc/image/azure/iPerf/iPerf_Server_2.png)

### iPerf Client
啟動完Server後，接著就可以來啟動iPerf Client，我們可以使用下列指令啟動。
```cmd
.\iperf3.exe -c xxx.xxx.xxx.xxx
```

`-c`就是啟動Client的意思。

如果Server指定其他Port Number的話，那Client這邊也要指定相同Port才能夠進行測試。
```cmd
.\iperf3.exe -c xxx.xxx.xxx.xxx -p 5202
```

如下圖左方為遠端VM的Server端、右方為Client端。

![](/Teamdoc/image/azure/iPerf/iPerf_1.png)

欄位說明如下:

Interval|Transfer|Bandwidth
--------------------------|:--------|:-----
每筆量測區間(尾碼為花費秒數)|傳輸資料量|速度(每秒多少Mbits, Mbits/s)

最下方為該次的測量結果。
```cmd
[ ID] Interval           Transfer     Bandwidth
[  4]   0.00-300.00 sec  1.39 GBytes  39.9 Mbits/sec                  sender
[  4]   0.00-300.00 sec  1.39 GBytes  39.9 Mbits/sec                  receiver
```

## Azure VM Networking port rule
請注意，如果你是透過Private IP連接Azure VM，通常Networking中會包含`AllowVnetInBound`規則、以允許所有的Inbound ports，所以iPerf都可以連得到。`但是，如果是經由Public IP連接VM的話，由於預設只允許3389 port進來，所以我們還必須另外增加一條Inbound port rule、以允許iPerf的5201 port進來`，如下圖所示。

![](/Teamdoc/image/azure/iPerf/iPerf_2.png)

## 常用的Parameters
這邊僅列出幾個常用的參數說明。
- -s 啟動iPerf Server
- -c [#Server IP#] 啟動iPerf Client並指定Server IP
- -t [#Seconds#] 指定總共的量測時間(單位: sec)
- -n [#bytes#] 總共要傳送的資料量(單位: byte)
  (`請注意-t與-n不能同時使用，因為一個指定時間測試、一個是指定資料量測試，兩者條件互斥`)
- ---logfile [#File Path#] 指定將測試結果寫入檔案(`如果指定這個，CLI上就不會顯示Log`)
- -P [#Parallel Number#] 指定要平行量測的數目(`指定2就是同時跑兩個Stream，依此類推`)
- -b [#bandwidth#] 指定測試要使用多少頻寬(`0的話則是無限制，跟不帶入-b是一樣意思`)

## 其他參數
可使用下列指令列出所有相關參數及說明
```cmd
.\iperf3.exe
```

```cmd
iperf3: parameter error - must either be a client (-c) or server (-s)

Usage: iperf [-s|-c host] [options]
       iperf [-h|--help] [-v|--version]

Server or Client:
  -p, --port      #         server port to listen on/connect to
  -f, --format    [kmgKMG]  format to report: Kbits, Mbits, KBytes, MBytes
  -i, --interval  #         seconds between periodic bandwidth reports
  -F, --file name           xmit/recv the specified file
  -B, --bind      <host>    bind to a specific interface
  -V, --verbose             more detailed output
  -J, --json                output in JSON format
  --logfile f               send output to a log file
  -d, --debug               emit debugging output
  -v, --version             show version information and quit
  -h, --help                show this message and quit
Server specific:
  -s, --server              run in server mode
  -D, --daemon              run the server as a daemon
  -I, --pidfile file        write PID file
  -1, --one-off             handle one client connection then exit
Client specific:
  -c, --client    <host>    run in client mode, connecting to <host>
  -u, --udp                 use UDP rather than TCP
  -b, --bandwidth #[KMG][/#] target bandwidth in bits/sec (0 for unlimited)
                            (default 1 Mbit/sec for UDP, unlimited for TCP)
                            (optional slash and packet count for burst mode)
  -t, --time      #         time in seconds to transmit for (default 10 secs)
  -n, --bytes     #[KMG]    number of bytes to transmit (instead of -t)
  -k, --blockcount #[KMG]   number of blocks (packets) to transmit (instead of -t or -n)
  -l, --len       #[KMG]    length of buffer to read or write
                            (default 128 KB for TCP, 8 KB for UDP)
  --cport         <port>    bind to a specific client port (TCP and UDP, default: ephemeral port)
  -P, --parallel  #         number of parallel client streams to run
  -R, --reverse             run in reverse mode (server sends, client receives)
  -w, --window    #[KMG]    set window size / socket buffer size
  -M, --set-mss   #         set TCP/SCTP maximum segment size (MTU - 40 bytes)
  -N, --no-delay            set TCP/SCTP no delay, disabling Nagle's Algorithm
  -4, --version4            only use IPv4
  -6, --version6            only use IPv6
  -S, --tos N               set the IP 'type of service'
  -Z, --zerocopy            use a 'zero copy' method of sending data
  -O, --omit N              omit the first n seconds
  -T, --title str           prefix every output line with this string
  --get-server-output       get results from server
  --udp-counters-64bit      use 64-bit counters in UDP test packets

[KMG] indicates options that support a K/M/G suffix for kilo-, mega-, or giga-

iperf3 homepage at: http://software.es.net/iperf/
Report bugs to:     https://github.com/esnet/iperf
```

## 結論:
iPerf是很專業的一套網速測試工具，基本上大多數情況應該都可以使用，`不過經由前輩提醒，基本上不限制頻寬的話，它會有多少就吃掉多少，這真的要特別注意一下!不然如果在公司內網進行測試，說不定會塞爆網路頻寬、造成其他人都不能使用，切忌切記!`

## 參考
1. [[iperf.fr]](https://iperf.fr/)
2. [[指令式的網路速度測試工具 iPerf3 ，揪出網路頻寬真實的一面]](https://walker-a.com/archives/5857)
3. [[Iperf使用說明]](https://m1016c.pixnet.net/blog/post/145780230)