---
title: Azure - Virtual Network Peering
categories:
 - 技術文件
 - Azure
date: 2020/1/13
updated: 2020/1/13
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, Virtual Network, Peering]
---
Author: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Purpose
Virtual Network(虛擬網路)，以下簡稱vNet，是Azure平台上用來規劃私人網域的一種資源，但它有作用域的限制，`本文主要在說明如何透過Peering將兩個不同區域的vNet配對起來，讓兩地使用不同vNet的服務，可以位於同一個私人網域、並能互相存取`。
<!-- more -->

## Introduction
vNet是Azure上各種資源透過網路互相存取的基礎，透過適當的規劃`我們可以將數個Azure資源規劃在相同的虛擬私人網域中，具有隔離、保護及安全性，外部無法隨便存取內部的資源服務`。如果某些服務需要提供外界使用，則可以設定公開IP或Domain Name讓外界可以存取服務，也可以開放給On-Premises Network存取。

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Two Virtual Network in different data center
- Two VMs with the different two vNets

## Concepts
vNet有幾個觀念如下:
- Address Space:  
位址空間，`Azure會為每個具有網路存取能力的資源配置一個虛擬私人IP`，例如:規劃一台VM、位於一個具有B Class的10.0.0.0/16網域中，那這台VM可能會被配發一個私人IP為10.0.0.5，而這個IP在10.0.0.0/16這個網域內能被存取。
- Subnets:  
子網路，也`就是一個大網域內的一個個子網路IP的子集合`，他可以讓我們進一步切割多個子網段，以進行更細部的區隔，也可以針對不同子網路進行安全性設定。例如:我們規劃兩個C Class子網路為10.0.1.0/24及10.0.2.0/24。
- Regions:  
`指的是一個個區域內的Azure資料中心，這個概念主要在於Region、而非有多少個資料中心(通常我們也會將資料中心視作同一個Region)`，因為實際的軟硬體規劃可能在同一區域內、不同的Buildings，但這些軟硬體可能都被規劃在相同區域內。  
`這邊要特別注意，由於Azure的先天限制，vNet僅能作用於同一個Region內，如果要跨Region就必須在另一個Region設定一個vNet、再把兩者配對起來，這樣兩個vNet才可以互相溝通`。
- Subscription:  
訂閱是所有Azure服務的計費基礎，所以vNet也必須在某一個訂閱下進行規劃與設定。

## Peering
`Peering指的是兩個不同的vNet的配對，也就是連結不同vNet、讓多個vNet可以互相溝通的基礎設施`。當vNet中的服務想要存取另一個vNet中的服務時，可以透過Peering Gateway協助Relay、將一個vNet內的要求轉發至另一個vNet中的服務，而這些要求/回應則是透過私人的IP/Domain進行，不會被送至外部網路，達到安全性的目的。

設定步驟:
1. 先開啟好兩個不同Region的vNet，如下圖所示，分別位於East Asia及South East Asia兩地。
![](/Teamdoc/image/azure/VirtualNetwork/vnet1.png)
2. 選擇其中一個vNet、並進入Settings &rarr; Peering &rarr; Add。
![](/Teamdoc/image/azure/VirtualNetwork/vnet2.png)
3. 輸入各必填欄位，並指定好Peering的目標vNet，再按下OK按鈕新增。  
  Note:  
  `Virtual Network欄位必須指定另一個vNet、不能指定正在設定的vNet。`
![](/Teamdoc/image/azure/VirtualNetwork/vnet3.png)
4. 接著就會開始建立Peering，且Azure會幫我們自動建立好另一個vNet的Peering。
![](/Teamdoc/image/azure/VirtualNetwork/vnet4.png)
5. 建好後進入Peering查看，可發現已經幫我們設定好B Class的私人IP網域。
![](/Teamdoc/image/azure/VirtualNetwork/vnet5.png)
6. 再進入另一個vNet查看，Azure也已經幫我們建好另一個私人IP的設定。
![](/Teamdoc/image/azure/VirtualNetwork/vnet6.png)

經過以上的步驟，現在開始我們就可以透過Peering，連接兩個vNet並讓兩個私人網域內的服務互相溝通。

## VMs
為了測試我們剛建好的Peering，我們需要分別在這兩個Region各建一台VM、並指定各自使用的vNet。
![](/Teamdoc/image/azure/VirtualNetwork/vm1.png)

1. 我們先透過`Public IP`遠端進入其中一台VM。
![](/Teamdoc/image/azure/VirtualNetwork/vm2.png)
2. 接著再透過`Private IP`遠端進入另一台VM，`如果能夠成功連線、表示Peering已建立成功`。
![](/Teamdoc/image/azure/VirtualNetwork/vm3.png)

## 結論:
由於`vNet的先天限制，無法設定跨Region的虛擬網路，但透過Peering我們依然可以將位處不同位置的vNet連接起來，讓使用兩個不同vNet的服務可以互相溝通`，且能夠根據我們指定的vNet自動建立兩者的對應，是很方便的功能。

## 參考
1. [What is Azure Virtual Network?](https://docs.microsoft.com/en-us/azure/virtual-network/virtual-networks-overview)
2. [Create, change, or delete a virtual network peering](https://docs.microsoft.com/en-us/azure/virtual-network/virtual-network-manage-peering)