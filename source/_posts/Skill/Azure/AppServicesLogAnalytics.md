---
title: Azure - How to use Log Analytics?
categories:
 - 技術文件
 - Azure
date: 2024/1/18
updated: 2024/1/18
thumbnail: https://i.imgur.com/OTE99ml.png
layout: post
tags: [Azure, App Services]
---

![](/Teamdoc/image/azure/logos/web_app.png)

## Purpose
`Azure Monitor`其中一環也是監控很重要的一個部分就是Log的存取，這些Log不僅僅可以讓我們找出錯誤發生的原因、也能夠用來分析，找出系統弱點、`Hot Spot`、使用率...等，但我發現目前有些服務還是沒有設定將Log存進`Log Analytics`，其實`Log Analytics`對於`Azure`各服務來說是非常強大的功能，基本上幾乎所有的服務都已經能夠使用它。所以本文的目的就是介紹如何設定`Log Analytics`以幫助我們進一步利用`Kusto Query`語法來查詢Log。
<!-- more -->

## Log Analytics Workspace
首先，在儲存Log之前我們要先為`Log Analytics`建立工作區(`Workspace`)，這樣後續的`Diagnostic Settings`才能為它指定要工作的區域，如下圖。

![](/Teamdoc/image/azure/AppServicesLogAnalytics/workspace1.png)

## Diagnostic Settings
接著，我們就可以到服務的`Monitoring`清單找到`Diagnostic Settings`，然後按下`+ Add diagnostic setting`以新增設定，如下圖。

![](/Teamdoc/image/azure/AppServicesLogAnalytics/diagnostic1.png)

進入之後，我們可以在左邊選取我們想要儲存的Log類別、Metrics計量相關的資料，然後到右邊勾選`Send to Log Analytics workspace`、下拉選單選取我們前一小節建立的`Log Analytics Workspace`再儲存設定。

![](/Teamdoc/image/azure/AppServicesLogAnalytics/diagnostic2.png)

設定完成後就會如下圖所示，出現在我們的設定清單中。

![](/Teamdoc/image/azure/AppServicesLogAnalytics/diagnostic3.png)

## Kusto Query
等一段時間紀錄一些Logs後，我們可以到`Monitoring`清單找到`Logs`，然後進入選擇其中一個查詢範例，我們這邊就以查詢`Top 5 clients`(前五名呼叫)的範例來說明。

```
// Top 5 Clients 
// Top 5 clients which are generating traffic. 
AppServiceHTTPLogs
| top-nested of _ResourceId by dummy=max(0), // Display results for each resource (App)
  top-nested 5 of UserAgent by count()
| project-away dummy // Remove dummy line from the result set
```

查詢結果如下圖，我們可以很方便地由`Log Analytics`統計出我們想要知道的相關資訊。

![](/Teamdoc/image/azure/AppServicesLogAnalytics/logs1.png)

## Conclusion
其實`Log Analytics`已經用了很長的時間了，真的是很好用的功能。不過，`Kusto Query`還是得下點功夫學習，目前我也還很生疏要多研究一下，但還是看到不少服務只是單純把這些logs存進`Storage Account - Blob`、以檔案的形式存放，雖然說Logs本來就是單純存檔就好，不過`Azure`的logs可不只能拿來當紀錄而已，所以改存`Log Analytics Workspace`可以發揮更大的作用喔!

## References
1. N/A