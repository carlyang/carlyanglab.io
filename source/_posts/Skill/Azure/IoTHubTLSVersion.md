---
title: Azure - How to check TLS Version when handshacking with IoT Hub
categories:
 - 技術文件
 - Azure
date: 2023/1/13
updated: 2023/1/13
thumbnail: https://i.imgur.com/d1Wvf0c.png
layout: post
tags: [Azure, IoT Hub, TLS]
---

![](/Teamdoc/image/azure/logos/iot_hub.png)

`IoT Hub`可做為地對雲資料傳輸的入口，作為暫存資料、遠端管理Edge端裝置等目的，具有低延遲、高可靠度等特性，很適合用來同步IoT資料進入各式各樣`Big Data`資料來源的前哨站，所以其安全性也非常重要，尤其是地端裝置對`IoT Hub`溝通時使用的TLS版本。
<!-- more -->

## Purpose
以目前來說，`Azure`的`MS Defender`等安全性漏洞偵測工具，幾乎都會把TLS版本檢查列為必要項目(`Critical`)，而`IoT Hub`本身其實對於`TLS 1.0/1.1/1.2`等版本都支援，必須要看地端Host的OS設定來決定使用何種`TLS`版本，本文的目的在說明如何確認Host對應`IoT Hub`使用的`TLS`版本、及如何設定地端Host以使用`TLS 1.2`。

## Use WireShark
`Wireshark`是一個網路通訊Log工具，詳細如何使用本文就不再多說明，只要在你想要錄製的機器安裝好、打開它並選擇要錄製Log的網路卡，便可開始錄製Log。  

- Step 1. 查出你要錄製的服務IP(這邊指`IoT Hub`)
![](/Teamdoc/image/azure/IoTHubTLSVersion/iothub1.png)

- Step 2. 使用`wireshark`開始錄製Log，就可以在下圖紅框處看見真正用來與`IoT Hub`溝通使用的`TLS`版本。

Note: 只要確認錄製過程中，網卡上有跟`IoT Hub`溝通的動作就可以停止，基本上不會沒錄到，如果真的沒有可能通訊在其他張網卡上面，請重選網卡後再錄製一次。
![](/Teamdoc/image/azure/IoTHubTLSVersion/iothub2.png)

## IoT Hub SDK
當我們在地端建立`Application`時常會使用`IoT Hub SDK`來幫助處理資料上傳與`IoT Hub`間的溝通工作，而`IoT Hub SDK`對於使用`TLS`版本的原則基本上是由作業系統本身自己決定，只有`.NET Framework 4.5.1`是`IoT Hub SDK`直接Hard Code寫在程式碼內指定使用`TLS 1.2`，引用[IoT Hub SDK GitHub](https://github.com/Azure/azure-iot-sdk-csharp/blob/main/configure_tls_protocol_version_and_ciphers.md#configuring-the-tls-version)如下。

>## Configuring the TLS version
>When targetting .NET Standard, the SDK does not specify the TLS version. Instead, it is preferred to let the OS decide; this gives users control over security.  
>One exception to this is clients using .NET Framework 4.5.1 which does not have a "let the OS decide" option. In this case, the SDK is hard-coded to use the latest version, TLS 1.2, only.  
>WARNING - The .NET Framework 4.5.1 is no longer supported, meaning no security updates, so it is highly recommended that clients update to the latest .NET version (or LTS version) to benefit from the latest security fixes.  
>To ensure your Azure IoT .NET application is using the most secure version one should configure the operating system to use explictly disable the undesirable SChannels. See below for operating system-specific instructions.

## Windows Registry
所以，如果我們想要指定使用`TLS`版本，就必須要從Host的作業系統下手(本文使用的是`Windows Storage Server 2016 Standard`)。  
首先輸入`regedit`打開機碼編輯器找到下列位置，接著各加入`Enabled = 1`及`DisabledByDefault = 0`兩個DWORD，如此即可開啟`TLS 1.2`。

```
HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Client
```

另外，由於地端對`IoT Hub`的角色是`Client`、所以`Server`的註冊機碼這邊沒有用到，使用上也是加入`Enabled`及`DisabledByDefault`兩個DWORD，位置如下。
```
HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Server
```

Note:  
如果有其他版本如`TLS 1.0`、`TLS 1.1`等、`建議都刪除不要留!`我在設定的過程中發現如果有加入其他TLS版本的機碼，反而在連`IoT Hub`時會發生`The SSL connection could not be established`錯誤。

![](/Teamdoc/image/azure/IoTHubTLSVersion/registry1.png)

詳細機碼的設定可參考[MS Doc](https://learn.microsoft.com/en-us/windows-server/security/tls/tls-registry-settings?tabs=diffie-hellman#tls-dtls-and-ssl-protocol-version-settings)

## minimum TLS Version
除了上述地端設定外，其實`IoT Hub`也可以限制使用的TLS最小版本，也就是說我們可以讓`IoT Hub`只接受`TLS 1.2`，如下圖(擷取自[[MS Doc]](https://learn.microsoft.com/en-us/azure/iot-hub/iot-hub-tls-support#tls-configuration-for-sdk-and-iot-edge))。

![](/Teamdoc/image/azure/IoTHubTLSVersion/iot-hub-tls-12-enforcement.png)

但是這個功能有兩個比較大的問題。  
1. 如果已有使用中的`IoT Hub`則必續重建新的`IoT Hub`，然後將Application重新更新連線到新的`IoT Hub`上，這是因為指定`minimum TLS Version`功能只能在一開始新建服務時才能設定，無法半途更改設定。
2. 另一個問題就是目前指定`minimum TLS Version`功能只有開放在`US`的幾個`Regions`，並沒有開放到其他區域，引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/iot-hub/iot-hub-tls-support#tls-12-enforcement-available-in-select-regions)如下。

>For added security, configure your IoT Hubs to only allow client connections that use TLS version 1.2 and to enforce the use of cipher suites. This feature is only supported in these regions:
>- East US
>- South Central US
>- West US 2
>- US Gov Arizona
>- US Gov Virginia (TLS 1.0/1.1 support isn't available in this region - TLS 1.2 enforcement must be enabled or IoT hub creation fails)
>
>To enable TLS 1.2 enforcement, follow the steps in Create IoT hub in Azure portal, except

## Conclusion
之所以寫這篇文，是因為最近公司遇到一些安全性的檢查，必須要確保相關的`Azure`服務使用`TLS 1.2`版本，否則被掃到漏洞的話也是很危險，加上微軟其實一直在倡導用戶將TLS版本升到1.2。因此，為了符合全球各地區對安規的要求，升級到`TLS 1.2`更是勢在必行，否則不久後地端系統很可能就不能正常連線`Azure`服務囉!各位朋友要多注意一下。

## References
1. [azure-iot-sdk-csharp](https://github.com/Azure/azure-iot-sdk-csharp/blob/main/configure_tls_protocol_version_and_ciphers.md)
2. [TLS 1.2 enforcement available in select regions](https://learn.microsoft.com/en-us/azure/iot-hub/iot-hub-tls-support#tls-12-enforcement-available-in-select-regions)
3. [TLS, DTLS, and SSL protocol version settings](https://learn.microsoft.com/en-us/windows-server/security/tls/tls-registry-settings?tabs=diffie-hellman#tls-dtls-and-ssl-protocol-version-settings)