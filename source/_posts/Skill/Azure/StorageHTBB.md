---
title: Azure - High-throughtput Block Blob
categories:
 - 技術文件
 - Azure
date: 2024/3/20
updated: 2024/3/20
thumbnail: https://i.imgur.com/8uMcIBL.png
layout: post
tags: [Azure, Blob]
---
作者: Carl Yang

![](/Teamdoc/image/azure/logos/storage_account.png)

## Purpose
最近公司使用`Azure Stream Analytics`持續串流資料進入`Storage Account`的`Block Blob`中，但出現吞吐量超過上限的錯誤，於是就仔細地查了一下發生原因、也想辦法解決這個錯誤，故本文目的在分享一下問題處理的過程、相關的資訊及解決方案。
<!-- more -->

## Server Busy
一開始是發現當我們在串流資料進`Block Blob`時，因為我們設定的`Alert Rule`發出警告，我們設定的`Metric`是資料寫入延遲秒數上限超過300秒(也就是五分鐘)就發出，檢查錯誤詳細資訊發現如下訊息。

>Azure Storage Source:Azure.Storage.Blobs

>Message:Operations per second is over the account limit.

>Status: 503 (Operations per second is over the account limit.)

>ErrorCode: ServerBusy

看起來是`Storage Account`中的`Blob`寫入時發生錯誤，而我們設定是寫入`Block Blob`類型的資料，所以應該是碰到某樣限制的上限了，於是我們就來查了一下[[官方文件]](https://learn.microsoft.com/en-us/azure/storage/blobs/scalability-targets#scale-targets-for-blob-storage)。

## Block Blob Throughput
根據文件所述，看起來符合條件的`嫌疑犯`應當是下面紅框這條，因為我們一次串流只寫一個`Block Blob`、且查閱錯誤的`Block Blob`並沒有出現在`Storage`內，所以可以確認當時是在寫入單一`Block Blob`時發生的。

![](/Teamdoc/image/azure/StorageHTBB/blob1.png)

再進一步查看說明，引述如下。

>Throughput for a single blob depends on several factors. These factors include but aren't limited to: concurrency, request size, performance tier, speed of source for uploads, and destination for downloads. To take advantage of the performance enhancements of high-throughput block blobs, upload larger blobs or blocks. Specifically, call the Put Blob or Put Block operation with a blob or block size that is greater than 256 KiB.

也就是說，超過`Throughput`的因素很多，包括並發、大小、服務效能、上下傳的速度等因素，`所以就必須要改用另一種具有高效Throughput的Blob才行!`

## HTBB
`HTBB`全名是`High-throughtput Block Blob`，基本上只要符合下表的`Storage Account`版本都會`HTBB`都會生效，`不過各位要仔細看一下，v1、v2、Blob storage一般版本必須要單次寫入4MiB以上的才會增加Throughput，除非是Premium版的BlockBlobStorage才只需要256KiB!`於是檢查了一下我們每個`Block Blob`的大小，每個都大約1~2MiB而已，看來只能改用Premium版的BlockBlobStorage的了。

![](/Teamdoc/image/azure/StorageHTBB/blob2.png)

所以，我們一開始建立`Storage Account`時就需要指定`Premium`版的`Block Blob`如下圖位置，`Performance`請選`Premium`版、`Premium accountt type`請選擇`Block Blobs`，接著建立`Storage Account`即可。

Note:  
建議舊的`Block Blob`利用`AzCopy`批次複製到新的`Premium`版`Storage Account`，處理效能會好上不少。

![](/Teamdoc/image/azure/StorageHTBB/blob3.png)

## Conclusion
基本上`HTBB`是很好的設計，基本上也不增加成本，不過如果是像我們的情境、大部分都小於4MiB大小、又必須提升`Throughput`的話，就必須要改用`Premium`版的`Block Blob`，這樣成本就會高一點了，各位使用之前要考慮一下能否接受`Premium`版的收費價格喔!

## References
1. [Scale targets for Blob storage](https://learn.microsoft.com/en-us/azure/storage/blobs/scalability-targets#scale-targets-for-blob-storage)
2. [High-Throughput with Azure Blob Storage](https://azure.microsoft.com/en-us/blog/high-throughput-with-azure-blob-storage/)