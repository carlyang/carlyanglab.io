---
title: Azure - VMs Availability
categories:
 - 技術文件
 - Azure
date: 2019/12/17
updated: 2019/12/17
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, Availability Set, Fault Domain, Update Domain]
---
作者: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Purpose
Azure Availability(可用性)意指其服務的可用程度、能力等，可被理解為在單位時間內、服務正常運作時間的比例，其他像是HA(High Availability)、[SLA(Service Level Agreement)](https://azure.microsoft.com/en-us/support/legal/sla/virtual-machines/v1_8/)等都是在闡述這方面的相關議題，而`本文旨在探討Azure VMs的可用性及其容錯/更新機制`。  
<!-- more -->

## Introduction
關於Azure VMs的Availability主要有三個部分，分別是Availability Set、Fault Domain及Update Domain，以下就三個部分逐一說明。

## Azure Regions, Zones and Data Centers
探討Availability之前，需要先了解Azure的各區域概念。
1. Region: `可視為一個大區域，內含多個Zones，例如: East US、West US。`
2. Zone: `可視為一個小區域，其上是Regions、其下包含多個Data Centers。`
3. Data Center: `包含一或多個Buildings，每個Building都是一組實體設備的集合。`
![](https://azurecomcdn.azureedge.net/cvt-d3ca73af0503f7978684b66d4dcbe009a0f5f109255ca2723875553ddc111308/images/page/global-infrastructure/regions/understand-regional-architecture.png)

## Availability Set
以VMs來說，`為一組邏輯概念上的VMs集合`。假設現在建立一台VM、並有另一台完全相同的複本、以在硬體/軟體發生錯誤或更新時，能夠即時切換使用、避免服務中斷，就必須先建立一個Availability Set，並指定Fault Domain(下節說明)與Update Domain(下下節說明)，這時候VM就會有兩台以上完全相同、更新也完全相同的VM做為備援，另一台就可以在當前這台發生錯誤時，即時替換上場。

下圖為建立Azure VM時的設定畫面。如果我們想要有一台複本，就要先指定Availability Options為`Availability Set`，如果沒有Availability Set就需要建立一個新的。
![](/Teamdoc/image/azure/availability/availability_set1.png)

在建立新的Availability Set時，就要指定Fault Domain及Update Domain數目。
![](/Teamdoc/image/azure/availability/availability_set2.png)

最後，再將其他設定都完成後，建立的VM總共就會有兩個、一台為作用中、而其所做的任何變更、都會同步到複本上。

Note:  
1. Availability Set是一種邏輯概念上的VMs集合，他可以讓所有的VMs複本同時在Fault Domain及Update Domain上工作。
2. 會造成軟硬體錯誤的情況，概略可分為以下三種:
>- Unplanned Hardware Maintainance Events:  
    When Azure platform predicts that the hardware or any platform components associated to a physical machine is about to fail.  
>- An unexpected downtime:  
    Rarely occurs  
>- Planned Maintenance events:  
    Periodic updates made by Microsfot.  
節錄自[[MS Doc]](https://docs.microsoft.com/en-us/azure/virtual-machines/windows/manage-availability)

3. Availability Options中會看到還有另外兩個選項，Virtual Machine scale set(preview)因為是預覽版，先略過不提，而`Availability Zone則是能夠將VMs建置在相同Region、不同Zone上`。
![](/Teamdoc/image/azure/availability/availability_zone1.png)
4. `Availability Zone是指同一Region中，不同的實體位置`，所以可以將VMs部署在多個Zone中，避免因為同一個Zone中的硬體故障造成服務中斷。
5. 而`Zone是一組實體資源的概念，它可以橫跨一或多個Data Center並裝載獨立的電源、冷卻系統及網路設備，但都在同一個Region內`，關於Availablity Zone的描述引用如下。
> Availability Zones are unique physical locations within an Azure region. Each zone is made up of one or more datacenters equipped with independent power, cooling, and networking.  
節錄自[[What are Availability Zones in Azure?]](https://docs.microsoft.com/en-us/azure/availability-zones/az-overview)
6. 另外還有Paired Regions，可以讓VMs橫跨不同Region部署，例如:East US與West US，詳細可參考[[What are paired regions?]](https://docs.microsoft.com/en-us/azure/best-practices-availability-paired-regions#what-are-paired-regions)。

## Fault Domain
Fault Domain`預設會將VM複本建立在同一個機房、不同機櫃上，以防硬體發生錯誤或更新時，不會因為同一機櫃上的錯誤而全部癱瘓`。

以下圖為例，我們設定了三組Fault Domain FD0~FD2，那麼我們的VM複本就會被建立在不同Fault Domain上，當F0中的第一台VM掛掉、那麼FD1或FD2中的其中一台VM複本就會被啟動、以維持服務不中斷。
![](https://docs.microsoft.com/en-us/azure/includes/media/virtual-machines-common-manage-availability/ud-fd-configuration.png)

Note:  
1. 下圖Fault Domain的數目，`不同的Data Center有不同的上限，但建議不要設為1，否則就失去Fault Tolerant的作用了。`
![](/Teamdoc/image/azure/availability/availability_set2.png)
2. 各Region的Fault Domain上限可參考下列連結。  
[[Number of Fault Domains per region]](https://docs.microsoft.com/en-us/azure/virtual-machines/windows/manage-availability#number-of-fault-domains-per-region)

如果我們是選擇前一小節中所說的Availability Zone的話，就會如下圖所示，`VM複本會被建立在相同Region、不同Zone的實體位置上，當Zone1因某些原因掛了，還有Zone2/Zone3的VM可以接續服務`。這樣的好處就是可以橫跨多個Data Center，避免一個Data Center因某些緣故失效時，還有另一個Data Center內的VM可維持服務。
![](https://docs.microsoft.com/en-us/azure/includes/media/virtual-machines-common-regions-and-availability/three-zones-per-region.png)

## Update Domain
Update Domain則是指`當軟體需要更新時，並不會一次全部更新VMs複本，而是依照我們建立Availability Set時、所設定的Update Domain數目，將VMs自動分群後、逐群進行更新及重啟`，如此才不會因為軟體更新及重啟造成服務中斷。

如下圖所示，當軟體更新時、會先更新UD0~UD2，待更新完成才會再進行其他部分的更新。
![](https://docs.microsoft.com/en-us/azure/includes/media/virtual-machines-common-manage-availability/ud-fd-configuration.png)

## 結論:
由以上可知，Availability Set是為了VMs的複本及高可用性而設計的，它是邏輯概念上、針對VMs的複本進行分配與管理的一套機制。再深入去看，可以發現，`Availability Set真正的實作機制，其實是依靠Fault Domain及Update Domain的設定，透過Azure的虛擬機器自動部署，並在需要的時候啟動VM複本以維持服務`。因此，我們可以將Fault Domain視為硬體容錯機制、而Update Domain則視為軟體容錯機制。

## 參考
1. [Manage the availability of Windows virtual machines in Azure](https://docs.microsoft.com/en-us/azure/virtual-machines/windows/manage-availability)