---
title: Azure - How to Add/Update Custom Domain Certificate in API Management
categories:
 - 技術文件
 - Azure
date: 2024/3/11
updated: 2024/3/11
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management]
---

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
公司導入`API Management`(下稱`APIM`)也有不短的時間了，也因為多了這一層`API Gateway`，我們不但把微服務化的後端APIs集中化管理、也增加了許多的安全性機制，例如:Private Auth、Network Restriction、Virtual Network..等，`APIM`真的是優點很多很棒的服務，其中有一項功能稱為`Custom Domain`，讓我們可以自訂`Domain Name`，但是這個`Domain Name`需要定時更新憑證以保證安全性，故本文的目的在為各位說明如何新增自訂`Domain Name`及更新自訂`Domain Name`的憑證。
<!-- more -->

## Custom Domain
`Custom Domain`簡單來說就是我們可以自訂對外公開的`Domain Name`，讓外部使用這個`Domain Name`連線到`APIM`，而不是使用`APIM`預設的`[APIM Name].azure-api.net`。使用`Custom Domain`的好處主要在於隱藏後端`APIM`的真實位置，也就是外部呼叫時並不知道實際上是`Azure`的`.azure-api.net`位置，較不容易被有心人士針對`Azure`進行攻擊。

Note:  
而`Custom Domain`在建立之初就需要一個申請好的憑證，這個憑證要請各位自行向CA單位先申請好，本文接下來才能夠順利進行。

## Add Custom Domain
1. 首先我們先進入`Custom Domain`功能，這時候已經可以看見預設內建好的`Domain Name: *.azure-api.net`，我們接著按下上方的`Add`按鈕。

![](/Teamdoc/image/azure/APIMCustomDomainCert/add1.png)

2. 接著我們輸入各欄位，說明如下。

- `Hostname`: 請輸入完整的`Domain Name`。
- `Certificate`: 由於我們要自訂`Domain Name`，所以請下拉選單選擇`Custom`。
- `Certificate file`: 請選擇轉備好的`.pfx`檔。
- `Password`: 請輸入`.pfx`檔的密碼。
- `Default SSL binding`: 強烈建議勾選，這是指必須要以`https`方式連線。
- `Add`: 最後按下Add按鈕即可。

![](/Teamdoc/image/azure/APIMCustomDomainCert/add2.png)

3. 再來會回到上一頁的清單，我們就可以看到我們自訂的`Custom Domain`，上方也會有提示訊息提醒我們要再按下`Save`按鈕儲存設定，我們直接儲存後等待`APIM`更新完成即可生效。

Note:  
`請特別注意，因為新增憑證需要更動到APIM內部設定、並且APIM要重新啟動才能夠套用新的憑證!所以這個步驟依照個人的經驗，可能需要等待30~60分鐘不等的時間，這段時間內APIM是無法連線的!所以特別提醒大家，新增憑證務必要選擇非工作時間來進行才不會影響日常作業!`

![](/Teamdoc/image/azure/APIMCustomDomainCert/add3.png)

## Update the Certificate of Custom Domain
在成功新增`Custom Domain`後我們就可以開始使用它了，不過每個憑證都是有效期的、我們的也不例外，目前是一年Renewal一次憑證，所以接下來就是說明如何更新憑證。

1. 我們先準備好憑證檔(.pfx)以及憑證密碼，然後進入下圖`Custom Domain` -> 點選要更新憑證的`Custom Domain`(同時也可以看到現有憑證的到期時間)。

![](/Teamdoc/image/azure/APIMCustomDomainCert/cert1.png)

2. 進入後我們可以看到憑證到期日下方有個`Change`按鈕按下進行下一步。

![](/Teamdoc/image/azure/APIMCustomDomainCert/cert2.png)

3. 接著就會出現兩個欄位，要你選擇憑證檔的位置以及所需的密碼，輸入完後按下下方的`Update`按鈕。

![](/Teamdoc/image/azure/APIMCustomDomainCert/cert3.png)

4. 這時候回到上一頁畫面，會提示我們要按下`Save`按鈕進行更新，直接按下即可。

![](/Teamdoc/image/azure/APIMCustomDomainCert/cert4.png)

5. `Save`後右上角會出現`Updating`訊息(如下圖一)、而提示訊息的部分則會顯示`Service is being updated...`(如下圖二)。

Note:  
`請特別注意，因為更新憑證需要更動到APIM內部設定、並且APIM要重新啟動才能夠套用新的憑證!所以這個步驟依照個人的經驗，可能需要等待30~60分鐘不等的時間，這段時間內APIM是無法連線的!所以特別提醒大家，更新憑證務必要選擇非工作時間來進行才不會影響日常作業!`

![](/Teamdoc/image/azure/APIMCustomDomainCert/cert5.png)

![](/Teamdoc/image/azure/APIMCustomDomainCert/cert6.png)

6. 等`APIM`更新完成後我們就可以看到憑證到期時間已經更新為新的時間。

![](/Teamdoc/image/azure/APIMCustomDomainCert/cert7.png)

## Conclusion
自從使用`APIM`後，我們也將許多微服務透過`APIM`集中並套用了許多安全性的機制，例如:自訂的Auth服務、Network Restriction、Virtual Network...等，讓我們雲端服務的安全性更上層樓，事實上也因為有了`APIM`一定程度上解決了公司對於安全性的要求(公司非常要求安全性)，而`Custom Domain`也不會將我們的真正位址曝露在外，加上憑證的驗證及效期的更新、促使我們要定期進行更新避免危險性，就像我們的銀行帳密也是會建議定期更換一樣，都是提高安全性的好方法。

## References
1. N/A