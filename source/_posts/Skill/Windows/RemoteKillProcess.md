---
title: How to kill process remotely?
categories:
 - 技術文件
 - Windows
date: 2019/08/16
updated: 2019/08/16
thumbnail: https://i.imgur.com/clVjgmy.png
layout: post
tags: [Windows, taskkill, PSExec]
---
作者: Carl Yang

![](/Teamdoc/image/Windows/windows_logo.png)

## Purpose
系統常有些惱人的Process卡在那邊不動，通常就是手動進工作管理員End Task即可，也有人直接使用taskkill指令結束它。但如果是遠端電腦上的Process呢?如果在別台電腦上可就沒那麼容易Kill了，本文主要在說明如何遠端下指令結束有問題的Process。
<!-- more -->

## Tools
本篇使用PSExec工具進行遠端指令操作，大家可從[[PSExec]](https://docs.microsoft.com/en-us/sysinternals/downloads/psexec)下載。

## PSExec Command
執行下列指令以遠端登入、並執行cmd程式。
>.\psexec -u domain\admin user -p password \\\\HostNameOrIP cmd

1. 執行命令前請先切換至PSExec.exe的所在目錄。
2. -u參數: ```遠端電腦上具有Admin權限的使用者帳號```。
3. -p參數: ```遠端電腦上具有Admin權限的使用者帳號```的密碼。
4. \\\\HostNameOrIP: 遠端電腦的名稱或IP，```請注意本地端電腦必須與遠端電腦位於同一網域中```。
5. cmd: 就是要執行遠端電腦的cmd.exe。```理論上來說改用powershell也可以，但就我實際執行情況來說是無效的，powershell沒有任何反應```，或許跟權限有關，未來這部分有新的做法再更新給大家。

## Command Mode
- 遠端登入後就可看到如下cmd畫面，剩下的就是原本執行cmd的操作。保險起見，先用tasklist查出完整的Process名稱。  
![](/Teamdoc/image/Windows/RemoteKillProcess/RemoteKillProess1.png)

- 再用taskkill結束Process。
![](/Teamdoc/image/Windows/RemoteKillProcess/RemoteKillProess2.png)

Note:  
1. 其實taskkill有filter可用、帶入*號即可，可以另外用```taskkill /?```查詢相關參數說明，```但個人很不建議這樣使用```，因為系統內Process可能非常多，用了模糊刪除可能會連不該刪的也刪掉，所以我才會先用```tasklist```先確認完整名稱再做刪除。
2. /f參數: 強制結束Process
3. /im參數: 指定的Process名稱。

## Others
過程中發現，有可能在kill時```發生存取被拒```的情形，查了一下網路跟比對遠端電腦的系統版本，發現可能跟Windows 10的UAC設定有關，也就是說遠端指令操作時，Win10的UAC是比較嚴格的，如果沒有開放權限就可能發生此狀況。  
這邊有找到一篇文章可以參考看看[[開啟Win10遠端指令權限]](https://kheresy.wordpress.com/2017/06/09/windows-10-remote-command/)，不過我在執行時發現，這必須要Admin在local端執行、並重開機後才能生效，目前暫時無法驗證效果。

## 結論:
其實很多時機點都有可能需要進行遠端指令操作，甚至像我這次的情況就是因為分隔南北兩地，我在北部不太可能為了一台機器要停止Process、就跑到南部去處理，這時候遠端的操作就不可避免了，給大家參考看看。

## 參考
1. [PsExec v2.2](https://docs.microsoft.com/en-us/sysinternals/downloads/psexec)  
2. [開啟 Windows 10 的遠端指令權限](https://kheresy.wordpress.com/2017/06/09/windows-10-remote-command/)