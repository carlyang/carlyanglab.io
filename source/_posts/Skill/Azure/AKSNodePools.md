---
title: AKS - Node Pools
categories:
 - 技術文件
 - Azure
date: 2023/12/20
updated: 2023/12/20
thumbnail: https://i.imgur.com/5sNBEdu.png
layout: post
tags: [Azure, AKS]
---

![](/Teamdoc/image/azure/logos/aks.png)

## Purpose
`K8s`底層的Host依然是一台具有完整作業系統的機器，而這台機器可以是一台實體機、也可以是一台`Virtual Machine`，而導入`Azure`雲端平台後理所當然地都會使用`Virtual Machine`來配置所需資源、同時也是導入雲端平台後優勢，我們可以依照需求動態調整`VM`的規格來符合當下的使用量，所以建立`AKS`之初就必定需要建立`VMs`，這些`VMs`具有相同組態設定的都會加以適當地群組歸類、方便管理及維護，而這些群組則被稱為`Node Pools`，本文的目的就在位各位介紹`AKS`中的`Node Pools`相關概念。
<!-- more -->

## Node Pools
`Node Pools`可以視為一群具有相同組態設定的`VMs`、被適當地加以群組規劃以便於管理及維護，同一個`Node Pool`包含多個`Nodes`、每個`Node`都相當於一台`VM`、同一個`Node Pool`中的`Nodes`都具有相同的`Scale`及組態，而`Node Pools`又可區分為`System Node Pool`及`User Node Pool`，進入位置如下圖。

![](/Teamdoc/image/azure/AKSNodePools/nodepool1.png)

進入後我們可以查看`Node pools`標籤，接著我們可以看到下方顯示兩種`Node pools`、`agentpool`及`userpool`，`agentpool`就是`AKS`系統使用的pool、上面會跑一些`AKS`系統本身相關的服務，而`userpool`則是用來跑一些我們自己的應用程式`Pods`。

說明如下:
- `Node pool`: Pool的名稱。
- `Provisioning state`: `Node pool`目前最新的狀態，它可能會隨著Pool的不同操作而顯示不同的狀態，例如:更新`K8s`版本、Scale調整...等。
- `Power state`: 顯示Pool是否啟動狀態、是否能接受指令啟動或部署`Pods`，它只會根據`Start`或`Stop`操作而變更狀態。
- `Scale method`: 這是顯示目前Pool的Scale設定，預設是`Autoscale`、同時也是建議設定，這是因為`AKS`可以根據`Autoscale`的設定內容動態調整Pool的Scale，我們可以點選進入詳細設定頁面，如下圖。

在設定頁面中我們可以更改`Minimum node count`及`Maximum node count`，這是同一個Pool中最小最大的`Nodes`使用量，`AKS`可以在這個範圍內動態調整`Nodes`數量，預設最小是2個`Nodes`、最大是100個`Nodes`。

Note:  
請注意，`每個AKS叢集中，每一個Node Pool最大可以包含1000個Nodes，不同Node Pools最多可以有5000個Nodes`。

![](/Teamdoc/image/azure/AKSNodePools/nodepool2.png)

在設定頁面下半部我們還可以看到目前`VM`的大小、最大CPU及Memory設定等資訊，下面還有過去一段時間(下拉選單可改選)中的CPU、Memory使用量等，對於我們日常維護管理很方便，如下圖。

![](/Teamdoc/image/azure/AKSNodePools/nodepool3.png)

- `Target nodes`: 目前的autoscaler下啟動的目標`Nodes`數量，目前的目標是2個。
- `Ready nodes`: 實際待命的`Nodes`數量，可以跟`Target nodes`搭配觀察，一般兩者數量會一致、表示目前的`Nodes`都正常。
- `Autoscaling status`: 目前`Autoscaling`的狀態，可能會有`Scaling up`、`Scaling down`、`Back off`(scaling失敗時的回復狀態)、`Failed to scale up`、`Failed to scale down`等。
- `Mode`: 指目前`Node Pool`是`System Node Pool`還是`User Node Pool`，`System Node Pool`預設用來跑`AKS`系統相關的東西、`User Node Pool`預設用來跑我們自己的應用程式`Pods`。
- `Kubernetes version`: 目前的`K8s`版號。
- `Node size`: 目前`Node`設定的`VM Scale`。
- `Operating system`: 目前`VMs`的作業系統。

## Nodes
接下來我們再來看`Nodes`標籤內的資訊。每個`Node`就相當於一台`VM`、每個`VM`有自己的`Scale`，但是我們針對`Node Pool`設定的`Scale`、`AKS`會套用在相同`Node Pool`中的所有`Nodes`，所以我們可以看到預設是每個`Node Pool`至少2個`Nodes`、同一`Node Pool`中的`Nodes`的`Scale`都會相同，如下圖紅框。

Note:  
請注意，因為目前Loading很小，所以`Nodes`目前是啟動最小值2個，這個值可能會因為Loading不同而變化。

![](/Teamdoc/image/azure/AKSNodePools/node1.png)

- `Node`: 也就是目前的`Node`名稱，細看可以發現名字中有`vmss`字樣，這是因為`Azure`對於`AKS VM`的命名規則所致。
- `Status`: 目前`Node`的狀態，正常應該都要顯示`Ready`搭配綠色勾勾、如果顯示不一樣可能就要檢查一下是否正常。
- `CPU`: 目前`Node`的CPU使用率，如果一直降不下來要注意一下。
- `Memory`: 過去五分鐘內`Node`的記憶體使用率，太高要注意一下，基本上建議保持80%以下較安全。
- `Disk`:過去五分鐘內`Node`的磁碟使用率， 
- `Pods`: 目前`Node`內正在跑的`Pods`數量，`可以點選進入查看詳細的Pods資訊(日常維運很常用到)`，如下圖。

![](/Teamdoc/image/azure/AKSNodePools/node2.png)

- `Node pool`: 目前`Node`所屬的`Node pool`。
- `Kubernetes version`: 目前的`K8s`版號。

## Conclusion
本文主要在介紹日常`AKS`最常用的功能-`Node pools`，也說明了`Node pools`的最基本的概念，目的是讓大家能夠熟悉`AKS`的`Node pool`相關工具，尤其是在日常維運時常常各式各樣的疑難雜症都必須要學會查閱這些功能內的資訊，而且`Node pool`也可以明確地讓我們知道出問題的是哪台`VM`上的哪個`Pod`，如果能夠再搭配`ApplicationInsights`錄製`Pods`內應用程式的紀錄，基本上系統出問題應該都能夠查出原因，所以熟悉`Node pool`是`AKS`必須要知道的項目之一。

## References
1. N/A