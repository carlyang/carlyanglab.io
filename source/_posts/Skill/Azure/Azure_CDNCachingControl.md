---
title: Azure - Cache Control of Content Delivery Network
categories:
 - 技術文件
 - Azure
date: 2020/1/1
updated: 2020/1/1
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, Cache Control, Content Delivery Network, CDN]
---
作者: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Purpose
Content Delivery Network(簡稱CDN)，是Azure對於全球性的靜態資料傳遞所提供的服務，本文主要說明CDN的Cache Behavior、Cache Expiration Duration、Query String Caching Behavior三者的設定方式。
<!-- more -->

## Introduction
Content Delivery Network(以下簡稱CDN)，是Azure對於全球性的靜態資料傳遞所提供的服務，舉凡css、javascript、.mp4(Video files)等，屬於`靜態檔案資料的都可以透過CDN、發佈至全球指定的資料中心。而將這些靜態資料實體檔案放到各存放點的過程，就稱之為Cache，意指將這些資料快取至全球各地，這樣子當使用者透過全球網路連線、取用這些靜態資料時，就可以直接從最接近的端點取得，不需要回到原始儲存來源要求資源，也能降低原始資料來源的負擔`。

## Prerequisites
請準備具有下列條件的環境。
- Azure Valid Subscription

## point-of-presence
`point-of-presence(以下簡稱POP)，指非原始資料來源的其他儲存位置`，當我們指定CDN要發佈靜態資料至全球各地時，各個實際儲存快取資料的節點。

## Caching Rules
我們可以透過Azure Portal進入CDN服務、左邊Menu下方的Caching Rules連結進入設定。
![](https://docs.microsoft.com/en-us/azure/cdn/media/cdn-caching-rules/cdn-caching-rules-btn.png)

進入後可看到`Global caching rules`及`Custom caching rules`兩個區塊，`Global caching rules`就是本文要說明的部分，`Custom caching rules`因為概念都跟Global相同、僅有MATCH相關設定必須指定為自訂的URL Relative Path，就不再重複說明，不過`必須知道的是Custom的設定會覆蓋Global的設定`。
![](https://docs.microsoft.com/en-us/azure/cdn/media/cdn-caching-rules/cdn-caching-rules-page.png)

## Cache Behavior
關於快取行為的設定、如下圖中的第一個下拉選單，目前分以下三種說明。
![](https://docs.microsoft.com/en-us/azure/cdn/media/cdn-caching-rules/cdn-global-caching-rules.png)

- Bypass cache:  
`不要快取`且會忽略origin-provided cache-directive標頭，也就是不管如何都回從原始來源讀取資源。
- Override:  
`覆寫快取資源`，忽略origin-provided cache duration、改用自定義的快取周期時間，一但快取超過這個時間，就會重新從原始資料來源取的資源並更新，且此設定不會覆寫標頭內的[Cache-control:no-cache]。
- Set if missing:  
`如果POP內的資源不存在，才會從原始來源取得資源並更新快取，此設定一樣會參考快取周期`，過期後就會清除、並從來源更新POP快取。

## Cache expirationi duration
快取資料的過期時間，可以設定天數、時數、分鐘數、秒數，POP內的資源一旦超過這個時間就會失效。
設定如下圖第二個下拉選單。
![](https://docs.microsoft.com/en-us/azure/cdn/media/cdn-caching-rules/cdn-global-caching-rules.png)

## Query string caching behavior
當我們透過URI存取資源時，可以帶入URL的Query String來進行查詢，例如:http://www.contoso.com/content.mov?field1=value1&field2=value2，filed1及field2就是查詢參數。
而Query string caching behavior的設定，`可以接受/忽略Query String，並將傳進來的URI快取起來，如果該URI有被要求過、就會自動從近端POP讀取資源，而Query String則會被Pass給原始資料來源`，所以當這個URI被快取時是否包含Query String、就變成很重要的一部分，以下先說明各設定的意義。
![](https://docs.microsoft.com/en-us/azure/cdn/media/cdn-query-string/cdn-query-string.png)

- Ignore query strings:  
預設模式，`此設定會在第一次的時候將Query String丟給資料來源，爾後不管這個Query String為何，都會從近端POP節點讀取快取資源，直到Expiration Time到期`後、才會連同當下的Query String再pass給原始資料來源進行查詢及快取。
- Bypass caching for query strings:  
`此模式會直接忽略Query String、不將其傳遞給原始資料來源，也就是不管任何時候、只有URL的前面部分會被傳遞給資料來源，並快取資源到近端的POP節點`，爾後前端要求者就會使用這個POP快取節點的資源，直到Expiration Time到期。
- Cache every unique URL:  
此模式會`針對完整的URL進行快取(包含Query String)，這時候會將完全相同的URL傳遞給資料來源、並快取至近端POP節點，除非整個URL都相同(包含Query Stirng)才會從近端POP節點存取資源，否則都會從資料來源取得資源後、快取至近端POP節點，直到此快取的Expiration Time到期為止`。

## 結論:
當網站或API等網路站台的使用者擴及全世界時，CDN就能夠禿顯出它的重要性了，一般狀況下小範圍內的使用者(例如:台灣)使用CDN其實效益不大，而且也必須注意POP節點快取的時效性。但總體來說，當有一個Site的使用者必須跨海、甚至跨洲來存取時，基本CDN就要派上用場了。

## 參考
1. [Control Azure CDN caching behavior with caching rules](https://docs.microsoft.com/en-us/azure/cdn/cdn-caching-rules)
2. [Control Azure CDN caching behavior with query strings - standard tier](https://docs.microsoft.com/en-us/azure/cdn/cdn-query-string)