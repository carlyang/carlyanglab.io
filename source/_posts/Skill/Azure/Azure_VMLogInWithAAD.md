---
title: Azure - Login to VM using AAD Authentication
categories:
 - 技術文件
 - Azure
date: 2022/2/15
updated: 2022/2/15
thumbnail: https://i.imgur.com/lCrEvb3.png
layout: post
tags: [Azure, VM, Azure Active Directory]
---
Author: Carl Yang

![](/Teamdoc/image/azure/logos/vm.png)

## Purpose
`Azure VM`做為`IaaS`的基礎架構，基本上不太可能不用到它，例如:`Azure DevOps`想在雲端上Build，除了一開始建VM時一定要建的Local Admin帳號外，可能還會有些特定情境想要使用`Azure Active Directory`(以下簡稱`AAD`)內已有的帳號遠端`RDP`登入操作，本文即在介紹如何設定`Azure VM`以使用`AAD`帳號進行身分驗證。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Azure Active Directory(Sufficient Account)
- Azure VM(`Windows Server 2019 Datacenter edition or Windows 10 1809 and later`)

## Introduction
當我們建立一台`Azure VM`通常會給予多人使用，有些是真實的人員帳號、有些是服務帳號，但不同帳號會有不同的`User Profile`，這跟我們平常使用的Windows一樣。  
但既然是在雲端上，是否有更方便的身分驗證方式呢?其中一種方式就是使用`AAD`、利用現有的驗證機制輕鬆管理`Azure VM`的遠端帳號。

## 設定步驟
1. 建立一台`Azure VM`，建立步驟就先略過、主要注意支援`AAD`驗證的系統必須要是`Windows Server 2019 Datacenter`或是`Windows 10 1809`以上的版本才行，後面我們還要為`Azure VM`安裝擴充功能，才能夠正常使用。  

    Hint:  
    - 接下來的步驟需要使用`Azure Cloud Shell`進行指令操作，請先登入`Azure Portal`並開啟`Azure Cloud Shell`，如下圖。  
    - 如果你的訂閱下沒有使用過`Azure Cloud Shell`，他會需要一點時間建立要用到的`Storage Account`服務。
![](/Teamdoc/image/azure/Azure_VMLogInWithAAD/cloud_shell_1.png)

2. 接著我們先切換至作用的`Subscription`
    ```powershell
    az account set -s [Your Subscription Name]
    ```

    Hint:  
    有些人的帳戶下會有多個不同的訂閱，必須要事先確認好`Azure VM`所在的訂閱才能繼續。

3. 對已建好的VM安裝`AAD`擴充套件
    ```powershell
    az vm extension set --publisher Microsoft.Azure.ActiveDirectory --name AADLoginForWindows -g [Resource Group Name] --vm-name [Your VM Name]
    ```

    Hint:  
    publisher: `Microsoft.Azure.ActiveDirectory`及Name: `AADLoginForWindows`這兩個是固定名稱，請勿更改。

4. 使用特定`AAD account`及VM、建立`AAD role`對應的帳號及作用的VM。
    ```powershell
    $username="[Your AAD account]"
    $vm=$(az vm show -g [Resource Group Name] --name [Your VM Name] --query id -o tsv)
    az role assignment create --role "Virtual Machine Administrator Login" --assignee $username --scope $vm
    az role assignment create --role "Virtual Machine User Login" --assignee $username --scope $vm
    ```

    Hint:  
    兩個Role: `Virtual Machine Administrator Login`及`Virtual Machine User Login`其實只要賦予其中一種就可以，如果需要進行一些管理者權限操作的話，還是建議使用Administrator role比較不會有問題。

5. 接著我們使用建立`Azure VM`時就先建好的Local Admin帳號遠端登入，然後手動在VM加入`AAD`的帳號。
    ```powershell
    net localgroup "Remote Desktop Users" /add "AzureAD\[Your AAD account]"
    ```
    Hint:  
    localgroup: `Remote Desktop Users`及帳號前綴`AzureAD`這兩個是固定名稱，請勿更改。

6. 再來進入VM的遠端設定、關閉`NLA`。  
This PC -> Properties -> Advanced system settings -> Remote -> 取消勾選Network Level Authentication  
![](/Teamdoc/image/azure/Azure_VMLogInWithAAD/remote_1.png)

7. 下載`Azure VM`的.rdp檔並用編輯器開啟、接著加入下面兩行並存檔，如下圖。
    ```text
    enablecredsspsupport:i:0
    authentication level:i:2
    ```  
![](/Teamdoc/image/azure/Azure_VMLogInWithAAD/remote_2.png)

  Hint:  
  - `enablecredsspsupport:i:0`是關閉CredSSP support，CredSSP是一種用在RDP的加密通訊協定、Client & Server兩端都需要支援，如果有問題會導致RDP直接報錯，我們就無法另行輸入`AAD`帳號登入。
  - `authentication level:i:2`則是當驗證失敗時，會跳出Warning讓我們自行決定繼續連線或是結束。
  - `請注意，如果自行使用mstsc.exe連線，因為沒有上述兩項設定、只會直接回應登入失敗`，如果還是希望使用mstsc.exe連線、仍然需要另存.rdp檔並改設定後才行，詳細可參考[[Remote Desktop Connection 6.0 prompts you for credentials before you establish a remote desktop connection]](https://docs.microsoft.com/en-us/troubleshoot/windows-server/remote/remote-desktop-connection-6-prompts-credentials)
  - 執行下載的.rdp檔後，會出現如下圖的畫面，接著輸入`AAD`帳密登入即可，`請注意，登入帳號必須使用AzureAD\[Your AAD account]的格式`。
![](/Teamdoc/image/azure/Azure_VMLogInWithAAD/login_1.png)

8. 順利使用`AAD Account`登入`Azure VM`後，我們可以簡單用一般模式開啟`Powershell`查看當下的使用者名稱，如下圖。
![](/Teamdoc/image/azure/Azure_VMLogInWithAAD/login_2.png)

## Multi-factor Authentication (MFA) 
根據微軟文件的說明[[MS Doc]](https://docs.microsoft.com/en-us/azure/active-directory/devices/howto-vm-sign-in-azure-ad-windows#mfa-sign-in-method-required)，如果你的`AAD Account`有開啟`MFA`，會導致`Azure VM`登入失敗，經實測確實必須關閉`MFA`才能夠順利登入，這部分就看未來是否有新的調整。

## 結論:
通常我們需要操作`Azure VM`時，應該都會先有個`AAD account`，否則也無法登入`Azure Portal`管理雲端服務，但原本的`Azure VM`並沒有整合`AAD`的驗證、現在開放之後對於遠端帳號管理方便不少。不過目前`Azure VM`整合`AAD`還在Preview階段，正式GA的時間還沒有看到所以可能會有些小問題，包括手動改.rdp檔、進`Azure VM`加`AAD account`等步驟，未來還有變動的可能性，基本上還是希望有個統一設定的功能開放出來，而不是自行進入`Azure VM`跑指令。

## 參考
1. [[Login to Windows virtual machine in Azure using Azure Active Directory authentication]](https://docs.microsoft.com/en-us/azure/active-directory/devices/howto-vm-sign-in-azure-ad-windows)
2. [[Log in with RDP to Windows Azure VM using Azure AD]](https://charbelnemnom.com/log-in-with-rdp-to-a-windows-azure-vm-using-azure-ad/)
3. [[Log in with Remote Desktop to an Azure VM using Azure Active Directory]](https://www.youtube.com/watch?v=hbKNrNjQpUw)
4. [[Remote Desktop Connection 6.0 prompts you for credentials before you establish a remote desktop connection]](https://docs.microsoft.com/en-us/troubleshoot/windows-server/remote/remote-desktop-connection-6-prompts-credentials)