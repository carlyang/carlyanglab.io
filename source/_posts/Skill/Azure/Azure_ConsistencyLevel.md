---
title: Azure - Consistency Level of Cosmos DB
categories:
 - 技術文件
 - Azure
date: 2020/1/3
updated: 2020/1/3
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, Consistency Level, Cosmos DB]
---
作者: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Purpose
Azure Cosmos DB的Consistency Level(一致性層級)`主要在多個複本(Replicas)的NoSQL DB資料上，提供一套讀取的保證性規則、以保證讀取資料的一致性`，本文主要在說明五個一致性層級的概念。
<!-- more -->

## Introduction
在多個資料複本上讀取資料，可能因為地理因素、使用者在不同地點看到的資料會不一樣，這在很多跨域系統上可能會造成很大的問題，例如:訂機票。故`Azure Cosmos DB根據不同的應用情境，提供不同等級的一致性保證，一致性層級越低、效能越高，反之則效能越低、但能夠保證資料不會有不一致的狀況發生`。

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Azure Cosmos DB

## Consistency Level
Azure Cosmos DB提供五種一致性層級，以下簡要說明。
- Strong:  
強式一致性，能夠`確保所有複本的資料都是最新版本，使用者讀取的資料一定是所有複本資料都更新完成後才呈現`。換句話說，當有一份資料正在被寫入，必須等到所有複本資料都更新完畢，最新的資料才會被檢視。

- Bounded Staleness:  
限定過期，此層級`保證使用者讀取到的資料一定會依照寫入順序取得、且取得的資料是根據指定的K個版本內或一定期間T內的資料值`，也就是說此層級提供具有循序寫入且在最近的幾版或幾秒鐘內的資料。

- Session:  
工作階段，此層級`保證在相同工作階段內看到的資料都是最新的`，但不保證對於同份資料、其他使用者的更新也能同步讀取。同時也遵循一致性前置詞的規則、一定依照更新順序取得資料、而最終的更新也能夠在所有複本更新完成後取得。

- Consistent Prefix:  
一致性前置詞，此層級`保證所有讀取的資料都會按照寫入順序取得`，不會發生這時間點讀取到3號版本資料、下一個時間點卻讀取到1號版本資料的狀況，當所有複本都更新完成後，仍然可以取得最新版本的資料。

- Eventual:  
最終版本，此層級`保證在所有資料都寫入完成、且沒有其他更新後，資料可以被讀取`，換句話說，它`不保證依照一致性前置詞的順序、也不保整工作階段內的最新資料、更沒有保證資料會在一定數目的K版本內或時間T內`，此層級僅保證最後的寫入資料可以被讀取到、同時也是效率最快的層級。

## 結論:
透過一致性層級的設定，Azure Cosmos DB可以在全球的複本內依照規則進行寫入及更新，使用者也可以依照不同情境使用資料，例如:訂機票就必須要有強式一致性，否則同一張票可能會被多個人訂走。但如Facebook的文章更新，就算一個使用者看到舊的版本也沒關係，最終都會更新到最新的版本，這時候就適用Eventual規則。

## 參考
1. [Consistency levels in Azure Cosmos DB](https://docs.microsoft.com/en-us/azure/cosmos-db/consistency-levels)