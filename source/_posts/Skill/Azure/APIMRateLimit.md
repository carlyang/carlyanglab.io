---
title: Azure - Rate Limit in APIM
categories:
 - 技術文件
 - Azure
date: 2021/10/10
updated: 2021/10/10
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management, Rate Limit]
---
Author: Carl Yang

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
為了安全性的問題，很多時候我們會希望API呼叫能夠維持在正常的頻率下運作，防止像`DDoS`攻擊造成服務癱瘓，尤其是在微服務的架構下，API服務可能會又多又複雜，故集中在`API Management(以下簡稱APIM)`進行`Gateway`管控後，我們就可以利用`Gateway`的特性進行一些安全性的預處理，本文主要在介紹`APIM`中的幾種存取控制原則。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- API Management

## Introduction
當我們把眾多的API服務集中在`APIM`進行`Gateway`管控後，想要利用`Gateway`的特性進行一些安全性的預處理時，可以參考`APIM`的`Access Restriction Policies`，微軟文件說明有幾種設計原則如下。

>- Check HTTP header - Enforces existence and/or value of a HTTP header.
>- Limit call rate by subscription - Prevents API usage spikes by limiting call rate, on a per subscription basis.
>- Limit call rate by key - Prevents API usage spikes by limiting call rate, on a per key basis.
>- Restrict caller IPs - Filters (allows/denies) calls from specific IP addresses and/or address ranges.
>- Set usage quota by subscription - Allows you to enforce a renewable or lifetime call volume and/or bandwidth quota, on a per subscription basis.
>- Set usage quota by key - Allows you to enforce a renewable or lifetime call volume and/or bandwidth quota, on a per key basis.
>- Validate JWT - Enforces existence and validity of a JWT extracted from either a specified HTTP Header or a specified query parameter.
>- Validate client certificate - Enforces that a certificate presented by a client to an API Management instance matches specified validation rules and claims.

事實上並非每一種在實務情境下都會用到，所以本文主要介紹`Limit call rate by subscription`及`Limit call rate by key`兩種一般性需求的存取限制原則，如果有更進階的情境需要用到的話，可以參考[[微軟文件]](https://docs.microsoft.com/en-us/azure/api-management/api-management-access-restriction-policies)。  

題外話:  
在之前的研討會中有朋友提出疑問，在安全性的考量下使用`Access Restriction Policies`將不正常頻率的呼叫阻擋在`APIM`這關，只是把原本過多的`Request`改為擋在前一關的`APIM`，對於減少異常呼叫的次數沒有太大的幫助。  

其實這說法並沒有錯，確實無法減少異常呼叫的次數，試想想一堆跳板同時頻繁密集地攻擊，我們如何能去減少這樣的攻擊呢?  

不過，`雖然我們無法減少異常呼叫次數，卻可以避免大量的呼叫造成後端API的癱瘓，尤其是後端API可能需要存取DB、讀寫Blob檔案等I/O操作`，在`APIM`事先擋住就能夠避免這些狀況，所以`避免服務被癱瘓`才是我們的真正目的、而非減少異常呼叫次數，而且微軟針對`APIM`服務有特別調教過、也有不同等級的`SLA`保證(如下圖)。而`On-demands`正是雲端服務的精隨之一，我們可以依照個別需求動態調整服務等級，甚至有些特殊情況是可以另外跟微軟談談看的。

![](/Teamdoc/image/azure/APIMRateLimit/apim_tiers1.png)

## Limit call rate by subscription
在之前的文章[[Azure - How to use subscription to limit to access APIM]](/2021/09/15/Skill/Azure/APIMSubscription/)有提到`Subscription`的進階應用、其中之一就是可以用在`Access Restriction Policies`中，我們可以限制某個`Subscription Key`在一段期間內的存取次數，例如:1分鐘內只能呼叫15次。

我們可以在設計工具中直接編輯`Inbound`的`XML`設定，加入下列標記。
```xml
<policies>
    <inbound>
        <base />
        <set-backend-service base-url="https://xxxxxxxx.azurewebsites.net" />
        <rewrite-uri template="/api/login" />
        <rate-limit calls="15" renewal-period="60" remaining-calls-variable-name="remainingCallsPerSubscription" />
    </inbound>
    <backend>
        <base />
    </backend>
    <outbound>
        <base />
    </outbound>
    <on-error>
        <base />
    </on-error>
</policies>
```

Note:  
`目前為止還沒有看到可以用視覺化設計工具設定Subscription的Rate Limit`，所以必須進入`Editor`直接撰寫`XML`標記。
- `calls`: 指的是允許的呼叫次數。
- `renewal-period`: 指的是重新累計的時間(秒數)。
- `remaining-calls-variable-name`: 這裡是告訴`APIM`我們新增一個變數來計算剩餘的可用次數，每次`Subscription`呼叫這裡就會少1、直到重新累計時間到後就會重設為初始次數15。

Note:  
這個`by subscription`在一個`Policy document(也就是同一份XML文件中)`中只能出現一次，引用微軟文件如下。
> This policy can be used only once per policy document.

完整的可用`XML`標記範例如下:  
```xml
<rate-limit calls="number" renewal-period="seconds">
    <api name="API name" id="API id" calls="number" renewal-period="seconds">
        <operation name="operation name" id="operation id" calls="number" renewal-period="seconds" 
        retry-after-header-name="header name" 
        retry-after-variable-name="policy expression variable name"
        remaining-calls-header-name="header name"  
        remaining-calls-variable-name="policy expression variable name"
        total-calls-header-name="header name"/>
    </api>
</rate-limit>
```

## Limit call rate by key
這裡的`Key`跟`Subscription Key`不同、它是指我們自訂的特定統計值，例如:我們可以指定某個IP的呼叫頻率，方法如下。

方法1: 我們可以透過設計工具直接在`Inbound`中加上`Rate Limit Policy`。  
- `Number of calls`: 允許的次數。
- `Renewal period (in seconds)`: 重新計算的秒數。
- `Counter Key`: 選擇`IP address`。
- `Increment condition`: 要用來計算累計的條件，這裡我們自訂(`Custom`)計算HTTP 200的次數(`@(context.Response.StatusCode == 200)`)。

![](/Teamdoc/image/azure/APIMRateLimit/apim_ratelimit_key1.png)

方法2: 我們可以在設計工具中直接編輯`Inbound`的`XML`設定，加入下列標記。
- `calls`: 指的是允許的呼叫次數。
- `renewal-period`: 指的是重新累計的時間(秒數)。
- `counter-key`: 是我們要用來計算的統計值。
- `increment-condition`: 我們設定用來累計的條件。
- `remaining-calls-variable-name`: 這裡是告訴`APIM`我們新增一個變數來計算剩餘的可用次數，每次呼叫`remainingCallsPerIP`就會少1、直到重新累計時間到後就會重設為初始次數15。

```xml
<policies>
    <inbound>
        <base />
        <set-backend-service base-url="https://xxxxxxxx.azurewebsites.net" />
        <rewrite-uri template="/api/crypto/encrypt" />
        <rate-limit-by-key calls="15" renewal-period="60" counter-key="@(context.Request.IpAddress)" increment-condition="@(context.Response.StatusCode == 200)" remaining-calls-variable-name="remainingCallsPerIP" />
    </inbound>
    <backend>
        <base />
    </backend>
    <outbound>
        <base />
    </outbound>
    <on-error>
        <base />
    </on-error>
</policies>
```

Note:  
這個功能不能使用在`Consumption tier`喔!因為`Consumption tier`基本上是拿來開發階段使用的，`APIM`必須開在其他的層級才能使用這個功能。
> This feature is unavailable in the Consumption tier of API Management.

完整的可用`XML`標記範例如下:  
```xml
<rate-limit-by-key calls="number"
                   renewal-period="seconds"
                   increment-condition="condition"
                   counter-key="key value" 
                   retry-after-header-name="header name" retry-after-variable-name="policy expression variable name"
                   remaining-calls-header-name="header name"  remaining-calls-variable-name="policy expression variable name"
                   total-calls-header-name="header name"/>
```

## 結論:
本文只介紹了`by subscriptioni`及`by key`兩種`Rate Limit`方式，也是大部分情況下都可以用的`Policy`，不過其它還有更多種的功能如`Set usage quota by subscription`及`Set usage quota by key`可用，礙於篇幅就留待下篇再來說明了，謝謝大家。

## 參考
1. [[API Management access restriction policies]](https://docs.microsoft.com/en-us/azure/api-management/api-management-access-restriction-policies)
2. [[Explore pricing options]](https://azure.microsoft.com/en-us/pricing/details/api-management/#pricing)
3. [[API Management policy expressions]](https://docs.microsoft.com/en-us/azure/api-management/api-management-policy-expressions)