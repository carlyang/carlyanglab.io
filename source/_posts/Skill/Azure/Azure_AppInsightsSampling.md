---
title: Azure - Application Insights Sampling
categories:
 - 技術文件
 - Azure
date: 2020/1/4
updated: 2020/1/4
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, Application Insights, Sampling]
---
Author: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Purpose
透過傳送遙測資料到Azure上的Application Insights(以下簡稱AppInsights)，可以記錄我們應用系統(API、Web...etc)各式各樣的Log，可做為後續分析、監控等等的資料依據，`本篇文章主要說明三種取樣方式`。
<!-- more -->

## Introduction
所有的雲端服務都有其額度限制，例如網路流量、資料大小、Request Unit...等，AppInsights也不例外。當然，我們我建立服務時也可以指定用多少付多少的Consumption模式，但這種方式往往在發現用量飆高後才回頭調整設定，這時候的成本已經付出。  
讓我們回到AppInsights的部分，由於它做為非常有效的雲端平台監控機制，必須`仰賴從Application端送上來的遙測資料、進行整理及分析才能發揮作用，但這種遙測資料並不一定需要高度頻繁地發送，不但影響網路流量、也增加不必要的AppInsights費用`，所以[如何取樣(Sampling)]這件事就變得非常重要了。

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Create Applicatioin Insights Service on Azure

## Sampling
AppInsights中的取樣指的是`從應用程式端取得遙測資料這個動作`，而這個取樣動作中的`取樣頻率`、`發送時間間隔`、`統計值的比例`等設定就會影響到每月的服務使用額度了。

## Sampling Types of Application Insights
目前有三種AppInsghts的取樣方式，說明如下:
- Adaptive Sampling:  
稱為調適性取樣，這種取樣是設定在應用程式端的SDK上、也是預設的取樣模式，它可以`自動調整遙測資料大小，目前僅支援ASP.Net、ASP.Net Core等server-side的遙測`。

- Fixed-rate Sampling:  
固定速率取樣，此種方式`可根據自訂的速度進行取樣，可以讓遙測資料的取樣速度依照自訂的設定進行`，也是屬於應用程式端的取樣類型。

- Ingestion Sampling:  
內嵌取樣，這種方式是`在Azure Portal上設定，能夠依照我們在服務端設定的速率進行取樣，但它不能減少由應用程式端而來的遙測流量，僅能依照設定自動捨棄不需要的遙測資料，所以這種方式可以降低服務的使用額度、不至於超過每月配額`。

Note:  
請特別注意，`當Adaptive Sampling或Fixed-rate Sampling啟用時，服務端的Ingestion Sampling將會自動失效。`

## Configuration
.Net AppInsights SDK的組態設定位於ApplicationInsights.config中，詳細的屬性設定可查閱[[MS Doc]](https://docs.microsoft.com/en-us/azure/azure-monitor/app/sampling#adaptive-sampling-in-your-aspnetaspnet-core-web-applications)，主要就是一些XML標記法表示的屬性設定，設定完成後即會生效。

## 結論:
取樣設定關係到服務額度、也就是每月的費用，所以也是很重要的一環。如果對於監控沒有那種需要非常頻繁的資料情境，就可以靠這些設定進行調整，也能夠減少不必要的開支。

## 參考
1. [Sampling in Application Insights](https://docs.microsoft.com/en-us/azure/azure-monitor/app/sampling)