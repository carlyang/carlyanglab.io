---
title: Should I make HttpClient reusable?
categories:
 - 技術文件
 - WebAPI
date: 2018/8/28
updated: 2018/8/28
thumbnail: /Teamdoc/image/WebAPI/Thumb_HttpClientReusable.svg
layout: post
tags: 
 - HttpClient
---
作者: Carl Yang

## HttpClient in .NET
![](/Teamdoc/image/WebAPI/Thumb_HttpClientReusable.svg)
<center>([圖片來源](https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/Internet1.svg/1024px-Internet1.svg.png))</center>
當前RESTful API大行其道，具有跨平台、一致性等優點，且系統很常需要調用後端或第三方API以取得相關資訊。而.NET中目前最常用的HTTP元件就屬HttpWebRequest及HttpClient，但本文並非要說明兩元件的使用方式，而是<font style="color:red;">著重在.NET Framework 4.5以後新HttpClient元件的使用策略上</font>。
<!-- more -->

引用微軟對於HttpClient的簡要說明如下:
>Provides a base class for sending HTTP requests and receiving HTTP responses from a resource identified by a URI.
>([_Microsoft_](https://docs.microsoft.com/en-us/dotnet/api/system.net.http.httpclient?view=netframework-4.7.2s))

故HttpClient是follow RESTful Style的Client端處理http要求的元件，那他有什麼優點或特性呢?

1. Asyncronous:
	- 因為大部分都是非同步的方法，故可以不鎖住main thread、不需要收到resposne後才繼續執行(視有無await的情況而定)。
	- 他提供了很多的非同步處理方法供開發者使用。
2. Thread Safe:
	- 根據微軟的說明，他是具有執行緒安全的元件?!(個人認為不一定)。
3. Performance:
	- HttpClient具有較HttpWebRequest更好的執行效能(視使用方式而定)。

接下來就根據上述三個方面做說明。



### 非同步方法
HttpClient提供了像GetAsync()、PostAsync()、PutAsync()、DeleteAsync()等非同步方法，開發者可以容易地依照RESTful Style的method選擇使用對應的方法，且因為是非同步的方式、不需等待Services回應即可繼續執行，這特性常被利用在長時間的處理操作中，可以不卡住使用者的UI操作、讓使用者有更佳的使用體驗，但如果使用await或Task.Result，則會等待Services回應後才繼續執行。

範例如下:
```csharp
HttpClient httpClient = new HttpClient();

for (int i = 0; i < 10; i++)
{
	var response = await httpClient.GetAsync(@"http://www.google.com.tw/");

	Console.WriteLine($"For 1: No.{(i + 1)} time - {response.StatusCode}");
}

for (int i = 0; i < 10; i++)
{
	var response = httpClient.GetAsync(@"http://www.google.com.tw/").Result;

	Console.WriteLine($"For 2: No.{(i + 1)} time - {response.StatusCode}");
}
```
執行結果如下圖，每個迴圈分別打10次並依照順序Print到Screen上，而因為有await或Task.Result，每次Request都會等到Response後才繼續往下執行，也就是說會卡住Main Thread的流程。
![](/Teamdoc/image/WebAPI/HttpClient1.png)

接下來我們將code改為不等待回應的真正非同步模式。
```csharp
HttpClient httpClient = new HttpClient();

for (int i = 0; i < 10; i++)
{
	httpClient.GetAsync(@"http://www.google.com.tw/").ContinueWith(res =>
	{
		Console.WriteLine($" For 1: No.{(i + 1)} time - {res.Result.StatusCode}");
	}, TaskContinuationOptions.None);
}

Console.WriteLine(string.Empty);

for (int i = 0; i < 10; i++)
{
	httpClient.GetAsync(@"http://www.google.com.tw/").ContinueWith(res =>
	{
		Console.WriteLine($" For 2: No.{(i + 1)} time - {res.Result.StatusCode}");
	}, TaskContinuationOptions.None);
}
```
如果不等待回應，則由下圖結果可發現Response的結果並不會依照順序一個個執行。
![](/Teamdoc/image/WebAPI/HttpClient2.png)

綜合以上的結果，雖然HttpClient的方法大部分都提估了非同步的方式，但是使用上仍然要判斷什麼動作應該要等待、什麼動作不應該等待，例如，打API是為了獲取必要資料以進行下一步動作時，就應該要等待，反之，如果是可以只有背景工作的動作、就可以不使用await或Task.Result繼續往下執行。
<font style="color:red;">故與其說HttpClient提供許多非同步的方法，不如說HttpClient其實是提供了更大的同步/非同步使用彈性，讓開發者能夠撰寫具有更佳使用者體驗的AP。</font>

#### Thread Safe
這裡的Thread Safe指的是在多執行緒的環境下使用HttpClient不會造成異常的Exception，因為HttpClient會自行管理每一個送出的Request及Response，當多個Request在不同的Thread中被調用並送出後，HttpClient會將Request放入自己的Request Pool並逐一執行。
接下來我們利用Task.Run在迴圈中跑多個Thread、每個Thread都送一次Request，並加入trye...catch看看是否有Exception發生。
```csharp
try
{
	HttpClient httpClient = new HttpClient();

	for (int i = 0; i < 10; i++)
	{
		Task.Run(() => {
			httpClient.GetAsync(@"http://www.google.com.tw/").ContinueWith(res =>
			{
				Console.WriteLine($" For 1: No.{(i + 1)} time - {res.Result.StatusCode}");
			}, TaskContinuationOptions.None);
		});
	}

	Console.WriteLine(string.Empty);

	for (int i = 0; i < 10; i++)
	{
		Task.Run(() => {
			httpClient.GetAsync(@"http://www.google.com.tw/").ContinueWith(res =>
			{
				Console.WriteLine($" For 2: No.{(i + 1)} time - {res.Result.StatusCode}");
			}, TaskContinuationOptions.None);
		});
	}
}
catch (Exception e)
{
	Console.WriteLine(e);
}
```
由下圖結果得知，我們用Task.Run在多執行緒的情況下使用非同步方法並多次調用，並不會有Exception發生，說明HttpClient具有Thread Safe的特性。
![](/Teamdoc/image/WebAPI/HttpClient3.png)

但是在實務上中，大部分使用多執行緒、都是為了完成多個長時間工作，每個工作可能有不同的Request API，而不同的API可能需要不一樣的HTTP設定，例如，外網API需要設定Proxy、內網API不需要，有些API需要使用Authorization Header以做驗證等等，這樣的情況下不太可能都使用相同的設定去對API提Request，以下我們以Proxy設定作範例，測試在非同步且多執行緒的狀況。
```csharp
try
{
	HttpClient httpClient = new HttpClient();

	for (int i = 0; i < 10; i++)
	{
		Task.Run(() => {
			httpClient.GetAsync(@"http://www.google.com.tw/").ContinueWith(res =>
			{
				Console.WriteLine($" For 1: No.{(i + 1)} time - {res.Result.StatusCode}");
			}, TaskContinuationOptions.None);
		});
	}

	Console.WriteLine(string.Empty);

	for (int i = 0; i < 10; i++)
	{
		Task.Run(() => {
			var handler = new HttpClientHandler
			{
				Proxy = new WebProxy(@"http://172.21.10.18:80"),
				UseProxy = true
			};
			httpClient = new HttpClient(handler);

			httpClient.GetAsync(@"http://www.google.com.tw/").ContinueWith(res =>
			{
				Console.WriteLine($" For 2: No.{(i + 1)} time - {res.Result.StatusCode}");
			}, TaskContinuationOptions.None);
		});
	}

	Console.WriteLine(string.Empty);

	for (int i = 0; i < 10; i++)
	{
		Task.Run(() => {
			httpClient.GetAsync(@"http://www.google.com.tw/").ContinueWith(res =>
			{
				Console.WriteLine($" For 3: No.{(i + 1)} time - {res.Result.StatusCode}");
			}, TaskContinuationOptions.None);
		});
	}
}
catch (Exception e)
{
	Console.WriteLine(e);
}
```
由下圖可以發現，當我們以非同步方式跑多執行緒時，迴圈2的HttpClient被設定了proxy，導致執行結果只有後面迴圈2、迴圈3的可以成功、迴圈1的完全沒有動靜，這是因為迴圈1的Thread並沒有設定Proxy所以Request打不出去、但到了迴圈2時有設定Proxy就成功打出去了。
這下麻煩了，雖然HttpClient是Thread Safe的，他不會因為被用在多執行緒中而造成錯誤，但是他的相關設定卻可能會被其他Thread改掉。
![](/Teamdoc/image/WebAPI/HttpClient4.png)

由以上的實驗可以知道，<font style="color:red;">**多執行緒環境下共用HttpClient是沒有問題的，但是設定卻有可能因為現實情況中被不同的Thread改掉，一旦改掉後就可能影響下一個進來的Request，因此HttpClient並非完全Thread Safe的**</font>，需要仔細評估何時應該new新的instance、何時應該重覆使用相同的instance。
Sharad Cornejo Altuzar在[_微軟官方文件_](https://blogs.msdn.microsoft.com/shacorn/2016/10/21/best-practices-for-using-httpclient-on-services/)有些使用上的不錯建議，其中針對共用的部分如下。
>Do NOT recreate HTTPClient for each request. Reuse Httpclient as much as possible

意思是說，我們應該盡可能地重複使用相同的HttpClient instance、而非每個request都new一個來處理。但他也提到如下:

>Making it static is fine. This might not always be possible because you might need different properties but you should try to do it for related requests.

也就是說，<font style="color:red;">設定Static來共用是OK的，但根據不同的Properties則應該要適當地啟用新的instance，避免多執行緒的請況下、發生設定被改掉的種種問題</font>。

### 執行效能
在執行效能上，微軟宣稱HttpClient有較HttpWebRequest更佳的效能，讓我們來做個範例比較舊HttpWebRequest與新HttpClient的差異，並將執行時間Print到Screen看看各打10次的變化。
```csharp
Console.WriteLine(@"    HttpWebRequest:");
for (int i = 0; i < 10; i++)
{
	var request = (HttpWebRequest)WebRequest.Create(@"http://www.google.com.tw/");
	request.Method = @"GET";

	DateTime start1 = DateTime.Now;
	using (var response = (HttpWebResponse)request.GetResponse())
	{
		var iStatusCode = (int)response.StatusCode;

		Console.WriteLine($"        For 1: No.{(i + 1)} time - {iStatusCode}, Cost - {(DateTime.Now - start1).TotalMilliseconds / 1000}/s");
	}
}

Console.WriteLine(string.Empty);

Console.WriteLine(@"    HttpClient:");

for (int i = 0; i < 10; i++)
{
	var httpClient = new HttpClient();

	DateTime start2 = DateTime.Now;
	var res = await httpClient.GetAsync(@"http://www.google.com.tw/");

	Console.WriteLine($"        For 2: No.{(i + 1)} time - {(int)res.StatusCode}, Cost - {(DateTime.Now - start2).TotalMilliseconds / 1000}/s");
}
```
執行結果:
![](/Teamdoc/image/WebAPI/HttpClient5-1.png)
![](/Teamdoc/image/WebAPI/HttpClient5-2.png)
由上圖可知，各打10次的結果，舊HttpWebRequest平均約花費0.44秒、新HttpClient平均約花費0.20秒，顯示的確HttpClient的效能較佳，但新舊差異僅平均約0.24秒，如果在沒有效能的極致追求下，此差異可能就感覺沒有那麼明顯，但如果在某些特定情況下必須非常講求效能的話，使用HttpClient可能會較適合。

除了比較新舊兩種元件的效能外，現在我們再來看看HttpClient本身在效能上該如何使用才會有好的表現。程式碼第一個迴圈是每次都new一個新的instance來處理Request、第二個迴圈則是用相同instance來處理Request。
```csharp
double dSum1 = 0.0;
for (int i = 0; i < 10; i++)
{
	var request = new HttpClient();
	DateTime start1 = DateTime.Now;
	var res = await request.GetAsync(@"http://www.google.com.tw/");

	dSum1 += (DateTime.Now - start1).TotalMilliseconds / 1000;
	Console.WriteLine($"        For 1: No.{(i + 1)} time - {(int)res.StatusCode}, Cost - {(DateTime.Now - start1).TotalMilliseconds / 1000}/s");
}
Console.WriteLine($"    Avg: {dSum1 / 10}/s");
```
```csharp
double dSum2 = 0.0;
var httpClient2 = new HttpClient();
for (int i = 0; i < 10; i++)
{
	DateTime start2 = DateTime.Now;
	var res = await httpClient2.GetAsync(@"http://www.google.com.tw/");

	dSum2 += (DateTime.Now - start2).TotalMilliseconds / 1000;
	Console.WriteLine($"        For 2: No.{(i + 1)} time - {(int)res.StatusCode}, Cost - {(DateTime.Now - start2).TotalMilliseconds / 1000}/s");
}
Console.WriteLine($"    Avg: {dSum2 / 10}/s");
```
由下兩張圖的結果可知，是否共用instance對於效能的差異不大，不管有沒有共用都平均約花費0.39秒。
<center>_無共用instance:_</center>
![](/Teamdoc/image/WebAPI/HttpClient6.png)
<center>_共用instance:_</center>
![](/Teamdoc/image/WebAPI/HttpClient7.png)

<font style="color:red;">
Note:
**另外，由上圖也可知道另一項重要資訊，HttpClient在初始化的時候需要較長的時間，初始化約需要2.97秒，但初始化後重複送Request卻僅需0.07~0.15秒，故共用相同的Instance才有較佳的效能、每次Request就初始化新的instance並不會有更好的效能表現。**
</font>

<font style="color:red;">**But!!**</font>
再來我們用netstat -n來看看連線可以發現，無共用時會啟用10條TCP連線、有共用時僅會啟用1條TCP連線，我想大家應該都知道，HTTP連線數是有上限的、且每條連線占用一個port，如果系統短時間內發送大量請求的話，連線很可能會滿載，即便加大連線上限、也可能會耗盡系統記憶體，造成效能緩慢等問題，<font style="color:red;">**故在使用上還是建議能共用就盡量共用，基本原則是考慮打相同的URL都應該共用instance、不同的URL因為可能有不同的Properties才使用不同的instance處理HTTP請求**</font>，或者也可以另外自定義Group等方式，同個Group使用相同instance以減少連線數。
<center>_無共用instance:_</center>
![](/Teamdoc/image/WebAPI/HttpClient8.png)
<center>_共用instance:_</center>
![](/Teamdoc/image/WebAPI/HttpClient9.png)

## HTTP連線
在HttpClient的連線上，也建議如果能夠共用就盡量共用，而共用同一條連線的方法、除了使用相同instance外，也可以利用Keep-Alive表頭來保持連線狀態，如此HttpClient在收到Request後就會自動判斷使用相同的連線。但是在一些特定的網路環境中，例如在Load Balance後面的機器，就不適合常保持連線開啟的狀態。
針對HttpClient要保持連線開啟狀態，可以使用如下的ConnectionClose屬性、設為false表示不關閉連線，也就是在傳送Request時、會加入Keep-Alive表頭告訴API Server要保持連線。
```csharp
var httpClient = new HttpClient();
httpClient.DefaultRequestHeaders.ConnectionClose = false;
```

## Pre-heating
或稱預熱，因為HttpClient在初始化連線時會需要較久的時間，如下圖的第一筆Request需耗時2.97秒，故可以加入預熱機制、在一開始初始化的時候先打某個肯定活著的URL以將HttpClient熱身一下，以避免第一次的緩慢。
![](/Teamdoc/image/WebAPI/HttpClient7.png)
```csharp
var httpClient = new HttpClient();
//Preheating
httpClient.GetAsync(@"http://localhost/");
```
如下圖，預熱後第一次的Request就不需要那麼久的初始化時間。
![](/Teamdoc/image/WebAPI/HttpClient10.png)

## 結論
綜合上列所述，使用HttpClient基本上需要考量幾個條件:
1. 依照需求使用非同步(使用await或Task.Result為同步、反之為非同步)方法。
2. 多執行緒環境下，使用HttpClient需注意Properties會被修改的問題，如果有、則應該適時地使用不同instance處理Request。
3. 盡量Connection(是否Keep-Alive)及instance能共用就共用，除非不得已的情況才使用另外的instance，可提高效能、降低連線數、降低記憶體使用量。
