---
title: Azure Table Storage - Partition Limitation
categories:
 - 技術文件
 - Azure
date: 2019/08/29
updated: 2019/08/29
thumbnail: https://i.imgur.com/GhND5bU.png
layout: post
tags: [Azure, Azure Table Storage]
---
作者: Carl Yang

![](/Teamdoc/image/azure/logos/table_storage.png)

## Azure Table Storage
作為Azure NoSQL的結構化資料儲存體服務，Table Storage被歸類於Azure Storage Account內的一種資料儲存方式，我們可以把有固定欄位的相關資料存進去、以供未來查詢之用。
<!-- more -->

## 概念
Table Storage是用來儲存結構化、非關聯式(NoSQL)的資料，其中主要有三個概念，如下:  
- Table: 稱為資料表，他可視為一組實體的組合。其中儲存的實體可以具有多組不同的屬性(或稱欄位)。
- Entity: 也稱為實體，就是資料表內的一筆筆資料。
- Property: 屬性，為一組組Key/Value的配對，一個實體可以包含多組屬性。

## 系統欄位
Table Storage有三個內建屬性，每筆資料送進來都會具有這三個基本屬性值，簡述如下:

|屬性名稱|說明|
|---|---|
|PartitionKey|分割區名稱(Max 1 KB)|
|RowKey|資料識別值(Max 1 KB)|
|Timestamp|資料上傳時間|

- PartitionKey: 由於Table Storage的儲存方式具有分割區的概念，相同PartitionKey的資料、會被放在同一塊分割區中，且每個分割區的throughput高低都跟分割區的資料量有密切相關。
- RowKey: 同一個分割區內的實體都必須具有一個唯一識別的欄位值。
- Timestamp: 該筆實體的上傳時間。

## 限制
1. 建構子中的<font style="color:red;">PartitionKey</font>與<font style="color:red;">RowKey</font>是Azure Table Storage中的複合鍵(Compond Key)、也是唯一的索引表鍵。

    Note:  
    所以PartitionKey+ RowKey可以唯一識別Table中的某一筆實體。

2. <font style="color:red;">PartitionKey</font>與<font style="color:red;">RowKey</font>是由外部根據不同的資料特性、自行指定。

3. <font style="color:red;">PartitionKey</font>的長度最大可到1 KB。

4. <font style="color:red;">RowKey</font>的長度最大可到1 KB。

5. <font style="color:red;">PartitionKey</font>是用作資料分割，也就是實體檔案會依照不同的<font style="color:red;">PartitionKey</font>儲存，而<font style="color:red;">RowKey</font>則是每一筆實體的識別欄位，也就是相同分割中的每一筆實體的RowKey都必須不同。

6. 請務必注意，<font style="color:red;">每個Partition任一時刻只能提供一個Partition Server使用，但每個Partition Server能夠使用多個不同的Partition</font>，根據[[微軟文件]](https://docs.microsoft.com/en-us/rest/api/storageservices/designing-a-scalable-partitioning-strategy-for-azure-table-storage)的說明，引用如下:
>Partitions represent a collection of entities with the same PartitionKey values. Partitions are always served from one partition server and each partition server can serve one or more partitions.

5. 而每個Partition的最小吞吐量為<font style="color:red;">每秒500個實體</font>，而這個數字是最小的吞吐量，實際上要依當時的忙碌狀態而定、有可能可以更高。節錄[[微軟文件]](https://docs.microsoft.com/en-us/rest/api/storageservices/designing-a-scalable-partitioning-strategy-for-azure-table-storage)的說明，引用如下:
>Specifically, a partition has a scalability target of 500 entities per second. This throughput may be higher during minimal load on the storage node, but it will be throttled down when the node becomes hot or very active.

## 範例
- Interface
```csharp
public interface ITableService<T> where T : TableEntity, new()
{
    Task InsertAsync(T entity);
    Task InsertOrReplaceAsync(T entity);
    Task BatchInsertAsync(IEnumerable<T> entities);
    Task<T> GetEntityAsync(T entity);
    Task<IEnumerable<T>> GetAllAsync();
    IEnumerable<T> GetEntitiesByTime(DateTimeOffset startUtcTime, DateTimeOffset endUtcTime);
    Task DeleteAsync(T entity);
    Task BatchDeleteAsync(IEnumerable<T> entities);
    Task DeleteTableAsync();
    bool CheckTable(string tableName);
}
```

- Service
```csharp
public class TableService<T> : ITableService<T> where T : TableEntity, new()
{
    private readonly TableConfigInfo _cloudTableConfigInfo;
    private readonly CloudStorageAccount _cloudStorageAccount;
    private CloudTableClient _cloudTableClient;
    private readonly CloudTable _cloudTable;

    public TableService(TableConfigInfo tableConfig)
    {
        _cloudTableConfigInfo = tableConfig;

        CloudStorageAccount.TryParse(_cloudTableConfigInfo.AzureStorageConnectionString, out _cloudStorageAccount);

        _cloudTableConfigInfo = tableConfig;
        _cloudTable = GetTableAsync(_cloudTableConfigInfo.TableName).Result;
    }

    /// <summary>
    /// Generate table client
    /// </summary>
    /// <returns></returns>
    private CloudTableClient GetCloudTableClient()
    {
        //If no proxy setting, don't use custom DelegatingHandler.
        if (!string.IsNullOrEmpty(_cloudTableConfigInfo.ProxyUrl))
        {
            var delegatingHandler = new TableProxyHandler(new WebProxy(_cloudTableConfigInfo.ProxyUrl, true));

            _cloudTableClient = _cloudStorageAccount.CreateCloudTableClient(new TableClientConfiguration
            {
                RestExecutorConfiguration = new RestExecutorConfiguration
                {
                    DelegatingHandler = delegatingHandler
                }
            });
        }
        else
        {
            _cloudTableClient = _cloudStorageAccount.CreateCloudTableClient();
        }

        return _cloudTableClient;
    }

    /// <summary>
    /// Get table reference.
    /// </summary>
    /// <param name="tableName"></param>
    /// <returns></returns>
    private async Task<CloudTable> GetTableAsync(string tableName)
    {
        var tableClient = GetCloudTableClient();

        // Create a table client for interacting with the table service 
        var table = tableClient.GetTableReference(tableName);

        await table.CreateIfNotExistsAsync();

        return table;
    }

    /// <summary>
    /// Get table result from table storage
    /// </summary>
    /// <param name="entity"></param>
    /// <returns></returns>
    private async Task<TableResult> GetTableResult(T entity)
    {
        var retrieveOperation = TableOperation.Retrieve<T>(entity.PartitionKey, entity.RowKey);
        var result = await _cloudTable.ExecuteAsync(retrieveOperation);

        return result;
    }

    /// <summary>
    /// Insert single entity into table.
    /// </summary>
    /// <param name="entity"></param>
    /// <returns></returns>
    public async Task InsertAsync(T entity)
    {
        // Create the InsertOrReplace table operation
        var operation = TableOperation.Insert(entity);

        // Execute the operation.
        await _cloudTable.ExecuteAsync(operation);
    }

    /// <summary>
    /// If entity key exists, replace it. Otherwise, insert it.
    /// </summary>
    /// <param name="entity"></param>
    /// <returns></returns>
    public async Task InsertOrReplaceAsync(T entity)
    {
        var tableResult = await GetTableResult(entity);
        var replaceEntity = (T)tableResult.Result;

        if (null != replaceEntity)
        {
            //Replace entity
            replaceEntity = entity;
            var insertOrReplaceOperation = TableOperation.InsertOrReplace(replaceEntity);
            await _cloudTable.ExecuteAsync(insertOrReplaceOperation);
        }
        else
        {
            //Insert entity
            var insertOrReplaceOperation = TableOperation.InsertOrReplace(entity);
            await _cloudTable.ExecuteAsync(insertOrReplaceOperation);
        }
    }

    /// <summary>
    /// Batch insert entities per 1000 rows into table.
    /// </summary>
    /// <param name="entities"></param>
    /// <returns>If batch insertion failed since redundant data, it will add PartitionKey and RowKey into Exception.Data</returns>
    public async Task BatchInsertAsync(IEnumerable<T> entities)
    {
        var count = 0;
        var tableEntities = entities.ToList();
        var partitionKeys = tableEntities.Select(p => new { p.PartitionKey }).Distinct().ToList();

        //It can't have duplicate PartitionKey and RowKey in each batch insertion.
        foreach (var par in partitionKeys)
        {
            var batchOperation = new TableBatchOperation();

            foreach (var ety in tableEntities.Where(x => string.Equals(x.PartitionKey, par.PartitionKey)))
            {
                batchOperation.Insert(ety);

                count += 1;

                if (100 != count) continue;

                try
                {
                    await _cloudTable.ExecuteBatchAsync(batchOperation);
                }
                catch(StorageException e)
                {
                    if (e.RequestInformation.HttpStatusCode == 409 &&
                        e.RequestInformation.HttpStatusMessage.Contains("The specified entity already exists"))
                    {
                        var errors = e.RequestInformation.HttpStatusMessage.Split(':');
                        var index = Convert.ToInt32(2 <= errors.Length ? errors[0] : "0");

                        var partitionKey = batchOperation[index].Entity.PartitionKey;
                        var rowKey = batchOperation[index].Entity.RowKey;

                        e.Data.Add("PartitionKey", partitionKey);
                        e.Data.Add("RowKey", rowKey);
                    }

                    throw;
                }
                
                batchOperation.Clear();

                count = 0;
            }

            if (0 >= count) continue;

            try
            {
                //Insert the rest of entities
                await _cloudTable.ExecuteBatchAsync(batchOperation);
            }
            catch (StorageException e)
            {
                if (e.RequestInformation.HttpStatusCode == 409 &&
                    e.RequestInformation.HttpStatusMessage.Contains("The specified entity already exists"))
                {
                    var errors = e.RequestInformation.HttpStatusMessage.Split(':');
                    var index = Convert.ToInt32(2 <= errors.Length ? errors[0] : "0");

                    var partitionKey = batchOperation[index].Entity.PartitionKey;
                    var rowKey = batchOperation[index].Entity.RowKey;

                    e.Data.Add("PartitionKey", partitionKey);
                    e.Data.Add("RowKey", rowKey);
                }

                throw;
            }

            count = 0;
        }
    }

    /// <summary>
    /// Get single entity by partition key and row key.
    /// </summary>
    /// <param name="entity"></param>
    /// <returns></returns>
    public async Task<T> GetEntityAsync(T entity)
    {
        var tableResult = await GetTableResult(entity);
        var result = (T)tableResult.Result;

        return result;
    }

    /// <summary>
    /// Get all entities of table.
    /// </summary>
    /// <returns></returns>
    public async Task<IEnumerable<T>> GetAllAsync()
    {
        var list = new List<T>();
        TableContinuationToken token = null;
        var query = new TableQuery<T>();//.Where(
            //TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, _cloudTable.Name));

        do
        {
            var result = await _cloudTable.ExecuteQuerySegmentedAsync(query, token);
            token = result.ContinuationToken;
            list.AddRange(result.Results);
        } while (null != token);

        return list;
    }

    /// <summary>
    /// Get all entities by RowKey
    /// </summary>
    /// <param name="startUtcTime"></param>
    /// <param name="endUtcTime"></param>
    /// <returns></returns>
    public IEnumerable<T> GetEntitiesByTime(DateTimeOffset startUtcTime, DateTimeOffset endUtcTime)
    {
        var rangeQuery = new TableQuery<T>()
            .Where(
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterConditionForDate("Timestamp", QueryComparisons.GreaterThanOrEqual, startUtcTime),
                    TableOperators.And,
                    TableQuery.GenerateFilterConditionForDate("Timestamp", QueryComparisons.LessThanOrEqual, endUtcTime))
            );

        return _cloudTable.ExecuteQuery(rangeQuery);
    }

    /// <summary>
    /// Delete entity.
    /// </summary>
    /// <param name="entity"></param>
    /// <returns></returns>
    public async Task DeleteAsync(T entity)
    {
        var retrievedResult = await GetTableResult(entity);
        var deleteEntity = (T)retrievedResult.Result;
        if (deleteEntity != null)
        {
            var deleteOperation = TableOperation.Delete(deleteEntity);
            await _cloudTable.ExecuteAsync(deleteOperation);
        }
        else
        {
            throw new NullReferenceException("Cannot found entity");
        }
    }

    /// <summary>
    /// Batch delete entity.
    /// </summary>
    /// <param name="entities"></param>
    /// <returns></returns>
    public async Task BatchDeleteAsync(IEnumerable<T> entities)
    {
        var count = 0;
        var batchOperation = new TableBatchOperation();

        foreach (var ety in entities)
        {
            batchOperation.Delete(ety);

            count += 1;

            if (100 != count) continue;

            await _cloudTable.ExecuteBatchAsync(batchOperation);
            batchOperation.Clear();

            count = 0;
        }

        //Insert the rest of entities
        if (0 < count)
            await _cloudTable.ExecuteBatchAsync(batchOperation);
    }

    /// <summary>
    /// Delete table.
    /// Note:
    /// It can't create the same table after deleting a while.
    /// </summary>
    /// <returns></returns>
    public async Task DeleteTableAsync()
    {
        await _cloudTable.DeleteIfExistsAsync();
    }

    /// <summary>
    /// Check table exists or not.
    /// </summary>
    /// <param name="tableName"></param>
    /// <returns></returns>
    public bool CheckTable(string tableName)
    {
        var result =  _cloudTableClient
            .ListTables().Count(p => string.Equals(tableName, p.Name));

        return 0 < result;
    }
```

- 程式說明:  
  1. 使用Interface是為了統一方法、及方便外部調用，例如: 使用.Net Core時的DI Extensions。
  2. 為了<font style="color:red;">讓調用端自訂資料模型，並使用此Component將資料模型送進Azure Table Storage，這裡採用泛型設計</font>。
        ```csharp
        public class TableService<T> : ITableService<T> where T : TableEntity, new()
        ```

        上方範例使用泛型T作為Component - TableService的輸出類型，又<font style="color:red;">因為內部有很多與Azure SDK共用的實體屬性或方法</font>，故泛型條件約束必須繼承<font style="color:red;">TableEntity</font>並具有一個無參數的建構子。

        實際使用範例如下實體類別，我們可以自行指定<font style="color:red;">PartitionKey</font>與<font style="color:red;">RowKey</font>。  
        ```csharp
        public class TestEntityModel : TableEntity
        {
            public TestEntityModel()
            {
                PartitionKey = $"{DateTime.Now:yyyyMMddHH}";
                RowKey = Guid.NewGuid().ToString().Replace("-", "");
            }
            public string CustomerName { get; set; }
            public string Email { get; set; }
            public DateTime CurrentTime { get; set; } = DateTime.UtcNow;
        }
        ```

## 結論:
Table Storage相對下收費較便宜，又具有NoSQL的特性，快速、<font style="color:red;">儲存上限可高達500TiB</font>，是非常好用的服務，對於沒有關聯式需求的資料，其實是非常好的選擇。

## 參考
1. [Designing a Scalable Partitioning Strategy for Azure Table Storage](https://docs.microsoft.com/en-us/rest/api/storageservices/designing-a-scalable-partitioning-strategy-for-azure-table-storage)
2. [Azure Table storage overview](https://docs.microsoft.com/en-us/azure/cosmos-db/table-storage-overview)