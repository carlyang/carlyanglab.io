---
title: Azure - Failover Groups of SQL DB
categories:
 - 技術文件
 - Azure
date: 2023/2/13
updated: 2023/2/13
thumbnail: https://i.imgur.com/noxzDjY.png
layout: post
tags: [Azure, Azure SQL]
---

![](/Teamdoc/image/azure/logos/azure_sql.png)

`Azure SQL DB`提供了跨Region的容錯轉移功能，可以讓我們在平常的時候就同步`Primary` & `Secondary`間的資料，當錯誤發生導致`Primary` DB無法使用時就可以切過去`Secondary`繼續使用，但資料在某些特殊狀況下、還是有可能會有Miss的狀況。
<!-- more -->

## Purpose
`Azure SQL DB`的容錯轉移功能能幫助我們同步`Primary` & `Secondary`間的資料，在平時就做好容錯的準備，以防在錯誤發生時可以快速切換使用另一台繼續服務。   
但是在某些情況下還是有可能遺失資料，這在某些資料應用上是不能被接受的，引用[[MS Doc]](https://learn.microsoft.com/en-us/azure/azure-sql/database/auto-failover-group-sql-db?view=azuresql&tabs=azure-powershell#preparing-for-performance-degradation)如下:

> ## Potential data loss after failover  
>If an outage occurs in the primary region, recent transactions may not be able to replicate to the geo-secondary. If the automatic failover policy is configured, the system waits for the period you specified by GracePeriodWithDataLossHours before initiating an automatic geo-failover. The default value is 1 hour. This favors database availability over no data loss. Setting GracePeriodWithDataLossHours to a larger number, such as 24 hours, or disabling automatic geo-failover lets you reduce the likelihood of data loss at the expense of database availability.

也就是說，越頻繁地同步資料`Azure SQL DB`會盡可能地提供可用性、而非不遺失資料，如果拉長資料同步的時間則降低資料遺失的可能性、卻犧牲可用性。所以，本文主要在說明、為了能夠盡可能保證`Primary` & `Secondary`間的資料一致性，必須要有另外的處理方法。

## Failover Groups
如何設定容錯移轉群組可以參考[[MS Doc]](https://learn.microsoft.com/en-us/azure/azure-sql/database/auto-failover-group-configure-sql-db?view=azuresql&tabs=azure-portal&pivots=azure-sql-single-db)。  
除了設定好容錯移轉定期同步兩端資料外，為了保障兩端資料的一致性、我們可以執手動執行`sp_wait_for_database_copy_sync`系統預存程序，確保所有交易資料確實同步，使用範例如下:

```sql
USE db0;  
GO  
EXEC sys.sp_wait_for_database_copy_sync @target_server = N'ubfyu5ssyt1', @target_database = N'db0';  
GO
```

Note:  
這支`sp_wait_for_database_copy_sync`雖然也可以寫在其他地方、在每一次交易時被呼叫使用，但是在`Secondary`切換為`Primary`後，因為其中帶入的`Target Server` & `Target DB`也必須更換(平時`Secondary`不能改)。但何時要切換為`Primary`無法確定、且每次切換後要更新所有呼叫的地方也很麻煩，`所以建議這支SP還是以手動執行較佳!在需要的時候跑就好。`

## 效能的考量  
當我們在每次交易後執行`sp_wait_for_database_copy_sync`、無可避免地會影響執行效能，畢竟你是要求`Azure SQL DB`一定要等待同步結束、確保交易資料已經同步到`Secondary` DB後才真正結束處理。  

因此，就要考量這個效能延遲的程度是否能接受，通常這個同步的延遲會受廣域網路的影響較大，所以可以考慮將`Primary` & `Secondary`兩DB設置在`Paired Regioins`，因為`Paired Regioins`間的網路延遲會設計地較低、會有較佳傳輸速度及較低的延遲。例如: `Southeast Asia` & `East Asia`兩座DC，`Paired Regioins`資訊可以在[[MS Doc]](https://learn.microsoft.com/en-us/azure/reliability/cross-region-replication-azure)查閱。

當然，如果應用上延遲接受度有較大的空間，那將`Primary` & `Secondary`兩DB設置在非`Paired Regioins`也可以，不同`Regions`間的延遲可以參考[[MS Doc]](https://learn.microsoft.com/en-us/azure/networking/azure-network-latency)。  
以我公司為例，`Primary` DB設置在`Southeast Asia`、`Secondary` DB設置在`East Asia`或`Japan East or West`的延遲度如下圖。

![](/Teamdoc/image/azure/SQLDBFailoverGroups/latency1.png)

`East Asia`對`Southeast Asia`的延遲毫秒、相當於`Japan East or West`延遲毫秒的一半，乍看之下似乎差很多，但還是必須要看系統能夠接受的延遲程度而定。有些情境下考量的不僅僅只是網路延遲、實務上還是有很多其他因素的考量，例如:地理、政治、規模、距離..等等。

## Conclusion
容錯轉移功能提供了很大的容錯能力，等於是一台SQL DB的異地備援，能夠自動同步資料、保證資料的一致性，但是試想想、假設我們1小時同步一次資料、當有天發生錯誤`Primary` DB失效、剛好在第30分鐘時發生，那過去30分鐘內的資料就救不回來了，因為每一小時的同步還未執行，所以就必須想辦法克服這情境造成的資料遺失。不過，正常來說是不太容易發生這種狀況，只是平時多一分警惕、多做一道手續，當災難真的發生時才會起到作用，免得到時候欲哭無淚、資料想救都救不回來。  
而這支SP`基本上還是建議手動執行，在需要的時候跑就好。`因為有`Primary`與`Secondary`間切換後`Target Server & DB`要更換的問題，使它較不適合用在其他程式碼的自動呼叫上。

## References
1. [Prevent loss of critical data](https://learn.microsoft.com/en-us/azure/azure-sql/database/auto-failover-group-sql-db?view=azuresql&tabs=azure-powershell#preventing-the-loss-of-critical-data)
2. [Active Geo-Replication - sp_wait_for_database_copy_sync](https://learn.microsoft.com/en-us/sql/relational-databases/system-stored-procedures/active-geo-replication-sp-wait-for-database-copy-sync?view=sql-server-ver16)
3. [Cross-region replication in Azure: Business continuity and disaster recovery](https://learn.microsoft.com/en-us/azure/reliability/cross-region-replication-azure)
4. [Azure network round-trip latency statistics](https://learn.microsoft.com/en-us/azure/networking/azure-network-latency)
