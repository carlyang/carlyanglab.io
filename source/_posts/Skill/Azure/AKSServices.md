---
title: AKS - Services
categories:
 - 技術文件
 - Azure
date: 2023/11/18
updated: 2023/11/18
thumbnail: https://i.imgur.com/5sNBEdu.png
layout: post
tags: [Azure, AKS]
---

![](/Teamdoc/image/azure/logos/aks.png)

## Purpose
`AKS`可以幫助我們管理容器的生命週期，其管理的最小單位是`Pod`、每個`Pod`內可包含一或多個容器，但是當前端要與後端`Pod`溝通時網路路由要如何處理呢?`Pod`跑起來後是處於內部環境內、並不是像我們在IIS上架一個站台就可以讓外部隨便呼叫，所以這時候`Services`就是用來抽象化這些`Pod`與前端網路溝通相關的工作，本文的目的就在說明要如何使用`AKS`內的`Services`功能。
<!-- more -->

## Services
這邊先引用[官方文件](https://kubernetes.io/docs/concepts/services-networking/service/)的說明如下。

>In Kubernetes, a Service is a method for exposing a network application that is running as one or more Pods in your cluster.

也就是說，`Services`是一種用來公開`Pods`的對外網路服務應用，它可以讓前端透過如DNS的方式與後端的`Pods`溝通、而不用管`Services` <-> `Pods`之間的路由如何跑、內部IP是多少等問題，`Services`會自動將符合規則的Request導向對應的`Pods`並正確處理`Pods`的回應。

另一方面來說，`Pods`是有生命週期的，也就是說`Pods`也有生老病死，`AKS`能依照我們設定的規則去處理`Pods`的啟動/重啟、刪除等工作，每當`Pods`被啟動時都會賦予一組內部虛擬IP，也就是隨著每次的重啟這些虛擬IP有可能不同，所以就需要`Services`來負責這些內部網路組態的更新與連線，外部只需要知道Public URL是什麼即可。

引用官方的總結如下，`Service`定義了一組邏輯端點、讓後端的這些`Pods`可以被正常存取。

>The Service API, part of Kubernetes, is an abstraction to help you expose groups of Pods over a network. Each Service object defines a logical set of endpoints (usually these endpoints are Pods) along with a policy about how to make those pods accessible.

## Defining a Service
接著讓我們先來看一個最單純的範例(來源:[Reference](https://kubernetes.io/docs/concepts/services-networking/service/#defining-a-service))，假設我們有一個`Pod`設定使用9376連接埠，並且已經設定Label為app.kubernetes.io/name=MyApp，設定如下。

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  selector:
    app.kubernetes.io/name: MyApp
  ports:
    - protocol: TCP
      port: 80
      targetPort: 9376
```

- `apiVersion`: 內部API的版本號碼。
- `kind`: 資源類型，這邊就要宣告為Service。
- `metadata`
    - `name`: 資源名稱。
- `spec`
    - `selector`: 要套用的資源Label，app.kubernetes.io/name: MyApp就是一個被命名為MyApp的Pod。
- `ports`
    - `protocol`: 通訊協定，這邊指定要使用TCP。
    - `port`: 對外連接埠號碼，這邊就是設定對外為80。
    - `targetPort`: 內部連接埠號碼，這邊就是內部Pod的9376埠。

## Create a Service in AKS
進入`AKS`後有些複雜的`kubectl`指令操作都被做成GUI的功能方便我們使用，接下來我們就要來介紹如何在`AKS`中建立一個`Service`，位置如下圖。

![](/Teamdoc/image/azure/AKSServices/services1.png)

接著我們開始輸入各項目。
- `Name`: 自訂名稱。
- `Type`: 這邊先預設`ClusterIP`，也就是對應目前`AKS Cluster`內部的`Pods`等資源。
- `Namespace`: 目前這個`Service`的`Namespace`方便維護管理，關於`Namespace`可參考[[AKS - Namespaces]](/2023/11/13/Skill/Azure/AKSNamespaces/)。
- `Selector`: 選填項目。這就是上一小節說明的選擇器，我們可以透過指定Label為`app.kubernetes.io/name=MyApp`的資源套用此`Service`。
- `Port name`: 自訂Port名稱，預設會幫我們用`[service name] + -port`，基本上用預設即可。
- `Port number`: 對外Expose的Port號碼。
- `Target port`: 對應`AKS Cluster`的Port號碼。
- `Protocol`: 不設定的話預設就是TCP。
- `Node port`: 這個選項跟上面的`Type`連動，當我們指定為`NodePort`時此欄位才會被Enabled。
- `Labels`: 我們要給予這個`Service`的標籤設定。

![](/Teamdoc/image/azure/AKSServices/services2.png)

最後按下下方的Create按鈕建立`Service`、再進入`Service`就可以看到如下圖資訊，這時候我們已經Expose `TCP:80`這個Port並對應到`ClusterIP`為`10.0.26.5:9376`的內部端點，接著外部就可以透過80 Port來與`AKS Cluster`內部溝通。

![](/Teamdoc/image/azure/AKSServices/services3.png)

## Service Types
上一小節中有提到`Service`的`Type`(也就是服務的類型)，不同服務類型各有特定的用途，要依照我們當下建立的服務對應後端的情況而定，而上一小節僅先使用預設的`ClusterIP`來設定後端為`AKS Cluster`內部的資源，事實上為了不同的目的還有數種不同類型可使用，讓我們先在範例的`spec`下加上一個`type`屬性、值為`ClusterIP`，也就相當於上一小節中我們透過`GUI`建立`Service`時的`Type`下拉選單，如下圖。

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  type: ClusterIP
  selector:
    app.kubernetes.io/name: MyApp
  ports:
    - port: 80
      targetPort: 9376
```

![](/Teamdoc/image/azure/AKSServices/services4.png)

目前所有的`Service`類型說明如下:
- `ClusterIP`: 透過`AKS`集群的內部IP讓`Service`內資源可以互相溝通，同時也是預設的`Service`類型。如果想要讓`Service`開放給網際網路使用、則需要另外使用`Ingress`或`Gateway`，引述[[官方文件]](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types)說明如下:
>Exposes the Service on a cluster-internal IP. Choosing this value makes the Service only reachable from within the cluster. This is the default that is used if you don't explicitly specify a type for a Service. You can expose the Service to the public internet using an Ingress or a Gateway.
- `NodePort`: 透過每個節點的IP與靜態Port公開`Service`，而`AKS`會為它配置內部`cluster IP`就如同使用`type: ClusterIP`類型一樣，引述[[官方文件]](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types)說明如下:
>Exposes the Service on each Node's IP at a static port (the NodePort). To make the node port available, Kubernetes sets up a cluster IP address, the same as if you had requested a Service of type: ClusterIP.
- `LoadBalancer`: 使用外部的負載平衡器來對外公開你的`Service`，`AKS Cluster`本身不提供負載平衡器的功能，我們必須另外提供一個或是跟其他的雲服務整合，引述[[官方文件]](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types)說明如下:
>Exposes the Service externally using an external load balancer. Kubernetes does not directly offer a load balancing component; you must provide one, or you can integrate your Kubernetes cluster with a cloud provider.
- `ExternalName`: 此類型會將`Service`對應到外部的一個`Domain Name`，例如:api.foo.bar.example，引述[[官方文件]](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types)說明如下:
>Maps the Service to the contents of the externalName field (for example, to the hostname api.foo.bar.example). The mapping configures your cluster's DNS server to return a CNAME record with that external hostname value. No proxying of any kind is set up.

## Conclusion
本文僅先針對基礎的`Service`概念進行說明，並說明了`AKS`對於`Service`功能的基本操作，事實上`Service`的內容很複雜，又因為牽涉網路層的多樣性，所以`Service`的類型也是非常的博大精深。而也正是因為`Service`的抽象化特性，讓我們可以很好地將集群內外部的網路切割開來，讓我們可以較好地理解與規劃不同的對應，對於集群內部的這些IP生命週期可以交給`Service`幫我們自動更新處理，我們不需要擔心因為內部IP的變更而導致服務中斷。

## References
1. [Service](https://kubernetes.io/docs/concepts/services-networking/service/)