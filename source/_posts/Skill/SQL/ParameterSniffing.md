---
title: Store Procedure - Parameter Sniffing
categories:
 - 技術文件
 - SQL
date: 2018/9/2
updated: 2018/9/2
thumbnail: /Teamdoc/image/SQL/ParameterSniffing.png
layout: post
tags: 
 - Parameter Sniffing
---
作者: Carl Yang

## Writing Store Procedure
![](/Teamdoc/image/SQL/ParameterSniffing.png)
在撰寫T-SQL時常會用到Store Procedure，有再利用、已編譯、統一控制等優點，對於開發者來說也可以統一SP的撰寫規範，便於管理維護及後續的人接手調整，而今天就來淺談一下撰寫SP時，可能會遇到的問題-<font style="color:red;">**Parameter Sniffing**</font>。
<!-- more -->

## Parameter Sniffing
開發者可能會發現，有些系統可能在某特定時間點會變得很慢(或是相較以前變慢)、也可能是忽快忽慢的狀況，這時候就可以懷疑是否Parameter Sniffing的問題在作怪。
可能狀況如下:
- 執行SP會耗時很久、但把SP的Body拿出來單獨執行又沒有那麼緩慢。
- 執行查詢時忽快忽慢，難以判斷問題點。
- 隨著資料量越來越大，問題發生次數越來越頻繁，系統常有卡住的狀況。
- 越複雜的SP越有可能發生此問題。

而Parameter Sniffing指的是當SP第一次被Compile或Re-compile時，SQL Server會為這隻SP制定一個執行計畫(Execution Plan)，而這個執行計畫會根據傳入參數、Query、Execute()...等等操作，使用Optimizer來最佳化SP的執行效能。

大家可以試著觀察執行計畫的各個步驟、以釐清問題點。如下圖，SP最多的成本是在資料插入temp table的時候(95%)，資料表排序及掃瞄只佔了5%，但因為資料隨時在變化，可能月初跟月底做報表的資料量就有很大差異，故查詢的成本也會跟著變化。
![](/Teamdoc/image/SQL/ParameterSniffing1.png)

雖然<font style="color:red;">**SQL Server的執行計畫是為了最佳化執行效能、重複使用等作用，但它也可能會有失誤的時候**</font>。
例如，傳入SP的參數會再被拆解成更細節的Keywords以便複雜的巢狀查詢等等，SQL Server不太可能理解參數值的內容該如何處理，因此可能誤導SQL Server制定錯誤的執行計畫。
故為了避免可能發生的Parameter Sniffing問題，提供以下解決方法，供各位看官參考。

## 解決方案:
1. WITH RECOMPILE:
在SP中加入<font style="color:red;">**WITH RECOMPILE**</font>，此法SP會在每次被調用時、重新編譯SP內容、依照當下狀況重新制定最佳化執行計畫。

優點:
	- 執行計畫會依照當下狀況制定最佳化準則，而非一開始Create時的第一套執行計畫永遠使用下去。
缺點:
	- 每次執行都要重新編譯、耗時耗力。
```sql
ALTER PROCEDURE [dbo].[uspQueryArticles]
    (
      @p_KeyWords NVARCHAR(1000) = NULL ,
      @p_Tags NVARCHAR(1000) = NULL
    )
WITH RECOMPILE
	...略...
```
2. 使用<font style="color:red;">**Local Variable**</font>:
在SP中自定義一組內部變數、在一開始時去承接外部變數的值，查詢或其他操作都使用內部變數。

優點:
	- 外部變數就僅止於接收數值，SQL Server在執行最佳化執行計畫時就不會誤判，導致錯誤地制定執行計畫。
缺點:
	- 每個SP要多定義一組內部變數來承接外部傳入的值。
```sql
ALTER PROCEDURE [dbo].[uspQueryArticles]
    (
      @p_KeyWords NVARCHAR(1000) = NULL ,
      @p_Tags NVARCHAR(1000) = NULL
    )
AS
	...略...
	DECLARE @KeyWords NVARCHAR(1000);
	DECLARE @Tags NVARCHAR(1000);
	
	SET @KeyWords = @p_KeyWords;
	SET @Tags = @p_Tags;
	...略...
```
3. 拆解Store Procedure
將複雜的SP內部邏輯一一拆解成更細小的單元，可以做成多個SP或Function或View等等。

優點:
	- 每個SP都可以重複使用在不同地方。
	- 每個SP的工作量降低，減少耗時的狀況出現。
缺點:
	- 同一個Task會使用多個SP，增加複雜度。
	
## 結論:
通常在撰寫SP的開發者都應該熟悉產業的商業邏輯，而商業邏輯應該盡量少納入SP中。
因為複雜的商業邏輯很容易造成資料查詢的循環參照，最後就是效能不彰。所以建議應該將商業邏輯放在AP處理，SP等等資料庫物件就單純處理資料就好。

- 方法一的RECOMPILE如果SP都已經拆解到很小的單元、相對於查詢的成本來說可能低很多，那用RECOMPILE反而是最佳的方式，端看當時的狀況來判斷是否使用。

- 方法二的Local Variable個人建議其實應該制定成Coding rule，開發者都應該養成使用內部變數的習慣、可以避免未來可能發生的效能問題。

- 方法三的SP拆解，更像是一種最佳化的過程，在日常開發中本就應該仔細分析工作的細項及相互間的關聯，而不是將所有操作都擠在相同的SP中處理，故個人建議還是應該納入日常的開發規範中。

## 參考資料:
[[SQL Server] Parameter Sniffing: Pros and Cons, 參數探測](http://sharedderrick.blogspot.com/2017/12/sql-server-parameter-sniffing-pros-and.html)
[Parameter Sniffing - SQL Serer By Sunil Kumar Anna](https://www.slideshare.net/sunilannakumar/parameter-sniffing-sql-serer-by-sunil-kumar-anna)
