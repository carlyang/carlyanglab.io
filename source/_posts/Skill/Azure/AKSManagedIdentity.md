---
title: AKS - How To Use Managed Identity?
categories:
 - 技術文件
 - Azure
date: 2023/11/10
updated: 2023/11/10
thumbnail: https://i.imgur.com/5sNBEdu.png
layout: post
tags: [Azure, AKS]
---

![](/Teamdoc/image/azure/logos/aks.png)

## Purpose
`AKS`是`Azure`導入`K8s`後的雲端服務，除了`K8s`原本的完整功能外、與`Azure`現有架構的整合也是非常重要的一環，這不僅僅是新增了雲端`Docker`的服務、也納入如`Azure Monitor`、`Microsoft Entra ID`(原AAD)、`Managed Identity`...等，也就是說除了`K8s`的管理工作、還多了監控、Azure AD、身分驗證等功能，更加完整了一個平台所需的各方面需求，而本文目的就在介紹如何利用`Managed Identity`(受控識別)整合`Key Vault`的存取控制，以順利讓`AKS`內的Container存取相關機敏資訊。
<!-- more -->

## Key Vault
`Key Vault`是`Azure`所提供的一種金鑰保存庫，它不僅可以用來產生與保存金鑰，也可以用來保存`Secrets`、`Certificates`等，並且提供資料加密、外部存取`Authentication`及加密傳輸等，以達成機敏資訊真正的安全性存取。不過，根據微軟的建議，相關存取`Key Vault`的服務越少、安全性相對也就越高，所以建議如果要允許其它服務來存取、最好使用`Managed Identity`(受控識別)的方式、個別設定服務對`Key Vault`的`Access Policies`。

## Access Policies
首先，我們可以到下圖的位置進入`Access Policies`功能、然後點選`Create`按鈕建立新的`Access Policy`。

![](/Teamdoc/image/azure/AKSManagedIdentity/keyvault1.png)

接著先選取目前要給予的權限設定、接著按`next`進入下一步。

![](/Teamdoc/image/azure/AKSManagedIdentity/keyvault2.png)

再來我們要找出`AKS`所使用的`Principal`，才能在`Key Vault`中給予這個`Principal`進一步設定的權限。  
如下圖，先輸入`AKS`的名稱、我們可以找到兩個含有這個名稱的`Principal`，接著選取第一個`Principal`再`next`直到最後建立這個`Access Policy`。

![](/Teamdoc/image/azure/AKSManagedIdentity/keyvault3.png)

#### `**請注意，非常重要!**`
到目前為止應該都很順利設定了`Access Policy`，但是這樣設定下來你應該會發現`AKS`內的`Container`仍然無法正常存取`Key Vault`!  
`其實原因很簡單，就是我們選錯Principal了!AKS根本不是以相同名稱的Principal來存取Key Vault的!`

## AKS Nodes
上一小節先說明了如何設定`Key Vault`的`Access Policy`，但如果直接以`AKS`的`Principal`來設定權限卻仍然不通，這是由於`AKS`的架構所致，有興趣的朋友可以先參考[[AKS - The concepts of K8s]](/2023/11/02/Skill/Azure/AKSConceptsK8s/)這篇文章中的`Node`說明(裡面有說明並引用了`K8s`的文件及架構)。

>`K8s`的Node分為`master node`及`worker node`。`master node`為早期的名稱、目前已改為`control plane`，主要負責相關的管理工作。`worker node`則是代表不同台的`Host`，這個`Host`可以是一台實體機器、也可以是一台`VM`，每個`worker node`都有獨立的一套OS。

也就是說在`K8s`上的`container`事實上是跑在`Node`上的，不管是實體機器或是`Virtual Machine`都是一樣的意思，但在`Azure`基本上都是用`Virtual Machine`，所以當我們在設定`Key Vault`的`Access Policy`時應該要將權限給予`Node`的`Principal`、而非`AKS`的`Principal`，有幾台`Node`就要個別設定`Key Vault`的的權限。

根據以上的說明應該可以知道，接下來就是如何找出`Node`的`Principal`，我們先到當初建立`AKS`時所指定`VM`所在的`Resource Group`(這個`Resource Group`理論上應該不會跟`AKS`本身相同Group，但如果一開始都指定建立在同一個Group內的話那就要從同一個Group內去找)，然後找出類型為`Virtual machine scale set`、名稱尾巴為`-vmss`的資源，如下圖紅框。

![](/Teamdoc/image/azure/AKSManagedIdentity/node1.png)

我們先把這個`VM`名稱複製起來、再回到`Key Vault`的`Access Policies`中找出同名的`Principal`並設定其權限，最後建立這組設定即可，如下圖。

![](/Teamdoc/image/azure/AKSManagedIdentity/keyvault4.png)

Note:  
1. `請注意，這邊Pricipal指的是VM的Managed Identity(也是一種Service Principal)，所以我們必須要先開啟VM的System Assigned Identity!`開啟位置如下圖。  
2. 當然我們也可以使用`User Asigned Identity`，那回到`Key Vault`的設定時就要找自訂的名稱、就不一定是跟`VM`同名。
3. 另外，`也要特別注意這邊指的是Virtual machine scale set、不是Instances`，每個`Virtual machine scale set`可以包含多個Instances，但只會使用同一個`Managed Identity`。

![](/Teamdoc/image/azure/AKSManagedIdentity/node2.png)

## Conclusion
本文主要在說明`AKS`的`Node`及`Managed Identity`的設定，也就是`Azure`上`Virtual Machine`的受控識別身分。  
事實上，當`AKS`要去存取其它資源時，就是使用這些`VM`的身分去進行的，所以當我們的`AKS`同時跑多台`VM`時，由於`AKS`會自動幫我們分配當下`空閒`的`VM`來做為`Container`執行的`Host`，所以任何一台`Node`都有可能以這個身分去存取其他服務，所以如果有多個`Virtual machine scale set`、只要有可能跑到該`Container`的都要設定`Managed Identity`的權限才行。

## References
1. N/A