---
title: Json.Net - Model Mapping
categories:
 - 技術文件
 - Json.Net
date: 2018/12/30
updated: 2018/12/30
thumbnail: https://i.imgur.com/VfzqTxh.png
layout: post
tags: [Newtonsoft.Json, Json.Net, Model]
---
作者: Carl Yang

![](/Teamdoc/image/JsonDotNet/Logo.png)

## Json.Net(Newtonsoft.Json)
Json.Net或稱Newtonsoft.Json是非常廣泛應用於.Net架構的一套c#套件，用來處理Json資料的序列化、反序列化等操作，其中很重要的一環就是Model的處理，本篇文章主要在描述如何使用Json.Net處理Model Mapping的部分。
<!-- more -->

## Model Mapping
Data Model幾乎存在於各式各樣的Application中，舉凡DB Schema v.s. Object Properties、API的Request v.s. Response等等、都很常有資料屬性對應的需要，且很多時候在網路傳輸上、或CLR環境Runtime中Property的命名不一定相同，有很多特殊符號也必須先轉換為可接受的名稱、才能夠繼續處理，因而JSON的屬性名對應到當下環境所接受的命名規則是很重要的。

Json.Net就提供了很多的自訂Property供外部使用，讓開發者不用自己跑一堆為了對應而生的物件，根據他提供的JsonProperty就可以直接序列化及反序列化各式各樣的資料、方便又實用。
其實在撰寫本文以前，就已經用過很多次JsonProperty在處理Model Mapping的問題，為了加深印象及方便以後查詢、乾脆寫篇文章來做個紀錄。

## 必要元件
本篇範例是以.Net Core 2.1 Console Application及Newtonsoft.Json 12.0.1為開發環境，請務必先安裝下列套件以順利進行練習，如果各位讀者有自己的開發環境、也可以自行使用相對應的套件。

- [.Net Core SDK 2.1.502](https://dotnet.microsoft.com/download/dotnet-core/2.1)
- [Newtonsoft.Json 12.0.1](https://www.nuget.org/packages/Newtonsoft.Json/)

## 建立測試模型
這裡先建立一個BaseModel、具有一個基礎的屬性SerialNum，再建立一個子類別、具有自己的Index及Name屬性。
```csharp
internal class BaseModel
{
	public string SerialNum { get; set; }
}

internal class TestModel : BaseModel
{
	public int Index { get; set; }
	public string Name { get; set; }
}
```

## Sample
先在Main Function加入jsonData變數儲存測試用的Json Data，並將其反序列化至TestModel物件。
```csharp
static void Main(string[] args)
{
	const string jsonData = @"{""Index"":1,""Name"":""Test Name"",""SerialNum"":""123456""}";

	var model = JsonConvert.DeserializeObject<TestModel>(jsonData);
}
```

反序列化後我們可以看到Json.Net已自動將各屬性值Mapping到TestModel物件中。
![](/Teamdoc/image/JsonDotNet/code_sample1.png)

## Json屬性的對應
接下來我們加入一個新的Json屬性、名為"Temperature (°C)"。
```csharp
const string jsonData = @"{""Index"":1,""Name"":""Test Name"",""SerialNum"":""123456"",""Temperature (°C)"":""25.5""}";
```

這時候如果我們在TestModel中加入同名屬性"Temperature (°C)"，就會發現原本Json資料可以接受的屬性命名規則、在C#中是不能用的，所以我們改為只有"Temperature"。
```csharp
internal class TestModel : BaseModel
{
	public int Index { get; set; }
	public string Name { get; set; }
	public float Temperature { get; set; }
}
```

最後我們來跑跑看對應結果，Temperature並沒有成功對應到值25.5，因為Json.Net預設會尋找同名屬性來賦值、如果找不到就會略過它，那我們要如何將"Temperature (°C)"的值對應到TestModel中的"Temperature"屬性呢?答案就是使用JsonProperty Annotation。
![](/Teamdoc/image/JsonDotNet/code_sample2.png)

接下來我們在物件屬性Temperature中加上JsonProperty、並指定PropertyName = "Temperature (°C)"。
```csharp
internal class TestModel : BaseModel
{
	public int Index { get; set; }
	public string Name { get; set; }
	[JsonProperty(PropertyName = "Temperature (°C)")]
	public float Temperature { get; set; }
}
```

跑跑看對應結果，屬性Temperature已經成功被賦值為25.5。
![](/Teamdoc/image/JsonDotNet/code_sample3.png)

Note:
如上所述，我們很常遇到資料來源屬性與我們程式物件屬性不對稱的狀況，而且命名規則也不盡相同，使用上述方法就可以方便又快速地將Model屬性對應起來，不用再一個個自己賦值、既複雜又麻煩。

## 序列化Json資料
接下來我們將剛才的物件再序列化為JSON字串，結果如下。
```json
{"Index":1,"Name":"Test Name","Temperature (°C)":25.5,"SerialNum":"123456"}
```

<font style="color:red;">咦!!怎麼對應出來的屬性名稱是"Temperature (°C)"、不是"Temperature"?!</font>

這是因為預設Json.Net會以原本指定的PropertyName作為序列化時的名稱，那我們該怎麼指定序列化時要用Model的Property名稱、而非JsonProperty中的PropertyName呢?
這時候你可能會想說去JsonProperty中找看看有沒有相關的設定可用，但答案是No!!我找遍了JsonProperty都沒有相關的線索、可以給我一點靈感。<br/>
因為預設JsonProperty是Annotation的方式、告訴Json.Net套件屬性該如何對應，但序列化屬於"行為"、是AP在運作時候所要處理的部分，當然如果未來Json.Net有開放相關的JsonProperty也是可行，但目前看來只好自己另外想辦法處理。<p/>

Note:<br/>
1. 查[官方文件](https://www.newtonsoft.com/json/help/html/P_Newtonsoft_Json_Serialization_JsonProperty_UnderlyingName.htm)有個UnderlyingName屬性似乎有點線索，但實際上卻找不到該屬性(如下圖)，撰寫本文時尚未找到解法，日後如果有答案會再做更新。
![](/Teamdoc/image/JsonDotNet/code_sample4.png)

2. 其實，之所以會希望序列化後的JSON屬性要對應於物件的屬性名，也是因為資料的ETL其中一項、就是要"整理"資料，將各式各樣的資料來源整理成可行的、一致性的格式，後續分析才能順利進行。
<font style="color:red;">在這個範例中我們也可以看到，JSON屬性含有" (°C)"空白、上標Degree、左右括號等等特殊符號，這些字元都可能造成後續資料處理的種種問題，所以對應到符合規則的屬性名稱就顯得非常重要。</font>


Json.Net其實也提供了另外一個方式來指定反序列化時要如何處理屬性對應的問題，讓我們加入以下的class。
```csharp
internal class CustomContractResolver : DefaultContractResolver
{
	public bool UseJsonPropertyName { get; }

	public CustomContractResolver(bool useJsonPropertyName)
	{
		UseJsonPropertyName = useJsonPropertyName;
	}

	protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
	{
		var property = base.CreateProperty(member, memberSerialization);
		if (!UseJsonPropertyName)
			property.PropertyName = property.UnderlyingName;

		return property;
	}
}
```

從上面的Code我們可以看到，我們繼承了Json.Net的DefaultContractResolver、並覆寫其CreateProperty()。在這個CreateProperty()中，我們自行指定要使用property.PropertyName = property.UnderlyingName，就可以在序列化時使用Model的屬性名、而非JsonProperty的PropertyName。
接著我們再改一下序列化的Code如下。
```csharp
var result = JsonConvert.SerializeObject(
	model,
	new JsonSerializerSettings()
	{
		ContractResolver = new CustomContractResolver(false)
	});
```

new CustomContractResolver(false)建構子中帶入false表示告訴Resolver<font style="color:red;">不要使用JsonPropertyName、要改用UnderlyingName</font>，並將CustomContractResolver帶入JsonSerializerSettings中，這樣序列化後的JSON資料屬性、就會是Model的"Temperature"、而不是"Temperature (°C)"，結果如下。
```json
{"Index":1,"Name":"Test Name","Temperature":25.5,"SerialNum":"123456"}
```

## 序列化後的JSON屬性排序
從上方的例子來看，BaseModel中的屬性SerialNum總是被排在最後面，這是因為Json.Net序列化後的屬性順序會自行排序，如果我們想要依照自己的希望來排列呢?答案一樣在JsonProperty設定即可。
```json
{"Index":1,"Name":"Test Name","Temperature":25.5,"SerialNum":"123456"}
```

我們將兩個Model的JsonProperty加入Order = [n]。
```csharp
internal class BaseModel
{
	[JsonProperty(Order = 1)]
	public string SerialNum { get; set; }
}

internal class TestModel : BaseModel
{
	[JsonProperty(Order = 2)]
	public int Index { get; set; }
	[JsonProperty(Order = 3)]
	public string Name { get; set; }
	[JsonProperty(PropertyName = "Temperature (°C)", Order = 4)]
	public float Temperature { get; set; }
}
```

接著我們就可以看到序列化結果如下，SerialNum已經被排到最前面、Json屬性能夠依照我們想要的方式進行排序。
```json
{"SerialNum":"123456","Index":1,"Name":"Test Name","Temperature":25.5}
```

結論:<br/>
Json.Net的JsonProperty還有其他好用的功能可以使用、例如Required指定必填欄位...等等，本篇僅針對幾個部分做說明，詳細可參考[官方文件](https://www.newtonsoft.com/json/help/html/T_Newtonsoft_Json_Serialization_JsonProperty.htm)。

References:
[Json.Net Documentation](https://www.newtonsoft.com/json/help/html/Introduction.htm)