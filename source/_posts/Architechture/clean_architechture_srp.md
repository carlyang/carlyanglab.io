---
title: Clean Architechture 心得(1) - SRP單一職責原則
categories:
 - 技術文件
 - Architechture
date: 2019/06/09
updated: 2019/06/09
thumbnail: https://i.imgur.com/wyHkUgV.jpg
layout: post
tags: [Clean Architechture, SRP, C#]
---
作者: Carl Yang

![](/Teamdoc/image/architechture/logo.png)

## 目的
最近閒晃時看到`Clean Architechture - 無瑕的程式碼`這本書，光看標題就讓我很驚訝!  
怎麼這本書敢下這樣的標題，有人敢講自己`無暇`?!於是基於好奇心的驅使下，就買了這本書來看看。  
這本書提到很多軟體架構相關的知識，如SOLID原則、元件設計、架構規劃等等，不看則已、一看真是讓我驚為天人、相見恨晚阿.....呃，好像太誇張了。總之，它讓我想把一點心得寫下來，也印證一下自己的思考。  

本篇先從基本的SOLID-SRP單一職責原則及一點實務上程式的重構、以C#語言撰寫範例來印證一下自己的心得。
<!-- more -->

## Clean Architechture
乾淨的架構人人都想要，但我沒有。設計出乾淨的架構，我也沒這能力。什麼架構叫乾淨?我也還不知道。反正就是一篇心得文...XD這段很廢話，還請客倌自行略過。

## SRP單一職責原則
全名Single Responsibility Principle，是一種標準、一種讓開發人員可以遵循的規則之一，它其實也出現在很多的軟體開發書籍中，例如`Design Pattern`。傳統來說，這原則描述如下。
>一個模組應該有一個，且只有一個理由會使其改變。

看了上面這句話整個一頭霧水，心裡OS:可以講白話一點嗎?於是作者又加以解釋。
>一個模組應該只對唯一的一個使用者或利益相關者負責。

哇，這下總算有點懂了。因為軟體系統是為了現實世界中的使用者而存在的，而不同使用者的操作可能不同，所以使用者的需求變更就可能會讓軟體系統不斷地修改，作者最後的結論如下。
>一個模組應該只對唯一的一個角色負責。

對於以往的概念來說，SRP描述我們在撰寫軟體系統時，應該要做到一個函式(或模組)只做一件工作，如果不能、表示這件工作應該要被拆分地更細一些，`但這其實不是SRP真正要說的`。事實上，軟體系統是為了使用需求而存在的，不管我們切得多細、最終都要符合需求，軟體系統才有價值。所以運用SRP原則時，`應該要以使用者角色為思考基礎，達到針對一個角色、一個模組、做一件工作，如果有其他的工作則應該要以擴充的方式、增加不同函式(或模組)提供角色新的功能`。

## Facade Pattern(外觀模式)
根據以上對於SRP的了解，剛好最近公司系統有個Alarm的功能，用來提醒使用者發生了某些錯誤，為了能夠針對角色做到單一職責原則，接下來使用Facade模式來將模組內部包裝起來，外部角色在調用模組時，只需要考慮`他要做什麼事`、不用管`系統怎麼做到這件事`。

事實上，這個Alarm通知需要做三件工作-`發送Email`、`發送SMS`及`發送Alarm事件至監控端電腦`，但外部功能在調用模組時只需要將Alarm的資訊傳給模組，其餘工作就由不同模組負責不同的工作內容，讓我們來看看下方的類別圖。
![](/Teamdoc/image/architechture/class_diagram.png)

上圖的AlarmFacade類別實作了IAlarmFacade介面，透過DI等方式注入實體，並提供外部角色調用的Notify函式，外部不需要知道三種通知如何運作、角色只要知道他現在需要發何種類型的通知即可，而三種通知則又各有三種不同的通知類型，由AlarmFacade統一操作及使用。

- IAlarmFacade/AlarmFacade:  
`提供Notify函式供外部調用。`

- ISmsService/SmsService:  
`提供發送簡訊內容至指定手機號碼。`

- IDropMailService/DropMailService:  
`發送Drop類型的Email至指定的收件者。`

- ILeakMailService/LeakMailService:  
`發送Leak類型的Email至指定的收件者。`

- IO2ExceedMailService/O2ExceedMailService:  
`發送O2Exceed類型的Email至指定的收件者。`

- IDropEventService/DropEventService:  
`發送Drop類型的Event至監控端電腦。`

- ILeakEventService/LeakEventService:  
`發送Leak類型的Event至監控端電腦。`

- IO2ExceedEventService/O2ExceedEventService:  
`發送O2Exceed類型的Event至監控端電腦。`

## 範例
詳列各Services類別內容太過冗長、又與本文所要闡述的SRP概念無太大關係，故這裡僅列出AlarmFacade的部分範例，最重要的是，`透過Facade Pattern切開內外部的關聯，提供單一的Notify函式予外部角色調用`。

由範例中我們也可以看到，透過DI注入各Services實體後，`AlarmFacade.Notify()`內部自行調用不同的Services進行作業。

```csharp
    public class AlarmFacade : IAlarmFacade
    {
        private readonly ConfigModel _config;
        private readonly IDropMailService _dropMailService;
        private readonly ILeakMailService _leakMailService;
        private readonly IO2ExceedMailService _o2ExceedMailService;
        private readonly IDropEventService _dropEventService;
        private readonly ILeakEventService _leakEventService;
        private readonly IO2ExceedEventService _o2ExceedEventService;
        private readonly ISmsService _smsService;

        public AlarmFacade(IOptions<ConfigModel> config,
            IDropMailService dropMailService,
            ILeakMailService leakMailService,
            IO2ExceedMailService o2ExceedMailService,
            IDropEventService dropEventService,
            ILeakEventService leakEventService,
            IO2ExceedEventService o2ExceedEventService,
            ISmsService smsService
            )
        {
            //Pass...
        }

        /// <summary>
        /// This function is for Drop Alarm
        /// </summary>
        /// <param name="alarmModel"></param>
        private void SendDropAlarm(AlarmModel alarmModel)
        {
            //Pass...
        }
        /// <summary>
        /// This function is for Leak Alarm
        /// </summary>
        /// <param name="alarmModel"></param>
        private void SendLeakAlarm(AlarmModel alarmModel)
        {
            //Pass...
        }
        /// <summary>
        /// This function is for O2 Exceed Alarm
        /// </summary>
        /// <param name="alarmModel"></param>
        private void SendO2ExceedAlarm(AlarmModel alarmModel)
        {
            //Pass...
        }

        public void Notify(AlarmModel alarmModel)
        {
            switch (alarmModel.Type)
            {
                case AlarmType.Dropped:
                    SendDropAlarm(alarmModel);
                    break;
                case AlarmType.Leak:
                    SendLeakAlarm(alarmModel);
                    break;
                case AlarmType.O2Exceed:
                    SendO2ExceedAlarm(alarmModel);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
```

## 結論
本文僅利用Facade Pattern(外觀模式)，達成單一角色、單一職責的SRP原則，`事實上並非只有Facade Pattern可以使用`，端看當下需求應用不同的情境，而書中的SRP原則僅是在闡述一種概念、
一種泛用性原則，可以讓軟體開發者依循的規則。

## 參考
1. [無瑕的程式碼－整潔的軟體設計與架構篇](https://www.tenlong.com.tw/products/9789864342945)