---
title: 使用WinRM取得遠端電腦資訊
categories:
 - 技術文件
 - Windows
date: 2018/6/3
updated: 2018/6/15
thumbnail: /Teamdoc/image/winrm/WinRMLogo.jpg
layout: post
tags: 
 - WinRM
 - Windows Remote Management
---
作者: Carl Yang

# [.NET] 使用WinRM取得遠端電腦資訊
以前需要取得遠端電腦資訊、作為系統管理參考，常會使用Windows Management Instrumentation(WMI)取得相關資訊。

最近發現微軟其實有個更好用的東西、叫做Windows Remote Management(WinRM)，

與WMI相同、都是windows內建的服務，使用上都大同小異，
除了效能較好、較快外，以常用的[Win32_OperatingSystem]來說、能夠取得的資訊也較多。

--------------------------------------------------------------------------------------------------
<!-- more -->
舊有的WMI在Windows中的服務原本即自動啟動、但WinRM則否，

故需要事先在被叫用端電腦做一些開啟的動作，步驟如下:

 1. 開啟PowerShell - 開始->所有程式->附屬應用程式->Windows PowerShell->Windows PowerShell->[右鍵"以系統管理員身分執行"]
 2. 輸入winrm quickconfig->[按Enter]
 3. 啟動WinRM主要有三個步驟，如下:

-   WinRM 未設定為在此電腦上接收要求。必須進行下列變更:

啟動 WinRM 服務。

將 WinRM 服務類型設定為延遲自動啟動。

進行這些變更 [y/n]?
![](/Teamdoc/image/winrm/winrm_1.png)
這裡要輸入y以啟動WinRM，按下Enter後就會啟動該電腦的WinRM服務、並且將啟動類型設為[自動(延遲啟動)]，而延遲啟動不表示每次重開機都不會啟動該服務，而是在重開機後、待所有Windows及相關主要服務啟動後、才會再啟動WinRM服務，這是為了節省Windows開機所需要的時間(微軟還真貼心)。
-   建立WinRM接聽程式….[略]
這個步驟主要是建立WinRM的listener、以接聽來自[http://*的命令](http://xn--%2A-us9dy9wk98c/)，及建立WinRM的防火牆例外，以免被防火牆擋住收不到遠端的呼叫。
-   LocalAccountTokenFilterPolicy….[略]
這個動作是在建立Windows機碼、以讓Windows的UAC(User Access Control)能夠允許遠端命令的存取，否則遠端會收到[Access Denied]的錯誤訊息。該機碼可在regedit中的路徑找到(也可手動建立該機碼)。

`[HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System]` -> `LocalAccountTokenFilterPolicy`，** Value **設為**1**
![](/Teamdoc/image/winrm/winrm_2.png)
-   設定TrustedHosts
在PowerShell中輸入下列指令，將TrustedHosts設為*，表示默認接收所有遠端的請求，當然也可以設定只允許某些電腦存取。

`winrm s winrm/config/client '@{TrustedHosts="*"}'`

![](/Teamdoc/image/winrm/winrm_3.png)
接下來使用C#做遠端測試連線:
```csharp
	//取得遠端IP或domain、及帳密
	string computer = @"127.0.0.1";
	string username = @"testAccount";
	string password = @"testPassword";

	string planePwd = password;

	//密碼加密
	SecureString securePwd = new SecureString();
	foreach (char c in planePwd)
	{
		securePwd.AppendChar(c);
	}

	WSManSessionOptions sessionOptions;
	CimSession session;
	CimCredential credential;

	//如果不是本機就需要處理Credentials、WSManSessionOptions
	if (false == "127.0.0.1".Equals(computer) && false == "localhost".Equals(computer.ToLower()))
	{
		credential =
			new CimCredential(PasswordAuthenticationMechanism.Default,
				computer,
				username,
				securePwd);

		sessionOptions = new WSManSessionOptions();
		sessionOptions.AddDestinationCredentials(credential);

		session = CimSession.Create(computer, sessionOptions);
	}
	else
	{
		session = CimSession.Create(computer);
	}

	var allOSInfo = session.QueryInstances(@"root\cimv2", "WQL", "SELECT * FROM Win32_OperatingSystem");

	//搜尋資訊
	foreach (CimInstance oneVolume in allOSInfo)
	{
		//抓取相關資料
		if (oneVolume.CimInstanceProperties["DriveLetter"].ToString()[0] > ' ')
		{
			//略
		}
	}
```