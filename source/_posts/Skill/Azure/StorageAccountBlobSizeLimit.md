---
title: Azure - Storage Blob的資料大小限制
categories:
 - 技術文件
 - Azure
date: 2022/12/3
updated: 2022/12/3
thumbnail: https://i.imgur.com/8uMcIBL.png
layout: post
tags: [Azure, Storage Account, Blob]
---

![](/Teamdoc/image/azure/logos/storage_account.png)

## Purpose
[[上一篇]](/2022/12/01/Skill/Azure/StorageAccountFileShareSizeLimit/)說明`FileShare`相關的大小限制，其中對於`PUT Range`每次只能上傳4 MiB的限制有了些疑問，想說該不會連`Blob`都有這個限制吧?!於是做了一點實驗發現，`Blob`這邊還真的也有這個限制阿!所以，本文就來說明一下針對`Blob`部分有4 MiB限制的調整。
<!-- more -->

## Limitation
關於`Blob`的4 MiB限制，我們使用SDK拿到的回傳錯誤訊息跟`FileShare`不太一樣，可參考[[MS Doc]](https://learn.microsoft.com/en-us/troubleshoot/azure/general/request-body-large)。

>## Symptoms
>Assume that you try to write more than 4 megabytes (MB) of data to a file on Azure Storage, or you try to upload a file that's larger than 4 MB by using the SAS url (REST API) or HTTPS PUT method. In this scenario, the following error is returned:

```
Code : RequestBodyTooLarge
Message: The request body is too large and exceeds the maximum permissible limit.
RequestId:<ID>
MaxLimit : 4194304
```

也就是說，重點在於使用了`SAS url (REST API) or HTTPS PUT method`，就會有4 MiB的限制。

## Append Blob
`Append Blob`提供我們一種可以不斷以附加的方式、將資料直接附加在檔案內容的尾端，適用於資料不斷更新、無法事先預知全部檔案內容的情境。  
之所以特別提`Append Blob`是因為在實驗過後發現，每次的`Append`動作就會發生超過4 MiB限制的錯誤!因為SDK背後的動作就是使用了`PUT Method`!

Note:  
基本上，只要用到[[PUT Blob]](https://learn.microsoft.com/en-us/rest/api/storageservices/put-blob)的動作都會遇到這限制。

## 程式調整
調整方式基本上跟`FileShare`的方式一樣，就是將資料分成多次上傳、每次的資料大小縮小到4 MiB以下即可，範例如下。

```csharp
public async Task AppendFileAsync(string blobFilePath, Stream content)
{
	var appendBlob = BlobContainerClient.GetAppendBlobClient(blobFilePath);

	if (!await appendBlob.ExistsAsync()) await appendBlob.CreateAsync();

	const int blockSize = 4000000;

	using var reader = new BinaryReader(content);

	//Write in batches
	while (true)
	{
		var buffer = reader.ReadBytes(blockSize);
		if (buffer.Length == 0)
			break;

		using var uploadChunk = new MemoryStream();
		uploadChunk.Write(buffer, 0, buffer.Length);
		uploadChunk.Position = 0;

		await appendBlob.AppendBlockAsync(uploadChunk);
	}

	if (content.CanSeek) content.Seek(0, SeekOrigin.Begin);
}
```

說明:
- `blockSize = 4000000`: 4MiB其實應該是4194304個bytes，但是為了保留點彈性就直接使用4000000 bytes了。
- `BinaryReader`: 這邊是用BinaryReader一次讀N個bytes出來上傳。
- `ploadChunk.Write(buffer, 0, buffer.Length)`: 這邊用另一個`MemoryStream`來準備該次要寫入的資料。
- `AppendBlockAsync`: 這個就是C# SDK提供的非同步方法，其中就是使用`PUT Method`操作。

Note:  
請注意，`這邊跟FileShare的處理稍有不同，因為FileShare必須指定寫入的Range、Append Blob這邊因為本來就是用附加的方式附在檔案尾端，所以不需要再指定位移量`。

## Conclusion
本文說明了`Append Blob`的4 MiB限制以及分批處理的方式，可以避免`RequestBodyTooLarge`的問題。

## References
1. [Error when you write more than 4 MB of data to Azure Storage: Request body is too large](https://learn.microsoft.com/en-us/troubleshoot/azure/general/request-body-large)
2. [Put Blob](https://learn.microsoft.com/en-us/rest/api/storageservices/put-blob)
