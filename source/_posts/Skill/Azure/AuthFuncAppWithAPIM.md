---
title: Azure - Function App authorizaton in API Management
categories:
 - 技術文件
 - Azure
date: 2022/8/15
updated: 2022/8/15
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, Function App]
---

![](/Teamdoc/image/azure/logos/apim.png)

最近公司使用`API Management`(下稱`APIM`)整合所有`Azure`上的APIs，透過統一的入口來達成身分驗證、權限管控及來源限制等安全性目的，避免APIs直接暴露在公有雲上，其中整合的對象之一就是`Function App`，但是透過`APIM`後`Function App`的驗證方式跟一般的稍有不同，本文就在說明`APIM`對於`Function App`是如何進行驗證的。
<!-- more -->

## Purpose
由於在`APIM`設定`Backend API`使用的`Function App`驗證方式跟一般`Functoin URL`不同，本文目的就在說明`APIM`對於`Function App`的驗證機制。

## Function App
首先，先簡單介紹一下`Function App`的兩種驗證方式。
1. 使用URL Parameter
一般狀況下我們會使用下方的`Function URL`方式來取得`Function App`的存取權限，也就是後方`?code=XXXXXXXXXXX`這一串參數。

```
https://[Function App Name].azurewebsites.net/api/TestAPI?code=XXXXXXXXXXX
```

取得方式如下圖:

![](/Teamdoc/image/azure/AuthFuncAppWithAPIM/funcapp1.png)

2. 另一種驗證方式，是在`HTTP Header`帶入`x-functions-key`，帶入的Key值我們可以在下方位置找到，而`APIM`這邊就是使用這種方式對`Function App`進行驗證的。主要的優點是`App Key`比較不需要因為`Function`的變動而跟著更新，而從`HTTP Header`帶入也比較安全、至少不是從URL直接可以看到。

![](/Teamdoc/image/azure/AuthFuncAppWithAPIM/funcapp2.png)

## APIM設定
1. 前一小節在`Function App`建立好`App Key`後，就可以將Key拿到下方`APIM`的`Named Values`建立一組Key-Value對應。

![](/Teamdoc/image/azure/AuthFuncAppWithAPIM/funcapp3.png)

2. 然後再到`Backends`建立後端API的對應(如果有用過應該會已建好)，如下圖。

![](/Teamdoc/image/azure/AuthFuncAppWithAPIM/funcapp4.png)

接著進入`Backend`的`Authorization credentials`建立一組要帶入的Header `x-functions-key`，右方再帶入之前建立好的`Name Valued`名稱即可，如下圖。

Note:
`Name Valued`在`Authorization credentials`這邊必須輸入名稱，實際執行的時候才會將Key值帶入Header喔!

![](/Teamdoc/image/azure/AuthFuncAppWithAPIM/funcapp5.png)

## Conclusion
`APIM`存取`Function App`的驗證方式與一般的不同，如果不是因為手動要調整`APIM`，進一步去察看詳細的設定，不太容易發現原來背後機制是這樣。雖然`APIM`本身針對Azure服務像`Function App`提供了一鍵載入的功能、方便又好用，也可以避免手動時可能發生的疏漏，但是有些狀況不得不用手動的方式進行調整，所以這些背後的運作機制還是需要了解一下才行。

