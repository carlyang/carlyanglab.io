---
title: .NET 6 - JSON反序列化Dynamic物件null屬性值的判斷方式
categories:
 - 技術文件
 - .NET 6
date: 2023/2/22
updated: 2023/2/22
thumbnail: https://i.imgur.com/QpyKkA4.png
layout: post
tags: [.NET 6, Dynamic Object]
---

![](/Teamdoc/image/DotnetCore/net6logo.png)

## Purpose
很多時候我們會貪方便使用`dynamic`物件來處理，但這其實是不安全的作法，尤其是在本文遇到的案例裡，直接使用`JsonConvert.DeserializeObject`反序列化`JSON`資料為`dynamic`造成後續的異常錯誤，本文主要在說明及如何修正。
<!-- more -->

## Prerequisites
本文是使用.NET 6作為開發環境進行練習，請先安裝下列項目。
- 請先安裝.NET 6.0 SDK[[Download]](https://dotnet.microsoft.com/en-us/download/dotnet/6.0)

## Dynamic Object
今日處理程式問題時發現，當我們使用`JsonConvert.DeserializeObject`反序列化`JSON`資料時，如果指定類型為`dynamic`時、有可能造成後續屬性使用`??`判斷值是否為null時造成異常錯誤並拋出中斷執行。

讓我們先看看有問題的程式碼。

```csharp
IEnumerable<DateTime?> TestFunc(List<dynamic>? objList)
{
    var result = new List<DateTime?>();

    try
    {
        objList.ForEach(o =>
        {
            result.Add(o.DateTimeTest ?? DateTime.UtcNow.AddHours(08));
        });
    }
    catch (Exception e)
    {
        Console.WriteLine(e);
        throw;
    }

    return result;
}

var result = TestFunc(JsonConvert.DeserializeObject<List<dynamic>>("[{\"DateTimeTest\":null}]"));
```

上面錯誤會發生在`o.DateTimeTest ?? DateTime.UtcNow.AddHours(08)`這邊，因為反序列化後的物件是`dynamic`的話，`JSON.NET`其實是使用`JToken`來處理各個屬性值，如下圖。

![](/Teamdoc/image/DotnetCore/DynamicPropertyNullRef/dynamic1.png)

`這時如果用C#的雙問號(??)來使用判斷式，會造成詭異的異常錯誤被拋出`，訊息如下。

>Microsoft.CSharp.RuntimeBinder.RuntimeBinderException: 'The best overloaded method match for 'System.Collections.Generic.List<System.DateTime?>.Add(System.DateTime?)' has some invalid arguments'

這是因為屬性被反序列化為`JToken`、它是`JSON.NET`內部處理使用的，.NET SDK的`雙問號(??)`並不認得這類型的資料，所以最後判斷`Invalid arguments`而拋出異常。

## 如何修正
既然是因為反序列化後的`JToken`無法被`雙問號(??)`處理，那我們就改用`primitive type`的方式、直接判斷null來處理即可，於是將程式修正如下。

```csharp
IEnumerable<DateTime?> TestFunc(List<dynamic>? objList)
{
    var result = new List<DateTime?>();

    try
    {
        objList.ForEach(o =>
        {
            result.Add(o.DateTimeTest == null ? DateTime.UtcNow.AddHours(08) : o.DateTimeTest);
        });
    }
    catch (Exception e)
    {
        Console.WriteLine(e);
        throw;
    }

    return result;
}

var result = TestFunc(JsonConvert.DeserializeObject<List<dynamic>>("[{\"DateTimeTest\":null}]"));
```

也就是說，只要回歸到最基本的`==`判斷即可正常處理。

## Conclusion
其實這個問題要被發覺說難似乎也不難，因為實際跑過一輪IDE自然會報錯、也就很容易被發現了。但是實際上卻是滿容易疏忽的，因為`雙問號(??)`概念上我們就會認會它只是簡化`==`的寫法，其實針對不同的`Runtime type`處理結果可能就是完全不同的兩回事，以後還是得多注意一下才行!

