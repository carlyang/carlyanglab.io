---
title: Azure - Products of API Management
categories:
 - 技術文件
 - Azure
date: 2022/5/13
updated: 2022/5/13
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management]
---

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
`API Management`(下稱`APIM`)中的`Product`可以視為一組具有同性質或同一領域的APIs所組成的`產品`，由於這組APIs的使用對象可能僅限於少數幾個群組，故我們就可以建立`Product`與`API`的對應、表示具有某個`Product`的要求才允許存取，所以`Product`也可以看做是一個群組，我們可以根據不同群組設定不同的`Policies`來達到管控的目的。
<!-- more -->

## 設定API檢查訂閱
`Subcription`可以想像為某個訂閱這組`API`的用戶，當他們要來存取這組`API`時就必須先`表明身分`，而這個身分指的就是在`HTTP Header`中帶入的`Subscription Key`，設定如下圖。

1. 我們先進入`APIM` -> 某個`API` -> `Settings` -> `Subscription`、然後勾選`Subscription required`，有需要可以修改`Header name`及`Query parameter name`，可以提高一點安全性，而這兩個一個是帶在`HTTP Header`中、另一個是帶在`URL Query Parameter`，不過是不太建議直接把Key這種機密資訊直接帶在URL上，最後按下儲存即可。

![](/Teamdoc/image/azure/APIMProducts/product1.png)

2. 這時候我們不帶入`Subscription Key`來試打看看，就會發現被擋回來`401 Access Denied`。

![](/Teamdoc/image/azure/APIMProducts/product2.png)

錯誤訊息如下:
>Access denied due to missing subscription key. Make sure to include subscription key when making requests to an API.

## 建立Product
1. 接著我們進入`Products`新建一個`Product`。

![](/Teamdoc/image/azure/APIMProducts/product3.png)

2. 填入各項資訊。
- `Display name`: 顯示名稱。
- `Id`: 預設是顯示名稱的全小寫。
- `Description`: 想要說明的描述。
- `Subscription count limit`: 該`Subscription`的最大連線數上線，不設定則沒有限制。
- `APIs`: 最重要的一項設定，`必須指定要套用Product的API`。

![](/Teamdoc/image/azure/APIMProducts/product4.png)

3. 接著進入剛新建好的`Product` -> `Subscriptions`，就可以看到`Primary`及`Secondary`兩組`Subscription Key`，基本上使用其中一個Key就可以了。最右方則是幾個Key的操作功能，可以在這裡重建、暫停、刪除Key。

![](/Teamdoc/image/azure/APIMProducts/product5.png)

4. 再來，我們回到`Postman`在`Header`中帶入剛建立好的`Subscription Key`、並試打看看，就會發現已經能成功取的回傳`200 Success`。

## Product Policy
經過前面的步驟我們就已經可以使用`Subscription Key`正常存取API了，不過各位一定很好奇為何要多這一個步驟、要多傳`Subscription Key`來驗證身分呢?  
其實除了安全性因素外，最重要的還是`群組的特性`，它可以讓我們方便地根據不同的`Subscription Key`來識別屬於哪一個`Product`及套用的相關`Policies`。  
所以`Subscription Key`就像是`User`、`Product`就像是群組，有需要時我們還可以根據不同的`Subscription Key`進行統計、分析，也可以根據不同的`Product`設定不同的`Policies`。

舉例來說，公司有些人是從公司內部要存取`API`、有些人是屬於外部合作夥伴也要來存取`API`，但是這兩個來源IP是不同的，我們也不能因為多個不同來源就把`API`全開，這也太危險了!所以我們就可以利用兩個`Product`設定不同的`IP Filter Policy`，來達到兩組不同白名單的管控，範例如下。

- 我們先在第一個`Product`加上允許`10.1.1.2`這個IP進來。
```xml
<policies>
    <inbound>
        <base />
        <ip-filter action="allow">
            <address>10.1.1.2</address>
        </ip-filter>
    </inbound>
    <backend>
        <base />
    </backend>
    <outbound>
        <base />
    </outbound>
    <on-error>
        <base />
    </on-error>
</policies>
```

- 我們再到第二個`Product`加上允許`10.1.1.3`這個IP進來。
```xml
<policies>
    <inbound>
        <base />
        <ip-filter action="allow">
            <address>10.1.1.3</address>
        </ip-filter>
    </inbound>
    <backend>
        <base />
    </backend>
    <outbound>
        <base />
    </outbound>
    <on-error>
        <base />
    </on-error>
</policies>
```

這時候我們就可以用這兩個IP試打`Postman`就會發現都正常成功，如果不是這兩個IP的話、就會拿到`401 Access Denied`回應，表示你非法存取被擋回來了。

Note:  
如果進一步交叉測試，我們用第二個`Product`的`Subscription Key`、`在第一個IP上試打`也會被擋回`401 Access Denied`。  
因為當你使用`Subscription Key`時就會套用它所屬的`Product Policy`，所以實際情況是、你用的第一個IP是`10.1.1.2`卻套用第二個`Product Policy`，而第二個`Product Policy`所允許的IP卻是`10.1.1.3`，所以就會被擋回了。

## Conclusion
透過`Product`及`Subscription Key`的設定，我們可以依照不同情境設計不同的`Policies`，達成不同的使用需求、又可以不影響安全性，所以使用`APIM`非常建議一定要搭配`Product`的設計，而且基本上現在的系統都會有安全管控的需求，尤其是當我們用`APIM`統一了`APIs`的入口，那這個入口的管控就更加重要了。

## References
1. [Tutorial: Create and publish a product](https://learn.microsoft.com/en-us/azure/api-management/api-management-howto-add-products?tabs=azure-portal)