---
title: Azure - Protect your Backend API in APIM
categories:
 - 技術文件
 - Azure
date: 2021/11/1
updated: 2021/11/1
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management, Rewrite URL]
---
Author: Carl Yang

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
`APIM`除了負責`Gateway`的工作之外，還有一個重要的特性就是能夠隱藏`Backend API`真正的`URL`，我們可以設計`Frontend API`的`URL Route`與後端不同、藉此保護我們真正在執行動作的服務，本文主要在說明如何使用`APIM`的`Rewrite URL Policy`，來達成保護的目的。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- API Management

## Introduction
在開始之前，要先說明一下`APIM`在轉送`HTTP Request`時的處理方式，所謂的`Frontend API`就是我們用來開放給外部呼叫的`API`，而`Backend API`就是該`Frontend API`對應要轉送過去的目的地，而`Policies`就是整個從`Frontend API`到`Backend API`的中間過程要做的事情，而且是透過`XML`標記語法來設計，如下範例。  

整份`Policies`文件就是透過`inbound`、`backend`、`outbound`、`on-error`等區塊的設定來進行。
- `inbound`: Request進來後的處理。
- `backend`: 指對應的`Backend API`位址。
- `outbound`: 我們收到`Backend API`的Response後要進行的處理。
- `on-error`: 這個則是當`APIM`內部發生錯誤時要處理的部分。

> 所以，`Rewrite URL Policy就是應用在inbound處理的部分!`

```xml
<policies>
  <inbound>
    <!-- statements to be applied to the request go here -->
  </inbound>
  <backend>
    <!-- statements to be applied before the request is forwarded to 
         the backend service go here -->
  </backend>
  <outbound>
    <!-- statements to be applied to the response go here -->
  </outbound>
  <on-error>
    <!-- statements to be applied if there is an error condition go here -->
  </on-error>
</policies>
```

## API對於URL的操作
預設操作下，`APIM`會將`Frontend API`進來的Request後綴(`Suffix`)直接Append在`Backend API`設定的URL後面，然後往`Backend API`送過去，讓我們看個轉送的範例。

```text
Source: https://frontend.azurewebsites.net/api/login -> Target: https://backend.azurewebsites.net/api/login
```

如果我們不做任何改變，`APIM`會直接將`/api/login`這段附加在`Backend API`後端、最後變成`/api/login/api/login`，這樣就會轉送到不存在的`URL`位址。  
所以，我們可以在`Backend API`(如下圖)只設定根路徑為`https://backend.azurewebsites.net`，這樣附加上去後就會是正確的`https://backend.azurewebsites.net/api/login/`。

![](/Teamdoc/image/azure/APIMURLRewrite/backend_api1.png)

## Rewrite URL Policy
根據上一小節的說明，由於不做其他動作會直接進行URL Append，`所以開放這樣的Frontend API出去、等於是將Backend API的URL Route暴露給外部知道`，雖然呼叫端可能還是不知道`URL Root`是哪一個`Website`，但也難講有心人會不會從其他地方得知我們的`Backend API`實際位址，所以這時候就可以利用`Rewrite URL Policy`來將`Frontend API`進來的URL覆寫到另一個後端位址，藉此隱藏真正的URL以保護我們的API。

1. 首先我們要先在`inbound`中設定`Backend API`的根路徑。
![](/Teamdoc/image/azure/APIMURLRewrite/rewrite_url1.png)
```xml
<inbound>
    <base />
    <set-backend-service base-url="https://backend.azurewebsites.net" />
</inbound>
```

2. 然後再選擇加入`Rewrite URL Policy`。
![](/Teamdoc/image/azure/APIMURLRewrite/rewrite_url2.png)

3. 接著我們填入對應要複寫的`Backend API`的後綴(`Frontend API`會自動幫我們抓好)。
![](/Teamdoc/image/azure/APIMURLRewrite/rewrite_url3.png)

最後查看我們的`Policies`文件就會設定如下，這樣一來就會把每個從`https://frontend.azurewebsites.net/api/login`進來的Request轉送到`https://backend.azurewebsites.net/api/auth`去，整串對外的URL都可以受到`APIM`的隱藏及保護。
```xml
<inbound>
    <base />
    <set-backend-service base-url="https://backend.azurewebsites.net" />
    <rewrite-uri template="/api/auth" />
</inbound>
```

## 結論:
其實`APIM`的概念及操作對我們來說真的是很簡單易懂，而且有了`Gateway`特性讓我們可以從中進行不同的處理，像本文的`Rewrite URL`就可以隱藏並保護我們的後端API，避免真正的URL資訊被外部得知。試想想，如果被有心人知道我們後端設計的`URL Route`，那他是不是就有可能繞過`APIM`直打我們的後端API呢?!  
不過，不可否認地單是只有`Rewrite URL`可能安全性還是不太夠，所以可以的話後端API還是再加個`Azure Firewall`或`vNet`等的設定才會有更足夠的保護性，後續有機會再來跟大家分享其他`Azure`可用的保護措施。

## 參考
1. [Policies in Azure API Management](https://docs.microsoft.com/en-us/azure/api-management/api-management-howto-policies)