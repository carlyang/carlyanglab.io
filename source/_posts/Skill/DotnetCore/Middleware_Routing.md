---
title: .NET Core - Middleware routing in ASP.NET Core 3.1
categories:
 - 技術文件
 - .NET Core
date: 2021/06/16
updated: 2021/06/16
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [ASP.NET Core, Middleware, Routing]
---
作者: Carl Yang

![](/Teamdoc/image/DotnetCore/Logo.png)

## Purpose
本文主要在說明如何在不同的`Middleware`分支、將路由導回原本的`API Controller`進行後續處理。
<!-- more -->

## Prerequisites
本文是使用ASP.NET Core 3.1作為開發環境進行練習，請先安裝下列項目。
- 請先安裝ASP.NET Core 3.1 SDK([Download](https://dotnet.microsoft.com/download/dotnet/3.1))

## Introduction
我們在使用`ASP.NET Core`的`API Controller`時，很常需要在前面掛上`Middleware`預先進行一些檢查或處理，在`Middleware`處理完後、再把Request交回給後面的`API Controller`繼續後面的程序。  
但是如果我們同時有多個`Middleware`、要在不同的路由下各別調用時，由於`Middleware`的分支特性，會造成後續路由對應不到的狀況，所以就要再針對不同的`Middleware`分支，另外進行`Routing`及`EndPoints`的設定。

## 內建路由相關的Middlewares
相信有在使用.NET Core的朋友都知道，當我們在`Startup.cs`的`Configure()`中使用`app.UseXXX`或`app.MapXXX`時，其實就是在加入一些`Middleware`，其中有內建的、也可以有我們自訂的，其中跟路由有關的就是`app.UseRouting()`跟`app.UseEndpoints`(其他跟本文無關的先不提)，範例Code如下。

```csharp
app.UseRouting();
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});
```

前者是加入`EndpointRoutingMiddleware`進行路由選擇、後者是加入`EndpointMiddleware`宣告各個Controller所對應的端點，如此`ASP.NET Core`才會利用`Middleware`將每個Request的路由導向正確的Controller端點。

## Middleware的分支特性
關於`Middleware`的分支，其實就是一個個的`Pipeline`管道，當Request進入某一條`Middleware`的`Pipeline`後，就跟其他的無關了。也就是說，我們在主線分支上的處理，並不會跟另一條分支有關係，例如以下的Code。

```csharp
app.MapWhen(context => context.Request.Path.Value.StartsWith("/api/databind", StringComparison.OrdinalIgnoreCase),
    p =>
    {
        p.UseMiddleware<DataBindMiddleware>();
    });
app.UseMiddleware<AuthMiddleware>();
app.UseRouting();
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});
```

當我們進入`MapWhen`的`DataBindMiddleware`分支後，`DataBindMiddleware`處理後的Request都找不到對應的`API Controller`、也就是說後面的`AuthMiddleware`、`app.UseRouting()`及`app.UseEndpoints()`都不會再被執行或找到對應路由，最後就會拿到`404 Not found`的回應。  

這個是很重要的觀念，因為`API Controller`的路由正確對應與否，都關係著Client端能否正常使用，而`Middleware`的分支卻會讓這條路被消滅(好像說得太嚴重XD)，所以就要另外再進行設定才能正常運作。

## 在分支加入路由相關的Middlewares
其實前面說了這麼多，大家應該有發現其實解決方法就是把`app.UseRouting()`及`app.UseEndpoints()`加回`DataBindMiddleware`分支就可以解決`404 Not found`的問題，如下範例。

```csharp
app.MapWhen(context => context.Request.Path.Value.StartsWith("/api/databind", StringComparison.OrdinalIgnoreCase),
    p =>
    {
        p.UseMiddleware<DataBindMiddleware>();

        p.UseRouting();
        p.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    });

app.UseMiddleware<AuthMiddleware>();
app.UseHttpsRedirection();
app.UseRouting();
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});
```

換句話說，如果今天想要在特定路由下避開`AuthMiddleware`的檢查，我們就可以用`app.MapWhen()`加入另一個`Middleware`來處理，但處理完又要可以繼續往特定的`API Controller`路由跑，這時候就必須在分支重新指定路由及端點的`Middleware`來進行後續處理，這是很重要的觀念!

## 結論
其實在寫到這部分的時候特別恍惚了一下，覺得很奇怪為何`DataBindMiddleware`跑完後不會自動再往`API Controller`走，而且直接回應`404 Not found`，思考了一下後才想到`Middleware`的分支觀念，一切就豁然而解了，所以特別紀錄一下本文、也加深印象，免得以後又忘記了，也希望小小的經驗能幫助到需要的朋友。

## 參考
1. N/A