---
title: Vue.js的生命週期
categories:
 - 技術文件
 - Front-End
date: 2018/6/10
updated: 2018/6/10
thumbnail: /Teamdoc/image/vue_lifecycle/VueLogo.png
layout: post
tags: 
 - Vue.js 
 - Lifecycle
---
作者: Carl Yang

# Lifecycle

Vue.js是眾多前端框架中的一種，專注於Web前端視覺化物件的處理與呈現，其中最重要的就是View及Model的處理。而每一份網頁起始時，都會初始化Vue Instance對DOM文件進行處理，故了解Vue Instance的生命週期、對於使用Vue.js開發前端Web的開發人員來說，是非常重要的一環。

對於Vue.js的定義，引述如下:
>Vue.js是一款流行的JavaScript前端框架，旨在更好地組織與簡化Web開發。Vue所關注的核心是MVC模式中的視圖層，同時，它也能方便地取得資料更新，並通過元件內部特定的方法實現視圖與模型的互動。([_Wiki_](https://zh.wikipedia.org/wiki/Vue.js))

<!-- more -->

Note:
**_此圖為Vue 2.0以後的新架構。_**
![](/Teamdoc/image/vue_lifecycle/lifecycle.png)
<center>(資料來源:[_Vue.js_](https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram))</center>

圖片中的中間方塊及圓形是Vue的主要流程、而兩邊白底紅框的部分則是我們可以hook的event，我們可以透過各個階段的event撰寫我們想要做的事情。

以下做個實例:
## 生命週期事件處理
先建立一個Vue Instance、並加入各個events
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>vue-test</title>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
  </head>
  <body>
    <div id="app">{{message}}</div>
    <script>
        var vm = new Vue({
          el: '#app',
          data() {
            return {
              message: 'This is vue test'
            }
          },
          beforeCreate: function() { 
            console.log('beforeCreate: el = ' + this.$el + ', message = ' + this.message);
           },
          created: function() { 
            console.log('created: el = ' + this.$el + ', message = ' + this.message);
            },
          beforeMount: function() { 
            console.log('beforeMount: el = ' + this.$el + ', message = ' + this.message);
            },
          mounted: function() { 
            console.log('mounted: el = ' + this.$el + ', message = ' + this.message);
            },
          //因instance初始化、以下event不會被觸發
          beforeUpdate: function() {
            console.log('beforeUpdate: el = ' + this.$el + ', message = ' + this.message + ', ' + this.PropMsg);
            },
          updated: function() {
            console.log('updated: el = ' + this.$el + ', message = ' + this.message + ', ' + this.PropMsg);
            },
          beforeDestroy: function() { 
            console.log('beforeDestroy: el = ' + this.$el + ', message = ' + this.message);
            },
          destroyed: function() { 
            console.log('destroyed: el = ' + this.$el + ', message = ' + this.message);
            }
        });
		
		vm._data.message = 'update test';
        </script>
  </body>
</html>
```

### 1. beforeCreate & created
透過觀察瀏覽器的console.log，可以發現Vue Instance的變化。
![](/Teamdoc/image/vue_lifecycle/beforeCreate.png)
- beforeCreate: 此階段可被視為Instance初始化之前，el及message屬性皆未被起始。
- created: 此階段實體內的各屬性會被賦予初始值，故可以看到資料中的message已被賦值為This is vue test，但el屬性尚未被初始化，因為el需要與DOM上的Element binding後才會被起始。

Note:
**如果想要在真正Bind到DOM以前做某些動作、例如更改data中的屬性message值，可在此階段操作。**

### 2. beforeMount & mounted
Vue Instance被初始化後、需要被掛載到DOM上，才可以真正對DOM進行操作。
![](/Teamdoc/image/vue_lifecycle/beforeMount.png)
- beforeMount: 尚未掛載、el仍為Undefined。
- mounted: 掛載後表示Vue Instance已開始可對DOM進行相關操作，此時el才會被起始。

Note:
**此時對於Vue Instance來說，才是真正開始能夠透過UI與使用者互動的階段。**

### 3. beforeUpdate & updated
掛載到DOM後就會起始資料的互動，例如使用者修改表單欄位值，此時會觸發update的兩個相關event。我們在new Instance後修改message值為'update test'，再觀察consol就會發現event被觸發。
```javascript
vm._data.message = 'update test';
```
![](/Teamdoc/image/vue_lifecycle/beforeUpdate.png)
- beforeUpdate: 資料改變後被觸發、message值已變為'update test'，但尚未繫結到UI上。
- updated: 此時DOM UI已被更新，使用者可看到文字已變化。

Note:
**Vue Instance被掛載到DOM上之後，所有透過DOM UI與使用者的互動都會觸發這兩個event，如lifecycle圖右偏下的區塊，開發者可以在此時進行相關的邏輯操作。**
![](/Teamdoc/image/vue_lifecycle/UICycle.png)

### 4. beforeDestroy & destroyed
此兩個事件基本上都是交由Vue本身的機制控制即可，開發過程中較少進行這部分的程式撰寫，但如果在特定情況下有必要、仍可利用來處理相關的動作。
此範例於consol中強制執行destroy動作:
```javascript
vm.$destroy()
```
![](/Teamdoc/image/vue_lifecycle/beforeDestroy.png)
- beforeDestroy: 此時Vue Instance還可以使用、尚未被銷毀。
- destroyed: 此時Vue Instance已被銷毀，所有的Bindings、event等等都已解除，如果有Sub-Instance也會一同被回收。

Note:
<font color='red'>**各個觸發事件的型態都是function。**</font>

## porps & Events
如果使用Vue Component撰寫Vue共用元件時，常會使用porps增加對外的繫結屬性，以讓外部能夠傳入相關資料供元件使用。我們加入porps: PropMsg進行演練。
```javascript
	porps: {
		PropMsg: String
	},
```
並在一開始就將PropMsg設值給message，期望中是希望在一開始的時候就由外界透過PropMsg賦值給內部的message使用。
```javascript
	message: this.PropMsg
```
但觀察consol發現原本在created之後就被賦值的message直到mounted後都還是Undefined。
![](/Teamdoc/image/vue_lifecycle/MessageAsign.png)
接下來我們下consol改變PropMsg的值，
但事後發現、<font color='red'>**天啊!!完全沒有更改到message的值，
message根本不會跟PropMsg連動阿!!**</font>
![](/Teamdoc/image/vue_lifecycle/PropAndData.png)
上圖中先將message改為'update test'、於是觸發了兩個update事件，
但再將PropMsg改為'This is vue test'、卻什麼觸發都沒有。

所以事實上，Vue在處理內部的message屬性、與對外介接的porps是兩個八竿子打不著關係的東西，而且事後賦值給PropMsg也不會觸發update的事件，雖然可以在內部使用this.PropMsg存取該屬性值，但他並不適合用來作為資料互動的作用，僅適合一次性在Vue Instance初始化時、由外部賦予不需要在操作過程中變化的資料。

這時候您可能會覺得:<font color='red'>**可是寫成Component就是要能夠共用阿!如果不能在不同地方共用元件、動態給予porps資料，那共用就沒有意義了阿!**</font>

的確，如此一來porps的作用性就降低了，但是有個折衷的方法，就是使用watch!!它可以讓你在每次porps變動時觸發同步的動作、將最新的值賦予內部的message屬性，並觸發update事件以更新DOM UI。

我們加入以下程式碼:
```javascript
  watch: { 
	PropMsg: function(newVal, oldVal) {
	  this.message = newVal;
	}
  },
```
![](/Teamdoc/image/vue_lifecycle/PropWatch.png)
如圖，兩個update事件被觸發了!內部message的值也被更新為props test，DOM UI也就同步更新了。
![](/Teamdoc/image/vue_lifecycle/PropTestWatchResult.png)

結語:
前端的眉毛真的長很多角阿.......(菸)