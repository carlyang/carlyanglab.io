---
title: .NET Core - The field & property serialization between System.Text.Json and Newtonsoft.Json
categories:
 - 技術文件
 - .NET Core
date: 2021/05/24
updated: 2021/05/24
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [.NET Core, System.Text.Json, Newtonsoft.Json, Serialization]
---
作者: Carl Yang

![](/Teamdoc/image/DotnetCore/Logo.png)

## Introduction
最近使用`System.Text.Json`與`Newtonsoft.Json`進行Model序列化時，發現對於Field及Property的結果不同，筆記一下及目前的解決方法供大家參考。
<!-- more -->

## Prerequisites
本文是使用.NET Core 3.1作為開發環境進行練習，請先安裝下列項目。
- 請先安裝.NET Core 3.1 SDK([Download](https://dotnet.microsoft.com/download/dotnet/3.1))

## System.Text.Json
`System.Text.Json`是.NET Core新處理JSON資料的套件並內建在SDK中，只要有安裝SDK即可呼叫使用，且效能比`Newtonsoft.Json`還要好。

## Newtonsoft.Json
或稱JSON.Net，是最廣泛為開發者使用的JSON處理套件，相信大家對它應該都不陌生。

## Field & Property
相信大家對於C# class的Field及Property都不陌生，但是我發現在序列化時`System.Text.Json`竟然不認得Field、反而`Newtonsoft.Json`能夠正常處理，接下來簡單寫個範例。

1. 首先我們先簡單建一個Model Class，其中包含一個Property、一個為Field。

```csharp
public class TestModel
{
    public string MessageProperty { get; set; } = "Property Test";
    public string MessageField = "Field Test";
}
```

2. 接著我們分別使用`System.Text.Json`及`Newtonsoft.Json`對它進行序列化操作。

```csharp
var model = new TestModel();

var data1 = System.Text.Json.JsonSerializer.Serialize(model);

var data2 = Newtonsoft.Json.JsonConvert.SerializeObject(model);
```

3. 從結果可以發現，用`System.Text.Json`序列化的JSON字串不會有Field。

```json
{
    "MessageProperty": "Property Test"
}
```

用`Newtonsoft.Json`就都通吃。
```json
{
    "MessageField": "Field Test",
    "MessageProperty": "Property Test"
}
```
天啊，這也太神奇了!原來我們都被`Newtonsoft.Json`給養壞了...從來沒去想過JSON序列化時會有Field及Property的差異。

4. 於是就去找找看有沒有相關資訊，還真的讓我找到下面這篇解決方法。  
[[#34558]](https://github.com/dotnet/runtime/issues/34558)

原來可以使用`JsonSerializerOptions.IncludeFields`打開它就OK了!

```csharp
var data1 = System.Text.Json.JsonSerializer.Serialize(
    model,
    new JsonSerializerOptions
    {
        IncludeFields = true
    }
);
```

不過，直覺應該沒那麼簡單吧，於是查了一下[[Microsoft Docs]](https://docs.microsoft.com/en-us/dotnet/api/system.text.json.jsonserializeroptions.includefields?view=net-5.0&viewFallbackFrom=netcore-3.1)才發現，`IncludeFields`屬性要到`.NET 5`才有支援阿...這下沒輒了，只好乖乖回去用`Newtonsoft.Json`了。  
>The requested page is not available for .NET Core 3.1. You have been redirected to the newest product version this page is available for.

## 結論
`System.Text.Json`是最新的JSON處理套件，目前`.NET Core`也是主推它來處理JSON操作，不過也因為是新開發的套件，雖然效能又更進步了、但支援度仍然還比不上`Newtonsoft.Json`的完整，而且上述的Field序列化問題，要使用到`.NET 5`才有辦法解決，有需要的朋友可以再斟酌看看要不要用它囉。

## 參考
1. [#34558](https://github.com/dotnet/runtime/issues/34558)
2. [JsonSerializerOptions.IncludeFields Property](https://docs.microsoft.com/en-us/dotnet/api/system.text.json.jsonserializeroptions.includefields?view=net-5.0&viewFallbackFrom=netcore-3.1)