---
title: Azure - Azure Data Explorer(3) - Advanced Operators
categories:
 - 技術文件
 - Azure
date: 2021/7/31
updated: 2021/7/31
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, ADX, Azure Data Explorer]
---
Author: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Purpose
前一篇[[Azure - Azure Data Explorer(2) - Operators]](/2021/07/24/Skill/Azure/Azure_ADX2/)介紹幾個最基礎的運算子，本篇再繼續介紹進一步的運算子操作，例如:計算欄位、彙總欄位等。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Azure Data Explorer

## Introduction
上篇[[Azure - Azure Data Explorer(2) - Operators]](/2021/07/24/Skill/Azure/Azure_ADX2/)介紹的運算子是最小單位的運算子，接下來要介紹的是較複雜操作的運算子，相對於傳統的T-SQL、`ADF`簡化了一些可能需要複雜操作才能夠達成的運算，只用一個指令就可以完成，更為貼近現實情境中的運用，以下就來一一介紹。

## extend
衍生運算子，類似於`T-SQL`的計算欄位，它可透過其他欄位的計算結果、自動產生的欄位，但不同的是、它不像`T-SQL`要事先定義好`Schema`，它可以寫在查詢指令中、並在每次查詢時計算出來。

```sql
external_table("ExternalTable")
| extend Duration = EventProcessedUtcTime - EventEnqueuedUtcTime
| project Duration
```

## summarize
彙總運算子，類似於`T-SQL`的`Group By`指令，可以幫我們依據不同欄位值分群、再針對各群進行不同的計算，例如`count`。

```sql
external_table("ExternalTable")
| summarize log_count = count() by LogType, HappenTime
| project LogType, HappenTime, log_count
```

Note:  
1. `請注意，因為是匯總欄位、只有by以後的欄位才能夠利用project取得，其他的欄位就查不到了`，這點跟`T-SQL`很像。
2. 如果`欄位類型是dynamic、也就是另一個JSON物件的話，summarize是不能使用的`，基本上要`primitive type`的欄位才能用來彙總。

## render
轉譯運算子，就是將查詢結果轉為圖形輸出，例如:長條圖。

```sql
external_table("ExternalTable")
| summarize log_count = count() by LogType, HappenTime
| project LogType, HappenTime, log_count
| render timechart
```

Note:  
1. `render`能夠支援的圖形有很多種，可參考下圖選擇想輸出的圖形。
2. 嘗試下來，這些圖形主要是在`查詢階段快速分析之用`(這也是`ADX`的主要目的)，可以很`方便地邊查邊看圖形，但如果要拿來做成報表、可能就沒有那麼多細節可以調控`。

![](/Teamdoc/image/azure/AzureDataExplorer/render1.png)

## 結論:
本篇介紹的運算子，概略來說可以跟上一篇的基本運算子一起列出，但我最後還是決定分開來講，這是因為上一篇的運算子比較偏向最小單位、最不可再被分割的動作，而這篇介紹的幾個已經偏向上層的應用情境，所以個人覺得還是分開來說明可能較不容易混淆。

## 參考
1. [[Write queries for Azure Data Explorer]](https://docs.microsoft.com/en-us/azure/data-explorer/write-queries)