---
title: Azure - 如何在API Management中使用快取
categories:
 - 技術文件
 - Azure
date: 2022/5/19
updated: 2022/5/19
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management]
---

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
快取是很多系統架構會使用的一種技巧，透過將資料的一小部分或特定條件下的`Subset`暫存在某個地方，然後在一定期間內如果有相同的要求進來，就可以直接使用這個暫存的資料回傳，不僅節省了資料查詢的成本、也降低了網路延遲性，本文的目的就在說明如何在`APIM`中使用快取功能。
<!-- more -->

## Cache
關於`APIM`的快取可以分為兩部分來說明，第一部分是`如何取得`、第二部分是`如何儲存`。

1. How to get
引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/api-management/cache-lookup-policy)如下。
>Use the cache-lookup policy to perform cache lookup and return a valid cached response when available. This policy can be applied in cases where response content remains static over a period of time.

如上引述內容，我們如果想要取得快取內容、必須使用`cache-lookup`原則在`inbound`區塊中設定，如下範例。

```xml
<inbound>
	<base />
	<cache-lookup vary-by-developer="false" vary-by-developer-groups="false" downstream-caching-type="none" must-revalidate="true" caching-type="internal" vary-by-header="" allow-private-response-caching="false">
		<vary-by-query-parameter>version</vary-by-query-parameter>
	</cache-lookup>
</inbound>
```

說明:
- `vary-by-developer`: 是否快取含有開發人員金鑰的要求。
- `vary-by-developer-groups`: 是否按照使用者群組來快取回應。
- `downstream-caching-type`: 是否允許下游快取(快取在瀏覽器或Proxy)，值有`none`(default)、`private`及`public`，分別表示不使用、允許下游私有快取或是允許下游私有及分享快取。
- `must-revalidate`: 當啟用下游快取時，此屬性會開啟或關閉閘道回應中的must-revalidate快取控制指示詞。
- `caching-type`: 有`internal`使用`APIM`內建的快取、或是`external`使用外部快取如`REDIS`等。
- `allow-private-response-caching`: 設定為`true`時可以快取包含`Authorization`標頭的資訊，不過快取權杖反而可能會造成困擾，建議是讓它預設值為`false`不要快取即可。
- `vary-by-header`: 指定標頭值的快取回應。
- `vary-by-query-parameter`: 輸入URL參數以快取回應，內可含多個參數名並以分號區隔。

Note:  
`請務必注意!使用cache-lookup原則必須搭配另一個cache-store原則，這是因為cache-lookup原則只能讓我們讀取快取，但也要有資料存進快取才行，所以必須要搭配另一組cache-store原則`，接下來就要介紹`cache-store`原則。

2. How to store
引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/api-management/cache-store-policy)如下。
>The cache-store policy caches responses according to the specified cache settings. This policy can be applied in cases where response content remains static over a period of time. 

由上可知，`cache-store`原則的快取條件，是依照`cache-lookup`原則設定來儲存、而且必須在`outbound`中設定，`當某個條件下的Resposne已被快取、則會直接使用快取回傳，如果沒有快取則cache-store會將Response儲存進快取資料區`，範例如下。

```xml
<outbound>
	<cache-store duration="seconds" />
	<base />
</outbound>
```

說明:
- `duration`: 設定快取的存活秒數。

## Conclusion
`APIM`能夠幫我們處理快取真的是很方便的功能，不過快取最主要還是用來儲存一些固定`routine`的回應，尤其`cache-lookup`原則只能判斷`Header`或`Query Parameter`，能夠應用的情境就受限了，畢竟我們真正想要快取的條件、基本上都在`HTTP Body`的`JSON`中，所以能夠用這個功能先盡可能減少一些後端API的`loading`，也是一個不錯的選擇，如果要更進階一點的應用就要使用其他方式了。

## References
1. [Get from cache](https://learn.microsoft.com/en-us/azure/api-management/cache-lookup-policy)
2. [Store to cache](https://learn.microsoft.com/en-us/azure/api-management/cache-store-policy)