---
title: Azure - How to get IoT Hub metadata in Stream Analytics
categories:
 - 技術文件
 - Azure
date: 2022/3/10
updated: 2022/3/10
thumbnail: https://i.imgur.com/d0birrt.png
layout: post
tags: [Azure, Stream Analytics, Event Hub]
---
Author: Carl Yang

![](/Teamdoc/image/azure/logos/stream_analytics.png)

## Purpose
經過[[上一篇]](/2022/03/09/Skill/Azure/StreamAnalyticsIPFilter/)的介紹可以了解到，`IoT Hub`可透過底層的`Event Hub`內建端點存取資料，特別是在Azure上的各種服務想要略過`IP Filter`的阻擋的情境，但透過`Event Hub`撈到的資料內容卻沒有原本`IoT Hub`的資訊、例如:Device ID，本文接續上一篇後、說明如何調整`Azure Stream Analytics`(以下簡稱`ASA`)的查詢語法，讓原本的`IoT Hub`資訊可重新被查出來使用。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- IoT Hub
- Stream Analytics

## Introduction
[[上一篇]](/2022/03/09/Skill/Azure/StreamAnalyticsIPFilter/)的文末有說明，在`ASA`改用`Event Hub`撈取`IoT Hub`的資料後，會有找不到原本`IoT Hub`資訊的狀況，尤其是上傳資料內的`Device ID`、基本上它是不可或缺的重要資訊，試想如果成千上百、甚至更多的IoT裝置上傳的資料，無法得知是由哪台裝置上傳的，不僅無法識別、分類、分析資料，還浪費了雲端上的運算資源(講白了就是白花錢)，真的是非常不好的影響。所以本文主要介紹三個部分，如何`重新取得IoT Hub的中繼資料`、如何`重構回原本的IoTHub JSON物件`以及如何`避免重複的EventHub資訊出現`，全都是關於`ASA`查詢語言的調整，如果您並非要找這方面的資訊，建議可以先略過本文。

## MetaData
首先要來介紹到底改用`Event Hub`拿到的資料是怎麼回事呢?  
其實就是`什麼資訊都沒有!`就是只有資料及`Event Hub`本身自帶的三個屬性。讓我們先來看看原本`IoT Hub`撈出來的資料。

```json
{
  //...略
  "EventProcessedUtcTime": "2022-03-09T13:00:25.0389593Z",
  "PartitionId": 1,
  "EventEnqueuedUtcTime": "2022-03-09T13:00:24.978Z",
  "IoTHub": {
    "MessageId": null,
    "CorrelationId": null,
    "ConnectionDeviceId": "xxxxx",
    "ConnectionDeviceGenerationId": "123456123456123456",
    "EnqueuedTime": "2022-03-09T13:00:24.969Z",
    "StreamId": null
  }
}
```

`EventProcessedUtcTime`、`PartitionId`、`EventEnqueuedUtcTime`三個屬性是`Event Hub`本身就自帶一定都會有、可以先略過不管，但`IoTHub`的JSON物件內容就是重要的`IoT Hub`資訊，`Device ID`就是其中的`ConnectionDeviceId`。  
但如果是從`Event Hub`撈的資料就只有`EventProcessedUtcTime`、`PartitionId`、`EventEnqueuedUtcTime`三個屬性而已!不會有`IoTHub`區塊。

## Stream Analytics Query - Get MetaData
為了能夠重新取得`IoTHub`區塊的資訊，我們需要調整一下查詢語法、加上`GetMetadataPropertyValue(ASAInput, '[EventHub].[IoTConnectionDeviceId]')`，就可以查出上傳的`Device ID`。

```sql
WITH IoTHubSource as 
(
 select
    *,
    GetMetadataPropertyValue(ASAInput, '[EventHub].[IoTConnectionDeviceId]') as [DeviceId]
 from ASAInput
)

-- Get Data and save to storage blob
select * into [BlobOutput]] from IoTHubSource
```

預設`Event Hub`的全部資訊如下，我們可以發現`IoTConnectionDeviceId`其實就被藏在裡面。

```json
{
  //...略
  "EventHub": {
    "EventEnqueuedUtcTime": "2022-03-08T16:07:33.6920000Z",
    "PartitionId": 3,
    "Publisher": "",
    "PartitionKey": "",
    "SequenceNumber": 5,
    "Offset": "25448",
    "IoTConnectionDeviceId": "xxxxxxxx",
    "IoTAuthMethod": "{\"scope\":\"device\",\"type\":\"sas\",\"issuer\":\"iothub\",\"acceptingIpFilterRule\":null}",
    "IoTAuthGenerationId": "123456123456123456",
    "IoTEnqueueTime": "2022-03-09T13:00:24.969Z",
    "IoTConnectionModuleId": "",
    "IoTInterfaceName": "",
    "IoTMessageSource": "Telemetry",
    "IoTDataSchema": "",
    "IoTSubject": ""
  }
}
```

所以如果我們想要撈特定`Device ID`的資料的話，可以加上`where`條件篩選特定裝置的資料出來，再將查詢調整如下。

```sql
WITH IoTHubSource as 
(
 select
    *,
    GetMetadataPropertyValue(ASAInput, '[EventHub].[IoTConnectionDeviceId]') as [DeviceId]
 from ASAInput
 where EventHub.IoTConnectionDeviceId = 'xxxxxx'
)

-- Get Data and save to storage blob
select * into [BlobOutput]] from IoTHubSource
```

Note:  
由於預設`EventHub`資訊是不會出現在資料內的，是因為我們呼叫`GetMetadataPropertyValue()`才被加上的，所以呼叫後才可以用`where EventHub.IoTConnectionDeviceId = 'xxxxxx'`來篩選特定`Device ID`的資料。

## Refactoring to IoT Hub JSON Object
在本文的應用情境中，因為是系統上線後才改用`Event Hub`撈資料，所以發生舊有格式不能吃新格式的問題，但又不太可能因為格式問題一條龍所有相關系統都改下去，所以`需要將新格式重構回舊的IoTHub JSON物件`，於是我們可以再利用`ASA`的`User Defined Function(以下簡稱UDF)`來將`EventHub`的格式轉回`IoTHub`的JSON物件、同時也調整屬性名稱，可建立`ASA UDF`如下圖位置。

![](/Teamdoc/image/azure/StreamAnalyticsEventHub/udf1.png)

我們改將整個`GetMetadataPropertyValue()`取得的Metadata物件輸入`UDF`中，再將各屬性重新對應回`IoTHub`的JSON物件、最後回傳JSON物件，這樣Query結果就可以重構為原本`IoTHub`的物件結構，調整如下。

```sql
WITH IoTHubSource as 
(
 select
    *,
    udf.GetIoTHubObject(GetMetadataPropertyValue(ASAInput, '[EventHub]')) as [IoTHub]
 from ASAInput
 where EventHub.IoTConnectionDeviceId = 'xxxxxx'
)

-- Get Data and save to storage blob
select * into [BlobOutput]] from IoTHubSource
```

Note:  
雖然說已經可以重構回原本的資料格式，但畢竟是改從`EventHub`資訊對應回來有些屬性還沒有找到，但在本文的情境中倒是沒有使用到這些欄位，所以就直接先壓null值。

```javascript
function GetIoTHubObject(metaData) {
    var info = {
        MessageId : null,
        CorrelationId : null,
        ConnectionDeviceId : metaData.IoTConnectionDeviceId,
        ConnectionDeviceGenerationId : metaData.IoTAuthGenerationId,
        EnqueuedTime : metaData.IoTEnqueueTime,
        StreamId : null
    };
    return info;
}
```

## Duplicated Event Hub Information
但是經過查詢後還是會發現、雖然`IoTHub`資訊出現了、但`EventHub`仍然會被撈出來、這樣還是跟原本的格式不同，這是因為我們在select欄位時使用了`星號wildcard`撈取所有欄位，如果不希望出現`EventHub`區塊、那就`必須將所有屬性明確指定出來`，調整如下。

```sql
WITH IoTHubSource as 
(
 select
    [PropertyA],
    [PropertyB],
    [PropertyC],
    udf.GetIoTHubObject(GetMetadataPropertyValue(ASAInput, '[EventHub]')) as [IoTHub]
 from ASAInput
 where EventHub.IoTConnectionDeviceId = 'xxxxxx'
)

-- Get Data and save to storage blob
select * into [BlobOutput]] from IoTHubSource
```

## Compatibility Level
調整過程中發現無論我怎麼賦予欄位新名稱、它永遠都會是小寫，這是因為舊版的`ASA`查詢不支援導致，但隨著版本更新目前將相容性層級調整至1.2版就沒有這個問題了，設定位置如下圖。

![](/Teamdoc/image/azure/StreamAnalyticsEventHub/compatibility_level1.png)

## 結論:
本文說明調整資料來源為`Event Hub`後、如何取得原本的`IoT Hub`資訊及重構資料格式，其實如果有其他的`ASA`應用情境，也可以參考`UDF`的作法(詳細的`UDF`使用教學可以詳閱下方的參考連結)，也是做個紀錄供各位朋友經驗參考，畢竟除了微軟文件以外`ASA`的使用文章真的是不太多。

## 參考
1. [[GetMetadataPropertyValue (Azure Stream Analytics)]](https://docs.microsoft.com/en-us/stream-analytics-query/getmetadatapropertyvalue)
2. [[JavaScript user-defined functions in Azure Stream Analytics]](https://github.com/MicrosoftDocs/azure-docs/blob/main/articles/stream-analytics/stream-analytics-javascript-user-defined-functions.md)