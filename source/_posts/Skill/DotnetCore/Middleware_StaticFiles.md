---
title: .NET Core - Static Files
categories:
 - 技術文件
 - .NET Core
date: 2019/01/13
updated: 2019/01/13
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [.NET Core, Static Files]
---
作者: Carl Yang

![](/Teamdoc/image/DotnetCore/Logo.png)

## 靜態檔案
靜態檔案指我們會去使用的檔案，這些檔案內容本身並不會動態變化，例如HTML、*.js、*.css、JPG、PNG...等等，通常這些檔案都被放在網站目錄中的資料夾，當Client發送請求後、如果有載入該檔案，就會被傳送到Client端的瀏覽器中、以供順利瀏覽或執行Scripts。

根據預設這些靜態檔案會被放在ASP.NET Core的wwwroot的資料夾內，但ASP.NET Core如果沒有開放使用這些資源，Client端就無法存取這些資源，這主要是安全性上的考量，正常來說也不應該隨便開放靜態檔案任憑外界存取，而是有需要時才開放。本文主要著墨在如何設定靜態檔案資料夾、修改預設及其他資料夾的存取。
<!-- more -->

## 新增靜態HTML、PNG檔
1. 我們先在wwwroot資料夾中新增images資料夾、並將Logo.png檔案放進去。
![](/Teamdoc/image/DotnetCore/static_files/staticfiles1.png)

2. 接著我們再新增一個純default.html檔案，並在其中加入<img>連結剛新增的PNG檔。
```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>This is static default HTML</title>
</head>
<body>
This is static default HTML and image
    <img src="~/images/Logo.png" alt="ASP.NET Core" />
</body>
</html>
```

3. 這時候我們來執行看看http://localhost:51610/default.html ，卻發現找不到網頁，如下圖。這是因為如果我們不設定使用StaticFiles的話，ASP.NET Core是不允許存取靜態檔案的。
![](/Teamdoc/image/DotnetCore/static_files/staticfiles2.png)

4. 我們在Startup.cs的Configure()中加入下列一行設定，向ASP.NET Core註冊一個Middleware、以使用靜態檔案。
```csharp
public void Configure(IApplicationBuilder app, IHostingEnvironment env)
{
	//...

	app.UseStaticFiles();
	
	//...
}
```

5. 接著我們再連一次http://localhost:51610/default.html ，就可以顯示HTML及裡面的圖片了。
![](/Teamdoc/image/DotnetCore/static_files/staticfiles3.png)

## 指定網站目錄
wwwroot是ASP.NET Core的預設網站目錄，但有的時候其實並不是那麼合適，因為IIS如果不改設定、其<font style="color:red;">預設路徑也是在C:\wwwroot 中，如果我們又將網站架設在IIS中，這時候路徑就會變成C:\wwwroot\wwwroot，這很容易讓人困惑</font>。所以有的時候還是必須調整預設網站目錄。

1. 調整預設目錄的方法也很簡單，就是在Program.cs中的CreateWebHostBuilder加入UseWebRoot即可。
```csharp
public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
	WebHost.CreateDefaultBuilder(args)
		//指定另一個資料夾為網站根目錄
		.UseWebRoot(@"website")
		.UseStartup<Startup>();
```

2. 接著我們再將稍早新增的圖片及default.html移至website資料夾中，即可順利顯示網頁及圖片。
![](/Teamdoc/image/DotnetCore/static_files/staticfiles3.png)

## 設定指定的靜態檔案目錄
上述的設定有個問題，就是只能根據設定的網站目錄存取靜態檔案，但實際上需要存取的目錄不會只有一個，例如: node_modules資料夾，如果不能開啟另外的資料夾，那Client端一樣無法存取，這時候我們就必須要多指定開啟另一個靜態檔案資料夾。

1. 我們先加入新資料夾Website2、再加入default2.html、HTML內的圖片連結一樣連到原Website\images\Logo.png
![](/Teamdoc/image/DotnetCore/static_files/staticfiles6.png)

2. 接著在Startup.cs的Configure()中加入UseStaticFiles()並指定StaticFileOptions以允許新靜態檔案資料夾路徑。
```csharp
public void Configure(IApplicationBuilder app, IHostingEnvironment env)
{
	//...
	
	app.UseStaticFiles();
	app.UseStaticFiles(new StaticFileOptions()
	{
		FileProvider = new PhysicalFileProvider(
			Path.Combine(env.ContentRootPath, @"website2")),
		RequestPath = new PathString("/website2")
	});

	//...
}
```

- default2.html
```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>This is the second default HTML</title>
</head>
<body>
This is the second default HTML and linked to Website\image\Logo.png
<br/>
    <img src="/images/Logo.png" alt="ASP.NET Core" />
</body>
</html>
```

3. 我們來連連看新的路徑http://localhost:51610/website2/default2.html ，即可瀏覽default2.html、且同時可連到原本的website目錄中的圖片，表示兩個靜態資料夾都已被設定允許存取。
![](/Teamdoc/image/DotnetCore/static_files/staticfiles5.png)

## 預設靜態檔案
上述的範例我們都要手Key位址去連靜態檔案，通常要使用者自己手動去Key in是不太可行的、也不友善，較佳的作法是當User請求網站時就能夠給予特定的網頁瀏覽、例如我們新增的default.html，這樣User只要連一個位址http://localhost:51610 就可以瀏覽到我們的首頁。

- 設定預設的靜態檔案在Startup.cs的Configure()中加入UseDefaultFiles()，而ASP.NET Core預設檔名如下列:
1. default.htm
2. default.html
3. index.htm
4. index.html

```csharp
public void Configure(IApplicationBuilder app, IHostingEnvironment env)
{
	//...
	//要註冊在UseStaticFiles之前才有效
	app.UseDefaultFiles();
	app.UseStaticFiles();
	//...
}
```

- 接著我們連看看http://localhost:51610 ，就會直接顯示\website\default.html了。
![](/Teamdoc/image/DotnetCore/static_files/staticfiles3.png)

Note:<br/>
<font style="color:red;">這裡必須特別注意，app.UseDefaultFiles()要註冊在app.UseStaticFiles()之前才會有效，否則Request進來會先跑UseStaticFiles，如果找不到就會Response、不會再跑UseDefaultFiles。</font>

- 如果我們的預設檔名不在上列的四種內、是我們自訂的網頁名稱，我們就要在UseDefaultFiles時傳入DefaultFilesOptions指定我們的預設檔名。
1. 我們先刪除原本的default.html、再新增一個檔名為MainPage.html的靜態檔案，內容如下。
![](/Teamdoc/image/DotnetCore/static_files/staticfiles7.png)

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>This is MainPage HTML</title>
</head>
<body>
    This is MainPage HTML and image
    <br />
    <img src="/images/Logo.png" alt="ASP.NET Core" />
</body>
</html>
```

2. 接著我們再調整一下註冊方式、傳入DefaultFilesOptions以設定自訂的預設靜態檔案。
```csharp
public void Configure(IApplicationBuilder app, IHostingEnvironment env)
{
	//...

	var defaultFiles = new DefaultFilesOptions();
	defaultFiles.DefaultFileNames.Add(@"MainPage.html");

	app.UseDefaultFiles(defaultFiles);
//...
}
```

3. 接著我們再連連看http://localhost:51610 ，就可以看到已經連到自訂的MainPage.html了。
![](/Teamdoc/image/DotnetCore/static_files/staticfiles8.png)

Note:<br/>
<font style="color:red;">請特別注意，由於default.html是ASP.NET Core內建允許的靜態檔名，請務必移除前幾個步驟所增加的default.html、才能夠連MainPage.html，否則還是會自動讀取default.html的內容。</font>

## 瀏覽檔案清單
大部分時候不應該讓使用者可以直接瀏覽網站內的檔案，但有些時候又不得不開放，例如:透過http存取File Server，這時候還是要開啟某個資料夾的瀏覽功能才行。

- 假設我們要開放website這個資料夾，我們可以透過app.UseFileServer()註冊FileServer的Middleware、並傳入FileServerOptions設定要開放的資料夾。

1. Website\images是我們要開放瀏覽的資料夾，讓外面可以存取圖片。
2. RequestPath = new PathString("/Images")是讓User可以透過http://localhost:51610/Images 瀏覽檔案。
3. EnableDirectoryBrowsing則是開放資料夾瀏覽權限。

```csharp
app.UseFileServer(new FileServerOptions()
{
	FileProvider = new PhysicalFileProvider(
		Path.Combine(env.ContentRootPath, @"Website\images")
	),
	RequestPath = new PathString("/Images"),
	EnableDirectoryBrowsing = true
});
```

- 最後我們連看看http://localhost:51610/Images ，就可以瀏覽資料夾的內容，<font style="color:red;">未來如果有需要連結檔案資源時，即可透過http://localhost:51610/Images/Logo.png 連結</font>。
![](/Teamdoc/image/DotnetCore/static_files/staticfiles9.png)

## Conclusion
.NET Core可以讓我們很簡單地透過Middleware的設定、控制靜態檔案的存取、也有助於前後端的SoC架構，是很好的設計模式。不過通常在File Server的設定上，比較不會為了檔案的存取、還去特別寫一隻ASP.NET Core Application來支援，這部分使用上可能較為冷門一點。

## References
[Static files in ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/static-files?view=aspnetcore-2.2)