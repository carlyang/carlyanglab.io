---
title: Azure OpenAI Service (2) - 服務建立及API調用
categories:
 - 技術文件
 - Azure
date: 2023/3/10
updated: 2023/3/10
thumbnail: https://i.imgur.com/xt5yNCJ.png
layout: post
tags: [Azure, Azure OpenAI Service]
---

![](/Teamdoc/image/azure/logos/openai.png)

## Purpose
[[上一篇]](/2023/03/05/Skill/Azure/OpenAI1/)簡單說明了關於`Azure OpenAI Service`的`Region`、`Pricing`及`GPT-3`模型，但是要建服務時發現還要另外跟微軟申請才可以，於是就填了表單送出、打算等10天後再繼續，沒想到竟然這麼快就收到通過的回覆，真是不得不稱讚一下微軟!本文的目的就是接續上一次的步驟，說明接下來如何建立`Azure OpenAI Service`以及如何使用它的API。
<!-- more -->

## Creation
申請通過後，微軟`Cognitive Services Gating Support`會有一封通知信，告訴你你的訂閱已經`Onboarding`如下圖，並且會告知通過使用的帳號資訊(訂閱ID)。`信中有提供了三個線上文件連結，很重要!裡面有包含如何部署模型、如何調用API的教學及範例，對於像我第一次使用的人來說非常重要`，建議大家可以先看過一遍再開始。

![](/Teamdoc/image/azure/OpenAI2/onboarding1.png)

接下來就開始建立`Azure OpenAI Service`的步驟吧。

1. 進入Create填寫必填資訊。

![](/Teamdoc/image/azure/OpenAI2/create1.png)

說明:  
- `Resource Group`: 因為我目前是POC使用，所以我選擇新建一個POC Group來放，就看各自的需求而定。
- `Region`: 我是選East US(目前只有美國幾個區域及西歐可以選用)。
- `Name`: 服務名稱。
- `Pricing tier`: Standard 0，也是目前唯一可以使用的層級。

2. 建立Tags

![](/Teamdoc/image/azure/OpenAI2/create2.png)

說明:  
這邊就看各自的需求，`Tags`可以幫助未來`Azure`服務的查詢與統計。

3. Review & Create

![](/Teamdoc/image/azure/OpenAI2/create3.png)

說明:  
最後就是再檢查一下資訊有沒有問題，然後按下`Create`按鈕就可以開始建立及部署了。

## 部署GPT-3 Model
[[上一篇]](/2023/03/05/Skill/Azure/OpenAI1/)有說明`GPT-3`模型內的`Davinci`是功能最豐富的模型(最佳速度是`Ada`模型)，所以我們就先部署`GPT-3`模型內的`Davinci`吧!

1. 首先，讓我們先進入`Azure OpenAI Studio`部署`GPT-3`模型，我們可以從服務首頁的連結進入，如下圖。

![](/Teamdoc/image/azure/OpenAI2/openai_studio1.png)

2. 再來我們先進入`GPT-3 Playground`(這邊還有另一個`ChatGPT Playground(preview)`，目前是預覽版、後續有機會再介紹)。

![](/Teamdoc/image/azure/OpenAI2/openai_studio2.png)

3. 接著進入`Deployments` -> `Create new deployment`。

![](/Teamdoc/image/azure/OpenAI2/openai_studio3.png)

4. Model我們選最新版的`Davinci 003`，然後輸入自訂的部署名稱(這個部署名稱請先記下來、後續試打`REST API`會用到)。

![](/Teamdoc/image/azure/OpenAI2/openai_studio4.png)

說明:  
模型命名有一定的規則，方便我們一眼辨識出來模型的作用，如下。

- `capability`: 此模型的能力，例如: `GPT-3`是使用text分析文字、`Codex`使用code表示分析程式碼。
- `family`: 屬於哪一系列的相關性模型，例如: `davinci`。
- `input-type`: 選填性質，只有內嵌模型才會遇到，例如: `doc`、`query`。
- `identifier`: 版本號碼。

```
{capability}-{family}[-{input-type}]-{identifier}
```

5. 部署完以後就可以看到部署完成的相關模型了。

![](/Teamdoc/image/azure/OpenAI2/openai_studio5.png)

6. 接著我們回到`GPT-3 Plaground`就可以開始輸入一些文字跟他開始對話，也有一些現成的Examples可使用。

![](/Teamdoc/image/azure/OpenAI2/openai_studio6.png)

說明:  
初次見面，當然還是要有點禮貌!哈哈~  
它的回答其實還滿人性化的，可以很明顯發現跟以往硬梆梆的AI回應不同，而且可能每次的回答會不一樣喔!

## 呼叫REST API
既然是服務當然少不了`REST API`的使用囉!這樣可以提供各式各樣不同的應用場景，例如: 聊天機器人、智能客服等，這樣我們就可以透過不同的前端介面如Web、Winform、Bot等、調用後端API來取得回應，接下來介紹如何使用`Postman`來模擬調用`REST API`的方方式。

1. 首先，`REST API`的URL如下範例。

```url
https://{your-resource-name}.openai.azure.com/openai/deployments/{deployment-id}/completions?api-version={api-version}
```

說明:  
詳細的說明可參考[[MS Doc]](https://learn.microsoft.com/en-us/azure/cognitive-services/openai/reference#rest-api-versioning)。
- `{your-resource-name}`: 這就是我們剛建立的`Azure OpenAI Service`名稱，以我剛建好的為例、完整的`FQDN`就是`openaipoc1.openai.azure.com`。
- `{deployment-id}`: 這邊就是之前說要先記下來的Deployment name，以我剛建好的為例、就是`davinci003-POC`。
- `{api-version}`: 這邊是指`REST API`的版本號碼，也就是目前`Azure OpenAI Service`的可用版本號碼，`2022-12-01`。

到這裡我們就可以組出完整的URL如下。

```url
https://openaipoc1.openai.azure.com/openai/deployments/davinci003-poc/completions?api-version=2022-12-01
```

2. 接下來我們還需要驗證`REST API`的存取權，而`Azure OpenAI Service`的`REST API`驗證是依賴每個`Request Header`中帶入的`api-key`，值可以在下圖中位置找到。

![](/Teamdoc/image/azure/OpenAI2/api_key1.png)

說明:  
- 依照往例`Azure`都會提供兩組Key(一組使用、一組備用)，`Azure OpenAI Service`這邊也是一樣，我們只要使用其中一個Key帶入`api-key`即可。
- 我們也可以使用上方的`Regenerate Key1`及`Regenerate Key2`按鈕、在這邊重新產Key1或Key2。

3. `REST API`一樣是採用`JSON`格式，並且`Request body`需要依照它的各屬性值設定來調用，如下範例。

```json
{
    "prompt": "請問你幾歲?",
    "max_tokens":2048
}
```

說明:  
- `prompt`: 字串或字串陣列，也就是我們想要輸入的語句。
- `max_tokens`: 這邊指的是回應時的`Token`上限，最大是2048(不同模型會不同上限，內嵌模型可到4096個`Token`)。

Note:  
有些翻譯會把這邊的`Token`翻譯成權杖，但個人是覺得不太合適，因為權杖隱含有身分識別、並且有允許/拒絕通行的實質意義，但這邊比較偏向`字詞`、也就是整句語句拆分的最小單位，所以我還是使用英文單字來撰寫。

4. 最後，我們把所有資訊輸入`Postman`來試打看看。

![](/Teamdoc/image/azure/OpenAI2/postman1.png)

Question:
```json
{
    "prompt": "請問如何減肥?",
    "max_tokens":2048
}
```

Answer:
```json
{
    "id": "cmpl-6sQpTSeCTAGX3a6zmTodH3mRQe72d",
    "object": "text_completion",
    "created": 1678430863,
    "model": "text-davinci-003",
    "choices": [
        {
            "text": "?\n\n1. 尋求專業醫療建議: 徵詢個人的醫療專家以獲得最合適的減肥計劃。\n\n2. 適當地鍛煉: 選擇適當的鍛煉方式來燃燒脂肪，例如有氧運動(跑步、游泳、單輪轉等)、肌耐力訓練(瑜伽、健身運動和器械訓練)等。\n\n3. 遵守健康的飲食習慣: 從日常饗食中減掉一些高脂肪的食品，並適當地增加一些水果和蔬菜的攝入量，以達到減肥的目的。\n\n4. 睡眠足夠: 睡眠是維持正常生理機能的重要條件，充足的睡眠可以有效防止低血糖的出現，以避免對飲食的依賴。\n\n5. 保持積極的心態: 改變不良的飲食習慣往往會出現困難，但堅持下去將會得到健康的體態和美麗的体态。因此，保持積極正面的心態可以為減肥助一臂之力。",
            "index": 0,
            "finish_reason": "stop",
            "logprobs": null
        }
    ],
    "usage": {
        "completion_tokens": 605,
        "prompt_tokens": 16,
        "total_tokens": 621
    }
}
```

說明:  
- `id`: 每一次的呼叫都會有唯一的id值。
- `object`: 執行的物件，例如: `text_completion`、`embedding`。
- `model`: 該次使用的模型。
- `choices`: `Azure OpenAI Service`有可能回應多個內容，所以這邊是以陣列來回應。
- `usage`: 這邊則是該次使用的所有`Token`數量。

## Conclusion
本文先簡單介紹如何調用`Azure OpenAI Service`的`REST API`，可以撰寫程式調用後端API、也可以直接使用`Azure OpenAI Studio`來指定模型、部署，也可以即時使用`Playground`功能來進行對話，其中也包含一些範例可以使用。不過，目前僅先部署`GPT-3`的`davinci`模型，雖然是功能性最佳的模型、但是速度就真的是比較慢，這就要看大家怎麼使用了，如果是希望能快速回覆的話可以改用`Ada`模型。

## References
1. [Quickstart: Get started generating text using Azure OpenAI Service](https://learn.microsoft.com/en-us/azure/cognitive-services/openai/quickstart?pivots=programming-language-studio)
2. [Azure OpenAI Service REST API reference](https://learn.microsoft.com/en-us/azure/cognitive-services/openai/reference)