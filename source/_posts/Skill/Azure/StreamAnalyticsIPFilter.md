---
title: Azure - Stream Analytics connects to IoT Hub through IP Filter
categories:
 - 技術文件
 - Azure
date: 2022/3/9
updated: 2022/3/9
thumbnail: https://i.imgur.com/d0birrt.png
layout: post
tags: [Azure, Stream Analytics, IoT Hub, IP Filter]
---
Author: Carl Yang

![](/Teamdoc/image/azure/logos/stream_analytics.png)

## Purpose
本文主要在介紹如何讓`Azure Stream Analytics`(以下簡稱`ASA`)通過`IoT Hub`的`IP Filter`設定。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- IoT Hub
- Stream Analytics

## Introduction
當我們希望建立安全的`Azure`服務環境，`Azure Firewall`中有一個功能就是`IP Filter`，它內建於各服務的`Networking`中、讓開發者可以方便地設定IP白名單，透過`CIDR`的格式來限制可存取服務的來源IP。  
本文以`IoT Hub`為例、說明當設定了`IP Filter`後，造成`ASA`無法順利連線`IoT Hub`拉資料時的解決辦法，能夠同時達成服務對外溝通的安全性、也能夠讓Azure平台上、相同訂閱內的其他服務順利通過`IP Filter`的限制。

![](/Teamdoc/image/azure/StreamAnalyticsIPFilter/ip_filter1.png)

## Event Hub
首先，要先說明一點、`IoT Hub`的底層其實就是`Event Hub`，它只是針對IoT應用建構了更多、更進階、更符合IoT領域的許多功能，所以我們可以在`IoT Hub`內看到許多`Event Hub`的影子，像是`Built-in endpoints`、`Consumer Group`...等，都是原本`Event Hub`就有的東西，而`IP Filter`就是在整個`IoT Hub`服務的Network上建構了過濾IP的功能，`所以IP Filter可以同時過濾IoT Hub或Event Hub的連線IP`(這點很重要)，如下圖所示兩個紅框、上方是開啟`IoT Hub`的過濾、下方是開啟`Event Hub`的過濾。

![](/Teamdoc/image/azure/StreamAnalyticsIPFilter/ip_filter2.png)

由於`IoT Hub`需要開放給對外連線才能夠上傳資料，但對於內建端點的底層`Event Hub`來說卻不需要，所以當我們使用`ASA`連線`IoT Hub`時、因為`ASA`是屬於Azure內網的服務，就`可以透過內建端點直接連線底層的Event Hub以通過IP Filter的過濾，但前提是IP Filter作用於內建端點的設定要關閉`，引用微軟文件如下。

> If you must use Azure Stream Analytics (ASA) to read messages from an IoT hub with IP filter enabled, disable the Apply IP filters to the built-in endpoint option, and then use the event hub-compatible name and endpoint of your IoT hub to manually add an Event Hubs stream input in the ASA.

## ASA Input
經過前小節的說明，我們可以瞭解到想要通過`IoT Hub`的`IP Filter`過濾，就需要在`ASA`的Input指定類型為`Event Hub`、而不是原本的`IoT Hub`。不過，設定過程中還是不斷地try and error才找出真正的設定方式(不得不說，微軟說明真的是不夠清楚明瞭)，步驟說明如下。

1. 首先我們先進到`IoT Hub`的`Built-in endpoints`，把`Event Hub-compatible name`、`Shared access policy`、`Event Hub-compatible endpoint`三個資訊Copy出來後續會用到，再新增一個`Consumer Group`等下給`ASA`使用。

![](/Teamdoc/image/azure/StreamAnalyticsIPFilter/built-in_endpoints1.png)

2. 再來我們進到`ASA`的Input、新增一個Source為`Event Hub`的輸入。

![](/Teamdoc/image/azure/StreamAnalyticsIPFilter/asa_input1.png)

3. 各欄位說明如下
    - Input alias: 必填，一個方便識別的名稱就好。
    - 選擇`Provide Event Hub settings manually`手動填寫設定。
    - Event Hub namespace: 必填，這時候必須回到我們先前Copy出來的`Event Hub-compatible endpoint`字串，然後抓出`[name space]`位置的字串填入。  
    > Endpoint=sb://`[name space]`.servicebus.windows.net/;SharedAccessKeyName=`[policy name]`;SharedAccessKey=`[policy key]`;EntityPath=`[Event Hub-compatible name]`
    - Event Hub name: 填入我們先前Copy出來的`Event Hub-compatible name`。
    - Event Hub consumer group: 填入我們先前建立好的`Consumer Group`。
    - Authentication Mode: 選擇Connection String
    - Event Hub policy name: 填入我們先前Copy的`Shared access policy`(也可以填入連線字串的`[policy name]`)。
    - Event Hub policy key: 填入連線字串中的`[policy key]`。  
    其餘欄位就看需要再調整，一般來說保持預設就好，最後就按Save按鈕儲存設定、就可以在`ASA`的查詢中使用這個Input避開`IoT Hub`的`IP Filter`連線撈資料了。

![](/Teamdoc/image/azure/StreamAnalyticsIPFilter/asa_input2.png)

## 結論:
本文介紹如何在`ASA`中避開`IoT Hub`的`IP Filter`過濾，並請記得`IoT Hub`中的`Apply IP filters to the built-in endpoint?`務必要關閉，否則`Built-in endpoints`依然會被套用`IP Filter`喔!  
成功透過內建端點連線後，其實還有跟`ASA`查詢相關的Code需要調整，原因是改用底層`Event Hub`連線後預設的`IoTHub`資訊就沒有了，但我們要撈資料其實還是需要相關的資訊、例如:DeviceId，但礙於篇幅就留待下篇詳細說明。

## 參考
1. [[How filter rules are applied]](https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-ip-filtering#how-filter-rules-are-applied)
2. [[Stream data from Event Hubs]](https://docs.microsoft.com/en-us/azure/stream-analytics/stream-analytics-define-inputs#stream-data-from-event-hubs)