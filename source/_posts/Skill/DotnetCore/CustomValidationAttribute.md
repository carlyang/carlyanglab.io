---
title: .NET Core - Custom Validation Attribute
categories:
 - 技術文件
 - .NET Core
date: 2021/04/29
updated: 2021/04/29
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [.NET Core, API, Validation, DataAnnotation]
---
作者: Carl Yang

![](/Teamdoc/image/DotnetCore/Logo.png)

## Introduction
對於API的Request Model已經有內建的許多`Validation Attributes`可以使用，例如: Required等，但有些時候因為當下的檢查似乎不是那麼符合想要的結果，驗證錯誤的訊息也不一定都完全符合自己想要的內容，所以`自訂Validation Attribute`就是另一個彈性變化、可依照自己想要的方式進行驗證的方法。
<!-- more -->

## Prerequisites
本文是使用ASP.NET Core 3.1作為開發環境進行練習，請先安裝下列項目。
- 請先安裝ASP.NET Core 3.1 SDK([Download](https://dotnet.microsoft.com/download/dotnet/3.1))

## Custom Validation Attribute
自訂的`Validation Attribute`其實很簡單，請先建立自訂的Attribute Class如下。
```csharp
using System.ComponentModel.DataAnnotations;

namespace ValidationAttributes
{
    public class NotNullOrEmptyAttribute : ValidationAttribute
    {
        public string GetErrorMessage() => ErrorMessage;

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var data = value as string;

            return string.IsNullOrEmpty(data) ? 
                new ValidationResult(GetErrorMessage()) : ValidationResult.Success;
        }
    }
}
```
這個Attribute只是簡單判斷String類型的Property是否為NULL或Empty，主要用來說明自訂的方式。
- 首先`Using System.ComponentModel.DataAnnotations`
- 接著將自訂的Class繼承`ValidationAttribute`，這個`ValidationAttribute`就位於`System.ComponentModel.DataAnnotations`namespace中。
- 然後覆寫並實作IsValid()，實作內容就可以自訂不同的檢查及錯誤訊息。

## Base Controller
接下來為了要在每個Controller進來的時候進行檢查，我們建立一個`抽象類別BaseController`、並加上`ValidateModel()`，然後每個Controller都繼承這個BaseController，以在後續呼叫檢查。
```csharp
using Microsoft.AspNetCore.Mvc;

namespace IND4.Portal.API.Controllers
{
    public abstract class BaseController : ControllerBase
    {
        protected bool ValidateModel<T>(T model, out IActionResult result) where T : class
        {
            var chkResult = TryValidateModel(model, nameof(T));

            if (!chkResult)
            {
                result = new BadRequestResult();
            }
            else
            {
                result = new OkResult();
            }

            return chkResult;
        }
    }
}
```
Note:  
這邊使用泛型`T`來接收不同類型的Request Model，這樣就可以共用Function。

## Request Model驗證
然後我們只要在每個實作Controller的Action最前方呼叫`ValidateModel()`，並把Request Model丟進去就可以進行檢查，如果檢查不通過、則會回傳`validateResult`、如果通過就會回傳`OkResult`。
```csharp
[Route("Demo")]
[HttpPost]
public IActionResult PostDemo(ReqDemoModel request)
{
    if (!ValidateModel(request, out var validateResult))
        return validateResult;

    return new OkResult();
}
```

## 加上自訂屬性
這時候各位可能就覺得奇怪，那我前面所說的`自訂驗證訊息`在哪呢?  
別忘了，我們做的是Request Model的`Attribute`，當然就要在`Model Properties`加上我們的自訂屬性囉，而自訂驗證訊息也是在這邊輸入的。
```csharp
using ValidationAttributes;
using System.ComponentModel.DataAnnotations;

namespace Demo
{
    public class ReqDemoModel
    {
        [NotNullOrEmpty(ErrorMessage = "DemoProperty cannot be null or empty")]
        public string DemoProperty { get; set; }
    }
}
```
Note:  
- 在使用Annotation的時候就不需要再加上尾巴的`Attribute`字樣了。
- 而且可以在每個地方指定不同的`ErrorMessage`，是不是有很高的彈性呢?XD

## 結論
透過以上自訂的`Validation Attribute`、以及抽象類別`BaseController`的`ValidateModel()`，就可以在每個`Action`、每個`Request Model`自訂自己想要的驗證規則，並回傳自訂的驗證訊息囉。基本上，未來除非要自訂不同的驗證規則、就要加上新的`Validation Attribute`，否則就是建立新的Request Model並套上不同的Attribute及自訂驗證訊息就好，簡單又有彈性。

## 參考
1. N/A