---
title: .NET Core - JWT in ASP.NET Core Web API
categories:
 - 技術文件
 - .NET Core
date: 2021/06/23
updated: 2021/06/23
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [ASP.NET Core, Token, JWT]
---
作者: Carl Yang

![](/Teamdoc/image/DotnetCore/Logo.png)

## Purpose
本文主要在說明如何在`ASP.NET Core Web API`專案中使用`JWT`作為`Token`驗證及授權的方法。
<!-- more -->

## Prerequisites
本文是使用ASP.NET Core 3.1作為開發環境進行練習，請先安裝下列項目。
- 請先安裝ASP.NET Core 3.1 SDK([Download](https://dotnet.microsoft.com/download/dotnet/3.1))

## Introduction
`JWT`全名為`JSON Web Token`，簡單來說他是一套`Token`管理套件，當你身分驗證通過後可以利用它輸入相關資訊，包括發行者、Sign Key、使用者名稱、過期時間及宣告等資訊，產生一組`Token`發放給通過`Authentication`(驗證)的人，爾後在有效期間內這個`Token`就能夠用來進行`Authorization`(授權)，詳細可參考官網[[JWT]](https://jwt.io/)。

## Package
微軟在`ASP.NET Core 3.0`以後移除了`Microsoft.AspNetCore.Authentication.JwtBearer`，所以本文使用的專案`ASP.NET Core 3.1`就必須要另外先安裝好這個套件。

## Importants
要產生`JWT`前有機個必要資訊要先說明一下，也必須要提供出來給接下來產生`Token`之用。
- `Claim`: 這個是`賦予JWT的宣告資訊`，未來我們可以透過`對稱金鑰`解密`JWT`來獲得詳細資訊，包括這個`JWT`是誰發的、發給誰、過期時間等資訊。
- `Issuer`: 意指發行者，也就是這個`JWT`是誰發的，都會被加密在發出去的`JWT`中。
- `Sign Key`: 因為每個發放`JWT`的系統可能都不同，所以需要有一個唯一的Key來產生對稱加解密金鑰，這樣才具有安全性，也就是只有同一個地方的加解密金鑰才可以對`JWT`加解密。

Note:  
所以這個`SignKey`很重要，要慎重決定這個Key要怎麼給，不能所有人都知道這Key長怎樣，本文就不方便公開這個SignKey的產生方式了，各位看官可以自行決定。

- `Expire Seconds`: 過期秒數，這是用來設定`JWT`的過期時機的，本文是利用秒數將當下時間加上這個秒數，作為該`JWT`的過期時間。
- `Role`: 這個資訊其實可選擇性使用，它其實就是自訂的一串文字，可用來分群、分身分別等，可以加入`JWT`的`Claim`宣告資訊中，這樣後續就可以利用對稱金鑰解密後判別身分別等資訊。

## Sample
1. Generate Function
下列Function就是產生`JWT`的範例，其中幾個重要如下:
- `Role`: 如果有需要辨識身分或是分群等，可以輸入這個資訊加入`JWT`中。
- `ClaimsIdentity`: 就是宣告資訊。
- `SymmetricSecurityKey`: 這個就是利用`Sign Key`產生對稱金鑰的方式。
- `SigningCredentials`: 這個是利用產生好的對稱金鑰、並指定加密演算法，以獲得`Credentials`資訊，用來後面`Token`的描述。
- `SecurityTokenDescriptor`: 再來把`issuer`、`ClaimsIdentity`、`Expires Time`及`Credentials`提供給Descriptor處理。
- `JwtSecurityTokenHandler`: 產生`JWT`之用，例如: `CreateToken`產生`SecurityToken`、`WriteToken`將`SecurityToken`序列化為字串。

```csharp
public static string GenerateToken(
    string issuer, 
    string signKey, 
    string userName,
    int expireSeconds = 30,
    IList<string> roles = null
    )
{
    var claims = new List<Claim>
    {
        new Claim(JwtRegisteredClaimNames.Sub, userName),
        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
    };

    //If needed, add roles information
    if (null != roles && 0 < roles.Count) 
        claims.AddRange(roles.Select(r => new Claim("roles", r)));

    var identity = new ClaimsIdentity(claims);

    var encryptionKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(signKey));

    // Must be more then 16 characters
    var credentials = new SigningCredentials(encryptionKey, SecurityAlgorithms.HmacSha256Signature);

    var descriptor = new SecurityTokenDescriptor
    {
        Issuer = issuer,
        Subject = identity,
        Expires = DateTime.Now.AddSeconds(expireSeconds),
        SigningCredentials = credentials
    };

    var handler = new JwtSecurityTokenHandler();
    var securityToken = handler.CreateToken(descriptor);
    var token = handler.WriteToken(securityToken);

    return token;
}
```

## Authenticatioin
`JWT`是一個加密安全性的Token，但它並不能處理`要不要發給現在這個人Token`，所以我們還需要先驗證進來的這個人能不能發給它`JWT`，以Application來說，通常就是透過`AppId`及`AppSecret`。

Note:  
- 本範例是將`AppId`及`AppSecret`存進`Azure Key Vault Service`中，當拿到`Login`進來的資訊後，會再跟`Azure Key Vault Service`中的直進行比對，如果正確就表示可以發給這個人`JWT`。
- `Azure Key Vault Service`不是本文主題相關，就先略過。詳細可參考[[Azure - Key Vault Service]](https://carlyang.gitlab.io/2020/05/21/Skill/Azure/Azure_KeyVault/)

```csharp
[Route("Login")]
[HttpPost]
public IActionResult Login([FromBody] LoginModel request)
{
    var response = new LoginModel();

    var secret = _keyVaultService.GetSecretAsync(request.AppId)
        .ConfigureAwait(true)
        .GetAwaiter()
        .GetResult();

    if (secret.Value.Equals(request.AppSecret))
    {
        var token = JwtHelper.GenerateToken(
            _config.JwtInfo.Issuer,
            _config.JwtInfo.SignKey,
            request.AppId,
            _config.JwtInfo.ExpireSeconds
            );

        var createdUtcTime = DateTime.Now;

        response.IsSuccess = true;
        response.Result = "Success";
        response.AccessToken = token;
        response.CreatedUtcTime = createdUtcTime;
    }
    else
    {
        response.IsSuccess = false;
        response.Result = "Invalid AppSecret";
    }

    return new OkObjectResult(response);
}
```

## Startup
為了要能夠使用`JWT`的驗證機制，檢查`JWT`是否合法，我們必須在`Startup`的`ConfigureServices()`中加入`JwtBearer`的驗證設定，如下。

- `IncludeErrorDetails`: 驗證失敗時是否回應`WWW-Authenticate`標頭，這裡會顯示失敗的詳細錯誤原因。
- `NameClaimType` & `RoleClaimType`: 宣告資訊。
- `ValidateIssuer` & `ValidIssuer`: 用來驗證是否是`Issuer`發出去的`JWT`，通常應該都要打開，否則隨便丟一個`JWT`進來，不管是否自己發出去的`JWT`，我想這也是很危險的。
- `ValidateAudience`: 指Client是誰、要不要驗證是否這個Client端發過來的`JWT`，不過通常做為服務應該不會要去驗證Client的身分。
- `ValidateIssuerSigningKey`: 這個就是前面提到用來產生對稱加解密金鑰的`Sign Key`，這樣就可以解密進來的`JWT`並取得資訊來驗證`JWT`是否合法。
- `ClockSkew`: 
    1. 時間偏移量，預設值為五分鐘。
    2. 當Token的Expires設定小於五分鐘時，會滿五分鐘後才Expired。
    3. 如果Token的Expires設定大於五分鐘，則不會有影響。
    4. 改為TimeSpan.Zero則沒有偏移時間、五分內Expires到期就會Unauthorized。  
    [[JWT Token authentication, expired tokens still working, .net core Web Api]](https://stackoverflow.com/questions/43045035/jwt-token-authentication-expired-tokens-still-working-net-core-web-api)

```csharp
services
    .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.IncludeErrorDetails = true;

        options.TokenValidationParameters = new TokenValidationParameters
        {
            NameClaimType = "https://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier",
            RoleClaimType = "https://schemas.microsoft.com/ws/2008/06/identity/claims/role",

            ValidateIssuer = true,
            ValidIssuer = Configuration["JwtInfo:Issuer"],

            ValidateAudience = false,

            ValidateLifetime = true,

            ValidateIssuerSigningKey = true,

            IssuerSigningKey = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(Configuration["JwtInfo:SignKey"])
                ),

            ClockSkew = TimeSpan.Zero
        };
    });
```

## API Authorize
如果要順利進行`JWT`驗證及授權，還必須要在`Startup`的`Configure()`中加入`Middleware`。

```csharp
app.UseAuthentication();
app.UseAuthorization();
```

最後，在要進行驗證的`API Controller`加上`[Authorize]`Annotation，就可以選擇性讓API要不要判斷`JWT`是否有效。

```csharp
[Authorize]
[Route("api/[controller]")]
[ApiController]
public class AuthorizationController : BaseController
```

## 結論
`JWT`應該是目前很多人採用的Token交換機制，功能豐富又有現成套鍵可用，而且安全性也不錯，加上關鍵的`Issuer`、`Sign Key`及`Claim`等都可以自訂、以因應不同的商業邏輯實作，真的是很好用的套件。  
本文只是分享`JWT`的幾個基本使用方法，詳細的內容還是要請讀者閱讀[[官網文件]](https://jwt.io/introduction)會最詳細，且它也有`Debugger`方便我們驗證產生的`JWT`是否正常。

## 參考
1. [[JWT]](https://jwt.io/)
2. [[JWT Token authentication, expired tokens still working, .net core Web Api]](https://stackoverflow.com/questions/43045035/jwt-token-authentication-expired-tokens-still-working-net-core-web-api)
3. [[拜讀保哥大神的文章]](https://blog.miniasp.com/post/2019/12/16/How-to-use-JWT-token-based-auth-in-aspnet-core-31)