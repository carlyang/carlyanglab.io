---
title: Azure - 如何查詢WindowsFxVersion & LinuxFxVersion可用的系統版本
categories:
 - 技術文件
 - Azure
date: 2023/2/21
updated: 2023/2/21
thumbnail: https://i.imgur.com/OTE99ml.png
layout: post
tags: [Azure, App Services]
---

![](/Teamdoc/image/azure/logos/web_app.png)

## Purpose
最近因為一些安全性因素，`Azure`上的`Function Apps`被偵測出沒有指定`Application Code Frameworks`，但程式幾乎都是`C#`相關專案、都有指定`Target Framework`，只好先準備好相關資訊以備後續之用，本文目的先記載`Azure`上可使用版本資訊，以防事後又忘記指令。
<!-- more -->

## Security Issue
最近被掃出下面這條安全性問題，但又不確定自訂系統版本會不會有什麼壞處，例如:自訂後就不會跟著`Azure`升級新版、或者弄不好版本不合等問題，到時候反而拿石頭砸自己的腳、得不償失，只好先記下來以防後面要使用時找不到資訊。

![](/Teamdoc/image/azure/AppServicesFxVersion/oak9_1.png)

## Windows Azure CLI command
命令如下: 

```powershell
az functionapp list-runtimes --os windows --query "[].{stack:join(' ', [runtime, version]), WindowsFxVersion:windows_fx_version, SupportedFunctionsVersions:to_string(supported_functions_versions[])}" --output table
```

目前結果如下:

```
Stack              SupportedFunctionsVersions
-----------------  ----------------------------
dotnet-isolated 7  ["4"]
dotnet-isolated 6  ["4"]
dotnet 6           ["4"]
node 18            ["4"]
node 16            ["4"]
node 14            ["4","3"]
java 17.0          ["4"]
java 11.0          ["4","3"]
java 8.0           ["4","3"]
powershell 7.2     ["4"]
custom             ["4","3"]
```

## Linux Azure CLI command
命令如下: 

```powershell
az functionapp list-runtimes --os linux --query "[].{stack:join(' ', [runtime, version]), LinuxFxVersion:linux_fx_version, SupportedFunctionsVersions:to_string(supported_functions_versions[])}" --output table
```

目前結果如下:

```
Stack              LinuxFxVersion       SupportedFunctionsVersions
-----------------  -------------------  ----------------------------
dotnet-isolated 7  DOTNET-ISOLATED|7.0  ["4"]
dotnet-isolated 6  DOTNET-ISOLATED|6.0  ["4"]
dotnet 6           DOTNET|6.0           ["4"]
node 18            Node|18              ["4"]
node 16            Node|16              ["4"]
node 14            Node|14              ["4","3"]
python 3.10        Python|3.10          ["4"]
python 3.9         Python|3.9           ["4","3"]
python 3.8         Python|3.8           ["4","3"]
python 3.7         Python|3.7           ["4","3"]
java 17.0          Java|17              ["4"]
java 11.0          Java|11              ["4","3"]
java 8.0           Java|8               ["4","3"]
powershell 7.2     PowerShell|7.2       ["4"]
custom                                  ["4","3"]
```

## Conclusion
查詢這個可用清單主要是為了確認那些版本是可用的，但這樣在部署時指定特定版本、對於`Azure PaaS`服務來說，到底是好還是不好?!還真是不好說。但是不管怎樣總是有備無患，至少瞭解可以指定的`linuxFxVersion`或`windowsFxVersion`有哪些，後續再看看怎麼處理被抓到的這個`Security Issue`吧。

