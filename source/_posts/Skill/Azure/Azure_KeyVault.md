---
title: Azure - Key Vault Service
categories:
 - 技術文件
 - Azure
date: 2020/5/21
updated: 2020/5/21
thumbnail: https://i.imgur.com/OlN82Li.png
layout: post
tags: [Azure, Key Vault]
---
Author: Carl Yang

![](/Teamdoc/image/azure/logos/key_vault.png)

## Purpose
本文主要在描述`如何在雲端平台上使用Azure Key Vault儲存系統的機密資料，例如:金鑰、Secret等`。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Azure Key Vault Service

## Introduction
Azure Key Vault可以讓我們安全地在雲端上儲存金鑰、Secret或憑證等機密資訊，它可以幫我們進行軟體加密或硬體加密，主要優點如下引用[[MS Doc](https://docs.microsoft.com/en-us/azure/key-vault/general/overview)]。

##### Centralize application secrets
>Centralizing storage of application secrets in Azure Key Vault allows you to control their distribution.

`Azure Key Vault具有集中管理的優點，我們可以透過Azure Portal很容易地新增、刪除、Disable這些機密資訊。`

##### Securely store secrets and keys
>Secrets and keys are safeguarded by Azure, using industry-standard algorithms, key lengths, and hardware security modules (HSMs). The HSMs used are Federal Information Processing Standards (FIPS) 140-2 Level 2 validated.

`Azure Key Vault也會自動幫我們的這些機密資訊進行軟硬體加解密，例如RSA、HSM。不過，硬體加密(HSM)的費用會比較貴一些，這就要看需求的程度了。`

##### Monitor access and use
>Once you have created a couple of Key Vaults, you will want to monitor how and when your keys and secrets are being accessed.

`我們也可以透過Azure Portal的Activity Log查閱相關的活動紀錄，藉此進行管控，如下圖。`
![](/Teamdoc/image/azure/KeyVault/portal1.png)

##### Simplified administration of application secrets
>When storing valuable data, you must take several steps. Security information must be secured, it must follow a life cycle, and it must be highly available.

`透過Azure Portal我們確實也可以簡化相關的管理工作。`

##### Integrate with other Azure services
>As a secure store in Azure, Key Vault has been used to simplify scenarios like:

>- Azure Disk Encryption
>- The always encrypted functionality in SQL server and Azure >- SQL Database
>- Azure App Service.

>Key Vault itself can integrate with storage accounts, event hubs, and log analytics.

`Azure Key Vault也可以與其它特定的Azure服務整合，例如使用Storage Account備份、允許特定的App Service存取等等。`

## Features
- Soft Delete:
  1. Azure Key Vault提供Soft Delete的功能、也就是虛刪除的意思，`當我們刪除一組Secret後，其實他並不會真的刪除，而是會暫存一段時間後才真正清除`。
  
  2. Soft Delete會根據Key Vault設定的存留期(Retention period days)暫存被刪除的資料，預設是90天。
  
  3. 在存留期間如果想要救回，則必須使用Azure CLI的方式一個個挑出來還原。
  4. 存留期內無法再新增同名的Key值。
  
  5. `整個Key Vault服務也受Soft Delete的保護，也就是說存留期間內不能再新增同名的Key Vault服務。`
  
  6. 雖然Soft Delete有安全性上的優點，可以防止誤刪救不回來的情況，但是`存留期僅能設定7~90天的範圍，也就是說至少也要7天後才能再新增同名的Key值`。  
![](/Teamdoc/image/azure/KeyVault/portal2.png)

  Note:  
  - `請特別注意，Soft Delete的功能一旦開啟就無法關閉，就算刪除整個Key Vault服務，也會受到存留期影響、無法馬上新增同名Key Vault服務。`
  - 另外，還有個跟存留期相關的功能，就是`Purge protection`。這個功能是`保護deleted狀態的資料、在存留期到期之前不被清除`，也是一開啟就無法關閉，請特別注意。
![](/Teamdoc/image/azure/KeyVault/portal5.png)

- Backup/Restore
  1. Azure Key Vault也提供備份/還原的功能，我們可以一個個將資料Download下來為`*.[XXX]backup`檔案，`且這個檔案內容也是經過加密的，一般人無法直接看到明文資料`。  
![](/Teamdoc/image/azure/KeyVault/portal3.png)
  
  1. 我們可以另外保存`*.[XXX]backup`檔案，需要時再利用下列功能還原回Key Vault。  
![](/Teamdoc/image/azure/KeyVault/portal4.png)

- Expiration Date
  1. 我們可能會注意到，不論是Key或Secret都會有個Expiration Date的屬性，但`它到期後並不會自動清除資料，只是一個參考用途`。
  2. `Expiration Date也是一旦設定過就無法清除，只能不斷更新為新的時間`。  
![](/Teamdoc/image/azure/KeyVault/portal6.png)

- Access Policies
  1. Azure Key Vault是`透過設定存取原則(Access Policies)來保護機密資料`，只有具有存取權限的使用者或Application可以存取，如下圖的[Add Access Policy]。
![](/Teamdoc/image/azure/KeyVault/portal7.png)
  1. Access Policies主要`分為兩大類的存取權驗證，一是使用者帳戶、另一個是Managed Service Identity`。
    - 使用者帳戶: 如上圖中的[User]區塊，就是指`該訂閱(Subscription)中的Azure AD內，所具有的使用者帳戶`。
    - Managed Service Identity: 如上圖中的[Applicatioin]區塊，簡稱為MSI，`就是我們為Azure應用系統所賦予的一個虛擬身分，而這個身分可以套用權組設定等等的Azure AD功能`。
    - Role-based Access Control: 簡稱RBAC，`透過指定Azure AD特定角色，就能夠賦予該角色內的成員、具有Key Vault的存取權限`，所以我們也可以為User或Application設定RBAC角色。

    Note:  
    - 如果我們給予應用系統一個MSI身分，`這個身分就相當於一個使用者帳戶，接著我們就可以將這個虛擬帳戶設定給特定的Role，最後就可進一步利用這個Role管理存取權限`。
    - 賦予MSI後，我們就可以在Azure AD中查找到同名的Service Principal(服務主體)，並可針對這個服務主體進行相關設定，下圖為Azure Key Vault設定服務主體的畫面。
![](/Teamdoc/image/azure/KeyVault/portal8.png)

## 結論:
Azure Key Vault提供雲端平台上的安全性加解密服務，也提供Soft Delete的功能防止資料不正常遺失，`但必須特別注意到，Azure Key Vault並不適用於會動態變更的資料，例如Token`，因為Soft Delete會有存留期限制、而Expiration Date僅是用做參考用，故類似於Token這種交換機制、又必須過期更新的資料，就不是那麼地合適。

Azure Key Vault也可以儲存憑證資料，但本文沒有太過地說明，有興趣者可以自行參考下列微軟文件。

另外，`根據Azure IaaS的架構，Azure Key Vault也可以指定vNet，進一步將服務置於私有網路內、以隔絕外界的不當存取，但這方式就會增加開發/維運等的負擔，需要慎重考慮`。

## 參考
1. [About Azure Key Vault](https://docs.microsoft.com/en-us/azure/key-vault/general/overview)