---
title: Azure - 如何在API Management隱藏特定資訊
categories:
 - 技術文件
 - Azure
date: 2022/6/1
updated: 2022/6/1
thumbnail: https://i.imgur.com/2KGeUZq.png
layout: post
tags: [Azure, API Management]
---

![](/Teamdoc/image/azure/logos/apim.png)

## Purpose
基於一些安全性因素，有可能從後端來的資訊需要轉換為另一個較符合的文字，本文就在說明如何使用`find-and-replace`原則處理這樣的情境。
<!-- more -->

## find-and-replace
很多時候後端服務回傳的內容可能不是那麼合適，所以就需要在`APIM`這邊先進行轉換、再把結果回傳給呼叫端，這時候的搜尋與替換就可以使用`find-and-replace`原則來處理，引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/api-management/find-and-replace-policy)說明如下。

>The find-and-replace policy finds a request or response substring and replaces it with a different substring.

也就是說，當後端服務回應`APIM`後，`APIM`可以檢查回應Body內是否具有特定的字串，有的話就可以先行轉換為另一個字串、再回傳給呼叫端。事實上，有很多的機密資訊如`後端服務的位址`、`版本號`等，我們既然透過`APIM`整合隱藏後端APIs了，如果一不小心後端服務把這些相關的資訊帶入Body傳回去，那不就還是沒有安全性可言了嗎?範例如下。

```xml
<find-and-replace from="what to replace" to="replacement" />
```

說明:
- `from`: 要被取代的字串。
- `to`: 要取代的資串。
- `請特別注意!find-and-replace只能處理Body內的字串，標頭那些是無法使用的喔!`
- 如果想要清除某個子字串，可以把`to`留空即可。

使用範圍:
如下範例，`inbound`、`outbound`、`backend`、`on-error`都可以使用，用在`inbound`及`backend`就是替換`HTTP Request`的Body，用在`outbound`則是替換`HTTP Response`的Body。`on-error`的話則不一定、要看當下錯誤是發生在哪個區塊，不同區塊的Error資訊不一定相同。

```xml
<policies>
  <inbound>
    <!-- statements to be applied to the request go here -->
  </inbound>
  <backend>
    <!-- statements to be applied before the request is forwarded to 
         the backend service go here -->
  </backend>
  <outbound>
    <!-- statements to be applied to the response go here -->
  </outbound>
  <on-error>
    <!-- statements to be applied if there is an error condition go here -->
  </on-error>
</policies>
```

## Conclusion
`find-and-replace`原則雖然很簡單、易懂，但是`關鍵還是在於要怎麼知道什麼資訊要隱藏!`我們會盡可能地找出特定的資訊來替換或刪除(例如:後端位址)、但也很難完全顧到，畢竟Body內會包含什麼我們也很難事前得知，所以還是必須先適當地規劃設計，才可以從資料源頭避免機密資訊外洩。

## References
1. [Find and replace string in body](https://learn.microsoft.com/en-us/azure/api-management/find-and-replace-policy)