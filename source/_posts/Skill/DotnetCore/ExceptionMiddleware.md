---
title: .NET Core - How to use Middleware to catch API Exception
categories:
 - 技術文件
 - .NET Core
date: 2021/04/29
updated: 2021/04/29
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [.NET Core, Middleware, API, Exception]
---
作者: Carl Yang

![](/Teamdoc/image/DotnetCore/Logo.png)

## Introduction
很多時候我們需要使用try...catch抓取系統Runtime時發生的Exception，以獲得更多的Debug資訊，但是每個Function都做一次try...catch實在太煩人，也太多重工了。本文主要在說明如何利用自訂Middleware抓取內層發生的Exception，節省太多的重工狀況。
<!-- more -->

## Prerequisites
本文是使用ASP.NET Core 3.1作為開發環境進行練習，請先安裝下列項目。
- 請先安裝ASP.NET Core 3.1 SDK([Download](https://dotnet.microsoft.com/download/dotnet/3.1))

## Middleware
相關的Middleware概念及使用方法，可以參考另一篇[[.NET Core - Middleware]](https://carlyang.gitlab.io/2019/01/06/Skill/DotnetCore/Middleware/)

## Exception Middleware
首先我們先建立一個自訂的Middleware Class，並在建構式傳入RequestDelegate、實作非同步Invoke Function。
```csharp
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Middlewares
{
    public class ExceptionHandleMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandleMiddleware(
            RequestDelegate next
            )
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            string requestBody = null;

            try
            {
                using var reader = new StreamReader(context.Request.Body, Encoding.UTF8);

                context.Request.Body.Seek(0, SeekOrigin.Begin);

                requestBody = reader.ReadToEndAsync()
                    .ConfigureAwait(true)
                    .GetAwaiter()
                    .GetResult();

                context.Request.Body.Seek(0, SeekOrigin.Begin);

                await _next.Invoke(context);
            }
            catch (HttpRequestException ex)
            {
                var extraInfo = new Dictionary<string, string>()
                {
                    { "RequestBody", requestBody }
                };

                var status = HttpStatusCode.ServiceUnavailable;

                if (0 < ex.Data.Count)
                    foreach (DictionaryEntry o in ex.Data)
                    {
                        extraInfo.Add(o.Key.ToString(), o.Value.ToString());

                        if (string.Equals(o.Key.ToString(), "HttpStatusCode", StringComparison.OrdinalIgnoreCase))
                            status = Enum.Parse<HttpStatusCode>(o.Value.ToString());
                    }

                await HandleHttpRequestExceptionAsync(context, ex, status);
            }
            catch (Exception e)
            {
                var extraInfo = new Dictionary<string, string>()
                {
                    { "RequestBody", requestBody }
                };

                if (0 < e.Data.Count)
                    foreach (DictionaryEntry o in e.Data)
                    {
                        extraInfo.Add(o.Key.ToString(), o.Value.ToString());
                    }

                await HandleExceptionAsync(context, e);
            }
        }

        private async Task HandleExceptionAsync(
            HttpContext context,
            Exception e
            )
        {
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            if (!context.Response.Headers.ContainsKey("Content-Type"))
                context.Response.Headers.Add("Content-Type", "application/json");

            await WriteResponseAsync(context, $"Exception: {e}");
        }

        private async Task HandleHttpRequestExceptionAsync(
            HttpContext context,
            HttpRequestException e,
            HttpStatusCode status = HttpStatusCode.ServiceUnavailable
            )
        {
            context.Response.StatusCode = (int)status;

            if (!context.Response.Headers.ContainsKey("Content-Type"))
                context.Response.Headers.Add("Content-Type", "application/json");

            await WriteResponseAsync(context, $"HTTP Exception: {e}, details: {JsonSerializer.Serialize(e.Data)}");
        }

        private async Task WriteResponseAsync(HttpContext context, string message)
        {
            await context.Response.WriteAsync(
                JsonSerializer.Serialize(new
                {
                    IsSuccess = false,
                    Result = message
                })
            );
        }
    }
}
```

## Http Request
Middleware是一種巢狀的呼叫，透過每個HTTP Request進來後的RequestDelegate，一層一層用`Invoke(HttpContext)`將Request往內層傳進去處理，每一層就可以自訂一些想要的操作，包括try...catch。
而ASP.NET Core會依照順序逐個調用每個Middleware的Invoke()，所以我們可以適當地在Invoke()中加上try...catch，並在其內呼叫`await _next.Invoke(context)`將request往內層傳遞，如下方的Invoke()實作。
```csharp
public async Task Invoke(HttpContext context)
{
    string requestBody = null;

    try
    {
        using var reader = new StreamReader(context.Request.Body, Encoding.UTF8);

        context.Request.Body.Seek(0, SeekOrigin.Begin);

        requestBody = reader.ReadToEndAsync()
            .ConfigureAwait(true)
            .GetAwaiter()
            .GetResult();

        context.Request.Body.Seek(0, SeekOrigin.Begin);

        await _next.Invoke(context);
    }
    catch (HttpRequestException ex)
    {
        var extraInfo = new Dictionary<string, string>()
        {
            { "RequestBody", requestBody }
        };

        var status = HttpStatusCode.ServiceUnavailable;

        if (0 < ex.Data.Count)
            foreach (DictionaryEntry o in ex.Data)
            {
                extraInfo.Add(o.Key.ToString(), o.Value.ToString());

                if (string.Equals(o.Key.ToString(), "HttpStatusCode", StringComparison.OrdinalIgnoreCase))
                    status = Enum.Parse<HttpStatusCode>(o.Value.ToString());
            }

        await HandleHttpRequestExceptionAsync(context, ex, status);
    }
    catch (Exception e)
    {
        var extraInfo = new Dictionary<string, string>()
        {
            { "RequestBody", requestBody }
        };

        if (0 < e.Data.Count)
            foreach (DictionaryEntry o in e.Data)
            {
                extraInfo.Add(o.Key.ToString(), o.Value.ToString());
            }

        await HandleExceptionAsync(context, e);
    }
}
```
上方的catch我先抓取HttpRequestException、再抓取Exception，目的是為了區分出HTTP的錯誤類型、或是其他可能發生的內部錯誤類型。  
- 如果是Http類型的Exception`預設回應503狀態碼、如果是其他狀態則回應當時的狀態碼，並且將錯誤訊息及當下Request的資訊寫入Response Body回傳給Client端`。  
- 如果是其他類型的Exception則`回應500內部伺服器錯誤的狀態，並且將Exception資訊寫入Response Body回傳給Client端`。

## Use Middleware
建立完自訂的Middleware後，再回到`Startup`的`Configure()`內使用`app.UseMiddleware<ExceptionHandleMiddleware>()`調用自訂的Middleware。  
Note:  
`這裡要特別注意Use的先後順序`，因為ASP.NET Core API的Middleware調用順序，是依照這裡Use的順序執行的，如果`ExceptionHandleMiddleware`順序放太後面，那麼`前面先跑所發生的Exception就抓不到了`。  
```csharp
public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
{
    if (env.IsDevelopment())
    {
        app.UseDeveloperExceptionPage();
    }

    app.UseCors("CorsPolicy");

    app.Use((context, next) =>
    {
        context.Request.EnableBuffering();
        return next();
    });

    app.UseMiddleware<ExceptionHandleMiddleware>();

    app.UseHttpsRedirection();

    app.UseRouting();

    app.UseAuthorization();

    app.UseEndpoints(endpoints =>
    {
        endpoints.MapControllers();
    });
}
```

## 結論
透過以上的自訂Middleware就可以抓到內部發生的所有Exception，`然後可以在Invoke()內加上各自想要處理的不同操作`，如此一來就不用每個Function都try...catch一次，然後再把Exception資訊往自己想要的地方丟，我這邊是會把錯誤訊息往Teams丟，就有即時監控的效果了，不過這部分我就沒有列在本文中，就依照各位看倌因地制宜、看各位當時的情況進行不同的處理。

## 參考
1. [.NET Core - Middleware](https://carlyang.gitlab.io/2019/01/06/Skill/DotnetCore/Middleware/)