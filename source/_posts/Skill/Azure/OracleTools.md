---
title: Oracle Tools - How to connect to DB with TCPS protocol via SQL Developer & Toad
categories:
 - 技術文件
 - Azure
date: 2024/2/21
updated: 2024/2/21
thumbnail: https://i.imgur.com/POjBZXA.png
layout: post
tags: [Azure, Azure Oracle]
---

![](/Teamdoc/image/azure/logos/azureoracle.png)

## Purpose
最近專案需要連線Azure Oracle DB，但因為DB使用更安全的TCPS Protocol連線方式，導致相關工具的連線都要另外處理，所以本文目的就在介紹如何使用SQL developer & Toad兩種工具、針對TCPS Protocol進行設定。
<!-- more -->

## Prerequisite
在介紹目前最常被使用的兩種工具前，請先下載好下列兩樣安裝檔。
1. SQL Developer: [[Download]](https://www.oracle.com/database/sqldeveloper/technologies/download/)
2. Toad for Oracle: [[Download]](https://www.quest.com/register/141779/)

## SQL Developer
首先介紹如何使用SQL Developer連線Oracle DB。

1. 先下載系統對應的ZIP檔然後解壓縮放在自訂的位置即可，SQL Developer不需要安裝。

![](/Teamdoc/image/azure/OracleTools/sqldeveloper1.png)

2. 進入資料夾內打開sqldeveloper.exe

![](/Teamdoc/image/azure/OracleTools/sqldeveloper2.png)

3. 按左上角綠色+號新增連線設定

![](/Teamdoc/image/azure/OracleTools/sqldeveloper3.png)

4. Connection Type選擇Custom JDBC

![](/Teamdoc/image/azure/OracleTools/sqldeveloper4.png)

5. 接著下方輸入連線字串、然後按Test可以直接測試連線、按Save可以儲存連線設定方便以後使用，範例如下。

```
jdbc:oracle:thin:@(description=(address=(protocol=tcps)(host=<azure-url>)(port=2484))(connect_data=(service_name=<dbname>)))
```

![](/Teamdoc/image/azure/OracleTools/sqldeveloper5.png)

Note:  
- `請注意要先替換連線字串中的<azure-url>、<dbname>`
- `圖中紅框處的Username & Password必須要輸入才能登入DB`
- `勾選右方的Save password儲存密碼方便以後直接使用`
- 最後下方Custom JDBC URL輸入替換好的連線字串即可。
- 因為`Oracle DB是使用安全性較高的TCPS Protocol，所以才必須要自訂JDBC連線方式`。

6. 連線成功後就可以點選左方的Synonyms查看Table內容。

![](/Teamdoc/image/azure/OracleTools/sqldeveloper6.png)

## Toad
Toad的安裝就比較麻煩一點，要先安裝`Oracle Instant Client`。

1. 請先去下載[[Oracle Instant Client]](https://www.oracle.com/database/technologies/instant-client/winx64-64-downloads.html)，如下圖的Light輕量化版本。

![](/Teamdoc/image/azure/OracleTools/toad1.png)

2. Oracle Instant Client的安裝其實也是解壓縮後放到自己要的位置即可。不過，不同的是`要把Oracle Instant Client的路徑加進系統環境變數的Path中、這樣Toad才能夠自動從Path變數中參考Oracle Instant Client`，如下圖。

本文是放在`C:\instantclient_21_13`底下。

![](/Teamdoc/image/azure/OracleTools/toad2.png)

3. 接著我們進入`C:\instantclient_21_13\network\admin`新增一個TNS設定檔`tnsnames.ora`、然後輸入以下內容。

Note:  
`請注意要替換<ConnectionName>、<URL>、<service name>`。
- `<ConnectionName>`: 可以自訂名稱。
- `<URL>`: Oracle DB URL。
- `<service name>`: Oracle DB Name。
- 最後儲存tnsnames.ora檔案即可。

```
<ConnectionName> =
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCPS)(HOST = <URL>)(PORT = 2484))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = <service name>)
    )
  )
```

4. 接著打開Toad並在左方資料庫窗格按右鍵 -> New Connection

![](/Teamdoc/image/azure/OracleTools/toad3.png)

5. 先勾選右方Connect using Oracle Client(因為有設定Path所以這邊可以自動抓取)，然後再點選上方`TNSNames Editor`按紐以打開內容視窗，如下圖就可以看到我們剛建立的`tnsnames.ora`內容。

![](/Teamdoc/image/azure/OracleTools/toad4.png)

6. `tnsnames.ora`內容確認有抓到也沒問題後，可以先關掉並在右上方輸入Username & Password，最後按下右下方的Connect按紐即可連線。

![](/Teamdoc/image/azure/OracleTools/toad5.png)

## Conclusion
目前兩種工具SQL Developer & Toad應該是最常使用的Oracle DB查詢工具，SQL Developer是免費的但功能較陽春，Toad功能最豐富但要有license(本文僅使用30天試用版)，有需要的人可以考慮購買Toad License。