---
title: Azure - Service Bus
categories:
 - 技術文件
 - Azure
date: 2020/1/5
updated: 2020/1/5
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, Service Bus, Topic, Subscription]
---
Author: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Purpose
Service Bus(服務匯流排)是Azure上的訊息傳遞服務，透過發佈及接收的動作達成傳遞的目的，`本文主要在說明Service Bus中各個主要的概念`。
<!-- more -->

## Introduction
訊息(Message)在Service Bus中可以視為資料、透過服務協助暫存及傳遞的工作。最上層為Namespace、訊息傳遞類型包含兩種，佇列(Queues)、主題(Topics)，一個Namespace可以包含多個Queues及Topics、而一個Service Bus可以包含多個Namespaces。

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Create Azure Service Bus

## Namespace
Service Bus中的`Namespace可被視為一種容器(Container)，可以包含多個Queues及Topics，是一種邏輯概念上的分類`。

## Queue
Service Bus的Queue採用先進先出機制，當新的Message進來、會被加入Queue的起始位置，當有人拿走一筆Message、只能從Queue的結束位置取出一筆，`而被取走的Message就會從佇列中消失，其他人無法再取得同一筆Message`，通常使用在單一的點對點傳輸架構上，如下圖所示。
![](https://docs.microsoft.com/en-us/azure/service-bus-messaging/media/service-bus-messaging-overview/about-service-bus-queue.png)

## Topics & Subscriptions
Service Bus的`Topics提供一對多的訊息傳遞機制，不像Queue只能一次取得一筆，Senders可以透過相同的Topic發送訊息、Receivers(或稱Subcriber)可以透過訂閱(Subscription)取得訊息，所以同一筆訊息可以同時發送給多個Receivers`，如下圖所示。
![](https://docs.microsoft.com/en-us/azure/service-bus-messaging/media/service-bus-messaging-overview/about-service-bus-topic.png)

## Topic Filters & Actions
Topic Filters(主題過濾)可以`讓Subscrber接收訂閱的Message後、更進一步過濾訊息內容`，引用官方文件說明如下。
>- Boolean filters - The TrueFilter and FalseFilter either cause all arriving messages (true) or none of the arriving messages (false) to be selected for the subscription.
>- SQL Filters - A SqlFilter holds a SQL-like conditional expression that is evaluated in the broker against the arriving messages' user-defined properties and system properties. All system properties must be prefixed with sys. in the conditional expression. The SQL-language subset for filter conditions tests for the existence of properties (EXISTS), as well as for null-values (IS NULL), logical NOT/AND/OR, relational operators, simple numeric arithmetic, and simple text pattern matching with LIKE.
>- Correlation Filters - A CorrelationFilter holds a set of conditions that are matched against one or more of an arriving message's user and system properties. A common use is to match against the CorrelationId property, but the application can also choose to match against ContentType, Label, MessageId, ReplyTo, ReplyToSessionId, SessionId, To, and any user-defined properties. A match exists when an arriving message's value for a property is equal to the value specified in the correlation filter. For string expressions, the comparison is case-sensitive. When specifying multiple match properties, the filter combines them as a logical AND condition, meaning for the filter to match, all conditions must match.

主要有三種模式:
1. Boolean Filters:  
透過TrueFilter選取所有訊息、或FalseFilter部所有訊息。
2. SQL Filters:  
是一種類SQL的查詢語法，透過一個閣條件過濾要選取的訊息。
3. Correlatioin Filters:  
可以透過設定一組條件，針對訊息進行條件過濾，而這些條件可以是User或系統屬性、甚至如ContentType、MessageId等這些標頭資訊也都可以過濾。

Actions(動作)則是使用SQL Filter的類SQL語法、`可以對Message的屬性及屬性值進行改或新增屬性`，例如:加入註解。引述官方說明文件如下。
>With SQL filter conditions, you can define an action that can annotate the message by adding, removing, or replacing properties and their values.

## Register Handler
Service Bus SDK使用`RegisterMessageHandler()`註冊Handler並持續取得特定Topic的Message，詳細可參考[[MS Doc]](https://www.c-sharpcorner.com/article/azure-service-bus-topic-and-subscription-pub-sub/)。

## 結論:
Azure其時在Storage Account中還有一種Queue Storage服務，但它只能一次針對一個目標發送及接收，但`Service Bus可以進行一對多發送，且透過Message的狀態管理、控制Message的留存期`，是很好用的訊息傳遞服務。

## 參考
1. [What is Azure Service Bus?](https://docs.microsoft.com/en-us/azure/service-bus-messaging/service-bus-messaging-overview)
2. [Topic filters and actions](https://docs.microsoft.com/en-us/azure/service-bus-messaging/topic-filters)