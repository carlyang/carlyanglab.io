---
title: IoT Hub - File Upload
categories:
 - 技術文件
 - Azure
date: 2018/12/30
updated: 2018/12/30
thumbnail: https://i.imgur.com/d1Wvf0c.png
layout: post
tags: [File Upload, Blob]
---
作者: Carl Yang

![](/Teamdoc/image/azure/logos/iot_hub.png)

# IoT Hub - File Upload
IT Hub作為Azure在IND 4.0中的資料收集匣道，必須能夠容納各式各樣型態的資料，通常單筆的資料大小都不至於太大，但也不是沒有可能遇到，例如今天要說的檔案上傳。本篇主要在講述IoT Hub本身對於資料大小的限制、以及建議的做法。
<!-- more -->

## IoT Hub Message的限制
根據([微軟文件](https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-devguide-quotas-throttling))所描述，每次上傳IoT Hub的Message大小、有256KB的限制，節錄如下。
>Device-to-cloud messaging: Maximum message size 256 KB

<font style="color:red;">如果每次的Message會超過這個限制的話，就可以選擇使用檔案上傳的方式上傳資料。</font>

## 檔案上傳
IND 4.0中主要是收集機台的相關資料，如溫度、濕度、長度...等等，但不同機台可能是寫入RDB、NonSQL DB、也可能是寫入檔案。當寫入DB等等資料來源時，通常都是預先定義好格式的資料、大部分也都能切分為一筆筆的格式，但如果是寫入檔案、可能因為資料本身所代表的是以檔案為基礎的數值，這時候如果硬將他切分為一筆一筆的資料格式，還需要設計其他的關聯性欄位、來識別哪筆資料對應於哪個檔案，這狀況可能反而會有額外的計算資源、增加計算時間，也加大資料管理的難度，未來要進行分析時還要多一道資料整理的工作。

根據上述的情境，其實如果能夠直接以檔案為單位、將資料上傳至雲端，應該是最為直觀、也方便管理的方式，而Azure IoT Hub當然也提供檔案上傳的功能，設定方式非常簡單，如下步驟設定即可。

### Step 1
進入IoT Hub -> File upload -> Storage container settings

![](/Teamdoc/image/azure/iot_hub_file_upload/hub_setting1.png)

### Step 2
如果已有Storage Account畫面會自動顯示，如果尚未建立可以用上方[+Storage Account]新增，這裡我們直接進入Storage Account。

![](/Teamdoc/image/azure/iot_hub_file_upload/hub_setting2.png)

### Step 3
接著我們新增一個Blob Containerr叫做carltestfileupload、Public access level選擇Container，最後再按下OK新增。

![](/Teamdoc/image/azure/iot_hub_file_upload/hub_setting3.png)

### Step 4
回到前一個畫面選擇剛新增的Blob、再按下下方的Select按鈕完成設定。

![](/Teamdoc/image/azure/iot_hub_file_upload/hub_setting4.png)

### Step 5
回到IoT Hub畫面會自動選好剛新增的Blob Container，建議再將File Notificaion Settings設為開啟，未來如果有需要就會收到檔案上傳完成的通知，最後按下上方的Save按鈕儲存設定即可生效。

![](/Teamdoc/image/azure/iot_hub_file_upload/hub_setting5.png)

根據上述步驟完成後，我們可以知道其實IoT Hub只是利用原本的Blob Container、將兩者關聯起來後，當AP端指定上傳檔案時、就會自動將資料放到對應的Blob，這樣的好處是不需要再額外撰寫Code做檔案上傳的操作，直接利用IoT Hub幫我們轉存Blob即可。

### AP端程式Sample:
這邊的範例是直接使用HttpClient調用IoT Hub API，因為到目前為止、微軟的IoT Device SDK在檔案上傳的部分然為支援Proxy的設定，故改為自行發Http Request、並走Proxy出去外網。

Note:
<font style="color:red;">程式中所使用的HttpClientConfig及HttpClientHelper為自行另外撰寫的元件，其內部就是使用HttpClient進行Http Request，各位可自行改寫即可使用，因該元件與本文主題無關，故未附上。</font>

```csharp
private async Task UploadToBlobAsync(string fileName, Stream source, string iotHubUrl, string deviceId, string deviceKey, string proxy = null)
{
	FileUploadInfo uploadInfo = null;

	//Generate SAS Token for authoring
	var deviceSasToken = GetSasToken($"{iotHubUrl}/devices/{deviceId}", deviceKey, null, _fileUploadExpiredSeconds);

	var httpConfig = new HttpClientConfig()
	{
		Url = $"https://{iotHubUrl}/devices/{deviceId}/files?api-version=2018-06-30",
		ContentType = @"application/json",
		ProxyAddress = proxy,
		IsKeepAlive = false,
		Headers = new Dictionary<string, string>
			{
				{@"Authorization", deviceSasToken}
			},
		PostData = JsonConvert.SerializeObject(new { blobName = fileName })
	};

	HttpResponseMessage response = null;

	try
	{
		// step 1. get the upload info
		response = await HttpClientHelper.PostAsync(httpConfig);
		response.EnsureSuccessStatusCode();

		if (response.IsSuccessStatusCode)
		{
			uploadInfo =
				JsonConvert.DeserializeObject<FileUploadInfo>(await response.Content.ReadAsStringAsync());

			//Clear
			response = null;

			// step 2. upload blob
			httpConfig = new HttpClientConfig()
			{
				Url =
					$"https://{uploadInfo.hostName}/{uploadInfo.containerName}/{uploadInfo.blobName}{uploadInfo.sasToken}",
				ProxyAddress = proxy,
				IsKeepAlive = false,
				Headers = new Dictionary<string, string>
				{
					{@"x-ms-blob-type", @"blockblob"}
				}
			};
			response = await HttpClientHelper.PutAsync(httpConfig, new StreamContent(source));
			response.EnsureSuccessStatusCode();
		}
	}
	catch (Exception e)
	{
		//Get Status Code
		var statusResult = null != response ? (int)response.StatusCode : -1;

		if (-1 < statusResult)
		{
			//Determining status code, if 200 series, don't throw exception
			if (200 > statusResult && 299 < statusResult)
			{
				e.Data.Add(@"IoT Hub Response Info", $"HttpStatusCode: {statusResult}, Message: {response}, Stack Trace: {e.StackTrace}");
				throw;
			}
		}
		else
		{
			//If not HTTP exception, throw it directly
			throw;
		}
	}
	finally
	{
		//Whatever this uploading is success or not, complete it finally
		try
		{
			//If there has HttpResponse, it means the SAS token already created. So must to complete uploading operation.
			if (null != uploadInfo)
			{
				var isUploaded = response?.StatusCode == System.Net.HttpStatusCode.Created;
				var postData = JsonConvert.SerializeObject(new
				{
					correlationId = uploadInfo.correlationId,
					statusCode = isUploaded ? 0 : -1,
					statusDescription = response?.ReasonPhrase,
					isSuccess = isUploaded
				});

				//Clear
				response = null;

				// step 3. send completed
				httpConfig = new HttpClientConfig()
				{
					Url = $"https://{iotHubUrl}/devices/{deviceId}/files/notifications?api-version=2018-06-30",
					ContentType = @"application/json",
					ProxyAddress = proxy,
					IsKeepAlive = false,
					Headers = new Dictionary<string, string>
					{
						{@"Authorization", deviceSasToken}
					},
					PostData = postData
				};
				response = await HttpClientHelper.PostAsync(httpConfig);
				response.EnsureSuccessStatusCode();
			}
		}
		catch (Exception)
		{
			throw;
		}
	}
}

internal string GetSasToken(string resourceUri, string key, string keyName = null, uint expiredSeconds = 300)
{
	string sasToken = null;

	var expiry = Convert.ToString((int)(DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds + expiredSeconds);
	var stringToSign = System.Net.WebUtility.UrlEncode(resourceUri) + "\n" + expiry;
	var hmac = new HMACSHA256(Convert.FromBase64String(key));

	var signature = Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(stringToSign)));
	sasToken = keyName == null ?
		string.Format(CultureInfo.InvariantCulture, "SharedAccessSignature sr={0}&sig={1}&se={2}", System.Net.WebUtility.UrlEncode(resourceUri), System.Net.WebUtility.UrlEncode(signature), expiry) :
		string.Format(CultureInfo.InvariantCulture, "SharedAccessSignature sr={0}&sig={1}&se={2}&skn={3}", System.Net.WebUtility.UrlEncode(resourceUri), System.Net.WebUtility.UrlEncode(signature), expiry, keyName);

	return sasToken;
}
```

## 檔案上傳限制
根據([微軟文件](https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-devguide-quotas-throttling))所描述，IoT Hub背後會有一個Queue來處理每個檔案上傳所使用的SAS Token(在Step 1. get the upload info時Enqueue一個、在step 3. send completed時Dequeue一個)，且同一時間最大的SAS Token數目為10個，故必須要控制好同時檔案上傳的Device數目。

>File upload URIs:
>10000 SAS URIs can be out for a storage account at one time. 
>10 SAS URIs/device can be out at one time.

當超過10個SAS Token限制時，會收到下列回傳的錯誤訊息。
>StatusCode: 403, ReasonPhrase: 'Forbidden', Version: 1.1, Content: System.Net.Http.HttpConnection+HttpConnectionResponseContent, Headers: { Server: Microsoft-HTTPAPI/2.0 iothub-errorcode: DeviceMaximumActiveFileUploadLimitExceeded Date: Fri, 23 Nov 2018 06:10:50 GMT Content-Length: 263 Content-Type: application/json; charset=utf-8 }

## 建議
經過實際測試，一台裝置如果都能夠正常Complete，大部分情況下不會碰到這條限制，但是如果裝置數目很多、又或者多台裝置同時上傳的話就很可能超過，故建議如果有多台裝置的話、還是直接上傳至Blob較佳(Blob直接上傳檔案方式可參考[Blob](/2019/01/01/Skill/Azure/Storage_Account_Blob/#more))。

Note:
<font style="color:red;">個人覺得，IoT Hub這個限制對於IND 4.0來說真的很不恰當，因為工業設備量很容易就超過10這個數目，限制這個實在有點莫名其妙，但經與微軟溝通後、目前這個限制仍然無法排除，故個人建議還是直接使用Blob SDK做檔案上傳會最沒有問題。</font>