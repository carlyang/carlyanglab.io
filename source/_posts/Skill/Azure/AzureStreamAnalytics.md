---
title: Azure - Sync. Data from IoT Hub
categories:
 - 技術文件
 - Azure
date: 2019/08/05
updated: 2019/08/05
thumbnail: https://i.imgur.com/d0birrt.png
layout: post
tags: [Azure, IoT Hub, Azure Stream Analytics]
---
作者: Carl Yang

![](/Teamdoc/image/azure/logos/stream_analytics.png)

## 目的
為了要將上傳至IoT Hub的資料同步至各種資料儲存庫，常使用```Azure Stream Analytics(ASA)```同步資料，但其中對於資料來源的讀取有些要注意的限制。本文以IoT Hub為資料來源、探討其讀取限制及解決方法，以供使用ASA時的參考。
<!-- more -->

## 資料輸入
撰寫ASA SQL Like語法前，要先新增資料來源的輸入，本案例指定IoT Hub作為資料來源之一，請注意圖中的Consumer Group設定。
![](/Teamdoc/image/azure/AzureStreamAnalytics/asa1.png)

## 說明
IoT Hub做為大量的IoT資料儲存服務，可提供多種服務的資料來源，其背後是使用Event Hub作為基礎服務、提供資料存取的功能，而Event Hub提供資料讀取則是透過對外的Consumer Group做為橋梁。因此，當ASA對IoT Hub讀取資料時，會受限於每個Consumer Group只能有五個Receivers的限制，故如果ASA有以下錯誤訊息時，可以增加IoT Hub的Consumer Group來進行擴充。

參考文件如下:
[Maximum Event Hub receivers exceeded](http://thecodetricks.blogspot.com/2015/10/azure-stream-analytics-exceeded-maximum.html)

你可能會收到下列ASA錯誤Log:
>Maximum Event Hub receivers exceeded. Only 5 receivers per partition are allowed.
>Please use dedicated consumer group(s) for this input. If there are multiple queries using same input, share your input using WITH clause. 

上面的錯誤訊息有提到，可以在ASA的SQL中使用WITH Clause統一查詢資料來源，但因為我們的查詢可能會有多個Select...，每一個Select都會佔用一個Receiver額度，所以如果能夠將其分開來查詢，就能夠降低每個來源的使用量。

## 設定Consumer Group
IoT Hub增加Consumer Group也很簡單，可以進入IoT Hub &rarr; Built-in endpoints &rarr; Consumer Group，輸入新的Consumer Group Name即可。例如:你現在需要14個Receivers、每個Consumer Group有五個額度、那麼就要新增三個Consumer Group才會夠用，如下圖。

![](/Teamdoc/image/azure/AzureStreamAnalytics/asa2.png)

## 結論
經過上述的新增Consumer Group就可以有足夠的Receiver額度可以使用，雖然不是很高深的技術、但卻是容易忽略的小地方，本文提供朋友可能的參考解決方法之一，更好的解決方案，或許可以調教ASA的SQL語法、降低Receiver的使用量，也是可能的方向之一。

## 參考
1. [How can I define a consumer group into Azure IoT Hub?](https://stackoverflow.com/questions/42275723/how-can-i-define-a-consumer-group-into-azure-iot-hub)
2. [Azure IoT/Event Hub Consumer](https://streamsets.com/documentation/datacollector/latest/help/datacollector/UserGuide/Origins/AzureEventHub.html)
3. [Read device-to-cloud messages from the built-in endpoint](https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-devguide-messages-read-builtin)