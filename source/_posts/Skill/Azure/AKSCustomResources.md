---
title: AKS - Custom Resources
categories:
 - 技術文件
 - Azure
date: 2023/12/10
updated: 2023/12/10
thumbnail: https://i.imgur.com/5sNBEdu.png
layout: post
tags: [Azure, AKS]
---

![](/Teamdoc/image/azure/logos/aks.png)

## Purpose
[[AKS - ConfigMaps in Configuration]](/2023/12/05/Skill/Azure/AKSConfigMaps/)及[[AKS - Secrets in Configuration]](/2023/12/07/Skill/Azure/AKSSecrets/)兩篇介紹了自訂組態設定的兩種方式，但如果想要自訂義一些擴充性質的資源、而這些資源的存取是以透過擴充`Kubernetes API`的端點來完成，這時候就可以選擇使用`Custom Resources`，故本文目的主要在說明什麼是`Custom Resource Definition`及什麼情況下可以選擇使用`Custom Resources`。
<!-- more -->

## Custom Resources
`Custom Resources`的功能跟`ConfigMaps`或`Secrets`很類似，就是自訂一些資源讓呼叫端使用，但不同的是`Custom Resources`是透過擴充`Kubernetes API`端點的方式來設定，也就是說當我們創建完後會得到一串URI、而這URI可以讓我們直接存取這個資源集合(`Resource Collection`)，事實上`AKS`內部的`Pods`就是一種`Custom Resources`的自訂方式，引述[[官方文件]](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/#custom-resources)說明如下。

>A resource is an endpoint in the Kubernetes API that stores a collection of API objects of a certain kind; for example, the built-in pods resource contains a collection of Pod objects.

>A custom resource is an extension of the Kubernetes API that is not necessarily available in a default Kubernetes installation. It represents a customization of a particular Kubernetes installation. However, many core Kubernetes functions are now built using custom resources, making Kubernetes more modular.

>Custom resources can appear and disappear in a running cluster through dynamic registration, and cluster admins can update custom resources independently of the cluster itself. Once a custom resource is installed, users can create and access its objects using kubectl, just as they do for built-in resources like Pods.

那麼我們要如何選擇何時該使用`ConfigMaps`、何時該使用`Custom Resources`呢?[[官方文件]](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/#should-i-use-a-configmap-or-a-custom-resource)也給了我們一些建議如下。

- Use `ConfigMaps`:
  - 我們已經能事先知道這個資源的內容有些什麼的時候、而不是事後動態加入的，例如:環境變數、一般性`Key-value pairs`資訊...等。
  - 我們希望將這些資源放在同一個`ConfigMaps Key`中。
  - 如同[[AKS - ConfigMaps in Configuration]](/2023/12/05/Skill/Azure/AKSConfigMaps/)一文中提到的，當我們要創建的這些資源是要給`AKS`中的`Pods`使用的時候。
  - 我們想要讓這些資源是以環境變數的方式提供給`Pods`內部使用的情況。
  - 希望這些資源的更新可以透過類似`Deployment`的方式滾動更新。

- Use `Custom Resources`:
  - 希望透過Kubernetes client libraries或CLI的方式來建立及更新。
  - 能夠使用`kubectl`直接存取資源。
  - 想要建構相關的自動化機制、能夠監控資源的相關更新事件、並進行CRUD等動作(例如:Web Method)。
  - 想要撰寫automation來更新相關資源。
  - 想要使用 Kubernetes API的.spec、.status 和 .metadata等。
  - 當這一組資源是一種抽象化概念，也就是說它可能含有不明確、當下尚未知的資源，但它可能之後因為種種需求而動態建立的物件。

## Create Custom Resources in AKS
了解上述的`Custom Resources`基本概念及使用情境後，我們要開始來實際在`AKS`上建立一個`Custom Resource`，請先進入`AKS`的`Custom Resource`功能，如下圖。

進入後我們可以看到`Custom Resource`的相關資訊。
- `Resource`: 資源名稱，這邊會以群組的方式開合資源清單。
- `Group`: 群組名稱，也就是用來組織不同群組資源的依據。
- `Scopr`: 這邊如果顯示`Cluster`表示範圍屬於整個叢集。如果顯示`Namespaced`則表示隸屬於某個命名空間內，它會與`Namespace`的生命週期一致。
- `Version`: 版本號碼。
- `Age`: 顯示自從最後一次更新距今的天數。

![](/Teamdoc/image/azure/AKSCustomResources/crd1.png)


接著，我們試著來用`YAML`建立一個新的`Custom Resource Definition`(必須要先建立我們的自訂資源的相關定義)，範例如下。

- `apiVersion`: API版本號碼。
- `kind`: 資源類型，這邊固定為`CustomResourceDefinition`。
- `metadata.name`: 完整含Group的`複數名稱`(這邊要注意是使用下方`plural`定義的複數名稱)。
- `spec.group`: 群組名稱。
- `spec.vesions`: 版本資訊，我們可以在這邊透過定義不同版本的內容套用不同的設定。
- `spec.vesions.name`: 版本名稱。
- `spec.vesions.served`: 是否啟用。
- `spec.vesions.storage`: 是否儲存此版本(只能有一個版本設定為可儲存)。
- `spec.vesions.schema.openAPIV3Schema.type`: 資源的類型，這邊都定義為object。
- `spec.vesions.schema.openAPIV3Schema.properties.spec.type`: 物件類型。
- `spec.vesions.schema.openAPIV3Schema.properties.spec.properties`: 自訂義屬性。
- `spec.vesions.schema.openAPIV3Schema.properties.spec.properties`: 這邊就是自訂義屬性為`myValue`、型態為`string`。
- `scope`: 如為`cluster`則定義為叢集全域、如果是`Namespaced`則歸類於`Namespace`內、與`Namespace`的生命週期一致。
- `names.plural`: 複數名稱。
- `names.singular`: 單數名稱。
- `names.kind`: 類型名稱。
- `names.shortNames`: 名稱縮寫。
- ``: 
 
```yaml
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  name: customtests.mytest.aks.poc
spec:
  group: mytest.aks.poc
  versions:
    - name: v1
      served: true
      storage: true
      schema:
        openAPIV3Schema:
          type: object
          properties:
            spec:
              type: object
              properties:
                myValue:
                  type: string
  scope: Namespaced
  names:
    plural: customtests
    singular: customtest
    kind: CustomTest
    shortNames:
    - ct
```

再來我們先進入下圖的`Create a CRD`、將`YAML`範例貼上、最後按下`Add`按鈕即可建立新的`Custom Resource Definition`。
![](/Teamdoc/image/azure/AKSCustomResources/crd2.png)

![](/Teamdoc/image/azure/AKSCustomResources/crd3.png)

最後回到上一頁清單查看，已經可以看到我們建立好的`Custom Resource` - `Group: mytest.aks.poc` -> `CustomTest`，如下圖，

![](/Teamdoc/image/azure/AKSCustomResources/crd4.png)

Note:  
請注意，`上面範例僅是建立一個Custom Resource的定義，有了這個定義我們才可以開始新增這個定義類型的自訂資源!`

接下來我們實際建立一個`Custom Resource`、名稱為`my-customtest-object`。

```yaml
apiVersion: "mytest.aks.poc/v1"
kind: CustomTest
metadata:
  name: my-customtest-object
spec:
  myValue: "This is my custom test resource"
```

建立好後我們進入`CustomTest`查看，如下圖，我們就可以看到剛建立好的`my-customtest-object`了。

![](/Teamdoc/image/azure/AKSCustomResources/crd5.png)

最後我們使用指令查詢看看`CustomTest`的內容，接著就可以看到我們剛建好的`my-customtest-object`，表示我們接下來就可以使用`kubectl`指令來管理維護這些自訂資源，`事實上，這也表示Kubernetes API端點已被建立完成，因為指令kubectl就是跟API互相溝通的!`

```cmd
kubectl get CustomTest
```

```
NAME                   AGE
my-customtest-object   8m55s
```

## Conclusion
本文介紹`Custom Resources`的概念及相關使用的情境並讓大家有個參考，能夠判斷當下的情況是否適合使用`Custom Resources`，更重要的是為各位展示如何在`AKS`上使用`Azure Portal`提供的Web UI功能來管理維護`Custom Resources`，而`AKS`確實將許多複雜的指令簡單化為Web UI功能，對於一般性的觀禮維護作業真的方便許多。  
另一方面來說，因為各種使用情境不盡相同，本文無法明確地指定某個使用情境，這必須由各位朋友依據當下的需求來適當地規劃這些資源的使用，相信一定會有適合的使用情境的。

## References
1. [Custom Resources](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/)
2. [Extend the Kubernetes API with CustomResourceDefinitions](https://kubernetes.io/docs/tasks/extend-kubernetes/custom-resources/custom-resource-definitions/)