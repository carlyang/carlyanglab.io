---
title: Docker - 幾個常用的Docker Commands
categories:
 - 技術文件
 - Docker
date: 2018/9/9
updated: 2018/9/9
thumbnail: https://www.docker.com/sites/default/files/social/docker_facebook_share.png
layout: post
tags: 
 - Docker
---
作者: Carl Yang

## Docker
![](https://www.docker.com/sites/default/files/social/docker_facebook_share.png)
Docker是一個容器化的服務，讓開發者容易管理各式各樣的應用程式服務，其中有很多的指令需要使用，為了做個紀錄及方便日後查詢，將幾個常用的commands記錄下來，並簡述其作用。
<!--more-->

## Commands
這邊只列出幾個常用的指令說明，其他詳細內容可至[_Docker Docs_](https://docs.docker.com/engine/reference/builder/)查閱。

- #### 遠端操作Docker
Docker服務本身可透過遠端連線進行操作，搭配使用-H(Host之義)、並適當地設定docker服務(遠端Docker的deamon.json要加入"hosts": ["tcp://0.0.0.0:2375", "npipe://"])即可遠端對docker下指令操作。
```powershell
docker -H [DNS/IP] [commands]
```
- #### 查詢指令使用方法
直接下-h可查詢有哪些docker指令、如果是在指令後下-h則是列出該指令的相關參數及使用方法。
```
docker -h
docker ps -h
```
- #### 查看目前的images
查閱目前Docker內有那些images及其相關tags等資訊。
```
docker images
```
- #### 登入Container Registry
在拉Image前可能會先需要登入Container Registry。
```
docker login -u [User Name] -p [Password] [Container Registry Address]
```
- #### 拉Docker Image
預設下指令會去Docker Hub查詢有沒有Image，也可以指定去自己的Container Registry拉image。
```
docker pull [Container Registry]/[image name]:[image tag]
```
Note:
<font style="color:red;">**如果沒有指定tag就會預設拉最新版本的image。**</font>
- #### 執行Image的Container
使用docker run將Container跑起來，參數-it其實是-i -t的合併、-i是指跑在互動模式下，使用者可以透過command下指令操作，-t是指終端機模式、讓使用者透過command line進行操作，--rm則是告訴Docker在Container終止後立刻刪除、包含有使用到的volume等等資料，--name指的是image的名稱、-v則是指定Host與container內部要mapping的docker volume，-e則是指定跑起來後要帶入的環境變數名稱及值。
```
docker run -it --rm --name [Image Name] -v [Source Path]:[Target Path] -e "EnvironmentVariableName=Test"
```
- #### 推送image至指定的Container Registry。
可以將做好的image推送上去指定的Container Registry作為image hub之用，使用前可能會先需要使用docker login登入。
```
docker push [Container Registry]/[image name]:[image tag]
```
- #### 從.tar檔還原Docker Image
有時候我們的image不是存在公用空間的Container Registry，可能是以.tar的檔案方式存放，要先load進docker才能進行後續動作。
```
docker load < [Full Path]
```
- #### 將Image存入.tar檔
-o指的是要輸出的完整路徑及檔名、最後方帶入的則是Image的資訊
```
docker save -o "[Target Full path, e.g.D:\Test.tar]" [Container Registry]/[Image Name]:[Tag]
```
- #### 將Container存成檔案
Docker也可以直接將Container存成檔案。
```
docker export <CONTAINER ID> > [Full Path, e.g.D:\Test.tar]
```
- #### 建立Docker Image
將Publish的整包檔案，根據指定的dockerfile設定、建立Image。
```
docker build -t [Container Registry]/[Image Name]:[Tag] .
```
Note:
<font style="color:red;">**請特別注意最後方有一個'.'一定要有，它是告訴docker要將目前的所在目錄中的檔案、以及依照dockerfile中的設定Build出Image，如果沒有會建立失敗，而最後這個參數也可以指定到其他的完整目錄路徑。**</font>
- #### 查詢Container資訊
透過inspect指令可以列印出Container目前的Docker相關資訊，例如network、Volume等等。
```
docker inspect [container id]
```
- #### 進入Container內部查詢相關資訊
我們可以透過exec指令進入Container內部查詢相關資訊，-i指的是互動模式、最後方的cmd則是進入後要執行命令列輸入(也可以改成powershell)。
```
docker exec -i [container id] cmd
```
Note:
<font style="color:red;">**使用此指令時如果執行exit退出後，不會終止Container**</font>
- #### 列出目前的Container清單
列出目前執行中的Container清單資訊，此指令很常需要用到，因為可以查詢到Container ID以作為其他指令執行的參考。
```
docker ps
```
- #### 進入特定Container的STDIN模式
此指令也是用來對Container做操作。
```
docker attach [Container ID]
```
Note:
<font style="color:red;">**使用此指令時如果執行exit退出後，會連帶終止Container**</font>
- #### 刪除Container
刪除指定的Container，-f是force的意思、就是強制執行刪除、不等待工作完成。
```
docker rm -f [Container ID]
```
- #### 刪除Image
刪除一個或多個image，-f是強制刪除的意思。
```
docker rmi -f [Image ID1] [Image ID2]...
```
Note:
<font style="color:red;">**當該Image有執行中的Container時會無法刪除，需要先停止Container才能刪除Image。**</font>
- #### 查詢Container Logs
用來查詢AP執行時的logs紀錄，例如Console的STDOUT資訊等、都會被記錄進Docker的logs中，但要注意盡量不要寫太多、可能會造成Docker爆炸。
```
docker logs [Container ID]
```
- #### 查詢目前Container的各項系統資訊
此指令可用來查閱各Container目前的所有系統資訊，例如CPU使用率、記憶體使用率、磁碟I/O、網路I/O等等，也可以透過--format參數自訂要擷取的資訊(只下docker stats會列出所有資訊)。
```
docker stats --format "table {{.ID}}\t{{.Name}}\t{{.CPUPerc}}\t{{.MemUsage}}\t{{.NetIO}}\t{{.BlockIO}}\t{{.MemPerc}}\t{{.PIDs}}"
```
![](/Teamdoc/image/docker/DockerStats.png)

