---
title: Azure - The Trusted Services in Key-Vault Firewall
categories:
 - 技術文件
 - Azure
date: 2023/2/14
updated: 2023/2/14
thumbnail: https://i.imgur.com/OlN82Li.png
layout: post
tags: [Azure, Key-Vault]
---

![](/Teamdoc/image/azure/logos/key_vault.png)

最近使用`Key-Vault`的`Networking - Firewwall`功能時發現、有很多的`Azure`服務無法通過防火牆的檢查，概念上已經都是`Azure`上的服務了，應該都要可以存取才是?!其實不是這樣，同樣的`PaaS`服務、我們有別人也會有，總不能只要在`Azure`上的都能互相存取吧!所以不同的`Azure`服務之間也是必須要限制存取的。
<!-- more -->

## Purpose
使用`Key-Vault`的`Networking - Firewwall`功能時有個`Trusted Services`清單，他可以允許我們設定可存取`Key-Vault`的其他服務，例如:`App Services`。本文的目的在於說明`Key-Vault`的`Networking - Firewwall`功能中的`Allow IP Address`及`Trusted Services`功能。

## Allow IP Address
`Key-Vault`作為用來保存機密資訊的服務，安全性一定是很足夠的，尤其是在`Networking`的存取限制上，我們可以允許來自某些`Virtual Network`或是特定的`IP Ranges`的存取權。  
如下圖，我們可以進入`Key-Vault`->`Networking`->`Firewalls and virtual networks`->`Allow public access from specific virtual networks and IP addresses`->`Allow your client IP address`、輸入你要允許通過`Key-Vault Firewwall`的`IP Range`即可。

Note:  
這邊的`IP Range`表示法是採用`CIDR`方式，例如:`10.1.0.0/16`，表示IP在10.1.X.X的都可以允許存取。

![](/Teamdoc/image/azure/KeyVaultTrustedServices/keyvault1.png)

## Trusted Services
`Trusted Services`清單是指有些`Azure`服務及功能預設會在信任清單中，當我們勾選`Allow trusted Microsoft services to bypass this firewall`後，這些服務就能夠有`Key-Vault`的存取權、能夠通過`Key-Vault`的`Firewall`，如下圖。

![](/Teamdoc/image/azure/KeyVaultTrustedServices/keyvault2.png)

引述[[MS Doc]](https://learn.microsoft.com/en-sg/azure/key-vault/general/network-security?WT.mc_id=Portal-Microsoft_Azure_KeyVault#key-vault-firewall-enabled-trusted-services-only)的說明如下。

>## Key Vault Firewall Enabled (Trusted Services Only)
>When you enable the Key Vault Firewall, you'll be given an option to 'Allow Trusted Microsoft Services to bypass this firewall.' The trusted services list does not cover every single Azure service. For example, Azure DevOps isn't on the trusted services list. This does not imply that services that do not appear on the trusted services list not trusted or insecure. The trusted services list encompasses services where Microsoft controls all of the code that runs on the service. Since users can write custom code in Azure services such as Azure DevOps, Microsoft does not provide the option to create a blanket approval for the service. Furthermore, just because a service appears on the trusted service list, doesn't mean it is allowed for all scenarios.

相信上面這段說明已經很清楚了，這個`Trusted Services`清單並不是允許某個`Azure`服務，他就整個`Key-Vault`功能都可以通過，而是還要看是哪種`情境`。所以，`Trusted Services`清單允許的是`Azure`服務 + `特定的Key-Vault功能`，像我們本來以為`Trusted Services`清單內有包含`App Service`、它就可以通過`Key-Vault Firewall`存取`Key`、`Secret`等機密資訊。事實上，`Trusted Services`清單允許的只有`App Service`部署`Certificates`功能。我們可以進入[[Trusted services]](https://learn.microsoft.com/en-sg/azure/key-vault/general/overview-vnet-service-endpoints#trusted-services)查看詳細的內容。  
如下圖，對於`App Service`的`Key-Vault`存取、它僅允許部署`App Service Certificates`而已。


清單中還有列出其他的`Trusted Services`，當我們需要時務必先來這邊查閱是否服務及情境都符合需求。

![](/Teamdoc/image/azure/KeyVaultTrustedServices/keyvault3.png)

## Conclusion
綜合以上所述，`Trusted Services`是有限的、它並不是只針對服務、還必須考慮使用情境來決定，所以如果我們想要允許`App Service`可以存取`Key-Vault`中的`Key`、`Secret`等機密資訊，我們仍然必須回到白名單這邊、把所有`App Service`的`Outbound IPs`加入才行。  
另外，其實也還有使用`Virtual Networks`的方式，將所有相關服務置於同一私有網段內，然後再回到白名單這邊加入私有網段的`Subnet`，不過這跟本篇目的無關就看未來再另外說明。

## References
1. [Key Vault Firewall Enabled (Trusted Services Only)](https://learn.microsoft.com/en-sg/azure/key-vault/general/network-security?WT.mc_id=Portal-Microsoft_Azure_KeyVault#key-vault-firewall-enabled-trusted-services-only)
2. [Trusted services](https://learn.microsoft.com/en-sg/azure/key-vault/general/overview-vnet-service-endpoints#trusted-services)