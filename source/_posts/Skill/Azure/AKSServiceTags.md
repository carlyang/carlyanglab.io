---
title: AKS - How to set Service Tags to allow Azure services to access
categories:
 - 技術文件
 - Azure
date: 2023/12/12
updated: 2023/12/12
thumbnail: https://i.imgur.com/5sNBEdu.png
layout: post
tags: [Azure, AKS, Service Tags]
---

![](/Teamdoc/image/azure/logos/aks.png)

## Purpose
最近為了增加`AKS`的網路安全性、必須限制來源存取，一般來說就是進入`Networking`開啟`IP Ranges`並輸入白名單(`CIDR`表示法)、來允許特定來源可以存取。但是除了我們自己能夠確認的來源外、還有很多其他服務也必須要允許存取，例如: `Azure App Service`，可是這些雲上的服務無法確保它的IP永遠不變，而且這些服務的IP也都是受`Azure`管控，我們不太可能直接使用`IP Ranges`的方式輸入白名單，一旦有天IP變了就會突然無法存取`AKS`。所以，本文的目的就是如何在`AKS`內設定`Service Tags`以允許特定類型的`Azure`服務來存取。
<!-- more -->

## Networking IP Ranges
如下圖所示，如果我們已經知道有那些`IP Ranges`要被允許存取、且這些`IP Ranges`的變更是受我們能夠管控的，那就可以進入`Networking`功能(下圖紅框)來設定白名單。

![](/Teamdoc/image/azure/AKSServiceTags/servicetags1.png)

## Service Tags
`Service Tags`的概念口語上來說、就是利用一個關鍵字(例如:`ActionGroup`)來表示`Azure`上的一組同類型服務的`Public IP Ranges`，而`AKS`就可以讓我們指定要允許的特定`Service Tag`白名單、不必使用動態變更的`IP Ranges`，避免時常不定時地更換IP，如下圖紅框所示為`ActionGroup`以及目前對應的`IP Ranges`。

![](/Teamdoc/image/azure/AKSServiceTags/servicetags2.png)

事實上`Service Tags`對應的`IP Ranges`也是時常在更換的，微軟會不定時更新這份對應資訊，我們需要到[[最新Service Tags對應表下載]](https://www.microsoft.com/en-us/download/details.aspx?id=56519)這邊下載來查詢。

## Service Tags Settings
上述的資訊都準備好之後，我們就要來著手設定`AKS`的`Service Tags`，但是找了半天卻找不到任何地方可以設定，原來`AKS在Azure Portal上根本就沒有提供UI功能可以設定!`  
所以，我們就必須要直接編輯`AKS`的`YAML`設定。

`YAML`標記語言如下:
```yaml
service.beta.kubernetes.io/azure-allowed-service-tags: "ActionGroup"
```

1. 首先，我們先進入`AKS`的`Services and ingresses`、找到`ingress-nginx-controller`這個服務、再點選它進入詳細內容。

![](/Teamdoc/image/azure/AKSServiceTags/servicetags3.png)

Note:  
`ingress-nginx-controller`就是`AKS`的`Nginx`服務，負責反向代理等等工作，所以為了能夠設定`Service Tags`白名單，就必須要告訴`Nginx`目標`Service Tags`為何。

2. 接著再進入`YAML`並找到`ingress-nginx-controller` -> `annotations`下、貼上`azure-allowed-service-tags`標記(要注意縮排位置，這是`YAML`的格式)，如下圖所示，最後儲存後就會生效。

Note:  
修改這個設定並不會造成`AKS`斷線，請放心!

![](/Teamdoc/image/azure/AKSServiceTags/servicetags4.png)

## Conclusion
`AKS`是`Azure`平台導入`K8s`雖然有段時間了，但`K8s`博大精深，可能微軟對於一些細部的功能支援度還是不夠，所以`Azure Portal`上還沒有提供功能來調整`Service Tags`設定，不過因為微軟積極導入`K8s`功能，相信不久後也會推出相關功能支援，到時候就不用這麼麻煩處理了。

## References
1. [Azure IP Ranges and Service Tags – Public Cloud](https://www.microsoft.com/en-us/download/details.aspx?id=56519)