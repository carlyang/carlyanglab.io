---
title: Azure - How to use TNS to connect to Oracle DB
categories:
 - 技術文件
 - Azure
date: 2024/4/24
updated: 2024/4/24
thumbnail: https://i.imgur.com/oHcuJIO.png
layout: post
tags: [Azure, Azure Data Factory]
---

![](/Teamdoc/image/azure/logos/adf.png)

## Purpose
[上一篇](/2024/02/22/Skill/Azure/ADFOracleLinkedService)介紹如何在`ADF`中使用`Linked Service`連接`Oracle DB`，但當時也發現對於`TNS`及`TCPS`似乎並不支援，這使得在雲端使用更安全的加密連線不可行，經過這段時間的研究終於找到一個折衷的方式，本文的目的在於為各位介紹如何使用`TNS`的方式進行`TCPS`連線`Oracle DB`。
<!-- more -->

## Prerequisite
在開始前請先準備好下列Oracle資料庫連線資訊，
1. DB URL
2. Port
3. SID/Service Name
4. Username
5. Password

## 建立TNSNAMES.ORA檔
請先依照以下格式設定`TNS`，因為稍後我們會使用到它。

```
<TNS Name> =
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCPS)(HOST = <URL>)(PORT = <Port Number>))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = <service name>)
    )
  )
```

Note:  
1. `請注意，要替換<ConnectionName>、<URL>、<Port Number>、<service name>`。
    - `<ConnectionName>`: 可以自訂名稱。
    - `<URL>`: Oracle DB URL。
    - `<Port Number>`: Oracle DB使用的通訊埠。
    - `<service name>`: Oracle DB Name。
    - 最後儲存`TNSNAMES.ORA`檔案即可，並記錄下檔案存放路徑，例如:`C:\TNSNAMES.ORA`，後面我們要把這個路徑寫入`Linked Service`的`JSON`設定中。
2. 如果依照原本`TNSNAMES.ORA`檔案的使用方式，我們還需要先安裝`Oracle Instane Client`、再把`TNSNAMES.ORA`檔案放進`Oracle Instane Client`的資料夾內才能成功讀取，但因為接下來後續會改用`Self-Hosted Integration Runtime`來讀取檔案，`故這邊不需要安裝Oracle Instane Client`。

## Self-Hosted Integration Runtime
`Self-Hosted Integration Runtime`(以下簡稱`SHIR`)的詳細說明可以參考[[Azure - Azure Data Factory的自我裝載整合執行階段]](/2023/03/03/Skill/Azure/ADFSelfHostedIntegratonRuntime/)。

請先在`Linked Service`上建好`SHIR`並將`Key`先Copy下來，然後在要連線`SHIR`的機器上安裝好`IR`並填上剛Copy下來的`Key`，如果網路一切正常的話應該就可以成功連線，大家可以到[[Download IR]](https://www.microsoft.com/en-us/download/details.aspx?id=39717)下載安裝檔。

待安裝完成並成功連線至`ADF`後應該會看到如下圖所示綠燈狀態。

Note:  
要先安裝`IR`是因為我們必須要依靠一台Hosted機器從地端載入`TNS`設定、並從地端進行連線，`目前在ADF雲端上要直接進行Oracle DB的TCPS連線尚不支援!`不過，在目前的使用情境下還不需要在雲端進行直接連線，從地端連線撈資料還比較安全，因為這樣資料就不需要經過公有雲造成疑慮、並且防火牆只須開放公司對`Oracle DB`連線的IP即可。

![](/Teamdoc/image/azure/ADFOracleUseTNS/IR1.png)

## Linked Service
- 接下來我們先進`ADF`的`Linked Service`功能然後新增一個連線服務，我們先輸入`Oracle`搜尋 -> 點選`Oracle`選項 -> 按下左下角的`Continue`按鈕進行下一步，出現設定視窗後我們先輸入名稱、並指定使用我們先前建立的`SHIR`，如下圖。

![](/Teamdoc/image/azure/ADFOracleUseTNS/linkedservice1.png)

- 接著我們修改一下的`JSON`設定範例，然後勾選`Specify dynamic contents in JSON format`、在方塊中填入改好的JSON、使用下方的`Test connection`測式連線是否成功，最後按下`Create`按鈕即可建立，如下圖。

```json
{
	"properties": {
		"type": "Oracle",
		"typeProperties": {
			"connectionString": "ServerName=<TNS Name>;TNSNamesFile=<TNS File Path>;User Id=<UserID>;Password=<Password>"
		}
	}
}
```

![](/Teamdoc/image/azure/ADFOracleUseTNS/linkedservice2.png)

Note:  
1. 替換說明如下。
    - `<TNS Name>`: `TNS`檔內設定的連線名稱。
    - `<TNS File Path>`: `TNS`檔的所在路徑。
    - `<UserID>`: 使用者帳號。
    - `<Password>`: 使用者密碼。
2. `請注意，當我們建立Linked Service後，關閉視窗下次再重開會發現設定似乎跑掉，但實際跑查詢仍然是正常的`，這是因為`ADF Studio`預設開啟的UI畫面並不是使用JSON的設定方式，它會將我們輸入的JSON轉成上方各欄位資訊來顯示(如下圖)，但個人還是覺得有點怪，因為像`Port`及`Connection type`兩欄會變成預設值、`Host`也會顯示空白，但這幾個資訊都是設定在`TNS`檔中且值也不對、可能會造成混淆，個人是覺得如果有勾選`Specify dynamic contents in JSON format`就保持顯示JSON內容就好，可能看後續會不會修正這問題了。

![](/Teamdoc/image/azure/ADFOracleUseTNS/linkedservice3.png)

## Conclusion
這段時間以來嘗試各種方式要建立`TCPS`連線`Oracle DB`都不得其法，最後才在`SHIR`及`JSON`的部分查詢到方法，基本上[[微軟文件]](https://learn.microsoft.com/en-us/azure/data-factory/connector-oracle?tabs=data-factory#linked-service-properties)也是略為帶過`TNSNAMES.ORA`檔而已，並沒有說明詳細的使用方式。

但是為了在`ADF`使用更安全的`TCPS`連線，還是必須要能夠支援才行，故本文利用`SHIR`載入`TNSNAMES.ORA`檔內的連線設定，並且透過自訂JSON的`Linked Service`設定、直接從地端Host與`Oracle DB`進行`TCPS`連線，比使用一般的`TCP`連線更加安全，而且只要在防火牆白名單允許公司的連線IP通過即可，不需要允許`ADF`的整個`IP Range`，是比較安全的方式。

## References
1. [Linked service properties](https://learn.microsoft.com/en-us/azure/data-factory/connector-oracle?tabs=data-factory#linked-service-properties)