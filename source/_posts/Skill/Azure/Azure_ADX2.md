---
title: Azure - Azure Data Explorer(2) - Operators
categories:
 - 技術文件
 - Azure
date: 2021/7/24
updated: 2021/7/24
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, ADX, Azure Data Explorer]
---
Author: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Purpose
前一篇[[Azure - Azure Data Explorer(1)]](/2021/07/17/Skill/Azure/Azure_ADX1/#more)介紹如何建立`External Table`以存取`Azure Blob`、並簡單使用幾種查詢指令示範如何查詢其中的檔案內容，本篇則是進一步介紹`Azure Data Explorer`(以下簡稱`ADX`)常見的幾種運算子及使用範例。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Azure Data Explorer

## Introduction
運算子(`Operators`)在`ADX`中不是指`+、-、*、/(加減乘除)`這種運算符號，而是針對類SQL查詢中一些最小的運算動作，例如`count`、`take 5`、`project`等等，從查詢面來看這些運算子是最小、最基本不太能再被分割的動作，而這些運算子卻可以被用在大部分的情境中，引用[[MS Doc]](https://docs.microsoft.com/en-us/azure/data-explorer/write-queries)的說明如下，接下來就來介紹一下幾種常見運算子。

>A query in Azure Data Explorer is a read-only request to process data and return results. The request is stated in plain text, using a data-flow model designed to make the syntax easy to read, author, and automate. The query uses schema entities that are organized in a hierarchy similar to SQL: databases, tables, and columns.

## Common Operators
- `count`: 傳回目前查詢資料的筆數。  
下方查詢回傳外部資料表`'ExternalTable'`目前的資料筆數。

```sql
external_table("ExternalTable") 
| count
```

- `take`: 取回目前外部資料表的N筆資料`(是取回資料的N筆、要看它資料抓取順序而定)`  
請注意`take` 後面一定要帶整數數字、否則會顯示`Syntax Error`。

```sql
external_table("ExternalTable") 
| take 10
```

- `project`: 取回目前外部資料表欄位的子集合。  
也就是說假設今天資料表有A、B、C欄位，下列指令就只會回傳B欄位的資料。

```sql
external_table("ExternalTable") 
| project B
```

- `where`: 使用條件過濾資料這部分使用方式跟`T-SQL`非常相似。  
這邊要注意，`如果資料表欄位類型是datetime的話，必須使用todatetime()把值轉換為datetime才不會錯誤`，這是因為`ADX`不像T-SQL那樣會自動幫你轉換類型。

```sql
external_table("ExternalTable")
| where SystemTime >= todatetime("2021-07-23") and SystemTime <= todatetime("2021-07-24")
| sort by EventEnqueuedUtcTime desc
```

- `sort`: 針對特定欄位排序。  
使用`sort`時後面必須帶入`by [欄位名稱] [asc(升冪)/desc(降冪)]`，而且`一定要填asc或desc否則ADX不會幫你排序`，這跟`T-SQL`不寫時自動依照asc排序資料不同。

```sql
external_table("ExternalTable")
| where SystemTime >= todatetime("2021-07-23") and SystemTime <= todatetime("2021-07-24")
| sort by EventEnqueuedUtcTime desc
```

- `top`: 這跟前面的`take`指令不同，它才是回傳頭N筆資料。  
除了補足`take`指令不一定是前N筆的部分，`top`指令後面一定要指定排序欄位、也就是`by [欄位名稱] [asc(升冪)/desc(降冪)]`，才會知道要依照什麼順序抓前N筆。

```sql
external_table("ExternalTable")
| top 5
```

## 結論:
本文僅先介紹幾種常見的`ADX`運算子使用方式及範例，基本上都是很貼近實務上會使用到的概念，後續會再介紹其他更進階的運算子，例如`彙總(summary)`、`計算欄位(extend)`等的進階用法。

## 參考
1. [[Write queries for Azure Data Explorer]](https://docs.microsoft.com/en-us/azure/data-explorer/write-queries)