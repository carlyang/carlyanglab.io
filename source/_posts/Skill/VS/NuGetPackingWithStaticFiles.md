---
title: NuGet Package - Packing with Static Files
categories:
 - 技術文件
 - Visual Studio
date: 2019/05/23
updated: 2019/05/23
thumbnail: https://i.imgur.com/gEzvRbT.png
layout: post
tags: [Visual Studio, NuGet Package, Static Files]
---
作者: Carl Yang

![](/Teamdoc/image/VS/2019/NuGetPackingWithStaticFiles/VSWinIcon_100x.png)

## 目的
開發者很常需要將一些可重複、跨專案使用情境的元件，包裝為NuGet Package後發佈出去，而其中靜態檔案也可以藉由NuGet安裝達到分享的目的，本篇介紹如何以透過打包NuGet Package的方式分享靜態檔案。
<!-- more -->

## 靜態檔案
開發時可能有些靜態檔案需要分享給其他專案使用，例如:.json、.xml、.html等，舉凡如自訂義組態設定放在.json中供AP使用，或是一些網頁樣板等等內容。

## NuGet Package
這裡就不多贅述如何打包NuGet Package，主要在`如何將靜態檔案包進Package中`。

1. 首先我們在專案中加入一個新資料夾-configFiles、並放入兩個json檔。
   ![](/Teamdoc/image/VS/2019/NuGetPackingWithStaticFiles/packing1.png)

2. 接著再將檔案屬性中的Copy屬性設為Copy always，讓VS在Build的時候持續複製檔案至輸出資料夾。
   ![](/Teamdoc/image/VS/2019/NuGetPackingWithStaticFiles/packing2.png)

3. 再來我們開始編輯專案檔。
   ![](/Teamdoc/image/VS/2019/NuGetPackingWithStaticFiles/packing3.png)

4. 我們找到下面的xml tag，將路徑改為`configFiles\\*.*`，表示載入該資料夾下的所有檔案，如果要限定副檔名、可改為*.json等字串。  
最後，`在None標籤下、跟CopyToOutputDirectory標籤同層的下方，加入Pack標籤且設為true`，意思就是要將靜態檔案一起包入NuGet Package中。
```xml
  <ItemGroup>
    <None Update="configFiles\*.*">
      <CopyToOutputDirectory>Always</CopyToOutputDirectory>
      <pack>true</pack>
    </None>
  </ItemGroup>
```

5. 接著我們pulish專案後，使用解壓縮軟體解開*.nupkg檔，觀察打包完的內容。
   ![](/Teamdoc/image/VS/2019/NuGetPackingWithStaticFiles/packing4.png)

6. 進入查看我們會發現多了content及contentFiles兩個資料夾，這資料夾是打包時預設的，我們可以暫時不用理會他、直接進入`contentFiles`即可。
   ![](/Teamdoc/image/VS/2019/NuGetPackingWithStaticFiles/packing5.png)

7. 進入`contentFiles`後我們一路深入資料夾檢視，最後就可以發現我們的兩個.json檔已經被包進來了。
   ![](/Teamdoc/image/VS/2019/NuGetPackingWithStaticFiles/packing6.png)

## Install NuGet Package
1. 打包完Package後，接著就是如何使用了。我們先安裝Package進專案中，裝好後會發現VS自動幫我們在專案內建好外部連結。
   ![](/Teamdoc/image/VS/2019/NuGetPackingWithStaticFiles/packing7.png)

    > 這是連結到`%userprofile%\.nuget\packages\`下的package資料夾，所以你不會在專案資料夾內看到實體檔案。

2. 我們一樣再把這些檔案的Copy屬性設為Copy always，如此我們要Debug或是未來發佈時，`output資料夾內才會包含這些靜態檔案`。
   ![](/Teamdoc/image/VS/2019/NuGetPackingWithStaticFiles/packing2.png)

3. 但這樣.csproj檔內的路徑就會是絕對路徑，為了不要一直手動修改，我們還需要做點調整，一樣是修改XML tage。我們先找到下列的XML tag，修改路徑為`$(UserProfile)\.nuget\packages\[your package name]\*\contentFiles\any\netstandard2.0\configFiles\*.*`，要注意是用`$(UserProfile)`下的`[your package name]\*\`位置，且`*是用來替換原本的Package版號`。
```xml
<None Update="$(UserProfile)\.nuget\packages\[your package name]\*\contentFiles\any\netstandard2.0\configFiles\*.*">
    <CopyToOutputDirectory>Always</CopyToOutputDirectory>
</None>
```

4. 最後，記得在Code中讀靜態檔的時候，需要抓對路徑才能順利讀檔案。取得路徑的其中一個方式，可使用`AppDomain.CurrentDomain.BaseDirectory`，抓取當下的執行路徑。

## 結論
很多時候當我們有些檔案想要隨著專案需求、分享給不同的Code使用時，其實檔案內容都一樣，實在很沒必要每個專案都自己Copy一份，且這些靜態檔案更常是相依在元件本身，所以將靜態檔案隨著NuGet Package一起共享，也不失為一種好方法喔!

## 參考
1. [dotnet pack does not include files from the tools folder](https://github.com/dotnet/cli/issues/7566)