---
title: .NET Core - CORS跨網域請求
categories:
 - 技術文件
 - .NET Core
date: 2019/01/09
updated: 2019/01/09
thumbnail: https://i.imgur.com/m8rIjyr.png
layout: post
tags: [.NET Core, CORS]
---
作者: Carl Yang

![](/Teamdoc/image/DotnetCore/Logo.png)

## Cross-Origin Resource Sharing(CORS)
跨來源資源共享是由[W3C](https://www.w3.org/TR/cors/)所制定的技術規範，他制定了瀏覽器在不同網域(跨網域)間的溝通機制，引文如下:
>This document defines a mechanism to enable client-side cross-origin requests. Specifications that enable an API to make cross-origin requests to resources can use the algorithms defined by this specification. If such an API is used on http://example.org resources, a resource on http://hello-world.example can opt in using the mechanism described by this specification (e.g., specifying Access-Control-Allow-Origin: http://example.org as response header), which would allow that resource to be fetched cross-origin from http://example.org.

這是由於安全性議題而衍生的機制，因為瀏覽器針對不同網域的資源存取、必須要有其限制，如果各式各樣的資源都可以透過瀏覽器隨便取用，那這個安全性就會非常低、也可能造成巨大的損失。但很多時候有必要允許跨域存取、例如RESTful API作為共通的HTTP協定，可能要提供多個不同網站調用。因此CORS是為了瀏覽器進行跨域請求時而衍生的一套安全機制，當API不允許此網站存取時也能夠加以限制，而.NET Core本身以Middleware的方式註冊CORS以處理不同來源的跨域請求。
<!-- more -->

## Prerequisites
本文是使用.NET Core 2.2作為開發環境進行練習，請先安裝下列項目。
- 請先安裝.NET Core 2.2 SDK([Download](https://dotnet.microsoft.com/download))

## 跨域請求
如下圖，當瀏覽器從Web Server取得網頁內容後、javascript需要去另一個網域上的API Server取得資料時(因為兩台Server不同網域、形成跨網域的條件)，會先對該API Server發Preflight Request詢問是否允許CORS，如果API允許就會回傳帶有Access-Control-Allow-Origin的標頭給瀏覽器，接著瀏覽器就可以連同Access-Control-Allow-Origin標頭實際發真正的Request給API Server，反之，如果回應沒有Access-Control-Allow-Origin就無法順利存取API。
![](/Teamdoc/image/DotnetCore/cors/cors1.png)

## CORS Middleware
.NET Core在CORS的使用上非常方便，本身是使用Microsoft.AspNetCore.Cors套件，可以註冊Middleware的方式來使用CORS。

Note:<br/>
<font style="color:red;">.NET Core 1.0以前沒有內含Microsoft.AspNetCore.Cors套件，需要先從NuGet自行安裝。</font>.NET Core 2.0後才內含進來。
```cmd
dotnet add package Microsoft.AspNetCore.Cors
```

- 我們要先在API專案的Startup.cs中的Configure()註冊。
```csharp
app.UseCors(@"CorsPolicy");//CorsPolicy是自訂的Policy Name
```

- 接著在ConfigureServices()加入Service進行CORS的設定。
```csharp
services.AddCors(options =>
{
	options.AddPolicy("CorsPolicy", p =>
	{
		p.AllowAnyOrigin()
			.AllowAnyHeader()
			.AllowAnyMethod()
			.AllowCredentials();
	});
});
```

1. AllowAnyOrigin:
允許任何來源的跨域請求，如果只允許特定幾個來源的請求，可以改用WithOrigins()、並帶入網址以允許一個來源請求，或多個來源用逗號隔開。

2. AllowAnyHeader:
允許任何的Header，如果只允許具有特定Header的請求，可改用WithHeaders()、允許多個用逗號隔開。

3. AllowAnyMethod:
允許任何HTTP Method的請求，如果只允許特定HTTP Method，可改用WithMethods(@"GET,POST")、允許多個用逗號隔開。

4. AllowCredentials:
允許CORS Credentials，當瀏覽器帶入Credentials時、API端也要允許才能夠通過。

## 全域註冊
既然是用Middleware的形式註冊使用，那當然CORS也有全域、區域的分別，全域註冊的方式就是上方所提的方式，在Startup.cs中的Configure()註冊，那整個API的所有Controller及Action都會套用。
```csharp
app.UseCors(@"CorsPolicy");//CorsPolicy是自訂的Policy Name
```

## 區域註冊
如果僅希望某幾個特定的Controller或Action套用CORS，一樣是使用Attribute的方式、把EnableCors加在Controller或Action上標註即可，不同點在於要帶入的是CORS Policy名稱、而非如自訂Middleware般帶入Type。
```csharp
//...
[EnableCors(@"CorsPolicy")]
public class ValuesController : ControllerBase
{
	//...
	[EnableCors(@"CorsPolicy")]
	public ActionResult<IEnumerable<string>> Get()
	//...
```

Note:<br/>
1. 在[微軟文件](https://docs.microsoft.com/en-us/aspnet/core/security/cors?view=aspnetcore-2.2)中會看到[Enable CORS in MVC]，其實指的就是區域註冊的意思、意指套用在特定的MVC Controller或Action上。
2. 如果是相反的情況、大部分都要套用CORS、只有少部分不要用CORS，那可以在不要套用CORS的Controller或Action上<font style="color:red;">改用DisableCors("[Your policy name]")</font>即可。

## 結論
.NET Core的CORS套件使用起來非常方便，不再像以前Framework時代、不僅要另外安裝套件、還要在web.config設定XML Tags，方便又實用。

## References
1. [Enable Cross-Origin Requests (CORS) in ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/security/cors?view=aspnetcore-2.2)