---
title: Azure - 如何讓Azure SQL只允許透過私有虛擬網路連線
categories:
 - 技術文件
 - Azure
date: 2023/3/6
updated: 2023/3/6
thumbnail: https://i.imgur.com/noxzDjY.png
layout: post
tags: [Azure, Azure SQL]
---

![](/Teamdoc/image/azure/logos/azure_sql.png)

## Purpose
`Azure SQL`的安全性一直是個被高度關注的議題，最近公司的`Azure`服務安全性也被抓到這一塊、被發現沒有綁定私人虛擬網路，雖然說`Azure SQL`有防火牆規則限制`Public Access`，加上公司的`Azure`訂閱帳號是採用`MFA`兩階段式驗證，但是如果`在資料傳輸上沒有綁私人網路、資料的路由會經由公共網路傳輸`，這也是個安全性疑慮(基本上都是加密傳輸，但就是有人覺得危險!)。故本文的目的就是介紹如何設定`Azure SQL Server`的`Private Endpoint`並綁定`vNET`，讓整個資料傳輸都在私有虛擬網路中、以達到更高的安全性。
<!-- more -->

## Private Endpoint
`Private Endpoint`(私人端點)的概念就是在`vNET`中建立一個屬於該服務的私有存取端點，只有在相同的`vNET`中的服務才能存取它，而`Private Endpoint`在建立之初就會被賦予一個私有IP(其他服務綁定`Private Endpoint`也是)以讓其它服務存取，引述[[MS Doc]](https://learn.microsoft.com/en-us/azure/private-link/private-endpoint-overview)說明如下。

>A private endpoint is a network interface that uses a private IP address from your virtual network. This network interface connects you privately and securely to a service that's powered by Azure Private Link. By enabling a private endpoint, you're bringing the service into your virtual network.

由上所述可以知道，`Private Endpoint`就是一個私有虛擬網路中一個網路介面，讓其它服務知道該往哪個地方去連線及存取。(另外有提到`Private Link`這部份因為不在本文範圍，待後續再另開一篇介紹說明)

詳細支援`Private Endpoint`的`Azure`服務清單可參考[[MS Doc]](https://learn.microsoft.com/en-us/azure/private-link/private-endpoint-overview#private-link-resource)。

`要注意一下，下列支援的是Azure SQL Server喔!不是Database`，不要搞錯了。

![](/Teamdoc/image/azure/SQLDBPrivateEndpoint/pe1.png)

Note:  
1. 有幾個要注意的地方，`就是當綁定Private Endpoint後，網路連線的建立是在虛擬網路中直接連線、服務提供者並沒有路由設定可用來建立這個連線`。

>Network connections can be initiated only by clients that are connecting to the private endpoint. Service providers don't have a routing configuration to create connections into service customers. Connections can be established in a single direction only.

2. `Private Endpoint`必須要被部署在相同的區域及訂閱中。

>The private endpoint must be deployed in the same region and subscription as the virtual network.

3. 可以同時在相同的虛擬網路上建立多個`Private Endpoint`、以支援多個服務間的溝通。

>Multiple private endpoints can be created with the same private-link resource. For a single network using a common DNS server configuration, the recommended practice is to use a single private endpoint for a specified private-link resource. Use this practice to avoid duplicate entries or conflicts in DNS resolution.

## 費用
基本上，`Private Endpoint`的計費是一個一個算的，而且資料的輸入/輸出都要計費，這部分也需要特別注意，以`EAST Asia`來算的話，預估費用如下圖。

Note:  
既然`Private Endpoint`的計費是一個一個算的，那每個服務當然也都必須綁一個`Private Endpoint`，所以其實每個月累加起來也可能不少喔!下圖是以小時為單位計價，而且`Private Endpoint`建立後就是24x7在運作、不太會關閉的。

![](/Teamdoc/image/azure/SQLDBPrivateEndpoint/pe2.png)

## Configuration
接下來就是`Azure SQL`上的設定步驟了，請先確定已先建立好一台可測試連線的`Virtual Machine`、以及一個已建好的`vNET`(建立`Virtual Machine`時就會順便建一個`vNET`了)，這邊就不再多說明這兩個服務的建立步驟了，只專注在`Azure SQL`的部分。

1. 進入`Azure SQL Server` -> `Security` -> `Networking` -> `Private access` -> `Create a private wndpoint`。

![](/Teamdoc/image/azure/SQLDBPrivateEndpoint/pe3.png)

2. Basic輸入各項必填欄位值。

![](/Teamdoc/image/azure/SQLDBPrivateEndpoint/pe4.png)

3. Resource的部分選擇要綁定的`Azure SQL Server`。

![](/Teamdoc/image/azure/SQLDBPrivateEndpoint/pe5.png)

4. Virtual Network請選擇與等下要用來測試的`Virtual Machine`相同的`vNET`。

![](/Teamdoc/image/azure/SQLDBPrivateEndpoint/pe6.png)

5. DNS的部分基本上都使用預設即可(除非有需要改其它設定)。

![](/Teamdoc/image/azure/SQLDBPrivateEndpoint/pe7.png)

6. Tag的部分基本上也使用預設即可(除非有需要改其它設定)。

![](/Teamdoc/image/azure/SQLDBPrivateEndpoint/pe8.png)

7. 檢查通過就可以按下Create按鈕新增`Private Endpoint`。

![](/Teamdoc/image/azure/SQLDBPrivateEndpoint/pe9.png)

8. 最後建立完後回到`Private access`設定，就可以看到剛才建立好的`Private Endpoint`。

![](/Teamdoc/image/azure/SQLDBPrivateEndpoint/pe10.png)

## 測試連線
接下來就是利用`Virtual Machine`實際測試連線，如下步驟。

1. RDP先進`Virtual Machine`、然後我們使用`SSMS`使用`SQL Aythentication`方式連線，就可以成功連線了。

![](/Teamdoc/image/azure/SQLDBPrivateEndpoint/test1.png)

2. 接著我們回到local端的機器，同樣打開`SSMS`輸入一樣的登入資訊，你會發現連線被擋了回來、並且告知`Deny Public Network Access is set to Yes.`。

![](/Teamdoc/image/azure/SQLDBPrivateEndpoint/test2.png)

這是因為建好`Private Endpoint`後`Azure SQL Server`的`Public access`設定會是`Disable`。

![](/Teamdoc/image/azure/SQLDBPrivateEndpoint/test3.png)

Note:  
`請注意，由於維運需要可能還是必須打開Azure SQL Server的外網存取，所以如果有必要重新打開Public access即可`，重新打開後就會依照之前使用防火牆規則及`Azure MFA`的方式(要看是否有使用)進行驗證。

## Conclusion
本文說明`Private Endpoint`在`Azure SQL Server`上的設定方式，並且搭配同一個`vNET`的`Virtual Machine`進行連線測試，以驗證`Private Endpoint`是否生效。事實上，`使用Private Endpoint的方式最大優點在於資料傳輸時的網路路由、是在同一個vNET中進行直接連線，如果僅使用Public access則資料傳輸時的路由就可能會經過公有網路路由`，對於一些特別重視安全性的考量會是比較好的方式。

## References
1. [What is a private endpoint?](https://learn.microsoft.com/en-us/azure/private-link/private-endpoint-overview)
2. [Create a private endpoint](https://learn.microsoft.com/en-us/azure/private-link/create-private-endpoint-portal?tabs=dynamic-ip#create-a-private-endpoint)