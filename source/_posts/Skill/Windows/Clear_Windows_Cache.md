---
title: Windows - Clear Cached Memory
categories:
 - 技術文件
 - Windows
date: 2019/05/21
updated: 2019/05/21
thumbnail: /Teamdoc/image/Windows/logo.png
layout: post
tags: [Windows, Cache, Memory]
---
作者: Carl Yang

![](/Teamdoc/image/Windows/windows_logo.png)

## Cached Memory
Windows在記憶體管理有個Cached統計值，主要用來暫存一些檔案快取的資料、以加快存取速度。
<!-- more -->

## Cached機制
Cached Memory是Windows用來暫存一些常用的資料，例如檔案等等。  
它會將這些快取資料暫存在實體記憶體中、佔用實體記憶體的空間，而且它會盡可能地快取資料以提高存取速度。  
當實體記憶體不足、又有其他優先權更高的Process要求記憶體空間時，就會釋放優先權較低的Cached空間出來。

## Task Manager
讓我們來觀察一下Task Manager中的記憶體統計值，由下圖可以看到目前總共12GB的實體記憶體，目前已使用6.2GB的量、而不可回收的Non-paged pool用了1.7GB的量、Cached目前則使用了5.4GB的量。  
我來算一下<font color="red">(1.7GB + 5.4GB = 7.1GB) > 6.2GB已使用的量</font>，怎麼好像哪裡怪怪地?!
所以事實上我們可以知道，5.4GB的Cached使用量其實並沒有被算在總共的6.2GB裡面，個人猜測...應該是因為Cached的部分是可以因應當下狀況進行回收的。

![](/Teamdoc/image/Windows/task_mgr1.png)

## File System Cached
接下來我們用工具[[RAMMAP.exe]](https://docs.microsoft.com/en-us/sysinternals/downloads/rammap)來觀察看看Cached內部的檔案快取。  
下圖紅框的Mapped File及MetaFile兩個就是有關於檔案快取的資料，Mapped File是檔案內容的快取、MetaFile則是NTFS相關的檔案資訊。

![](/Teamdoc/image/Windows/rammap1.png)

* 我們再切到File Summary裡面，可以看到已經快取在Cached裡的檔案。
  
![](/Teamdoc/image/Windows/rammap2.png)

* 按下F5則是可以重新整理當下的清單，取得最新的內容。
* 使用左上角的Empty &rarr; Empty StandBy List功能，則可以清除待命中(**指沒有任何其他Process在使用**)的檔案。

![](/Teamdoc/image/Windows/rammap3.png)

## Win32 API
有些時候在處理大量的檔案IO時，可能因為數量太大、造成短時間內塞滿記憶體的狀況，因此必須要自行決定何時清除，這時候我們就可以透過Win32 API強制要求清除Cached Memory。

  - Sample: 以下範例來自於參考文件1，有興趣的讀者可以進入參考。
  - 此範例是以.NET Core 2.2為Target Framework建立。

```csharp
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security.Principal;
using SMT.Data.ClearCachedMemory.Interfaces;

namespace SMT.Data.ClearCachedMemory.Services
{
    //Declaration of structures
    //SYSTEM_CACHE_INFORMATION
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    struct SYSTEM_CACHE_INFORMATION
    {
        public uint CurrentSize;
        public uint PeakSize;
        public uint PageFaultCount;
        public uint MinimumWorkingSet;
        public uint MaximumWorkingSet;
        public uint Unused1;
        public uint Unused2;
        public uint Unused3;
        public uint Unused4;
    }

    //SYSTEM_CACHE_INFORMATION_64_BIT
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    struct SYSTEM_CACHE_INFORMATION_64_BIT
    {
        public long CurrentSize;
        public long PeakSize;
        public long PageFaultCount;
        public long MinimumWorkingSet;
        public long MaximumWorkingSet;
        public long Unused1;
        public long Unused2;
        public long Unused3;
        public long Unused4;
    }

    //TokPriv1Luid
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    internal struct TokPriv1Luid
    {
        public int Count;
        public long Luid;
        public int Attr;
    }

    public class ClearFileCachedService : IClearFileCachedService
    {
        //Declaration of constants
        const int SE_PRIVILEGE_ENABLED = 2;
        const string SE_INCREASE_QUOTA_NAME = "SeIncreaseQuotaPrivilege";
        const string SE_PROFILE_SINGLE_PROCESS_NAME = "SeProfileSingleProcessPrivilege";
        const int SystemFileCacheInformation = 0x0015;
        const int SystemMemoryListInformation = 0x0050;
        const int MemoryPurgeStandbyList = 4;
        //const int MemoryEmptyWorkingSets = 2;

        //Import of DLL's (API) and the necessary functions 
        [DllImport("advapi32.dll", SetLastError = true)]
        internal static extern bool LookupPrivilegeValue(string host, string name, ref long pluid);

        [DllImport("advapi32.dll", SetLastError = true)]
        internal static extern bool AdjustTokenPrivileges(IntPtr htok, bool disall, ref TokPriv1Luid newst, int len, IntPtr prev, IntPtr relen);

        [DllImport("ntdll.dll")]
        internal static extern UInt32 NtSetSystemInformation(int InfoClass, IntPtr Info, int Length);

        [DllImport("psapi.dll")]
        internal static extern int EmptyWorkingSet(IntPtr hwProc);

        //Function to clear working set of all processes
        internal static void EmptyWorkingSetFunction()
        {
            //Declaration of variables
            string ProcessName = string.Empty;
            Process[] allProcesses = Process.GetProcesses();
            List<string> successProcesses = new List<string>();
            List<string> failProcesses = new List<string>();

            //Cycle through all processes
            for (int i = 0; i < allProcesses.Length; i++)
            {
                Process p = new Process();
                p = allProcesses[i];
                //Try to empty the working set of the process, if succesfull add to successProcesses, if failed add to failProcesses with error message
                try
                {
                    ProcessName = p.ProcessName;
                    EmptyWorkingSet(p.Handle);
                    successProcesses.Add(ProcessName);
                }
                catch (Exception ex)
                {
                    failProcesses.Add(ProcessName + ": " + ex.Message);
                }
            }

            //Print the lists with successful and failed processes
            Console.WriteLine("SUCCESSFULLY CLEARED PROCESSES: " + successProcesses.Count);
            Console.WriteLine("-------------------------------");
            for (int i = 0; i < successProcesses.Count; i++)
            {
                Console.WriteLine(successProcesses[i]);
            }
            Console.WriteLine();

            Console.WriteLine("FAILED CLEARED PROCESSES: " + failProcesses.Count);
            Console.WriteLine("-------------------------------");
            for (int i = 0; i < failProcesses.Count; i++)
            {
                Console.WriteLine(failProcesses[i]);
            }
            Console.WriteLine();
        }

        //Function to check if OS is 64-bit or not, returns boolean
        internal static bool Is64BitMode()
        {
            return Marshal.SizeOf(typeof(IntPtr)) == 8;
        }

        //Function used to clear file system cache, returns boolean
        internal void ClearFileSystemCache(bool ClearStandbyCache)
        {
            //Check if privilege can be increased
            if (SetIncreasePrivilege(SE_INCREASE_QUOTA_NAME))
            {
                uint num1;
                int SystemInfoLength;
                GCHandle gcHandle;
                //First check which version is running, then fill structure with cache information. Throw error is cache information cannot be read.
                if (!Is64BitMode())
                {
                    SYSTEM_CACHE_INFORMATION cacheInformation = new SYSTEM_CACHE_INFORMATION();
                    cacheInformation.MinimumWorkingSet = uint.MaxValue;
                    cacheInformation.MaximumWorkingSet = uint.MaxValue;
                    SystemInfoLength = Marshal.SizeOf(cacheInformation);
                    gcHandle = GCHandle.Alloc(cacheInformation, GCHandleType.Pinned);
                    num1 = NtSetSystemInformation(SystemFileCacheInformation, gcHandle.AddrOfPinnedObject(), SystemInfoLength);
                    gcHandle.Free();
                }
                else
                {
                    SYSTEM_CACHE_INFORMATION_64_BIT information64Bit = new SYSTEM_CACHE_INFORMATION_64_BIT();
                    information64Bit.MinimumWorkingSet = -1L;
                    information64Bit.MaximumWorkingSet = -1L;
                    SystemInfoLength = Marshal.SizeOf(information64Bit);
                    gcHandle = GCHandle.Alloc(information64Bit, GCHandleType.Pinned);
                    num1 = NtSetSystemInformation(SystemFileCacheInformation, gcHandle.AddrOfPinnedObject(), SystemInfoLength);
                    gcHandle.Free();
                }
                if (num1 != 0)
                    throw new Exception("NtSetSystemInformation(SYSTEMCACHEINFORMATION) error: ", new Win32Exception(Marshal.GetLastWin32Error()));
            }

            //If passes paramater is 'true' and the privilege can be increased, then clear standby lists through MemoryPurgeStandbyList
            if (ClearStandbyCache && SetIncreasePrivilege(SE_PROFILE_SINGLE_PROCESS_NAME))
            {
                int SystemInfoLength = Marshal.SizeOf(MemoryPurgeStandbyList);
                GCHandle gcHandle = GCHandle.Alloc(MemoryPurgeStandbyList, GCHandleType.Pinned);
                uint num2 = NtSetSystemInformation(SystemMemoryListInformation, gcHandle.AddrOfPinnedObject(), SystemInfoLength);
                gcHandle.Free();
                if (num2 != 0)
                    throw new Exception("NtSetSystemInformation(SYSTEMMEMORYLISTINFORMATION) error: ", new Win32Exception(Marshal.GetLastWin32Error()));
            }
        }

        //Function to increase Privilege, returns boolean
        internal static bool SetIncreasePrivilege(string privilegeName)
        {
            using (WindowsIdentity current = WindowsIdentity.GetCurrent(TokenAccessLevels.Query | TokenAccessLevels.AdjustPrivileges))
            {
                TokPriv1Luid newst;
                newst.Count = 1;
                newst.Luid = 0L;
                newst.Attr = SE_PRIVILEGE_ENABLED;

                //Retrieves the LUID used on a specified system to locally represent the specified privilege name
                if (!LookupPrivilegeValue(null, privilegeName, ref newst.Luid))
                    throw new Exception("Error in LookupPrivilegeValue: ", new Win32Exception(Marshal.GetLastWin32Error()));

                //Enables or disables privileges in a specified access token
                int num = AdjustTokenPrivileges(current.Token, false, ref newst, 0, IntPtr.Zero, IntPtr.Zero) ? 1 : 0;
                if (num == 0)
                    throw new Exception("Error in AdjustTokenPrivileges: ", new Win32Exception(Marshal.GetLastWin32Error()));
                return num != 0;
            }
        }

        public void Run()
        {
            //Clear working set of all processes
            EmptyWorkingSetFunction();

            //Clear file system cache
            ClearFileSystemCache(true);
        }
    }
}
```
以上範例主要有兩個動作，一個是清除各Process的Working Set Memory、另一個則是清除File Cached Memory。

  - EmptyWorkingSetFunction: 清除各Process的Working Set Memory，不過使用後會發現有很多Process是沒有存取權限的，而且<font color="red">強制刪除各Process使用的記憶體很可能會有副作用，建議是不要隨便使用它</font>。
  - ClearFileSystemCache: 清除File Cached Memory，由於此方法僅清除待命中的檔案快取，經過測試是沒有發現其他副作用，應可安全使用。

## 參考
1. [clear-the-windows-7-standby-memory-programmatically](https://stackoverflow.com/questions/12841845/clear-the-windows-7-standby-memory-programmatically)
2. [Windows記憶體都用到哪裡去了?](https://blog.darkthread.net/blog/where-have-all-the-memory-gone/)