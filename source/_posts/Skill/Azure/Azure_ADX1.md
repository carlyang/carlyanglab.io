---
title: Azure - Azure Data Explorer(1) - External Table & Blobs
categories:
 - 技術文件
 - Azure
date: 2021/7/17
updated: 2021/7/17
thumbnail: https://i.imgur.com/6TYOptx.jpg
layout: post
tags: [Azure, ADX, Azure Data Explorer]
---
Author: Carl Yang

![](/Teamdoc/image/azure/availability/aure_logo.png)

## Purpose
`Azure Data Explorer`(以下簡稱`ADX`)可用來查詢非常大量的資料，像是IoT等收集來的龐大資料量、可以快速查詢、統計、分析，他也支援非結構化的資料查詢。本文主要以最基本的介紹如何建立外部資料表、以存取Blob內的檔案內容，及最簡單的查詢語法，後續有時間會再陸續補充其他部分。
<!-- more -->

## Prerequisites
請準備具有下列條件的環境。
- Azure Subscription
- Azure Data Explorer

## Introduction
引用微軟文件的說明如下:

>- Scales quickly to terabytes of data, in minutes, allowing rapid iterations of data exploration to discover relevant insights.

>- Offers an innovative query language, optimized for high-performance data analytics.

>- Supports analysis of high volumes of heterogeneous data (structured and unstructured).

>- Provides the ability to build and deploy exactly what you need by combining with other services to supply an encompassing, powerful, and interactive data analytics solution.

簡單來說，`ADX`可以快速地在短時間內處理數TB的資料、提供類SQL的查詢語法並優化分析效能、支援大量的異質資料及與其他Azure服務的整合，例如本文所示範的查詢`Azure blob`檔案內的`JSON`資料。

## 建立外部資料表
`ADX`內有分一般的`Table`及`External Table`，兩者最大的不同點在於`Table`需要將資料`內嵌`進`ADX`中再進行查詢與分析，但`External Table`則是可以先定義好外部的檔案如`blob`、在跑的時候才去將資料拉進來查詢與分析，這樣的好處就是不需要花時間先將資料撈進來才進行處理。  

相關的`External Table`語法:

這是Show出目前所有`External Tables`、以及Show出特定的`External Table`資訊。
```sql
.show external tables
.show external table T
```

這是以`JSON`格式Show出`External Table`的`Schema`。
```sql
.show external table T schema as JSON
```

這是刪除特定的`External Table`
```sql
.drop external table ExternalBlob
```

這是建立`External Table`的語法。
```sql
.create external table ExternalTable (Timestamp:datetime, x:long, s:string) 
kind=blob 
partition by (Month:datetime = startofmonth(Timestamp)) 
pathformat = (datetime_pattern("'year='yyyy'/month='MM", Month)) 
dataformat=json 
( 
   h@'https://storageaccount.blob.core.windows.net/container1;secretKey' 
)
```

Note:  
1. `.create`: 請注意這語法也可以改用`.create-or-alter`，表示當`External Table`存在時會直接修改表定義。
2. `kind`: 指定外部資料的類型，例如:`blob`、`adl(Azure Data Lake)`。
3. `partition by`: 指定要用來分割`Partition`的來未及運算方式，例如: `startofmonth(Timestamp)`就是透過`Timestamp`欄位取得月份後再分割。`請注意，這邊的欄位必須要在上方()內先宣告好才能用`。
4. `pathformat`: 則是指定`blob`內的子資料夾、並且是用`Month`來區分。
5. `dataformat`: 則是用來宣告資料內容為`JSON`格式，並在其中指定`Azure Blob位址`，請注意這邊的位址必須包含`[URL] + [Secret Key]`(詳細可查閱`Azure Blob`的相關文件以取得這兩個資訊)。

## 一般查詢語法
一般的`External Table`查詢語法如下:

```sql
external_table("ExternalTable")
 | where Timestamp between (datetime(2020-01-01) .. datetime(2020-02-01))
 | where CustomerName in ("John.Doe", "Ivan.Ivanov")
```

Note:  
1. `external_table("ExternalTable")`: 就是我們要查詢資料的目標。
2. `where`: 當然就是查詢條件，我們不太可能不下條件直接查全部的資料。
3. `between (datetime(2020-01-01) .. datetime(2020-02-01))`: 這邊是查詢指定期間，雖然類似SQL語法但`and`改用`..`，`請注意，ADX對於資料類型不會像SQL那樣輸入字串自動轉換為DateTime格式再查詢，所以務必要使用datetime()先轉換過`。
4. `in`: 這邊用法同`SQL`。
5. `Pipe Symbol(|)`: 請注意`ADX`的條件必須每一行用`|`符號隔開、執行順序由上而下處理，每個`|`處理的資料來源都是上一行的`|`資料結果，所以它是使用`像水管串接的方式處理資料`。

## 建立`External Table`的對應
建立對應的目的，是為了事先定義好外部資料表有哪些欄位，這樣後續查詢才能夠知道有那些欄位可用，建立語法如下:

```sql
.create external table MyExternalTable mapping "Mapping1" '[{"Column": "rownumber", "Properties": {"Path": "$.rownumber"}}, {"Column": "rowguid", "Properties": {"Path": "$.rowguid"}}]'
```

## 非結構化`JSON`物件
我們都知道`JSON`是非結構化的資料格式，它的Property並不只能有`Primitive Type`如string、integer等類型，也可以是另一個`JSON`物件，所以`ADX`會顯示類型為`dynamic`、存取的時候可以用`Object["ColumnName"]`的方式存取。

例如以下的條件使用方式:

```sql
| where isempty(Router['WorkOrder']) != true and isempty(Router['Program']) != true
```

## 結論:
本文僅簡單介紹一下建立`External Table`的語法以存取`Azure Blob`資料、以及一般的查詢語法，後續會再介紹更進階的查詢方式。  
另外，本文並未提到`ADX`的效能，但事實上在使用的時候它的效能是非常快的，粗略估計約五千萬筆的資料能在230秒內查詢完畢(不過每筆資料的大小也會影響效能表現)，給大家參考看看。

## 參考
1. [[What is Azure Data Explorer?]](https://docs.microsoft.com/en-us/azure/data-explorer/data-explorer-overview)